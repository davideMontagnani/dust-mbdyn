
Coupling \dust{} with \precice{} has required the implementation of some modifications to the source code generating all \dust{} executables. Sections for coupling initialization, for data communication to and from the external software and for updating the aerodynamics surfaces and fields have been added to the solver. The preprocessor has been updated in order to read user input for coupling, and to build all the structure for data exhange and surface motion. The postprocessor needed only slight modifications. 

\subsection{Software update in \texttt{dust} solver}\label{sec:dust:time}
In this section, a pseudocode of the \dust{} solver shows the modifications needed for coupling \dust{} with external software through \precice{} library.
First, the object \texttt{precice} of class \texttt{t\_precice} is declared for handling a coupled simulation through \precice{}. This object is used both for managing data communication, and for updating coupled components of the aerodynamic model. Then, \dust{} participant to the coupled simulation is created, reading XML \precice{} configuration file.
\begin{lstlisting}[language=Fortran]

!> use and variable declaration
! ...

#if USE_PRECICE  ! --------------------------------
type(t_precice) :: precice
#endif  ! -----------------------------------------

#if USE_PRECICE  ! --------------------------------
!> preCICE set-up
call precice % initialize()
#endif  ! -----------------------------------------

\end{lstlisting}
%
After some preliminary operations, the mesh used for coupling the codes is defined and the fields involved in the communication are initialized. Initialization of the octree, the wake and the linear system follows. \textbf{No restart option is available for coupled simulations, since \mbdyn{} does not provide it.}
\begin{lstlisting}[language=Fortran]

!> Initialization of all the DUST input variables
call prms % Create...Option()

!> Initialization of simulation parameters
...

!> Geometry creation
call create_geometry()

#if USE_PRECICE  ! --------------------------------
!> Initialize preCICE mesh and fields
call precice % initialize_mesh()
call precice % initialize_fields()
#endif  ! -----------------------------------------

!> Initialize octree
call initialize_octree()

!> Initialize wake
call initialize_wake()

!> Initialize linear system
call initialize_linsys()

\end{lstlisting}
%
Before time loop starts, first communication is established between the coupled codes.
Time loop starts with the update of \dust{} explicit aerodynamic elements (lifting lines and actuator disks) and other minor tasks. A checkpoint of the exchanged fields is stored for being reloaded during sub-iterations of \precice{} implicit coupling. Then, \dust{} receives the kinematic variables of the structural nodes from the external software, and updates the surface of the coupled components and the near-field wake elements.
%
\begin{lstlisting}[language=Fortran]

#if USE_PRECICE  ! --------------------------------
!> Initialize preCICE communication
call precicef_initialize()
#endif  ! -----------------------------------------

!> === Time loop ===
do while ()

  !> Update explicit elements, and other minor computations
  call update_liftlin()
  call update_actdisk()

  #if USE_PRECICE  ! ------------------------------
  !> Save checkpoints and Read data, if needed
  call precicef_action_required( write_it_checkp )
  precice % fields % cdata = precice % fields % fdata
  call precicef_read_data( precice%fields%fdata )

  !> Update geometry and near-field wake
  call precice % update_elems()
  call precice % update_near_field_wake()
  #endif  ! ---------------------------------------

\end{lstlisting}
%
Then, the linear system is updated and solved for the intensity of the surface panel and vortex lattice elements. The solution of the non-linear lifting line problem follows. Once the intensity of the surface singularities has been evaluated, surface pressure distribution and elementary forces and moments are retrieved.
%
\begin{lstlisting}[language=Fortran]

  !> Assemble and solve linear system (implicit elems: p, v)
  call assemble_linsys()
  call solve_linsys()

  !> Solve explicit elems: l
  call solve_liftlin()

  !> Compute pressure, dforce and dmom
  call elems() % p % compute_pres()
  call elems() % p % compute_dforce()

\end{lstlisting}
Aerodynamic forces and moments are reduced to the nodes of the interface between the aerodynamic and structural meshes and sent to \mbdyn{}. Convergence check follows. If convergence is not reached, the checkpoint fields are reloaded and new sub-iteration begins. If convergence is attained, the timestep is finalized saving the status and updating the wake and the geometry of the uncoupled components for the next timestep.
\begin{lstlisting}[language=Fortran]

  #if USE_PRECICE  ! ------------------------------
  !> Update fields for data communication and write to
  ! external software
  call precice % update_force()
  call precicef_write_data( precice%fields%fdata )

  !> Check convergence
  if ( precicef_action_required( read_it_checkp ) )
    !> Not converged: reload checkpoint and iterate
  else
    !> Converged: finalize timestep
  #endif  ! ---------------------------------------

  !> Save results
  call save_status

  !> Update wake and geometry for the next dt
  call update_wake()
  call update_geometry()
  call complete_wake()

  #if USE_PRECICE  ! ------------------------------
  end if
  #endif  ! ---------------------------------------

end do

\end{lstlisting}

\subsection{Coupling with structural elements}\label{sec:dust:coupling}
\subsubsection{\texttt{ll} coupling}
{\color{red} TODO}

\subsubsection{\texttt{rigid} coupling}
In the \texttt{rigid} coupling, the rigid motion of a \dust{} component is defined by the motion of a \mbdyn{} node, $Q$, whose kinematic variables are available from \mbdyn{} in the global reference frame. The resultant-force and moment of the aerodynamic actions are referred to this node.
\texttt{rigid} coupling relies on the reference configuration of the components for data communication. The reference configuration of the coupling node, $(Q - O)^r$, is defined by the user in {\color{red} \texttt{refConfigNodes.in} (UPDATE?)},
while the reference configuration, $(P - O)^r$, of a coupled \dust{} component is defined in \texttt{dust\_pre.in} input file of the preprocessor.
%
\paragraph{Kinematic variables.} The \textbf{rotation} of all the points of the \dust{} component is inherited from the rotation of the coupling nodes, $R^{r\rightarrow g}$, provided by \mbdyn{} in the global reference frame.
An extra relative rotation of the reference configuration with respect to the global reference frame can be defined as a user input {\color{red} TODO: order of rotation?.
\begin{equation}
  R_1 = R_2 \, R_3 \ , 
\end{equation}
where \dots}
%
The \textbf{angular velocity} of the \dust{} component is inherited from the coupling node angular velocity, $\bm{\omega}_Q$,
\begin{equation}
 \bm{\omega}_P = \bm{\omega}_Q \ ,
\end{equation}
 provided by \mbdyn{} in the global reference frame.
%
For a rigid-body motion, the actual \textbf{position} of point $P$ of a \dust{} component reads
\begin{equation}\label{eqn:rigid:pos}
  (P-O)^{g} = (Q-O)^{g} + R^{r\rightarrow g} (P-Q)^r \ ,
\end{equation}
where $(P-Q)^r$ comes from the user definition of the reference configuration, while the position and the orientation of the node $Q$ are provided by \mbdyn{}.
%
For a rigid-body motion, the \textbf{velocity} of point $P$ of a \dust{} component reads
\begin{equation}
  \bm{v}_P = \bm{v}_Q + \bm{\omega}_Q \times (P-Q) \ ,
\end{equation}
where the velocity and the angular velocity of the node $Q$ are provided by \mbdyn{} in the global reference frame, while the vector position $(P-Q)^g$ in the global reference frame can be computed with Eq.(\ref{eqn:rigid:pos}).
%
\paragraph{Force and moment.}
The \textbf{resultant-force} $\bm{f}_Q$ acting on the node $Q$ is evaluated as the sum of the elementary forces $\bm{f}_e$ acting on the elements of the aerodynamic mesh,
\begin{equation}
    \bm{f}_Q = \sum_e \bm{f}_e \ .
\end{equation}
%
The \textbf{resultant-moment} $\bm{m}_Q$ acting on the node $Q$ reads
\begin{equation}
    \bm{m}_Q = \sum_e \left[ \bm{m}_e + (P_e-Q) \times \bm{f}_e \right] \ ,
\end{equation}
where $P_e$ is the centre of the $e$-th aerodynamic surface element, using the piecewise-constant approximation of aerodynamic force fields, implemented in \dust{}.
%
\paragraph{Consistency of kinematic and dynamic fields.} It is easy to prove that the power of forces and moments acting on the structure, $W_Q$, is equal to those acting on the aerodynamic grid, $W_a$,
\begin{equation}
\begin{aligned}
  W_Q & = \bm{f}_Q \cdot \bm{v}_Q + \bm{m}_Q \cdot \bm{\omega}_Q = \\
      & = \sum_e \bm{f}_e \cdot \bm{v}_Q + \sum_e \left[ \bm{m}_e + (P_e - Q) \times \bm{f}_e \right] \cdot \bm{\omega}_Q = \\
      & = \sum_e \left\{ \bm{f}_e \cdot \left[ \bm{v}_Q + \bm{\omega}_Q \times (P_e - Q) \right] + \bm{m}_e \cdot \bm{\omega}_Q \right\} = \\
      & = \sum_e \left\{ \bm{f}_e \cdot \bm{v}_e + \bm{m}_e \cdot \bm{\omega}_e \right\} = W_a \\
\end{aligned}
\end{equation}

\subsubsection{\texttt{rbf} coupling}
The expressions of the dynamic variables of \texttt{rbf} coupling are weighted-averages of the expressions used in \texttt{rigid} coupling. The kinematic variables, $\phi_p$, of a point $p$ of the aerodynamic surface of a \dust{} component is evaluated as the weighted-average,
\begin{equation}
  \phi_p = \sum_q w_{pq} \, \phi_q \ ,
\end{equation}
where $\phi_q$ is the same kinematic variable associated with the $q$-th structural node of the \mbdyn{} model
%
Weights $w_{pq}$ could be any set of non-negative real numbers, satisfying the normalization conditions
\begin{equation}
  \sum_{q} w_{pq} = 1  \quad , \quad \forall p \ ,
\end{equation}
since they define the weighted average of the variables associated to the structural nodes $q$ on the aerodynamic nodes $p$.
These coefficients could be proportional to some negative power, defined as a user input, of a norm of the vectors $(P_p - Q_q)$. As an example, using the local coordinates in the reference configuration $\Delta \bm{r}_{pq}$, the norm of these vectors can be defined as,
\begin{equation}
 \|(P_p - Q_q)\|^2 := \bm{r}_{pq}^T \, W \, \Delta\bm{r}_{pq} \ ,
\end{equation}
where $W$ is a positive (semi-)definite matrix, providing an ``anisotropy'' degree of freedom to the user in defining the (semi-)norm.
Threshold values and maximum number of influencing weights are two criteria, defined as user inputs as well, in order to restrict the average only to the ``significant'' structural nodes for each aerodynamic point.
{\color{red} User input definition and example}

\paragraph{Kinematic variables.}
The \textbf{position} of a point of the aerodynamic surface is evalutaed as,
\begin{equation}
    (P - O)^g = \sum_{Q \in I_P} w_{pq} \left\{ (Q - O)^g + R_Q^{r\rightarrow g} (P - Q)^r \right\} \ .
\end{equation}
Its \textbf{angular velocity} and \textbf{velocity} respectively read,
\begin{equation}
\begin{aligned}
  \bm{\omega}_P & = \sum_{Q \in I_P} w_{pq} \, \bm{\omega}_{Q} \ , \\
  \bm{v}_P & = \sum_{Q \in I_P} w_{pq} \left\{ \bm{v}_Q + \bm{\omega}_{Q} \times (P - Q) \right\} \ .
\end{aligned}
\end{equation}

\paragraph{Forces and moments.} The aerodynamic forces and moments referred to the structural nodes respectively read
\begin{equation}
\begin{aligned}
  \bm{f}_Q & = \sum_{e \in J_Q} w_{eq} \, \bm{f}_e \ , \\
  \bm{m}_Q & = \sum_{e \in J_Q} w_{eq} \left\{ \bm{m}_e + (P_e - Q) \times \bm{f}_e \right\} \ .
\end{aligned}
\end{equation}
%
It's easy to prove that these expressions of the loads are consistent with the expressions of the kinematic variables. The power of the loads acting on the structure, $W_s$, equals the power of the loads acting on the aerodynamic surface, $W_a$,
\begin{equation}
\begin{aligned}
  W_s & = \sum_{Q} \left\{ \bm{f}_Q \cdot \bm{v}_Q + \bm{m}_Q \cdot \bm{\omega}_Q \right\} = \\
      & = \sum_{Q} w_{eq} \left\{ \sum_{e \in J_Q} \bm{f}_e \cdot \bm{v}_Q + \sum_{e \in J_Q} \left[ \bm{m}_e + (P_e - Q) \times \bm{f}_e \right] \cdot \bm{\omega}_Q \right\} = \\
      & = \sum_{Q} \sum_{e \in J_Q} w_{eq} \left\{ \bm{f}_e \cdot \bm{v}_Q + \left[ \bm{m}_e + (P_e - Q) \times \bm{f}_e \right] \cdot \bm{\omega}_Q \right\} = \\
      & = \sum_{e} \sum_{Q \in I_e} w_{eq} \left\{ \bm{f}_e \cdot \bm{v}_Q + \left[ \bm{m}_e + (P_e - Q) \times \bm{f}_e \right] \cdot \bm{\omega}_Q \right\} = \\
      & = \sum_{e} \left\{ \bm{f}_e \, \cdot \, \sum_{Q \in I_e} w_{eq} \left[ \bm{v}_Q + \bm{\omega}_Q \times (P_e - Q) \right] + \bm{m}_e \cdot \sum_{Q \in I_e} w_{eq} \, \bm{\omega}_Q \right\} = \\
      & = \sum_e \left\{ \bm{f}_e \cdot \bm{v}_e + \bm{m}_e \cdot \bm{\omega}_e \right\} = W_a \\
\end{aligned}
\end{equation}

