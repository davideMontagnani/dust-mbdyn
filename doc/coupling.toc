\contentsline {chapter}{\numberline {1}Introduction}{2}{chapter.1}%
\contentsline {section}{\numberline {1.1}\texttt {dust}}{2}{section.1.1}%
\contentsline {section}{\numberline {1.2}MBDyn}{2}{section.1.2}%
\contentsline {section}{\numberline {1.3}preCICE}{3}{section.1.3}%
\contentsline {chapter}{\numberline {2}\texttt {dust}{}-MBDyn{} coupling}{4}{chapter.2}%
\contentsline {section}{\numberline {2.1}Coupling implementation}{4}{section.2.1}%
\contentsline {section}{\numberline {2.2}Coupling methods}{4}{section.2.2}%
\contentsline {chapter}{\numberline {3}MBDyn{} adapter for preCICE{}}{6}{chapter.3}%
\contentsline {section}{\numberline {3.1}Case-dependent Python{} script}{6}{section.3.1}%
\contentsline {section}{\numberline {3.2}\texttt {mbdynInterface.py}}{7}{section.3.2}%
\contentsline {section}{\numberline {3.3}\texttt {mbdynAdapter.py}}{8}{section.3.3}%
\contentsline {chapter}{\numberline {4}\texttt {dust}{} updates}{10}{chapter.4}%
\contentsline {section}{\numberline {4.1}Coupling through preCICE{}}{10}{section.4.1}%
\contentsline {subsection}{\numberline {4.1.1}Software update in \texttt {dust} solver}{10}{subsection.4.1.1}%
\contentsline {subsection}{\numberline {4.1.2}Coupling with structural elements}{13}{subsection.4.1.2}%
\contentsline {subsubsection}{\texttt {ll} coupling}{13}{section*.2}%
\contentsline {subsubsection}{\texttt {rigid} coupling}{14}{section*.3}%
\contentsline {paragraph}{Kinematic variables.}{14}{section*.4}%
\contentsline {paragraph}{Force and moment.}{15}{section*.5}%
\contentsline {paragraph}{Consistency of kinematic and dynamic fields.}{15}{section*.6}%
\contentsline {subsubsection}{\texttt {rbf} coupling}{15}{section*.7}%
\contentsline {paragraph}{Kinematic variables.}{16}{section*.8}%
\contentsline {paragraph}{Forces and moments.}{16}{section*.9}%
\contentsline {section}{\numberline {4.2}Hinged control surface}{17}{section.4.2}%
\contentsline {subsection}{\numberline {4.2.1}Two-dimensional example}{17}{subsection.4.2.1}%
\contentsline {subsection}{\numberline {4.2.2}Three-dimensional problems}{18}{subsection.4.2.2}%
\contentsline {subsubsection}{Hinge input}{19}{section*.10}%
\contentsline {subsubsection}{Hinge connectivity}{19}{section*.11}%
\contentsline {subsubsection}{Hinge motion}{20}{section*.12}%
\contentsline {paragraph}{Hinge nodes: position and orientation.}{20}{section*.13}%
\contentsline {paragraph}{Hinge nodes: rotation.}{20}{section*.14}%
\contentsline {paragraph}{Surface position due to hinge deflection.}{20}{section*.15}%
\contentsline {paragraph}{Surface velocity due to hinge deflection.}{21}{section*.16}%
