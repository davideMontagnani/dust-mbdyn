\babel@toc {italian}{}
\babel@toc {italian}{}
\contentsline {section}{\numberline {1}General interface between structural mesh and aerodynamic mesh}{1}{section.1}%
\contentsline {subsection}{\numberline {1.1}Kinematic variables: from MBDyn{} to \texttt {dust}{}}{1}{subsection.1.1}%
\contentsline {paragraph}{Position.}{1}{section*.2}%
\contentsline {paragraph}{Velocity.}{2}{section*.3}%
\contentsline {paragraph}{Angular velocity.}{2}{section*.4}%
\contentsline {subsection}{\numberline {1.2}Loads: from \texttt {dust}{} to MBDyn{}}{2}{subsection.1.2}%
\contentsline {paragraph}{Forces.}{2}{section*.5}%
\contentsline {paragraph}{Moments.}{2}{section*.6}%
\contentsline {paragraph}{Consistency of kinematic and loads translation.}{3}{section*.7}%
\contentsline {subsection}{\numberline {1.3}Weight evaluation}{3}{subsection.1.3}%
\contentsline {section}{\numberline {2}Aileron deflection}{3}{section.2}%
\contentsline {subsection}{\numberline {2.1}Two-dimensional example}{4}{subsection.2.1}%
\contentsline {subsection}{\numberline {2.2}Three-dimensional problems}{4}{subsection.2.2}%
\contentsline {subsubsection}{\numberline {2.2.1}Hinge input}{5}{subsubsection.2.2.1}%
\contentsline {subsubsection}{\numberline {2.2.2}Hinge connectivity}{5}{subsubsection.2.2.2}%
\contentsline {subsubsection}{\numberline {2.2.3}Hinge motion}{5}{subsubsection.2.2.3}%
\contentsline {paragraph}{Hinge nodes: position and orientation.}{6}{section*.8}%
\contentsline {paragraph}{Hinge nodes: rotation.}{6}{section*.9}%
\contentsline {paragraph}{Surface position due to hinge deflection.}{6}{section*.10}%
\contentsline {paragraph}{Surface velocity due to hinge deflection.}{7}{section*.11}%
