The model for the deflection of an aileron is introduced with a two-dimenisonal example first, and then extended to three dimensional deformable components. 

\subsection{Two-dimensional example}
In a two-dimensional problem, an aileron can be defined in the local reference frame of the component, by means of:
\begin{itemize}
  \item its hinge position, $\bm{r}_H$;
  \item its ``chordwise'' direction, $\bh{v}$;
  \item the dimension a blending region, aiming at avoiding irregular behavior of the surface mesh, $u$,
\end{itemize}
along with the rotation angle $\theta$. In a two-dimensional problem the rotation axis, $\bh{h}$, is assumed ortoghonal to the plane of the problem.

A hinge orthonormal reference frame is defined with the origin $\bm{r}_H$, and axes $\bh{v}$, $\bh{n} = \bh{z} \times \bh{v}$. The position of a point w.r.t. this reference frame reads
\begin{equation}
 \bm{r} = v \, \bh{v} + n \, \bh{n} + h \, \bh{h} \ .
\end{equation}
Three regions are defined using this reference frame:
\begin{enumerate}
 \item $v \leq -u$: no influence of the aileron rotation
 \item $v \geq  u$: rigid rotation around the hinge
  \begin{equation}
    \Delta \bm{r} = \sin \, \theta   \bh{h} \times \bm{r} + 
              ( 1 - \cos \, \theta ) \bh{h} \times \bh{h} \times \bm{r} \ . 
  \end{equation}
 \item $-u \le v \le  u$: blending region for avoiding irregular behavior
   \begin{equation}
     \Delta \bm{r} = \Delta \bm{r}^{blend}( \bm{r}-\bm{r}_H, \theta ) \ .
   \end{equation}
\end{enumerate}

\subsection{Three-dimensional problems}
In a three-dimensional problem, the reference configuration of an aileron is defined in the local reference frame of the component, by means of
\begin{itemize}
  \item the position of hinge nodes, $\bm{r}_{H,h}$;
  \item the ``chordwise'' direction, $\bm{v}^*$;
  \item the dimension of the blending region, in the chordwise direction.
\end{itemize}
Each node of the hinge may have its nodal reference frame providing its own orientation, e.g. if a structural coupling is defined between the aerodynamic components and a structural element.
The rotation of each hinge node either follows by the structural node or it is driven by an assigned law of motion.

Some steps are required from the definition of an hinge to the deformation of a components simulating the deflection of the moving surface.
\subsubsection{Hinge input}
A hinge is defined in the local reference frame of its own component, through
\begin{itemize}
  \item number of points, $n_{hp}$
  \item first and last node, $\bm{r}_{H,1}$, $\bm{r}_{H,np}$
  \item ``chordwise`` direction $\bh{v}^*$
  \item chordwise dimension of the blending region
\end{itemize}
From these geometrical entities, other geometrical entities follow
\begin{itemize}
  \item hinge axis, $\bm{h} = (\bm{r}_{H,np}-\bm{r}_{H,1})$, $\bh{h} = \bm{h}/|\bm{h}|$;
  \item the reference ortonormal vectors for the hinge as a whole,
  \begin{equation}
  \begin{aligned}
     & \bh{h} \\
     \bm{n} = \bm{v}^* \times \bh{h} \qquad , \qquad & \bh{n} = \bm{n}/|\bm{n}| \\
     & \bh{v} = \bh{h} \times \bh{n} \ .
  \end{aligned}
  \end{equation}
\end{itemize}


\subsubsection{Hinge connectivity}
Three regions are defined, for all the points with $h \in [ \, 0,(\bm{r}_{H,np}-\bm{r}_{H,1}) \cdot \bh{h} \ ]$
\begin{enumerate}
  \item $v \leq -u$: nothing to do
  \item $v \geq  u$: rigid rotation
  \item $v \in [\, -u,u \, ]$: blending region
\end{enumerate}
Each point of the surface in regions 2. and 3. is linked to the hinge nodes, obtaining its kinematic variables as a weighted average of the motion induced by the rotation of the hinge nodes.
Weights $w_{ph}$ are evaluated using only the $h$ component of the vectors connecting the surface point to the hinge nodes.


\subsubsection{Hinge motion}
Hinges are introduced in the code to allow for rotation of movable surfaces, such as ailerons, represented with a continuous deformation of the component surface. The evaluation of this deformation, both for original \dust{} components and flexible coupled components, needs some steps outlined below
\begin{itemize}
  \item evaluation of the position and orientation of the hinge nodes
  \item evaluation of the rotation of the hinge nodes, around local hinge rotation axis
  \item displacement of the surface points, due to hinge rotation
  \item velocity of the surface points, due to hinge rotation
\end{itemize}

\paragraph{Hinge nodes: position and orientation.}
The position and the orientation of the hinge nodes are evaluated through rigid motion expression for original \dust{} rigid components, while they are read from the external structural/dynamical software for coupled components.

\paragraph{Hinge nodes: rotation.}
The rotation around the hinge rotation axis must be provided as an input for original \dust{} rigid components, while it is evaluated from external structural/dynamical software for coupled components. {\color{red} TODO}

\paragraph{Surface position due to hinge deflection.}
The position of a point of a component, either a mesh node or the center of an element, can be decomposed as the sum of the position $\bm{r}_{p,0}$ without the effects of the hinge rotation, and the displacement due to this rotation $\Delta \bm{r}_{p,H}$,
\begin{equation}\label{eqn:hinge_pos}
  \bm{r}_{p}(t) = \bm{r}_{p,0}(t) + \Delta \bm{r}_{p,H}(t) \ .
\end{equation}
The dispacement of a surface point produced by the rotation of the moving surface is formally evaluated as
\begin{equation}
 \Delta \bm{r}_p = 
 \begin{cases}
   \displaystyle\sum_{h \in I_{p}} w^{rot}_{ph} \Delta \bm{r}^{rot}_{ph}( \bm{r}_p - \bm{r}_{H,h}, \theta_h ) \qquad & , \qquad \text{$\bm{r}_p$ in region 2.} \\
   \displaystyle\sum_{h \in I_{p}} w^{ble}_{ph} \Delta \bm{r}^{ble}_{ph}( \bm{r}_p - \bm{r}_{H,h}, \theta_h ) \qquad & , \qquad \text{$\bm{r}_p$ in region 3.}
 \end{cases}
\end{equation}
where the rotation (and the transformation in the blending region) needs to be performed around the local $\bh{h}$ axis, to follow the lobal motion of the component.

\paragraph{Surface velocity due to hinge deflection.} The velocity of the center of the elements needs to be evaluated for assigning the boundary condition in the surface panel equations of the linear system. The velocity of a point of the surface can be written as the sum of the velocity of the point without hinge rotations and the increment due to the hinge motion, i.e. the time derivative of equation (\ref{eqn:hinge_pos}),
\begin{equation}
  \bm{v}_{p}(t) = \f{d \bm{r}_{p}}{d t} = 
    \f{d \bm{r}_{p,0}}{d t} + \f{d \Delta \bm{r}_{p,H}}{d t} \ .
\end{equation}
So far, the velocity due to hinge motion is evaluated using a finite difference approximation, to avoid the difficulties arising in finding the analytical expression of the time redirvative of the displacement in the blending region, region 3 above.
