program sol2

use mod_general, only: &
    wp

use mod_precice, only: &
    t_precice, precice_mcl

implicit none

type(t_precice) :: precice
integer :: i, j, bool
integer :: nnodes
real(wp), allocatable :: rr(:,:)

integer, parameter :: n_max_fields = 6
character(len=precice_mcl) :: field_list(n_max_fields)
character(len=precice_mcl) :: type_list(n_max_fields)
character(len=precice_mcl) :: io_list(n_max_fields)

integer :: nf, hasdata

!> Initialize participant, mesh and fields ----------------------
call precice % initialize()
call precice % initialize_mesh()
call precice % initialize_fields()

!> Initialize coupling ------------------------------------------
call precicef_initialize( precice % dt_precice )
call precicef_ongoing   ( precice % is_ongoing )

! Time loop
do while ( precice%is_ongoing .ne. 0 )

  ! -------
  call precicef_action_required( precice%write_it_checkp , bool )
  if ( bool .eq. 1 ) then ! Save old state
    !> Save old state: forces and moments
    ! ... *** to do ***
    !
    !> PreCICE action fulfilled
    call precicef_mark_action_fulfilled( precice%write_it_checkp )

  else ! ???
    ! ... *** to do *** anything to to?
  end if

  !> Read data from structural solver
  do i = 1, size(precice%fields)
    if ( trim(precice%fields(i)%fio) .eq. 'read' ) then
      if ( trim(precice%fields(i)%ftype) .eq. 'scalar' ) then
        ! check ---
        write(*,*) ' read scalar field: ', trim(precice%fields(i)%fname)
        ! check ---
        call precicef_read_bsdata( precice%fields(i)%fid, &
                                   precice%mesh%nnodes  , &
                                   precice%mesh%node_ids, &
                                   precice%fields(i)%fdata(1,:) )
      elseif ( trim(precice%fields(i)%ftype) .eq. 'vector' ) then
        ! check ---
        write(*,*) ' read vector field: ', trim(precice%fields(i)%fname)
        ! check ---
        call precicef_read_bvdata( precice%fields(i)%fid, &
                                   precice%mesh%nnodes  , &
                                   precice%mesh%node_ids, &
                                   precice%fields(i)%fdata )
      endif
      ! ! check ---
      ! do j = 1, size(precice%fields(i)%fdata,2)
      !   write(*,*) precice%fields(i)%fdata(:,j)
      ! end do
      ! ! check ---
    end if
  end do

  ! ...
  ! ...
  ! ...
  !> Update force and moments to be passed to the structural solver
  do i = 1, size(precice%fields)
    if ( trim(precice%fields(i)%fio) .eq. 'write' ) then
      precice%fields(i)%fdata = 0.0_wp
      if ( trim(precice%fields(i)%ftype) .eq. 'scalar' ) then
        precice%fields(i)%fdata = -0.01_wp
      elseif ( trim(precice%fields(i)%ftype) .eq. 'vector' ) then
        precice%fields(i)%fdata(3,:) = -0.01_wp
      endif
    end if
  end do
  ! ...
  ! ...
  ! ...
  !> check ---
  write(*,*) ' --------------------------------- '
  do i = 1, size(precice%fields)
    write(*,*) trim(precice%fields(i)%fname)
    do j = 1, size(precice%fields(i)%fdata,2)
      write(*,*) precice%fields(i)%fdata(:,j)
    end do
  end do
  !> check ---
  ! ...
  ! ...
  ! ...

  !> Write force and moments to structural solver
  do i = 1, size(precice%fields)
    if ( trim(precice%fields(i)%fio) .eq. 'write' ) then
      if ( trim(precice%fields(i)%ftype) .eq. 'scalar' ) then
        call precicef_write_bsdata( precice%fields(i)%fid, &
                                    precice%mesh%nnodes  , &
                                    precice%mesh%node_ids, &
                                    precice%fields(i)%fdata(1,:) )
      elseif ( trim(precice%fields(i)%ftype) .eq. 'vector' ) then
        call precicef_write_bvdata( precice%fields(i)%fid, &
                                    precice%mesh%nnodes  , &
                                    precice%mesh%node_ids, &
                                    precice%fields(i)%fdata )
      endif
    end if
  end do

  call precicef_action_required( precice%read_it_checkp, bool )
  if ( bool .eq. 1 ) then ! timestep not converged
    !> Reload checkpoint state
    ! ... *** to do ***
    call precicef_mark_action_fulfilled( precice%read_it_checkp )
  else ! timestep converged
    !> Finalize timestep
    ! Do the same actions as a simulation w/o coupling
    ! *** to do *** check if something special is needed
  end if

  call precicef_ongoing( precice%is_ongoing )
  if ( precice%is_ongoing .eq. 1 ) then
    call precicef_advance( precice%dt_precice )
  end if


end do

call precicef_finalize()

end program sol2
