program sol2

use mod_general, only: &
    wp

use mod_precice, only: &
    t_precice, precice_mcl

implicit none

character(len=precice_mcl) :: config_file_name = './../precice-config.xml'
character(len=precice_mcl) :: solver_name = 'AeroSolver'
character(len=precice_mcl) :: mesh_name = 'AeroNodes'
integer :: comm_rank = 0, comm_size = 1
integer :: nd, dimensions
character(len=precice_mcl) :: pos_name = 'Position'
character(len=precice_mcl) :: vel_name = 'Velocity'
character(len=precice_mcl) :: for_name = 'Force'
character(len=precice_mcl) :: write_initial_data, read_it_checkp, write_it_checkp
integer :: mesh_id, pos_id, vel_id, for_id, is_ongoing
real(wp) :: dt_precice
integer :: i, j, bool
integer :: nnodes
integer, allocatable :: node_ids(:)
real(wp), allocatable :: rr(:,:), force(:,:), pos(:,:), vel(:,:), force_t(:,:)

!> === w/ subroutines ===========================================
! call precice % initialize()
! call precice % initialize_mesh()
! call precice % initialize_fields()

!> === w/o subroutines ==========================================
!> Initialize some preCICE variables
write_initial_data(1:precice_mcl)='                                                  '
read_it_checkp(    1:precice_mcl)='                                                  '
write_it_checkp(   1:precice_mcl)='                                                  '
call precicef_action_write_initial_data(write_initial_data)
call precicef_action_write_iter_checkp( write_it_checkp)
call precicef_action_read_iter_checkp(   read_it_checkp)

!> Initialize praticipant ---------------------------------------
call precicef_create( solver_name, config_file_name, &
                      comm_rank, comm_size )
call precicef_get_dims( dimensions ) ; nd = dimensions
call precicef_get_mesh_id( mesh_name, mesh_id )

!> Fields
call precicef_get_data_id( trim(pos_name), mesh_id, pos_id )
call precicef_get_data_id( trim(vel_name), mesh_id, vel_id )
call precicef_get_data_id( trim(for_name), mesh_id, for_id )

write(*,*) ' pos,vel,for_id: ', pos_id, vel_id, for_id
! stop

!> Mesh ---------------------------------------------------------
nnodes = 9
allocate(node_ids(nnodes)); node_ids = 0
allocate( rr(nd, nnodes) ); rr = 0.0_wp
do i = 1, nnodes ;  rr(2, i) = dble(i-1)/dble(nnodes-1) ;  end do

!> Nodes
call precicef_set_vertices( mesh_id, nnodes, rr, node_ids )
! do i = 1, nnodes
!   call precicef_set_vertex( mesh_id, rr(:,i), node_ids(i) )
! end do

do i = 1, nnodes
  write(*,*) node_ids(i), rr(:,i)
end do

!> Connectivity
!> Edges
! ...
!> Elements
! ...

allocate(force  (nd, nnodes)) ; force   = 0.0_wp 
allocate(  pos  (nd, nnodes)) ;   pos   = -333.3_wp
allocate(  vel  (nd, nnodes)) ;   vel   = -333.3_wp
allocate(force_t(nd, nnodes)) ; force_t = force

!> Initialization -----------------------------------------------
call precicef_initialize( dt_precice )
call precicef_ongoing   ( is_ongoing )
write(*,*) ' is_ongoing : ', is_ongoing

! Time loop
do while ( is_ongoing .ne. 0 )

  ! -------
  call precicef_action_required( write_it_checkp , bool )
  if ( bool .eq. 1 ) then ! Save old state
    write(*,*) ' ********** require write ********** '
    force_t = force
    call precicef_mark_action_fulfilled( write_it_checkp )
  end if

  !> Read data from structural solver
  call precicef_read_bvdata( pos_id, nnodes, node_ids, pos )
  call precicef_read_bvdata( vel_id, nnodes, node_ids, vel )
  ! do i = 1, nnodes
  !   call precicef_read_vdata( pos_id, node_ids(i), pos(:,i) )
  !   call precicef_read_vdata( vel_id, node_ids(i), vel(:,i) )
  ! end do
  write(*,*) ' pos, vel '
  do i = 1, nnodes ; write(*,*) pos(:,i), vel(:,i) ; end do

  !> Update force and moments to be passed to the structural solver
  force = 0.0_wp
  do i = 1, nnodes
    force(3,i) = -.01_wp
  end do

  write(*,*) ' force '
  do i = 1, nnodes ; write(*,*) force(:,i) ; end do

  !> Write force and moments to structural solver
  call precicef_write_bvdata( for_id, nnodes, node_ids, force )
  ! do i = 1, nnodes
  !   call precicef_write_vdata( for_id, node_ids(i), force(:,i) )
  ! end do

  call precicef_ongoing( is_ongoing )
  if ( is_ongoing .eq. 1 ) call precicef_advance( dt_precice )

  call precicef_action_required( read_it_checkp, bool )
  if ( bool .eq. 1 ) then ! timestep not converged
    write(*,*) ' ********** require read *********** '
    force = force_t   ! reaload state
    call precicef_mark_action_fulfilled( read_it_checkp )
  else ! timestep converged -> finalize timestep
    ! ...
  end if

end do

call precicef_finalize()

deallocate(rr)

end program sol2
