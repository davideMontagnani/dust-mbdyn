module mod_general

implicit none

private
public :: wp

!> Working precision
integer, parameter :: wp = 8

end module mod_general
