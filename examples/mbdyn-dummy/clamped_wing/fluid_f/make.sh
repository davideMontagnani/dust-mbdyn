#!/bin/bash

gfortran -g -fcheck=all -o sol2 \
/home/davide/Software/preCICE/precice-2.0.1/src/precice/bindings/f2003/precice.f03 \
mod_general.f90 mod_precice.f90 sol2.f90 \
-L/usr/local/lib -lprecice
