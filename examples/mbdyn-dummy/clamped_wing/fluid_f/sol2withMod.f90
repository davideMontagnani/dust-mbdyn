program sol2

use mod_general, only: &
    wp

use mod_precice, only: &
    t_precice, precice_mcl

implicit none

type(t_precice) :: precice
integer :: i, j, bool
integer :: nnodes
real(wp), allocatable :: rr(:,:)

integer, parameter :: n_max_fields = 6
character(len=precice_mcl) :: field_list(n_max_fields)
character(len=precice_mcl) :: type_list(n_max_fields)
character(len=precice_mcl) :: io_list(n_max_fields)

integer :: nf, hasdata

!> === w/ subroutines ===========================================
call precice % initialize()
call precice % initialize_mesh()
call precice % initialize_fields()

! !> === w/o subroutines ==========================================
! !> Initialize praticipant ---------------------------------------
! !> Default input for dust in a preCICE coupled simulation
! precice % config_file_name = './../precice-config.xml'
! precice % solver_name = 'AeroSolver'
! precice %   mesh_name = 'AeroNodes'
! precice % comm_rank = 0
! precice % comm_size = 1
! 
! !> Initialize PreCICE participant and mesh
! call precicef_create( precice % solver_name, & 
!                       precice % config_file_name, &
!                       precice % comm_rank, &
!                       precice % comm_size )
! 
! precice % mesh % mesh_name = precice % mesh_name
! call precicef_get_dims( precice % mesh % ndim )
! call precicef_get_mesh_id( precice % mesh % mesh_name, precice % mesh % mesh_id )
! 
! !> Initialize some preCICE variables
! precice % write_initial_data(1:precice_mcl)='                                                  '
! precice % read_it_checkp(    1:precice_mcl)='                                                  '
! precice % write_it_checkp(   1:precice_mcl)='                                                  '
! 
! call precicef_action_write_initial_data(precice % write_initial_data)
! call precicef_action_read_iter_checkp(  precice % read_it_checkp)
! call precicef_action_write_iter_checkp( precice %write_it_checkp)
! 
! !> Mesh ---------------------------------------------------------
! nnodes = 9
! allocate( rr(precice%mesh%ndim,nnodes) ); rr = 0.0_wp
! do i = 1, nnodes ;  rr(2,i) = dble(i-1)/dble(nnodes-1) ;  end do
! 
! !> Allocate participant%mesh fields ===========================
! allocate(precice%mesh%node_ids(nnodes)); precice%mesh%node_ids = 0
! allocate(precice%mesh%nodes( precice%mesh%ndim, nnodes ))
! 
! precice%mesh%nodes(:,1:nnodes) = rr
! 
! !> Nodes
! call precicef_set_vertices( precice%mesh%mesh_id, nnodes,  &
!                             precice%mesh%nodes, precice%mesh%node_ids )
! precice%mesh%nnodes = nnodes
! 
! !> check ---
! write(*,*) ' PreCICE, mesh % nodes: '
! do i = 1 , nnodes ;  write(*,*) precice%mesh%nodes(:,i) ;  end do
! 
! !> Connectivity
! !> Edges
! ! ...
! !> Elements
! ! ...
! 
! deallocate(rr)
! 
! !> Fields -------------------------------------------------------
! 
! 
! !> Fields that can be exchanged through PreCICE. Add a field if needed
! field_list(1) = 'Position'        ; type_list(1) = 'vector' ; io_list(1) = 'read' 
! field_list(2) = 'Velocity'        ; type_list(2) = 'vector' ; io_list(2) = 'read' 
! field_list(3) = 'Rotation'        ; type_list(3) = 'vector' ; io_list(3) = 'read' 
! field_list(4) = 'AngularVelocity' ; type_list(4) = 'vector' ; io_list(4) = 'read' 
! field_list(5) = 'Force'           ; type_list(5) = 'vector' ; io_list(5) = 'write'
! field_list(6) = 'Moment'          ; type_list(6) = 'vector' ; io_list(6) = 'write'
! 
! !> Count n. of exchanged fields
! nf = 0
! do i = 1, n_max_fields
!   call precicef_has_data( trim(field_list(i)), precice%mesh%mesh_id, hasdata )
!   if ( hasdata .eq. 1 ) nf = nf + 1
! end do
! 
! allocate( precice%fields( nf ) )
! nf = 0
! do i = 1, n_max_fields
!   call precicef_has_data( trim(field_list(i)), precice%mesh%mesh_id, hasdata )
!   if ( hasdata .eq. 1 ) then
!     nf = nf + 1
!     precice%fields(nf)%fname = trim(field_list(i))
!     precice%fields(nf)%fio   = trim(   io_list(i))
!     precice%fields(nf)%ftype = trim( type_list(i))
!     call precicef_get_data_id( trim(precice%fields(nf)%fname), &
!                                precice%mesh%mesh_id, precice%fields(nf)%fid )
!     !> Allocate and initialize fields to zero *** to do *** where to write initial data?
!     if ( trim(precice%fields(nf)%ftype) .eq. 'scalar' ) then
!       allocate( precice%fields(nf)%fdata(1, precice%mesh%nnodes) )
!       precice%fields(nf)%fdata = 0.0_wp
!     else if ( trim(precice%fields(nf)%ftype) .eq. 'vector' ) then
!       allocate( precice%fields(nf)%fdata( precice%mesh%ndim, &
!                                           precice%mesh%nnodes) )
!       precice%fields(nf)%fdata = 0.0_wp
!     end if
!   end if
! end do




!> check ---
do i = 1, size(precice%fields)
  write(*,*) trim(precice%fields(i)%fname), precice%fields(i)%fid
end do

call precicef_ongoing   ( precice % is_ongoing )
write(*,*) ' is_ongoing : ', precice % is_ongoing
call precicef_initialize( precice % dt_precice )

! Time loop
do while ( precice%is_ongoing .ne. 0 )

  ! -------
  call precicef_action_required( precice%write_it_checkp , bool )
  if ( bool .eq. 1 ) then ! Save old state
    !> Save old state: forces and moments
    ! ... *** to do ***
    !
    !> PreCICE action fulfilled
    call precicef_mark_action_fulfilled( precice%write_it_checkp )

  else ! ???
    ! ... *** to do *** anything to to?
  end if

  !> Read data from structural solver
  do i = 1, size(precice%fields)
    if ( trim(precice%fields(i)%fio) .eq. 'read' ) then
      if ( trim(precice%fields(i)%ftype) .eq. 'scalar' ) then
        ! check ---
        write(*,*) ' read scalar field: ', trim(precice%fields(i)%fname)
        ! check ---
        call precicef_read_bsdata( precice%fields(i)%fid, &
                                   precice%mesh%nnodes  , &
                                   precice%mesh%node_ids, &
                                   precice%fields(i)%fdata(1,:) )
      elseif ( trim(precice%fields(i)%ftype) .eq. 'vector' ) then
        ! check ---
        write(*,*) ' read vector field: ', trim(precice%fields(i)%fname)
        ! check ---
        call precicef_read_bvdata( precice%fields(i)%fid, &
                                   precice%mesh%nnodes  , &
                                   precice%mesh%node_ids, &
                                   precice%fields(i)%fdata )
      endif
      ! ! check ---
      ! do j = 1, size(precice%fields(i)%fdata,2)
      !   write(*,*) precice%fields(i)%fdata(:,j)
      ! end do
      ! ! check ---
    end if
  end do

  ! ...
  ! ...
  ! ...
  !> Update force and moments to be passed to the structural solver
  do i = 1, size(precice%fields)
    if ( trim(precice%fields(i)%fio) .eq. 'write' ) then
      precice%fields(i)%fdata = 0.0_wp
      if ( trim(precice%fields(i)%ftype) .eq. 'scalar' ) then
        precice%fields(i)%fdata = -0.01_wp
      elseif ( trim(precice%fields(i)%ftype) .eq. 'vector' ) then
        precice%fields(i)%fdata(3,:) = -0.01_wp
      endif
    end if
  end do
  ! ...
  ! ...
  ! ...
  !> check ---
  write(*,*) ' --------------------------------- '
  do i = 1, size(precice%fields)
    write(*,*) trim(precice%fields(i)%fname)
    do j = 1, size(precice%fields(i)%fdata,2)
      write(*,*) precice%fields(i)%fdata(:,j)
    end do
  end do
  !> check ---
  ! ...
  ! ...
  ! ...

  !> Write force and moments to structural solver
  do i = 1, size(precice%fields)
    if ( trim(precice%fields(i)%fio) .eq. 'write' ) then
      if ( trim(precice%fields(i)%ftype) .eq. 'scalar' ) then
        call precicef_write_bsdata( precice%fields(i)%fid, &
                                    precice%mesh%nnodes  , &
                                    precice%mesh%node_ids, &
                                    precice%fields(i)%fdata(1,:) )
      elseif ( trim(precice%fields(i)%ftype) .eq. 'vector' ) then
        call precicef_write_bvdata( precice%fields(i)%fid, &
                                    precice%mesh%nnodes  , &
                                    precice%mesh%node_ids, &
                                    precice%fields(i)%fdata )
      endif
    end if
  end do

  call precicef_action_required( precice%read_it_checkp, bool )
  if ( bool .eq. 1 ) then ! timestep not converged
    !> Reload checkpoint state
    ! ... *** to do ***
    call precicef_mark_action_fulfilled( precice%read_it_checkp )
  else ! timestep converged
    !> Finalize timestep
    ! Do the same actions as a simulation w/o coupling
    ! *** to do *** check if something special is needed
  end if

  call precicef_ongoing( precice%is_ongoing )
  if ( precice%is_ongoing .eq. 1 ) then
    call precicef_advance( precice%dt_precice )
  end if


end do

call precicef_finalize()

end program sol2
