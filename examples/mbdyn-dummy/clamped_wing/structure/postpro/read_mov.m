clear all ; clc ; close all ; 

%> Output
sys_movie = 1; dnt = 20 ;
node_plot = 1;
nel = 11;

%> Simulation
folder   = '../';  % /home/davide/Software/MBDyn/tutorials/' ;
tutorial = './' ;
casen    = 'output' ;
ext      = '.mov' ;

filen = [ folder, tutorial, casen, ext ] ;

printf('\n Filename: %s \n\n', filen);

%> Read data
dat =  dlmread( filen, '', 0, 0 ) ;
% idx, pos(3), Euler angles(3), vel(3), Omega(3)

%> N. Nodes, N. Timesteps
i_node_0 = dat(1,1);
n_nodes = 1; i_node=1;
while ( dat(i_node+1,1) ~= i_node_0 )
  n_nodes = n_nodes + 1;
  i_node = i_node+1;
end
nt = size(dat,1) / n_nodes;

printf(' N.Nodes: %d \n', n_nodes)
printf(' N.Dt   : %d \n\n', nt)

%> Movie: time evolution of the whole system
if ( sys_movie )
  figure('Position',[ 100 100 800 600 ])
  for it = 1 : dnt : nt
    plot3(dat((it-1)*n_nodes+1:it*n_nodes,2), ...
          dat((it-1)*n_nodes+1:it*n_nodes,3), ...
          dat((it-1)*n_nodes+1:it*n_nodes,4), ...
          'o-','LineWidth',2), grid on,  axis equal , axis([ -4 4 -4 4 -0.5 0.5]), view([ 135 30 ])
    title(sprintf('%d/%d',it,nt))
    xlabel('x'), ylabel('y'), zlabel('z')
    pause(0.01)
  end
end

%> Time evolution of node coordinate
if ( node_plot )
  in = n_nodes; % 1; % 
  % ic = 3;
  figure('Position',[100 100 1200 500])
  for ic = 1 : 3  
    % subplot(1,3,ic), plot(dat(in-2*nel:n_nodes:end, 1+ic)', 'b', 'LineWidth', 2) ; hold on
    % subplot(1,3,ic), plot(dat(in-  nel:n_nodes:end, 1+ic)', 'r', 'LineWidth', 2)
    subplot(1,3,ic), plot(dat(in      :n_nodes:end, 1+ic)', 'k', 'LineWidth', 2) ; hold off
  end

end


% === Check w/ analytic model ===
%> Infinitely rigid hub and infinitely elastic blades
rho = 1.225;        % air density
Om  = 10.;          % rotor angular velocity
R   = 1.;           % blade radius (w/o cut-out at hub)
c   =  .15;         % blade chord
cla = 2.*pi;        % slope of the lift curve
th  = 2.*pi/180.;   % aoa of the blade
M   =  .1;          % blade mass
k   =  .1;          % flap hinge stiffness

Maero = 1./8. * rho * Om^2 * R^4 * c * cla * th;
Min_b = 1./3. * M * Om^2;
Mk_b  = k
beta = Maero / ( Min_b + Mk_b ) ;

printf(' steady flap angle (analytical model): %8.4f \n', beta)




