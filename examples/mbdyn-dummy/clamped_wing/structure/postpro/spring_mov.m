clear all ; clc ; close all ; 

%> Output
sys_movie = 1; dnt = 25 ;
node_plot = 1;
nel = 1;

dt = 1.e-3;

xmin = -1.5  ; xmax = 1.5  ; 
ymin = -1.5  ; ymax = 1.5  ; 
zmin = -0.02 ; zmax = 0.02 ;
thview1 = 90.0
thview2 = 00.0

%> Simulation
% folder   = '/home/davide/Software/MBDyn/tutorials/' ;
% tutorial = 'c03_beamspring/' ;
folder   = '/home/davide/Software/MBDyn/tutorials/' ;
tutorial = 'p01_clampedwing/structure/' ;
casen    = 'output' ;   % 'springmass';   % 'output' ;
ext      = '.mov' ;

filen = [ folder, tutorial, casen, ext ] ;

printf('\n Filename: %s \n\n', filen);

%> Read data
dat =  dlmread( filen, '', 0, 0 ) ;
% idx, pos(3), Euler angles(3), vel(3), Omega(3)

%> N. Nodes, N. Timesteps
i_node_0 = dat(1,1);
n_nodes = 1; i_node=1;
while ( dat(i_node+1,1) ~= i_node_0 )
  n_nodes = n_nodes + 1;
  i_node = i_node+1;
end
nt = size(dat,1) / n_nodes;

printf(' N.Nodes: %d \n', n_nodes)
printf(' N.Dt   : %d \n\n', nt)

%> Movie: time evolution of the whole system
if ( sys_movie )
  figure('Position',[ 100 100 800 600 ])
  for it = 1 : dnt : nt
    plot3(dat((it-1)*n_nodes+1:it*n_nodes,2), ...
          dat((it-1)*n_nodes+1:it*n_nodes,3), ...
          dat((it-1)*n_nodes+1:it*n_nodes,4), ...
          'o-','LineWidth',2), grid on,  axis([ xmin xmax ymin ymax zmin zmax]), view([ thview1 thview2 ])
    title(sprintf('%d/%d',it,nt))
    xlabel('x'), ylabel('y'), zlabel('z')
    pause(0.01)
  end
end

%> Time evolution of node coordinate
if ( node_plot )
  in = n_nodes; % 1; % 
  % ic = 3;
  figure('Position',[100 100 1200 500])
  for ic = 1 : 3 
    tv = dt * [ 1:size(dat(in:n_nodes:end, 1+ic),1) ]' ; 
%   subplot(1,3,ic), plot(tv, dat(in-1:n_nodes:end, 1+ic)', 'b', 'LineWidth', 2) ; hold on
    subplot(1,3,ic), plot(tv, dat(in  :n_nodes:end, 1+ic)', 'k', 'LineWidth', 2) ; hold off
  end

end


