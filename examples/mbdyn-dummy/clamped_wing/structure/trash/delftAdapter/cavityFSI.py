import sys
print(' path:', sys.path)

from mbdynAdapter import MBDynHelper, MBDynAdapter

print(' Initialize MBDyn helper, for building membrane model')
mbd = MBDynHelper()

print(' Read .msh file')
mbd.readMsh('membrane.msh')

print(' Set mbd.controlDict and mbd.materialDict ')
mbd.controlDict = {'initialTime':0,'finalTime':100,'output frequency':10}
mbd.materialDict = {'E':25e+6, 'nu':0, 't':0.2, 'rho':500, 'C':0.000001}

print(' Initialize MBDyn adapter ')
adapter = MBDynAdapter(mbd)

print(' Run preCICE() ')
adapter.runPreCICE()

print(' ... end of the cavityFSI.py file. Goodbye!')
