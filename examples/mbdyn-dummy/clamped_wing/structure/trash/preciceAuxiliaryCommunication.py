# Function to send MBDyn nodes to the other solver

import precice
from precice import *

#> sendMBDynMesh ----------------------------------------------
def sendMBDynMesh(mbd, chord, twist):

  n = mbd.socket.nnodes
  print(' n: ', n)
  #> ~~~ PreCICE-aux communication: start ~~~~~~~~~~~~~~~~~~~~~~~~~
  #> Precice params: communication
  comm_rank = 0; comm_size = 1
  config_file_name = "./../precice-config-aux.xml"
  solver_name   = "MBDyn"
  mesh_name_mbd = "AuxilMBDynMesh"
  # mesh_name_flu = "AuxilFluidMesh"
  pos_name      = "Posit"   ; rot_name    = "Rotation"
  cho_name      = "Chord"   ; twi_name    = "Twist"
  dum_name      = "Dummy"
  
  print(" Configure preCICE-aux ...", end='')
  interface = precice.Interface( solver_name, config_file_name, \
                                 comm_rank, comm_size )

  print(" done.")
  dimensions  = interface.get_dimensions(); nd = dimensions
  mesh_id_mbd = interface.get_mesh_id( mesh_name_mbd )
  pos_id    = interface.get_data_id( pos_name, mesh_id_mbd )
  rot_id    = interface.get_data_id( rot_name, mesh_id_mbd )
  cho_id    = interface.get_data_id( cho_name, mesh_id_mbd )
  twi_id    = interface.get_data_id( twi_name, mesh_id_mbd )
  dum_id    = interface.get_data_id( dum_name, mesh_id_mbd )
  print(" done. \n")
  
  rr_mbd = np.zeros((n, nd))
  for i in np.arange(n):    # todo with reshape
    rr_mbd[i,:] = np.array([ i, 0., 0. ]) # nodal.n_x[i*nd:(i+1)*nd]
  node_id_mbd = interface.set_mesh_vertices( mesh_id_mbd, rr_mbd )
  
  #> Fields
  pos = np.ones((n, nd)); cho = np.ones((n))
  rot = np.ones((n, nd)); twi = np.ones((n))
  
  for i in np.arange(n):
    pos[i,:] = mbd.nodal.n_x[i*nd:(i+1)*nd]
    rot[i,:] = mbd.nodal.n_theta[i*nd:(i+1)*nd]  # check !
    cho[i  ] = chord
    twi[i  ] = twist
  
  print(' rr_mbd:', rr_mbd)
  # print(' pos   :', pos)
  # print(' rot   :', rot)
  
  dum = np.ones((n)) # dummy field read from fluidSolver
  
  dt_precice = interface.initialize()
  is_ongoing = interface.is_coupling_ongoing()
  print(" is_ongoing: ", is_ongoing)
  
  # cowic = interface.
  
  while ( is_ongoing ):
  
    interface.write_block_vector_data( pos_id, node_id_mbd, pos ); print(' pos: ', pos)
    interface.write_block_vector_data( rot_id, node_id_mbd, rot ); print(' rot: ', rot)
    interface.write_block_scalar_data( cho_id, node_id_mbd, cho ); print(' cho: ', cho)
    interface.write_block_scalar_data( twi_id, node_id_mbd, twi ); print(' twi: ', twi)
  
    dum = interface.read_block_scalar_data( dum_id, node_id_mbd ); print(' dum: ', dum)
  
    dt_precice = interface.advance(dt_precice)
    is_ongoing = interface.is_coupling_ongoing()
  
  interface.finalize()

  return pos, rot, interface
    
