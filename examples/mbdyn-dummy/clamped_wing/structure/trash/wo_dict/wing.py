
import time
import sys;
# set to path of MBDyn support for python communication
sys.path.append('/usr/local/mbdyn/libexec/mbpy');

import os;
import tempfile;
tmpdir = tempfile.mkdtemp('', '.mbdyn_');
path = tmpdir + '/mbdyn.sock';
print(' path: ', path)

os.environ['MBSOCK'] = path;
os.system('mbdyn -f wing -o output > output.txt 2>&1 &');

from mbc_py_interface import mbcNodal
from numpy import *
import numpy as np

import precice
from precice import *

# from mbdynAdapter import MBDynAdapter
# from preciceAuxiliaryCommunication import *
from mbdynInterface import MBDynInterface
from mbdynAdapter   import MBDynAdapter

#> ==============================================================
#> Hardcoded parameters
#> Does the other solver needs MBDyn nodes to build its own mesh?
writeNodes = True    # True #  False

#> MBDyn model parameters 
dt_set = 0.001
nEl = 4; nnodes = nEl*2     # nnodes exposed by MBDyn through external forces
chord = .1                  # wing chord (const)
twist = 2. * np.pi / 180.   # wing twist (const)


#> ==============================================================
#> Construct MBDyn/mbc_py interface
#> Initialize MBDyn/mbc_py interface: negotiate and recv()
mbd = MBDynInterface()
mbd.initialize( path=path, verbose=1, nnodes=nnodes, accels=1, \
                dumpAuxFile=True )

#> ==============================================================
#> Send MBDyn exposed nodes to the other solver, if needed

n = mbd.socket.nnodes
print(' n: ', n)
if ( writeNodes ):
  pos, rot, inter = mbd.sendMBDynMesh( chord, twist )

#> ==============================================================
#> Build reference "Lagrangian" grid for preCICE-MBDyn solver,
#  from the position negotiated through MBDyn-mbc_py interface
print(" Build structural model ")
print(" ... ")

print(" Initialize MBDyn adapter ")
adapter = MBDynAdapter( mbd )

if ( adapter.debug ):
  print(' adapter.code.name     : ', adapter.code.name )
  print(' adapter.code.mesh.name: ', adapter.code.mesh.name )
  for i in np.arange(len(adapter.code.field)):
    print(' adapter.code.field.name, id: ', adapter.code.field[i].name, \
                                            adapter.code.field[i].id )

# todo: clean the implementation
interface = adapter.interface
n  = adapter.mbd.socket.nnodes
nd = adapter.dim
pos_id = 0
vel_id = 1
for_id = 2
node_id = adapter.code.mesh.node_id
dt_precice = adapter.dt_precice
is_ongoing = adapter.is_ongoing

#> === MBDYN: Read node position and velocit form MBDyn =========
dummy = np.zeros((n, nd))
pos   = reshape( mbd.nodal.n_x , (n, nd) ) 
vel   = reshape( mbd.nodal.n_xp, (n, nd) )
force = dummy              # to be read from AeroSolver


adapter.runPreCICE()

# t = 0.
# while ( is_ongoing ):
# 
#   if ( interface.is_action_required( cowic ) ):
#     pos_t = pos; vel_t = vel
#     interface.mark_action_fulfilled( cowic )
#  
#   # Receive data from MBDyn
#   if ( mbd.nodal.recv() ):
#     print('**** break ****')
#     break
# 
#   #> Receive data from AeroSolver and set nodal.n_f field
#   force = interface.read_block_vector_data( for_id, node_id )
#   print(' force: '); print( force ) # ; time.sleep(2.0) 
#   for i in np.arange(n):
#     mbd.nodal.n_f[i*nd:(i+1)*nd] = force[i,:]
#     mbd.nodal.n_m[i*nd:(i+1)*nd] = .0
# 
#   #> Read position and velocity form MBDyn
#   print(' Reshape MBDyn arrays: nodal.n_x, .n_xp ')
#   pos   = reshape( mbd.nodal.n_x , (n, nd)) 
#   vel   = reshape( mbd.nodal.n_xp, (n, nd))
# 
#   #> Compute position and velocity
#   for i in np.arange(n):
#     print(i,': ', pos[i,:],', ',vel[i,:])
# 
#   #> 
#   if ( mbd.nodal.send(True) ):
#     break
# 
#   #> Write to AeroSolver
#   print(' Write_block_vector_data ')
#   interface.write_block_vector_data( pos_id, node_id, pos )
#   interface.write_block_vector_data( vel_id, node_id, vel )
# 
#   dt = min( dt_set, dt_precice )
# 
#   is_ongoing = interface.is_coupling_ongoing()
#   if ( is_ongoing ):
#     dt_precice = interface.advance(dt)
#   else:
#     break
# 
#   if ( interface.is_action_required( coric ) ): # dt not converged
#     pos = pos_t; vel = vel_t    # reload state
#     interface.mark_action_fulfilled( coric )
#   else: # dt converged
#     t = t + dt
# 
# print(' before finalize() ')
# interface.finalize()
# print(" Finalize auxiliary coupling ")
# 
# # inter.finalize()
# # print(" Finalize auxiliary coupling ")
# 
# #> ~~~ PreCICE coupling between mbdyn and aero solver ~~~~~~~~~~~
# 
# mbd.nodal.destroy();
# os.rmdir(tmpdir);

