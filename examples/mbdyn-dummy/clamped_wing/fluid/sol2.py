
import time
import os
import precice
from precice import *

import numpy as np

#> Time
t = 0.; tend = 3000.0; dt_set = 0.005
#> Aerodynamic parameters
rho = 1.; Uinf = 1.; cla = 2.*np.pi;

#> Auxiliary communication
auxiliary_comm = False

if ( auxiliary_comm ):
  #> === Precice parameters: auxiliary communication  ===
  comm_rank = 0
  comm_size = 1
  config_file_name_aux = "./../precice-config-aux.xml"
  # config_file_name = "./../precice-config.xml"
  #> Participant
  solver_name = "FluidSolver"
  #> All the meshes involved in the coupling
  # mesh_name_mbd = "AuxilMBDynMesh"
  mesh_name_flu = "AuxilFluidMesh"
  #> All the fields involved in the coupling
  pos_name = "Position"; rot_name = "Rotation"
  cho_name = "Chord"   ; twi_name = "Twist"
  dum_name = "Dummy"
  
  #> ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  #> Read auxiliary file
  filen = "./../nnodes.dat"
  iwrite = 1; ireadfile = 1
  while ( ireadfile ):
    if ( iwrite ):
      print(" Waiting for file: ", filen); iwrite = 0
    if ( os.path.isfile( filen ) ):
      fid = open( filen, "r" )
      n = fid.read(); fid.close(); ireadfile = 0
  print(" ... done. ")
  n = int(n)
  print(" n. nodes: ", n)
  #> ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  
  #> ==============================================================
  #> Auxiliary communication
  #> ==============================================================
  print(" Configure preCICE...")
  interface = precice.Interface( solver_name, config_file_name_aux, \
                                 comm_rank, comm_size )
  dimensions  = interface.get_dimensions();   nd = dimensions
  mesh_id_flu = interface.get_mesh_id( mesh_name_flu )          
  pos_id    = interface.get_data_id( pos_name, mesh_id_flu )
  rot_id    = interface.get_data_id( rot_name, mesh_id_flu )
  cho_id    = interface.get_data_id( cho_name, mesh_id_flu )
  twi_id    = interface.get_data_id( twi_name, mesh_id_flu )
  dum_id    = interface.get_data_id( dum_name, mesh_id_flu )
  # dummy_id    = interface.get_data_id( dummy_name, mesh_id_aux ); print('dummy_id   : ', dummy_id   )
  print(" ...preCICE configured. \n")
  
  rr_flu = np.zeros((n, nd))
  for i in np.arange(n):    # todo with reshape
    rr_flu[i,:] = np.array([ i, 0., 0. ]) # nodal.n_x[i*nd:(i+1)*nd]
  node_id_flu = interface.set_mesh_vertices( mesh_id_flu, rr_flu )
  
  #> Fields
  # pos = np.zeros((n, nd))
  # rot = np.zeros((n, nd))
  # cho = np.zeros((n))
  # twi = np.zeros((n))
  dum = np.zeros((n))
  
  print( " Initializing preCICE ... " )
  dt_precice = interface.initialize()
  is_ongoing = interface.is_coupling_ongoing()
  print( " Done. is_ongoing:", is_ongoing )
  
  while ( is_ongoing ):
  
    interface.write_block_scalar_data( dum_id, node_id_flu, dum ); print(' dum: ', dum)
  
    pos = interface.read_block_vector_data( pos_id, node_id_flu ); print(' pos: ', pos)
    rot = interface.read_block_vector_data( rot_id, node_id_flu ); print(' rot: ', rot)
    cho = interface.read_block_scalar_data( cho_id, node_id_flu ); print(' cho: ', cho)
    twi = interface.read_block_scalar_data( twi_id, node_id_flu ); print(' twi: ', twi)
  
    dt_precice = interface.advance(dt_precice)
    is_ongoing = interface.is_coupling_ongoing()
  
  interface.finalize()
  print(" Finalize auxiliary coupling")

  #> ~~~ PreCICE-aux communication: end ~~~~~~~~~~~~~~~~~~~~~~~~~~~
  
  print(" Build aerodynamic model ")
  rr = pos

else:
  n = 9
  rr = np.zeros((n,3))
  rr[:,1] = np.linspace(0., 1., n)
  print(' rr: '); print(rr)


#> ~~~ PreCICE coupling between mbdyn and aero solver ~~~~~~~~~~~
print(" Start preCICE coupling between solvers ")
#> === Precice parameters ===
comm_rank = 0; comm_size = 1
config_file_name = "./../precice-config.xml"
solver_name = "AeroSolver"                          # Participant
mesh_name   = "AeroNodes"                           # Mesh
pos_name = "Position"; vel_name = "Velocity"        # Fields
for_name = "Force"

print(" +++++ ")
interface = precice.Interface( solver_name, config_file_name, \
                               comm_rank, comm_size )
print(" +++++ ")
dimensions  = interface.get_dimensions(); nd = dimensions
mesh_id = interface.get_mesh_id( mesh_name )

pos_id  = interface.get_data_id( pos_name, mesh_id )
vel_id  = interface.get_data_id( vel_name, mesh_id )
for_id  = interface.get_data_id( for_name, mesh_id )

print(' pos,vel,for_id: ', pos_id, vel_id, for_id)

node_id = interface.set_mesh_vertices( mesh_id, rr )
print(' node_id:', node_id)
print(' rr     :', rr)

#> Initialize fields
dummy = np.ones((n, nd))
force = dummy              # to be computed
pos   = dummy              # to be read from MBDyn      
vel   = dummy              # to be read from MBDyn      
force_t = force

dt_precice = interface.initialize()
is_ongoing = interface.is_coupling_ongoing()
print(" is_ongoing: ", is_ongoing)

cowic = precice.action_write_iteration_checkpoint()
coric = precice.action_read_iteration_checkpoint()

# Hardcoded dy
b = 1.;  dy = b / n

t = 0.
while ( is_ongoing ):

  if ( interface.is_action_required( cowic ) ):
    print(' *********** require write ************ ')
    force_t[:,:] = force[:,:]
    interface.mark_action_fulfilled( cowic )
  
  pos = interface.read_block_vector_data( pos_id, node_id )
  vel = interface.read_block_vector_data( vel_id, node_id )
  # rot = interface.read_block_vector_data( rot_id, node_id )

  for i in np.arange(n):
    print(i,': ', pos[i,:],', ',vel[i,:])

  #> Compute force
  for i in np.arange(n):
    force[i,:] = .0
    force[i,2] = .01   #.5 * rho * Uinf * Uinf * cho[i] * cla * twi[i] * dy 
  
  print(' for_if :');print(  for_id )
  print(' node_id:');print( node_id )
  print(' force: '); print( force ) # ; time.sleep(2.0) 
  interface.write_block_vector_data( for_id, node_id, force )

  dt = min( dt_set, dt_precice )

  is_ongoing = interface.is_coupling_ongoing()
  if ( is_ongoing ):
    dt_precice = interface.advance(dt)
  else:
    break

  if ( interface.is_action_required( coric ) ): # dt not converged
    print(' *********** require read ************ ')
    force[:,:] = force_t[:,:]    # reload state
    interface.mark_action_fulfilled( coric )
  else: # dt converged
    t = t + dt


interface.finalize()
print(" Finalize auxiliary coupling ")

#> ~~~ PreCICE coupling between mbdyn and aero solver ~~~~~~~~~~~


