import sys
import os
import precice
from precice import *

#> ~~~ PreCICE-aux communication: start ~~~~~~~~~~~~~~~~~~~~~~~~~
#> Precice params: communication
comm_rank = 0; comm_size = 1
config_file_name = "./../pre.xml"
solver_name   = "fl_sol"
mesh_name     = "fl_mesh"
force_name = "force"
displ_name = "displ"
n = 5

print(" Configure preCICE-aux ...", end='')
interface = precice.Interface( solver_name, config_file_name, \
                               comm_rank, comm_size )
dimensions  = interface.get_dimensions(); nd = dimensions
mesh_id = interface.get_mesh_id( mesh_name )
print(" done. \n")

rr = np.zeros((n, nd))
for i in np.arange(n):    # todo with reshape
  rr[i,:] = np.array([ i, 0., 0. ]) # nodal.n_x[i*nd:(i+1)*nd]
node_id = interface.set_mesh_vertices( mesh_id, rr )

displ_id    = interface.get_data_id( displ_name, mesh_id )
force_id    = interface.get_data_id( force_name, mesh_id )
displ = np.zeros((n, nd))
force = np.zeros((n, nd))

dt_precice = interface.initialize()
is_ongoing = interface.is_coupling_ongoing()
print(" is_ongoing: ", is_ongoing)

while ( is_ongoing ):
  dt_precice = interface.advance(dt_precice)

interface.finalize()
print(" Finalize auxiliary coupling ")

#> ~~~ PreCICE-aux communication: end ~~~~~~~~~~~~~~~~~~~~~~~~~~~


