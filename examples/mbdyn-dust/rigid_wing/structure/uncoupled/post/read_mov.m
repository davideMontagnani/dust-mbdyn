
clear all; clc; close all

% filen = '../springmass.mov';
filen = '../output.mov';
dt = 1.e-3;

dat = dlmread(filen,'',0,0);

id= dat(:,1);

x  = dat(:,2); vx = dat(:, 8);
y  = dat(:,3); vy = dat(:, 9);
z  = dat(:,4); vz = dat(:,10);
tx = dat(:,5); ox = dat(:,11);
ty = dat(:,6); oy = dat(:,12);
tz = dat(:,7); oz = dat(:,13);
t = dt * [ 0:size(x,1)-1];

figure('Position',[100 100 1000 600])
subplot(2,2,1), plot(t,[ x, y, z], 'LineWidth', 2), grid on, legend( 'x', 'y', 'z')
subplot(2,2,2), plot(t,[vx,vy,vz], 'LineWidth', 2), grid on, legend('vx','vy','vz')
subplot(2,2,3), plot(t,[tx,ty,tz], 'LineWidth', 2), grid on, legend('tx','ty','tz')
subplot(2,2,4), plot(t,[ox,oy,oz], 'LineWidth', 2), grid on, legend('ox','oy','oz')
