
MBDyn - MultiBody Dynamics develop
configured on Mar 15 2020 at 16:12:55

Copyright 1996-2017 (C) Paolo Mantegazza and Pierangelo Masarati,
Dipartimento di Ingegneria Aerospaziale <http://www.aero.polimi.it/>
Politecnico di Milano                   <http://www.polimi.it/>

MBDyn is free software, covered by the GNU General Public License,
and you are welcome to change it and/or distribute copies of it
under certain conditions.  Use 'mbdyn --license' to see the conditions.
There is absolutely no warranty for MBDyn.  Use "mbdyn --warranty"
for details.

reading from file "springmass"
Creating scalar solver with Naive linear solver
Reading Structural(0)
Reading Structural(1)
Reading Joint(0)
Reading Joint(1)
Reading Joint(2)
Reading Joint(4)
Reading Body(1)
Reading Force(1)
Reading Force(2)
LOCAL connection to path="/tmp/.mbdyn_mxpt4f5s/mbdyn.sock"
End of simulation at time 10.001 after 10001 steps;
output in file "/home/davide/Software/DUST/dust-mbdyn/examples/mbdyn-dust/rigid_wing/structure/output"
total iterations: 90009
total Jacobian matrices: 20002
total error: 1.51778e-13

The simulation required 4.210 seconds of CPU time
MBDyn terminated normally
