# $Header: /var/cvs/mbdyn/mbdyn/mbdyn-1.0/tests/forces/strext/socket/springmass/springmass,v 1.8 2016/10/18 19:28:17 masarati Exp $
#
# MBDyn (C) is a multibody analysis code. 
# http://www.mbdyn.org
# 
# Copyright (C) 1996-2015
# 
# Pierangelo Masarati  <masarati@aero.polimi.it>
# Paolo Mantegazza  <mantegazza@aero.polimi.it>
# 
# Dipartimento di Ingegneria Aerospaziale - Politecnico di Milano
# via La Masa, 34 - 20156 Milano, Italy
# http://www.aero.polimi.it
# 
# Changing this copyright notice is forbidden.
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation (version 2 of the License).
# 
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# 
# Author: Pierangelo Masarati <masarati@aero.polimi.it>

begin: data;
  problem: initial value;
end: data;

begin: initial value;
  initial time: 0.;
  final time: 10.;
  time step: 1e-2;

  method: ms, .6;
  linear solver: naive, colamd;
  nonlinear solver: newton raphson, modified, 5;

  tolerance: 1e-9;
  max iterations: 50;

  derivatives coefficient: 1e-9;
  derivatives tolerance: 1e-9;
  derivatives max iterations: 20;

# output: iterations;
# output: residual;
end: initial value;

begin: control data;
  structural nodes:
    +1    # ground
    +1    # mass 1
  ;
  rigid bodies:
    +1    # mass 1
  ;
  joints:
    +1    # ground clamp
    +1    # spring for mass 1
    +1    # in line constraint for mass 1
    +1    # avoid rotation
  ;
  forces:
#   +1    # gravity
    +1    # external force
  ;
end: control data;

set: const integer GROUND = 0;
set: const integer MASS_1 = 1;

set: const real m = 1.0;
set: const real I = 1.0;

set: const real VZ = 0.0;
set: const real K = 1000.0; # (10.*2*pi)^2;
set: const real C =   10.0; # 2*0.1*10.*2*pi;

set: const real Ktheta = 1.e+9;
set: const real Ctheta = 1.e+9;

reference: GROUND,
  reference, global, null,
  reference, global, eye,
  reference, global, null,
  reference, global, null;

reference: MASS_1,
  reference, GROUND, null,
  reference, GROUND, eye,
  reference, GROUND, 0., 0., VZ,
  reference, GROUND, null;

begin: nodes;
  structural: GROUND, static,
    reference, GROUND, null,
    reference, GROUND, eye,
    reference, GROUND, null,
    reference, GROUND, null,
    output, no;

  structural: MASS_1, dynamic,
    reference, MASS_1, null,
    reference, MASS_1, eye,
    reference, MASS_1, null,
    reference, MASS_1, null;

end: nodes;

begin: elements;
  joint: 0, clamp, GROUND, node, node;

  joint: 1, deformable displacement joint,
    GROUND,
      position   , reference, GROUND, null,
      orientation, reference, GROUND, eye,
    MASS_1,
      position   , reference, MASS_1, null,
      orientation, reference, MASS_1, eye,
    linear viscoelastic isotropic, K, C,
    prestress, 0., 0., 0.;   # or 0., 0., -K*Z0-C*VZ0 ???

  joint: 2, in line,
    GROUND,
      position   , reference, GROUND, null,
      orientation, reference, GROUND,
        1, 1., 0., 0.,
        2, 0., 1., 0.,
    MASS_1;

  joint: 4, deformable hinge,
    GROUND,
      position   , reference, GROUND, null,
      orientation, reference, GROUND, eye,
    MASS_1,
      position   , reference, GROUND, null,
      orientation, reference, GROUND, eye,
	linear viscoelastic generic,
        Ktheta, 0., 0.,
        0., Ktheta, 0.,
        0., 0., Ktheta,
        Ctheta, 0., 0.,
        0., Ctheta, 0.,
        0., 0., Ctheta ;

  body: MASS_1, MASS_1,
    m,
    null,
    diag, I, I, I;

  force: 1, external structural,
    socket,
      create, yes,
      path, "$MBSOCK",
      no signal,
    coupling, tight, # loose,
    labels, no,
    sorted, yes, # yes,
    orientation, orientation vector,
    accelerations, yes,
    # orientation, orientation matrix, # default
    # orientation, euler 123,
    1,
      MASS_1;

end: elements;

# vim:ft=mbd

