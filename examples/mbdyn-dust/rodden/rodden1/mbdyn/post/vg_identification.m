
clear all; clc; close all;

Uv = [ 0:5:45, 47.5, 50, 51, 52.5];
filen_root = './../res_mbdyn/xcg+0.45_v+';
filen_ext  = 'ms.mov';

dt = 1.e-4; t0 = 0.; tend = 30.; t = t0:dt:tend;
ts_in = 1; ts_fin = 300000;

scMat = [];

figure; hold on

for j = 1 : length(Uv)
 
  filen = [ filen_root, num2str(Uv(j),'%2.1f'), filen_ext ];

  w = dlmread(filen,'',0,0);
  w = w(:,[3,7]);
  
  ts = t(ts_in:ts_fin);
  ws = w(ts_in:ts_fin,:);
  
  %> Set dummy small input 
  u = 1.0e-8 * ( 0.5 - rand(size(ws,1),1) );
  
  %> Identify the discrete-time system
  id_dat = iddata( ws, u, dt );
  % [ sysd, x0 ] = arx( id_dat, 4 );
  [ sysd, x0 ] = arx( id_dat, 'na', 8, 'nb', 1, 'nk', 1 );
  % [ sysd_tf  ] = arx( id_dat, 'na', 8, 'nb', 1, 'nk', 1 );
  
  %> Eigenvalues of the discrete-time A matrix
  sd = eig( sysd.a );
  
  %> Eigenvalues of the continuous-time A matrix
  sc = ( log( abs(sd) ) + i*arg(sd) ) / dt;
  
  plot(real(sc),imag(sc),'o','LineWidth',2), axis equal, grid on

  scMat = [ scMat, sc ];

end

% Frequency and damping
omMat = zeros(size(scMat));
gMat  = zeros(size(scMat));
for i2 = 1 : size(scMat,2)
  for i1 = 1 : size(scMat,1)

    if ( imag(scMat(i1,i2)) == 0.0 )
      omMat(i1,i2) = 0.0;
    else
      omMat(i1,i2) = abs(scMat(i1,i2));
    end

    gMat(i1,i2) = -2.0*real(scMat(i1,i2))/abs(scMat(i1,i2));

  end
end

figure;
subplot(2,1,1), plot(Uv, omMat,'o','LineWidth',2), grid on
subplot(2,1,2), plot(Uv,  gMat,'o','LineWidth',2), grid on


