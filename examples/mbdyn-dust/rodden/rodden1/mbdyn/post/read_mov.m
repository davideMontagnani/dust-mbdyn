
clear all; clc; close all

% filen = '../springmass.mov';
filen = { ...
          '../res_mbdyn/xcg+0.45_v+51.0ms.mov', ...
        };
dt = 1.e-4;

figure('Position',[100 100 1000 600])
for i = 1 : length(filen)

  dat = dlmread(filen{i},'',0,0);
  
  id= dat(:,1);
  
  x  = dat(:,2); vx = dat(:, 8);
  y  = dat(:,3); vy = dat(:, 9);
  z  = dat(:,4); vz = dat(:,10);
  tx = dat(:,5); ox = dat(:,11);
  ty = dat(:,6); oy = dat(:,12);
  tz = dat(:,7); oz = dat(:,13);
  t = dt * [ 0:size(x,1)-1];
  
  subplot(2,2,1), plot(t,[ x, y, z], 'LineWidth', 2), grid on, title('pos'), hold on
  subplot(2,2,2), plot(t,[vx,vy,vz], 'LineWidth', 2), grid on, title('vel'), hold on
  subplot(2,2,3), plot(t,[tx,ty,tz], 'LineWidth', 2), grid on, title('rot'), hold on
  subplot(2,2,4), plot(t,[ox,oy,oz], 'LineWidth', 2), grid on, title('ome'), hold on
end
