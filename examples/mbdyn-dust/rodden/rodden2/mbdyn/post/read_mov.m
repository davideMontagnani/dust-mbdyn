
clear all; clc; close all

% filen = '../springmass.mov';
filen = '../Rodden2.mov';
dt = 1.e-3;

dat = dlmread(filen,'',0,0);

id= dat(3:3:end,1);

x  = dat(3:3:end,2); vx = dat(3:3:end, 8);
y  = dat(3:3:end,3); vy = dat(3:3:end, 9);
z  = dat(3:3:end,4); vz = dat(3:3:end,10);
tx = dat(3:3:end,5); ox = dat(3:3:end,11);
ty = dat(3:3:end,6); oy = dat(3:3:end,12);
tz = dat(3:3:end,7); oz = dat(3:3:end,13);
t = dt * [ 0:size(x,1)-1];

figure('Position',[100 100 1000 600])
subplot(2,2,1), plot(t,[ x, y, z], 'LineWidth', 2), grid on, legend( 'x', 'y', 'z')
subplot(2,2,2), plot(t,[vx,vy,vz], 'LineWidth', 2), grid on, legend('vx','vy','vz')
subplot(2,2,3), plot(t,[tx,ty,tz], 'LineWidth', 2), grid on, legend('tx','ty','tz')
subplot(2,2,4), plot(t,[ox,oy,oz], 'LineWidth', 2), grid on, legend('ox','oy','oz')
