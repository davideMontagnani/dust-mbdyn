# #################################################################
#
# Title         Unrestrained Aeroelastic Divergence in a Dynamic Stability Analysis
# Type          Journal Article
# Author        William P. Rodden and E. Dean Bellinger
# URL           https://doi.org/10.2514/3.61559
# Volume        19
# Issue         9
# Pages         796-797
# Publication   Journal of Aircraft
# Date          1982
# Publisher     American Institute of Aeronautics and Astronautics
# DOI           10.2514/3.61559
#
# #################################################################
#
# Test case AH145A from NASTRAN's aeroelasticity user manual
#
# #################################################################

# distances taken from leading egde
set: const real m = 19.624421; # 1.3447 slug
set: const real chord = 1.8288; # 6 ft
set: const real x_EA = 0.4 * chord;
set: const real rho_EA = 0.4572; # 1.5 ft
set: const real J_EA = m * rho_EA * rho_EA;
set: const real x_AE = 0.25 * chord;
set: const real omega_h = 10; # rad / s
set: const real omega_theta = 25; # rad / s
set: const real damp_coeff = 0.03;

# 37 % chord mass center
#set: const real x_CG = 0.37 * chord;
# 45 % chord mass center
set: const real x_CG = 0.35 * chord;

set: const real J = J_EA - m * (x_EA - x_CG)^2;
set: const real Kh = omega_h^2 * m;
set: const real Ktheta = omega_theta^2 * J_EA;
set: const real Ch = Kh * damp_coeff / omega_h;
set: const real Ctheta = Ktheta * damp_coeff / omega_theta;

set: const real ALPHA_0 = 0.0000001*deg2rad;
set: const real Voo = 58.;

begin: data;
	problem: initial value;
end: data;

begin: initial value;
	initial time: 0.;
	final time: 100.;
	time step: 1.e-3;

	method: ms, .6;
	tolerance: 1.e-3;
	max iterations: 10;

	derivatives coefficient: 1.e-6;
	derivatives tolerance: 1.e+4;
	derivatives max iterations: 20;

	# nonlinear solver: newton raphson, modified, 4;
	# solver: umfpack, cc, block size, 2;
	# linear solver: naive, colamd, mt, 1, pivot factor, 1e-6;
	linear solver: umfpack, cc;

	# output: iterations;
	#output: residual;
	
	threads: disable;
end: initial value;

begin: control data;
	structural nodes:
		+1		# ground
		+1		# fuselage
		+1		# airfoil
	;
	rigid bodies:
		+1		# fuselage
		+1		# airfoil
	;
	joints:
		+1		# ground
		+1		# in line
		+1		# prismatic
		+1		# in line
		+1		# revolute rotation
		+1		# linear spring (rod)
		+1		# angular spring (deformable hinge)
	;
	air properties;
	aerodynamic elements:
		+1		# airfoil
	;
end: control data;

reference: 0,
	reference, global, null,
	reference, global, eye,
	reference, global, null,
	reference, global, null;
reference: 1,
	reference, 0, 0., -1, 0.,
	reference, 0,
		1, cos(ALPHA_0), sin(ALPHA_0), 0.,
		3, 0., 0., 1.,
	reference, 0, null,
	reference, 0, null;

begin: nodes;
	structural: 0, static,
		reference, 0, null,
		reference, 0, eye,
		reference, 0, null,
		reference, 0, null;
	structural: 1, dynamic,
		reference, 0, null,
		reference, 0, eye,
		reference, 0, null,
		reference, 0, null;
	structural: 2, dynamic,
		reference, 1, null,
		reference, 1, eye,
		reference, 1, null,
		reference, 1, null;
end: nodes;

begin: elements;
	joint: 0, clamp, 0, node, node;

	joint: 1, in line,
		0,
			position, reference, 0, null,
			orientation, reference, 0, 
				1, 1., 0., 0.,
				2, 0., 0., 1,
		1
	;
	joint: 2, prismatic,
		0,
			orientation, reference, 0, eye, 
		1,
			orientation, reference, 0, eye
	;
	joint: 3, in line,
		0,
			position, reference, 0, null,
			orientation, reference, 0, 
				1, 1., 0., 0.,
				2, 0., 0., 1,
		2
	;
	joint: 4, revolute rotation,
		0,
			position, reference, 0, null,
			orientation, reference, 0, 
				1, 1., 0., 0.,
				2, 0., 1., 0.,
		2,
			position, reference, 0, null,
			orientation, reference,  0, 
				1, 1., 0., 0.,
				2, 0., 1., 0.
	;
	joint: 5, rod,
		1,
			position, reference, node, null,
		2,
			position, reference, node, null,
		1.,
		linear viscoelastic, Kh, Ch
	;
	joint: 6, deformable hinge,
		0,
			position, reference, 0, null,
			orientation, reference, 0, eye,
		2,
			position, reference, 1, null,
			orientation, reference, 1, eye,
		linear viscoelastic generic,
			Ktheta, 0., 0.,
			0., Ktheta, 0.,
			0., 0., Ktheta,
			Ctheta, 0., 0.,
			0., Ctheta, 0.,
			0., 0., Ctheta
	;
	
	body: 1, 1,
		m,
		reference, node, null,
		diag, J, J, J;

	body: 2, 2,
		m,
		reference, node, x_EA - x_CG, 0., 0.,
		diag, J, J, J;

	air properties: std, SI,
		-1,0.,0., const, Voo;

	set: const integer NACA0012 = 12;
	c81data: NACA0012, "naca0012.c81";

	aerodynamic body: 1, 2,
		reference, node, null,
		reference, node, eye,
		1,
		const, chord,
		const, x_EA - 0.31 * chord,
		const, x_EA,# - 0.31000001 * chord,
		const, 0.,
		1,
		theodorsen, c81, NACA0012,	  
		#unsteady,
		#bielawa,
		jacobian, yes;

end: elements;
