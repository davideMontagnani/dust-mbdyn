# blade: spherical hinge, to allow flap, lead-lag and feathering motion
joint: CURR_BLADE + FLAP_HINGE, total joint,
	HUB,
		position, reference, CURR_BLADE + FLAP_HINGE, null,
		position orientation, reference, CURR_BLADE + FLAP_HINGE, eye,
		rotation orientation, reference, CURR_BLADE + FLAP_HINGE, eye,
	CURR_BLADE,
		position, reference, CURR_BLADE + FLAP_HINGE, null,
		position orientation, reference, CURR_BLADE + FLAP_HINGE, eye,
		rotation orientation, reference, CURR_BLADE + FLAP_HINGE, eye,
	position constraint, 1, 1, 1, null,
	orientation constraint, 0, 0, 0, null;

# flap/lead-lag spring/damper
joint: CURR_BLADE + FLAP_HINGE + 1, deformable hinge,
	HUB,
		position, reference, CURR_BLADE + FLAP_HINGE, null,
		orientation, reference, CURR_BLADE + FLAP_HINGE, eye,
	CURR_BLADE,
		position, reference, CURR_BLADE + FLAP_HINGE, null,
		orientation, reference, CURR_BLADE + FLAP_HINGE, eye,
	linear viscoelastic generic,
		diag, 0., 0., K_DAMPER,
		diag, 0., 0., C_DAMPER;

# (rigid) pitch link
joint: CURR_BLADE + PITCH_LINK, distance,
	CURR_BLADE,
		position, reference, CURR_BLADE + FLAP_HINGE, 0., PITCH_HORN_LEN, 0.,
	ROTATING_PLATE,
		# could be better defined...
		position, reference, CURR_BLADE, FLAP_OFFSET, PITCH_HORN_LEN, -PITCH_LINK_LEN,
	PITCH_LINK_LEN;

/*
# (flexible) pitch link
joint: CURR_BLADE + PITCH_LINK, rod,
	CURR_BLADE,
		position, reference, CURR_BLADE + FLAP_HINGE, 0., PITCH_HORN_LEN, 0.,
	ROTATING_PLATE,
		# could be better defined...
		position, reference, CURR_BLADE, FLAP_OFFSET, PITCH_HORN_LEN, -PITCH_LINK_LEN,
	PITCH_LINK_LEN,
	linear viscoelastic, 1e6, 1e2;
*/

body: CURR_BLADE, CURR_BLADE,
	BLADE_MASS,
	reference, CURR_BLADE + BLADE_CM, null, # -(R - FLAP_OFFSET)/2, 0., 0.,
	diag, BLADE_Ix, BLADE_Iy, BLADE_Iz;

# vim:ft=mbd
