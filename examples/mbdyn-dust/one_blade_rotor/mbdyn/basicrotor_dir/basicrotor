# Multibody System Dynamics
# MBDyn training #2
# Introduction to multibody dynamics with MBDyn
# Rigid blade with flap degree of freedom

begin: data;
  integrator: initial value;
end: data;

begin: initial value;
  initial time: 0.;
  final time: 2.;
  time step: 1.e-3;  # time step must allow about 100 steps/rev

  max iterations: 10;
  tolerance: 1.e-5;

  method: ms, 0.6;

  # linear solver: naive;
  # output: residual;

  # eigenanalysis: .05, use lapack, balance, no;
end: initial value;

set: const integer NBLADES = 1;

begin: control data;
  structural nodes: 
    +1    # ground (clamped)
    +1    # hub (imposed angular velocity)
    +2    # fixed and rotating swashplates
    +NBLADES*(
      +1  # blade (hinged to hub, flap only)
    )
  ;
  rigid bodies:
    +NBLADES*(
      +1  # blade
    )
  ;
  joints:
    +1    # ground clamp
    +1    # imposed angular velocity between hub and ground
    +3    # swashplate joints
    +NBLADES*(
      +1  # flap, lag hinge between hub and blade
      +1  # blade flap/lag spring/damper
      +1  # pitch link
    )
  ;
  forces:
    +1  # perturbation
  ;
  # output precision: 16;
end: control data;

# airframe
set: const integer GROUND = 0;

# rotor
set: const integer HUB = 100;
set: const integer FIXED_PLATE = 110;
set: const integer ROTATING_PLATE = 120;

# blade labels
set: const integer BLADE_1 = 1000;
set: const integer BLADE_2 = 2000;
set: const integer BLADE_3 = 3000;
set: const integer BLADE_4 = 4000;
set: const integer BLADE_5 = 5000;

# offsets from CURR_BLADE
set: const integer FLAP_HINGE = 10;
set: const integer BLADE_CM = 20;
set: const integer PITCH_LINK = 30;

# temporary variables
set: integer CURR_BLADE;
set: real CURR_PSI;

# data
set: const real OMEGA = 40.84;    # radian/s, hub angular velocity
set: const real R = 5.5;    # m, rotor radius

set: const real FLAP_OFFSET = 0.04*R;  # m, flap hinge offset
set: const real BLADE_MASS = 50.;  # kg, blade mass
set: const real BLADE_Ix = 1.;    # kg m^2, blade inertia about blade axis
set: const real BLADE_Iy = BLADE_MASS/12*(R - FLAP_OFFSET)^2;
set: const real BLADE_Iz = BLADE_MASS/12*(R - FLAP_OFFSET)^2;

set: const real HUB_Z = .5;    # m
set: const real PITCH_LINK_LEN = .35;  # m
set: const real PITCH_HORN_LEN = .20;  # m

set: const real K_DAMPER = 0.; # present when damper is elastomeric
set: const real C_DAMPER = 0.; # compute to have desired damping ratio (e.g. 0.3)
# note: the stiffness may be non-zero and even with cross-couplings when elastomeric bearings are used

/*
                    P
                     o    ^
                      \   |
                      |   h = PITCH_HORN_LEN
                      |   |
    mast              |   v
     +---------------o+--------------------------- blade
    O              hinge

      e = FLAP_OFFSET          
     <--------------->
    |P-O| = sqrt(h^2 + e^2)
 */

# swashplate controls
set: const integer COLLECTIVE_PITCH = 997;
set: const integer CYCLIC_LATERAL = 998;
set: const integer CYCLIC_LONGITUDINAL = 999;

# change as needed (changes should be smooth!)
drive caller: COLLECTIVE_PITCH,
  # const, 0.; # rad
  cosine, 0., pi/.5, 10.*deg2rad/2, half, 0.;
drive caller: CYCLIC_LATERAL,
  # const, 0.; # rad
  cosine, 0., pi/.5, 10.*deg2rad/2, half, 0.;
drive caller: CYCLIC_LONGITUDINAL,
  # const, 0.; # rad
  cosine, 0., pi/.5, 10.*deg2rad/2, half, 0.;

/*

       ^ z, OMEGA
       |
       |   7 y
       |  /
       | /
       |/      flap hinge
       +---o---------------> x
     FLAP_OFFSET
                         R

                           /-----------------
   flap frequency         /    3 FLAP_OFFSET
   --------------  = -\  / 1 + - -----------
       OMEGA           \/      2      R

 */

reference: GROUND,
  null, 
  eye, 
  null, 
  null;

reference: HUB,
  reference, GROUND, 0., 0., HUB_Z,
  reference, GROUND, eye,
  reference, GROUND, null,
  reference, GROUND, 0., 0., OMEGA;

reference: FIXED_PLATE,
  reference, HUB, 0., 0., -PITCH_LINK_LEN,
  reference, GROUND, eye,
  reference, GROUND, null,
  reference, GROUND, null;

reference: ROTATING_PLATE,
  reference, FIXED_PLATE, null,
  reference, HUB, eye,
  reference, HUB, null,
  reference, HUB, null;

set: CURR_BLADE = BLADE_1;
# set: CURR_PSI = (CURR_BLADE/BLADE_1)*2*pi/NBLADES; # Johnson's definition
set: CURR_PSI = -(CURR_BLADE/BLADE_1 - 1)*2*pi/NBLADES; # "logical" definition

reference: CURR_BLADE,
  reference, HUB, null,
  reference, HUB,
    3, 0., 0., 1.,
    1, cos(CURR_PSI), sin(CURR_PSI), 0.,
  reference, HUB, null,
  reference, HUB, null;

reference: CURR_BLADE + FLAP_HINGE,
  reference, CURR_BLADE, FLAP_OFFSET, 0., 0.,
  reference, CURR_BLADE, eye,
  reference, CURR_BLADE, null,
  reference, CURR_BLADE, null;

/* axis 1: blade axis in blade's reference frame
 * axis 2: chordwise direction in blade's reference frame
 * axis 3: out-of-plane direction in blade's reference frame
 */
reference: CURR_BLADE + BLADE_CM,
  reference, CURR_BLADE + FLAP_HINGE, (R - FLAP_OFFSET)/2., 0., 0.,
  reference, CURR_BLADE + FLAP_HINGE, eye,
  reference, CURR_BLADE + FLAP_HINGE, null,
  reference, CURR_BLADE + FLAP_HINGE, null;

begin: nodes;
  structural: GROUND, static,
    reference, GROUND, null, 
    reference, GROUND, eye, 
    reference, GROUND, null, 
    reference, GROUND, null;

  structural: HUB, static,
    reference, HUB, null, 
    reference, HUB, eye, 
    reference, HUB, null, 
    reference, HUB, null;

  structural: FIXED_PLATE, static,
    reference, FIXED_PLATE, null,
    reference, FIXED_PLATE, eye,
    reference, FIXED_PLATE, null,
    reference, FIXED_PLATE, null;

  structural: ROTATING_PLATE, static,
    reference, ROTATING_PLATE, null,
    reference, ROTATING_PLATE, eye,
    reference, ROTATING_PLATE, null,
    reference, ROTATING_PLATE, null;

  set: CURR_BLADE = BLADE_1;

  structural: CURR_BLADE, dynamic,
    reference, CURR_BLADE + BLADE_CM, null, 
    reference, CURR_BLADE + BLADE_CM, eye, 
    reference, CURR_BLADE + BLADE_CM, null, 
    reference, CURR_BLADE + BLADE_CM, null;
end: nodes;

begin: elements;
  # grounds airframe node
  joint: GROUND, clamp, GROUND, node, node;

  # prescribes rotation of hub node
  joint: HUB, total joint,
    GROUND,
      position, reference, HUB, null,
      position orientation, reference, GROUND, eye,
      rotation orientation, reference, GROUND, eye,
    HUB,
      position, reference, HUB, null,
      position orientation, reference, HUB, eye,
      rotation orientation, reference, HUB, eye,
    position constraint, 1, 1, 1, null,
    orientation constraint, 1, 1, 1,
      0., 0., 1., linear, 0., OMEGA; # c_0 + c_1 * t
    #orientation constraint, 1, 1, angular velocity,
    #  0., 0., 1., const, OMEGA;

  ### swashplate

  # displacement of plate for unit collective angle
  set: const real COLLECTIVE_RATIO = PITCH_HORN_LEN; # m/rad
  # rotation of plate for unit cyclic angle
  set: const real CYCLIC_RATIO = PITCH_HORN_LEN/sqrt(PITCH_HORN_LEN^2 + FLAP_OFFSET^2); # rad/rad

  # constrains fixed plate to airframe, prescribing requested motion
  joint: FIXED_PLATE, total joint,
    GROUND,
      position, reference, FIXED_PLATE, null,
      position orientation, reference, FIXED_PLATE, eye,
      rotation orientation, reference, FIXED_PLATE, eye,
    FIXED_PLATE,
      position, reference, FIXED_PLATE, null,
      position orientation, reference, FIXED_PLATE, eye,
      rotation orientation, reference, FIXED_PLATE, eye,
    position constraint, 1, 1, 1,
      0., 0., COLLECTIVE_RATIO,
        reference, COLLECTIVE_PITCH,
    orientation constraint, 1, 1, 1,
      component,
        mult, CYCLIC_RATIO, reference, CYCLIC_LATERAL,
        mult, CYCLIC_RATIO, reference, CYCLIC_LONGITUDINAL,
        inactive;

  # constrains rotating plate to fixed plate, allowing relative revolution only
  joint: ROTATING_PLATE + 1, total joint,
    FIXED_PLATE,
      position, reference, FIXED_PLATE, null,
      position orientation, reference, FIXED_PLATE, eye,
      rotation orientation, reference, FIXED_PLATE, eye,
    ROTATING_PLATE,
      position, reference, ROTATING_PLATE, null,
      position orientation, reference, ROTATING_PLATE, eye,
      rotation orientation, reference, ROTATING_PLATE, eye,
    position constraint, 1, 1, 1, null,
    orientation constraint, 1, 1, 0, null;

  # constrains rotating plate to hub, to keep its rotation in sync
  joint: ROTATING_PLATE, total joint,
    ROTATING_PLATE,
      position, reference, ROTATING_PLATE, null,
      position orientation, reference, ROTATING_PLATE, eye,
      rotation orientation, reference, ROTATING_PLATE, eye,
    HUB,
      position, reference, ROTATING_PLATE, null,
      position orientation, reference, ROTATING_PLATE, eye,
      rotation orientation, reference, ROTATING_PLATE, eye,
    position constraint, 0, 0, 0, null,
    orientation constraint, 0, 0, 1, null;

  ### blade elements

  set: CURR_BLADE = BLADE_1;

  # blade: spherical hinge, to allow flap, lead-lag and feathering motion
  joint: CURR_BLADE + FLAP_HINGE, total joint,
    HUB,
      position, reference, CURR_BLADE + FLAP_HINGE, null,
      position orientation, reference, CURR_BLADE + FLAP_HINGE, eye,
      rotation orientation, reference, CURR_BLADE + FLAP_HINGE, eye,
    CURR_BLADE,
      position, reference, CURR_BLADE + FLAP_HINGE, null,
      position orientation, reference, CURR_BLADE + FLAP_HINGE, eye,
      rotation orientation, reference, CURR_BLADE + FLAP_HINGE, eye,
    position constraint, 1, 1, 1, null,
    orientation constraint, 0, 0, 0, null;

  # flap/lead-lag spring/damper
  joint: CURR_BLADE + FLAP_HINGE + 1, deformable hinge,
    HUB,
      position, reference, CURR_BLADE + FLAP_HINGE, null,
      orientation, reference, CURR_BLADE + FLAP_HINGE, eye,
    CURR_BLADE,
      position, reference, CURR_BLADE + FLAP_HINGE, null,
      orientation, reference, CURR_BLADE + FLAP_HINGE, eye,
    linear viscoelastic generic,
      diag, 0., 0., K_DAMPER,
      diag, 0., 0., C_DAMPER;

  # (rigid) pitch link
  joint: CURR_BLADE + PITCH_LINK, distance,
    CURR_BLADE,
      position, reference, CURR_BLADE + FLAP_HINGE, 0., PITCH_HORN_LEN, 0.,
    ROTATING_PLATE,
      # could be better defined...
      position, reference, CURR_BLADE, FLAP_OFFSET, PITCH_HORN_LEN, -PITCH_LINK_LEN,
    PITCH_LINK_LEN;

  /*
  # (flexible) pitch link
  joint: CURR_BLADE + PITCH_LINK, rod,
    CURR_BLADE,
      position, reference, CURR_BLADE + FLAP_HINGE, 0., PITCH_HORN_LEN, 0.,
    ROTATING_PLATE,
      # could be better defined...
      position, reference, CURR_BLADE, FLAP_OFFSET, PITCH_HORN_LEN, -PITCH_LINK_LEN,
    PITCH_LINK_LEN,
    linear viscoelastic, 1e6, 1e2;
  */

  body: CURR_BLADE, CURR_BLADE,
    BLADE_MASS,
    reference, CURR_BLADE + BLADE_CM, null, # -(R - FLAP_OFFSET)/2, 0., 0.,
    diag, BLADE_Ix, BLADE_Iy, BLADE_Iz;

  force: BLADE_1, absolute,
    BLADE_1,
    position, reference, BLADE_1 + BLADE_CM, (R - FLAP_OFFSET)/2, 0., 0., # at the tip!
    0., 0., 1.,
      const, 0.;
      # sine, .1, pi/.01, 100., one, 0.;
end: elements;

# vim:ft=mbd
