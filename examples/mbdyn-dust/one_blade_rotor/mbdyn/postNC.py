import sys
import argparse
import netCDF4 as nc
import matplotlib.pyplot as plt

import sys,getopt
from netCDF4 import Dataset
from scipy.io import netcdf
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import os
from matplotlib.colors import LinearSegmentedColormap
import matplotlib.axes as maxes
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import rc
import warnings
#=============================================================

parser = argparse.ArgumentParser(\
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description='Plot variables in MBDyn output using Matplotlib.',\
        epilog='Multiple --var arguments can be provided.\n' + \
                'Components should be indicated as reported here:\n' + \

'------------------------------------------------------------------------------------------------\n' + \
'|	-x	: x-position									|\n' + \
'|	-y	: y-position									|\n' + \
'|	-z	: z-position									|\n' + \
'|	-ap	: x,y,z position components in 3 subplots collected in one plot			|\n' + \
'|	-u	: x-velocity									|\n' + \
'|	-v	: y-velocity									|\n' + \
'|	-x	: z-velocity									|\n' + \
'|	-av	: x,y,z velocity components in 3 subplots collected in one plot			|\n' + \
'|	-ex	: 1-Euler-like angle, (following the 1,2,3 convention)				|\n' + \
'|	-ey	: 2-Euler-like angle								|\n' + \
'|	-ez	: 2-Euler-like angle								|\n' + \
'|	-ae	: 1,2,3 Euler angles								|\n' + \
'|	-omx	: x-angular velocity								|\n' + \
'|	-omy	: y-angular velocity								|\n' + \
'|	-omz	: z-angular velocity								|\n' + \
'|	-aom	: x,y,z angular velocity components, 3 subplots collected in one plot		|\n' + \
'|	-apv	: x,y,z position and velocity components, 6 subplots collected in one plot	|\n' + \
'|	-tr	: 3D plot of the selected nodes trajectory					|\n' + \
'|	-fx	: x-force component								|\n' + \
'|	-fy	: x-force component								|\n' + \
'|	-fz	: x-force component								|\n' + \
'|												|\n' + \
'------------------------------------------------------------------------------------------------ '
                '\n'+\
                'e.g..\n' + \
                'to plot the x-velocity component and trajectory of the nodes 1,2,3 from output.nc:\n' + \

                '   >>python postNC.py output.nc -n1 -n2 -n3 -u -tr'
)

parser.add_argument('ncfile', metavar='ncfile', help='MBDyn NetCDF output file')
#parser.add_argument('--time', '-t', action='store_true',\
#        help='use simulation time on X axis')
parser.add_argument('--var', '-n', nargs=1, \
        help='variable[s] to be plotted', action='append', required=True)
parser.add_argument('--Fvar', '-f', nargs=1, \
        help='force[s] to be plotted', action='append', required=False)

parser.add_argument('--trajectory', '-tr', action='store_true',\
        help='for trajectory plotting')
parser.add_argument('--allposition', '-ap', action='store_true',\
        help='to plot all position components')
parser.add_argument('--allvelocity', '-av', action='store_true',\
        help='to plot all velocity components')
parser.add_argument('--allpositionvelocity', '-apv', action='store_true',\
        help='to plot all position and velocity components')
parser.add_argument('--xposition', '-x', action='store_true',\
        help='to plot x position component')
parser.add_argument('--yposition', '-y', action='store_true',\
        help='to plot y position component')
parser.add_argument('--zposition', '-z', action='store_true',\
        help='to plot z position component')
parser.add_argument('--xvelocity', '-u', action='store_true',\
        help='to plot x position component')
parser.add_argument('--yvelocity', '-v', action='store_true',\
        help='to plot y position component')
parser.add_argument('--zvelocity', '-w', action='store_true',\
        help='to plot z position component')

parser.add_argument('--xeuler', '-ex', action='store_true',\
        help='to plot x euler component')
parser.add_argument('--xangularvelocity', '-omx', action='store_true',\
        help='to plot x angular velocity component')
parser.add_argument('--yeuler', '-ey', action='store_true',\
        help='to plot y euler component')
parser.add_argument('--yangularvelocity', '-omy', action='store_true',\
        help='to plot y angular velocity component')
parser.add_argument('--zeuler', '-ez', action='store_true',\
        help='to plot x euler component')
parser.add_argument('--zangularvelocity', '-omz', action='store_true',\
        help='to plot z angular velocity component')
parser.add_argument('--alleuler', '-ae', action='store_true',\
        help='to plot all orientation components')
parser.add_argument('--allangularvelocity', '-aom', action='store_true',\
        help='to plot all angular velocity components')

parser.add_argument('--xforce', '-fx', action='store_true',\
        help='to plot x force component')
parser.add_argument('--yforce', '-fy', action='store_true',\
        help='to plot y force component')
parser.add_argument('--zforce', '-fz', action='store_true',\
        help='to plot z force component')

args = parser.parse_args()
#=============================================================

#--------------------------------------------------------------------------
#++++++++++++++++             DATA to PLOT          +++++++++++++++++++++++
#--------------------------------------------------------------------------

name_output = args.ncfile;
node_label = args.var;
force_label = args.Fvar;
nd = nc.Dataset(args.ncfile, 'r')
#print(node_label)
print('==================================================')
print('Starting data visualisation of ------->> '+name_output)
print('==================================================')
print('Number of nodes considered: ---------->>  '+str(len(node_label)))
print('==================================================')
warnings.filterwarnings("ignore")

#--------------------------------------------------------------------------
#++++++++++++      PLOTING PARAMTERS        +++++++++++#
#--------------------------------------------------------------------------
# set parameters for plotting (Interpreter, size, ...)
#import plotting_param
#plotting_param.plot_param()

#+++++++++      LINES        ++++++++#
#++++++++++++++++++++++++++++++++++++++
mpl.rcParams['lines.linewidth'] = 1
mpl.rcParams['lines.markersize'] = 4
mpl.rcParams['markers.fillstyle'] = "none"  ## {full, left, right, bottom, top, none}
mpl.rcParams['xtick.labelsize'] = 10
mpl.rcParams['ytick.labelsize'] = 10
mpl.rcParams['xtick.major.size'] = 3.5     ## major tick size in points
mpl.rcParams['xtick.minor.size'] = 2       ## minor tick size in points

#+++++++++      AXES        ++++++++#
#++++++++++++++++++++++++++++++++++++++
mpl.rcParams['axes.titlesize'] = 12         ## fontsize of the axes title
mpl.rcParams['axes.labelsize'] = 10         ## fontsize of the x any y labels
mpl.rcParams['axes.grid'] = True            ## display grid or not
mpl.rcParams['axes3d.grid'] = True          ## display grid or not for 3Dplot
mpl.rcParams['axes.grid.axis'] = "both"     ## which axis the grid should apply to
mpl.rcParams['axes.grid.which'] = "both"    ## gridlines at {major, minor, both} ticks
mpl.rcParams['axes.facecolor'] = "white"    ## axes background color
mpl.rcParams['axes.edgecolor'] = "black"    ## axes edge color
#mpl.rcParams['legend.loc'] = 'upper right'


#+++++++++      GRID        ++++++++#
#++++++++++++++++++++++++++++++++++++++
mpl.rcParams['grid.color'] = "black"         ## grid color
mpl.rcParams['grid.linestyle'] = "-"         ## solid
mpl.rcParams['grid.linewidth'] = 0.6         ## in points
mpl.rcParams['grid.alpha'] = .5              ## transparency, between 0.0 and 1.0

#+++++++++      FIGURE        ++++++++#
#++++++++++++++++++++++++++++++++++++++
## The figure subplot parameters.  All dimensions are a fraction of the figure width and height.
mpl.rcParams['figure.autolayout'] = False
mpl.rcParams['figure.subplot.left'] = 0.125  ## the left side of the subplots of the figure
mpl.rcParams['figure.subplot.right'] = 0.9    ## the right side of the subplots of the figure
mpl.rcParams['figure.subplot.bottom'] = 0.11   ## the bottom of the subplots of the figure
mpl.rcParams['figure.subplot.top'] = 0.88   ## the top of the subplots of the figure
mpl.rcParams['figure.subplot.wspace'] = 0.4    ## the amount of width reserved for space between subplots,
                                ## expressed as a fraction of the average axis width
mpl.rcParams['figure.subplot.hspace'] = 0.2    ## the amount of height reserved for space between subplots,
                                ## expressed as a fraction of the average axis height

#+++++++++      FONT        ++++++++#
#++++++++++++++++++++++++++++++++++++++
mpl.rcParams['font.family'] = 'serif'
    ## The font.family property has five values:
    ##     - 'serif' (e.g., Times),
    ##     - 'sans-serif' (e.g., Helvetica),
    ##     - 'cursive' (e.g., Zapf-Chancery),
    ##     - 'fantasy' (e.g., Western), and
    ##     - 'monospace' (e.g., Courier).


#-------------------------------------------------------------------
# definition for obtain axis equal for 3D plot (function not yet present for 3D case)
def axisEqual3D(ax):
    extents = np.array([getattr(ax, 'get_{}lim'.format(dim))() for dim in 'xyz'])
    sz = extents[:,1] - extents[:,0]
    centers = np.mean(extents, axis=1)
    maxsize = max(abs(sz))
    r = maxsize/2
    for ctr, dim in zip(centers, 'xyz'):
        getattr(ax, 'set_{}lim'.format(dim))(ctr - r, ctr + r)

#-------------------------------------------------------------------


time = nd.variables['time']
t = time[:]
#====================================================================
#inizialize the nodes positions
x = np.zeros((len(node_label) , len(t)))
y = np.zeros((len(node_label) , len(t)))
z = np.zeros((len(node_label) , len(t)))
#inizialize the velocity of nodes
u = np.zeros((len(node_label) , len(t)))
v = np.zeros((len(node_label) , len(t)))
w = np.zeros((len(node_label) , len(t)))
#inizialize the nodes orientation
ex = np.zeros((len(node_label) , len(t)))
ey = np.zeros((len(node_label) , len(t)))
ez = np.zeros((len(node_label) , len(t)))
#inizialize the angular velocity
om_x = np.zeros((len(node_label) , len(t)))
om_y = np.zeros((len(node_label) , len(t)))
om_z = np.zeros((len(node_label) , len(t)))
#inizialize forces
f_x = np.zeros((len(node_label) , len(t)))
f_y = np.zeros((len(node_label) , len(t)))
f_z = np.zeros((len(node_label) , len(t)))

#====================================================================

for N in np.arange(0, len(node_label), 1):
    #-----  position  -----#
    X_N = nd.variables['node.struct.'+str(args.var[N][0])+'.X']
    x[N,:] = X_N[:,0]
    y[N,:] = X_N[:,1]
    z[N,:] = X_N[:,2]
    #-----  velocity  -----#
    XP_N = nd.variables['node.struct.'+str(args.var[N][0])+'.XP']
    u[N,:] = XP_N[:,0]
    v[N,:] = XP_N[:,1]
    w[N,:] = XP_N[:,2]
    #-----  orientation  -----#
    #E_N = nd.variables['node.struct.'+str(args.var[N][0])+'.E']
    #ex[N,:] = E_N[:,0]
    #ey[N,:] = E_N[:,1]
    #ez[N,:] = E_N[:,2]
    #-----  angular velocity  -----#
    Om_N = nd.variables['node.struct.'+str(args.var[N][0])+'.Omega']
    om_x[N,:] = Om_N[:,0]
    om_y[N,:] = Om_N[:,1]
    om_z[N,:] = Om_N[:,2]

#------------------------------------------------------------------------------------
# x position   
if args.xposition:
 for N in np.arange(0, len(node_label), 1):   
     plt.figure(1)
     plt.suptitle('x-position')
     if N == 0:
      plt.ylabel('x [m]'), plt.xlabel('time [sec]')
     ax = plt.plot(t, x[N,:], label='node'+str(node_label[N]))
 plt.legend()
#------------------------------------------------------------------------------------
# y position 
if args.yposition:
 for N in np.arange(0, len(node_label), 1):     

     plt.figure(2)
     plt.suptitle('y-position')
     if N == 0:
      plt.ylabel('y [m]'), plt.xlabel('time [sec]')
     plt.plot(t, y[N,:], label='node'+str(node_label[N]))
 plt.legend() 
#------------------------------------------------------------------------------------
# z position     
if args.zposition:
 for N in np.arange(0, len(node_label), 1):
     plt.figure(3)
     plt.suptitle('z-position')
     if N == 0:
      plt.ylabel('z [m]'), plt.xlabel('time [sec]')
     plt.plot(t, z[N,:], label='node'+str(node_label[N]))
 plt.legend() 
#------------------------------------------------------------------------------------
# all position
if args.allposition:
 for N in np.arange(0, len(node_label), 1):
     plt.figure(18)
     plt.subplot(1, 3, 1)
     plt.suptitle('position')
     plt.plot(t, x[N,:]), plt.ylabel('x [m]'), plt.xlabel('time [sec]')
     plt.subplot(1, 3, 2)
     plt.plot(t, y[N,:]), plt.ylabel('y [m]'), plt.xlabel('time [sec]')
     plt.subplot(1, 3, 3)
     plt.plot(t, z[N,:], label='node'+str(node_label[N]))
     plt.ylabel('z [m]'), plt.xlabel('time [sec]')
 plt.legend(bbox_to_anchor=(1.05, 1.0), loc='upper left')

#------------------------------------------------------------------------------------
# x velocity
if args.xvelocity:
 for N in np.arange(0, len(node_label), 1):
     plt.figure(5)
     plt.suptitle('x-velocity')
     if N == 0:
      plt.ylabel('u [m/sec]'), plt.xlabel('time [sec]')
     plt.plot(t, x[N,:], label='node'+str(node_label[N]))
 plt.legend()
#------------------------------------------------------------------------------------
# y velocity
if args.yvelocity:
 for N in np.arange(0, len(node_label), 1):
     plt.figure(6)
     plt.suptitle('y-velocity')
     if N == 0:
      plt.ylabel('v [m/sec]'), plt.xlabel('time [sec]')
     plt.plot(t, y[N,:], label='node'+str(node_label[N]))
 plt.legend()
#------------------------------------------------------------------------------------
# z velocity
if args.zvelocity:
 for N in np.arange(0, len(node_label), 1):
     plt.figure(7)
     plt.suptitle('z-velocity')
     if N == 0:
      plt.ylabel('w [m/sec]'), plt.xlabel('time [sec]')
     plt.plot(t, z[N,:], label='node'+str(node_label[N]))
 plt.legend()
#------------------------------------------------------------------------------------
# all velocity
if args.allvelocity:
 for N in np.arange(0, len(node_label), 1):
     plt.figure(8)
     plt.subplot(1, 3, 1)
     plt.suptitle('velocity')
     plt.plot(t, u[N,:]), plt.ylabel('u [m/s]'), plt.xlabel('time [sec]')
     plt.subplot(1, 3, 2)
     plt.plot(t, v[N,:]), plt.ylabel('v [m/s]'), plt.xlabel('time [sec]')
     plt.subplot(1, 3, 3)
     plt.plot(t, w[N,:], label='node'+str(node_label[N]))
     plt.ylabel('w [m/s]'), plt.xlabel('time [sec]')
 plt.legend(bbox_to_anchor=(1.05, 1.0), loc='upper left')
#------------------------------------------------------------------------------------
# first rotation
if args.xeuler:
 for N in np.arange(0, len(node_label), 1):
     plt.figure(9)
     plt.suptitle('$e_x$ - orientation')
     if N == 0:
      plt.ylabel('$\Theta_x$ [deg]'), plt.xlabel('time [sec]')
     plt.plot(t, ex[N,:], label='node'+str(node_label[N]))
 plt.legend()
#------------------------------------------------------------------------------------
# second rotation
if args.yeuler:
 for N in np.arange(0, len(node_label), 1):
     plt.figure(10)
     plt.suptitle('$e_y$ - orientation')
     if N == 0:
      plt.ylabel('$\Theta_y$ [deg]'), plt.xlabel('time [sec]')
     plt.plot(t, ey[N,:], label='node'+str(node_label[N]))
 plt.legend()
#------------------------------------------------------------------------------------
# third rotation
if args.zeuler:
 for N in np.arange(0, len(node_label), 1):
     plt.figure(11)
     plt.suptitle('$e_z$ - orientation')
     if N == 0:
      plt.ylabel('$\Theta_z$ [deg]'), plt.xlabel('time [sec]')
     plt.plot(t, ez[N,:], label='node'+str(node_label[N]))
 plt.legend()
#------------------------------------------------------------------------------------
# all rotation
if args.alleuler:
 for N in np.arange(0, len(node_label), 1):
     plt.figure(12)
     plt.suptitle('$e$ - orientation')
     plt.subplot(1, 3, 1)
     plt.plot(t, ex[N,:])
     plt.ylabel('$\Theta_x$ [deg]'), plt.xlabel('time [sec]')
     plt.subplot(1, 3, 2)
     plt.plot(t, ey[N,:])
     plt.ylabel('$\Theta_y$ [deg]'), plt.xlabel('time [sec]')
     plt.subplot(1, 3, 3)
     plt.plot(t, ez[N,:], label='node'+str(node_label[N]))
     plt.ylabel('$\Theta_z$ [deg]'), plt.xlabel('time [sec]')
 plt.legend(bbox_to_anchor=(1.05, 1.0), loc='upper left')
#------------------------------------------------------------------------------------
# x angular velocity
if args.xangularvelocity:
 for N in np.arange(0, len(node_label), 1):
     plt.figure(13)
     plt.suptitle('$\Omega_x$ - angular velocity')
     if N == 0:
      plt.ylabel('$\Omega_x$ [deg/sec]'), plt.xlabel('time [sec]')
     plt.plot(t, om_x[N,:], label='node'+str(node_label[N]))
 plt.legend()
#------------------------------------------------------------------------------------
# y angular velocity
if args.yangularvelocity:
 for N in np.arange(0, len(node_label), 1):
     plt.figure(14)
     plt.suptitle('$\Omega_y$ - angular velocity')
     if N == 0:
      plt.ylabel('$\Omega_y$ [deg/sec]'), plt.xlabel('time [sec]')
     plt.plot(t, om_y[N,:], label='node'+str(node_label[N]))
 plt.legend()
#------------------------------------------------------------------------------------
# z angular velocity
if args.zangularvelocity:
 for N in np.arange(0, len(node_label), 1):
     plt.figure(15)
     plt.suptitle('$\Omega_z$ - angular velocity')
     if N == 0:
      plt.ylabel('$\Omega_z$ [deg/sec]'), plt.xlabel('time [sec]')
     plt.plot(t, om_z[N,:], label='node'+str(node_label[N]))
 plt.legend()
#------------------------------------------------------------------------------------
# all angular velocity
if args.allangularvelocity:
 for N in np.arange(0, len(node_label), 1):
     plt.figure(16)
     plt.suptitle('$\Omega$ - angular velocity')
     plt.subplot(1, 3, 1)
     plt.plot(t, om_x[N,:])
     plt.ylabel('$\Omega_x$ [deg/sec]'), plt.xlabel('time [sec]')
     plt.subplot(1, 3, 2)
     plt.plot(t, om_y[N,:])
     plt.ylabel('$\Omega_y$ [deg/sec]'), plt.xlabel('time [sec]')
     plt.subplot(1, 3, 3)
     plt.plot(t, om_z[N,:], label='node'+str(node_label[N]))
     plt.ylabel('$\Omega_z$ [deg/sec]'), plt.xlabel('time [sec]')
 plt.legend(bbox_to_anchor=(1.05, 1.0), loc='upper left')
#------------------------------------------------------------------------------------
# all position and all velocity
if args.allpositionvelocity:
 for N in np.arange(0, len(node_label), 1):
     plt.figure(17)
     plt.subplot(2, 3, 1)
     plt.suptitle('position && velocity')
     plt.plot(t, x[N,:]), plt.ylabel('x [m]'), plt.xlabel('time [sec]')
     plt.subplot(2, 3, 2)
     plt.plot(t, y[N,:]), plt.ylabel('y [m]'), plt.xlabel('time [sec]')
     plt.subplot(2, 3, 3)
     plt.plot(t, z[N,:]), plt.ylabel('z [m]'), plt.xlabel('time [sec]')
     plt.subplot(2, 3, 4)
     plt.plot(t, u[N,:]), plt.ylabel('u [m/s]'), plt.xlabel('time [sec]')
     plt.subplot(2, 3, 5)
     plt.plot(t, v[N,:]), plt.ylabel('v [m/s]'), plt.xlabel('time [sec]')
     plt.subplot(2, 3, 6)
     plt.plot(t, w[N,:], label='node'+str(node_label[N]))
     plt.ylabel('w [m/s]'), plt.xlabel('time [sec]')
 plt.legend(bbox_to_anchor=(1.05, 2.25), loc='upper left')




#------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------
# trajectory plot
if args.trajectory:
 for N in np.arange(0, len(node_label), 1):
     plt.figure(100)
     plt.suptitle('trajectory')
     if N == 0:
        ax = plt.axes(projection='3d')
        #axisEqual3D(ax)
     plt.plot(x[N,:],y[N,:],z[N,:],label='node'+str(node_label[N]))
     ax.set_xlabel('x')
     ax.set_ylabel('y')
     ax.set_zlabel('z')
    # trajectory plot of initial/final position
#axisEqual3D(ax)        #activate for have axis equal for trajectory 3D plot
if args.trajectory:
 plt.plot(x[:,0],y[:,0],z[:,0],'ro',label="initial position")
 plt.plot(x[:,-1],y[:,-1],z[:,-1],'b*',label="final position")
 plt.legend()





if force_label:
 for N in np.arange(0, len(node_label), 1):
    #-----  forces  -----#
    F_N = nd.variables['elem.force.'+str(args.Fvar[N][0])+'.F']
    f_x[N,:] = F_N[:,0]
    f_y[N,:] = F_N[:,1]
    f_z[N,:] = F_N[:,2]
 #------------------------------------------------------------------------------------
 # Fx 
 if args.xforce:
  for N in np.arange(0, len(force_label), 1):
     plt.figure(25)
     plt.suptitle('$F_x$ - x force')
     if N == 0:
      plt.ylabel('$F_x$ [N]'), plt.xlabel('time [sec]')
     plt.plot(t, f_x[N,:], label='node'+str(force_label[N]))
  plt.legend()
 #------------------------------------------------------------------------------------
 # Fy 
 if args.yforce:
  for N in np.arange(0, len(force_label), 1):
     plt.figure(26)
     plt.suptitle('$F_y$ - y force')
     if N == 0:
      plt.ylabel('$F_y$ [N]'), plt.xlabel('time [sec]')
     plt.plot(t, f_y[N,:], label='node'+str(force_label[N]))
  plt.legend()
 #------------------------------------------------------------------------------------
 # Fz 
 if args.zforce:
  for N in np.arange(0, len(force_label), 1):
     plt.figure(27)
     plt.suptitle('$F_z$ - z force')
     if N == 0:
      plt.ylabel('$F_z$ [N]'), plt.xlabel('time [sec]')
     plt.plot(t, f_z[N,:], label='node'+str(force_label[N]))
  plt.legend()



plt.show()
print('Closed')
print('==================================================')
