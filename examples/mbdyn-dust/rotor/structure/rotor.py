
import time
import sys;
# set to path of MBDyn support for python communication
sys.path.append('/usr/local/mbdyn/libexec/mbpy');

import os;
import tempfile;
tmpdir = tempfile.mkdtemp('', '.mbdyn_');
path = tmpdir + '/mbdyn.sock';
print(' path: ', path)

os.environ['MBSOCK'] = path;
os.system('mbdyn -f rotorhingeless -o output_offset > output_offset.txt 2>&1 &');

from mbc_py_interface import mbcNodal
from numpy import *
import numpy as np

import precice
from precice import *

# from mbdynAdapter import MBDynAdapter
# from preciceAuxiliaryCommunication import *
from mbdynInterface import MBDynInterface
from mbdynAdapter   import MBDynAdapter

#> ==============================================================
#> Hardcoded parameters
#> Does the other solver needs MBDyn nodes to build its own mesh?
writeNodes = False    # True #  False

#> MBDyn model parameters 
dt_set = 0.001
n_blades = 3
nEl = 4;                      # n.elements per blade
nnodes = (nEl*2+1)*n_blades   # nnodes exposed by MBDyn through external forces
chord = .1                    # wing chord (const)
twist = 2. * np.pi / 180.     # wing twist (const)


#> ==============================================================
#> Construct MBDyn/mbc_py interface
#> Initialize MBDyn/mbc_py interface: negotiate and recv()

mbd = MBDynInterface()

mbd.initialize( path=path, verbose=1, nnodes=nnodes, accels=1, \
                dumpAuxFile=True )

#> ==============================================================
#> Send MBDyn exposed nodes to the other solver, if needed
n = mbd.socket.nnodes
print(' n: ', n)
if ( writeNodes ):
  sys.exit(' writeNodes = True. Old implementation, not mantained. \
             Likely to be broken. Stop'); 
  pos, rot, inter = mbd.sendMBDynMesh( chord, twist )

#> ==============================================================
#> Build reference "Lagrangian" grid for preCICE-MBDyn solver,
#  from the position negotiated through MBDyn-mbc_py interface
print(" Build structural model ")
print(" ... ")

print(" Initialize MBDyn adapter ")
adapter = MBDynAdapter( mbd )

if ( adapter.debug ):
  print(' participant: ', adapter.p["name"] )
  print(' solver     : ', adapter.p["mesh"]["name"] )
  print(' fields     : ', adapter.p["fields"] )

#> ==============================================================
#> Start coupled simulation with PreCICE
adapter.runPreCICE()

