#!/bin/bash

ffmpeg -start_number 1 -i $1.%04d.png -c:v libx264 $1.mp4
