/*
joint: OFFSET + CURR_BLADE_3_PITCH_LINK, rod, # blade pitch link
	OFFSET + SW_ROT_NODE,
		position, reference, global, -0.0647700345968820, -0.2417249907297934, -0.4000000000000000,
	OFFSET + CURR_BLADE_3,
		position, reference, global, -0.0647700345968820, -0.2417249907297934, 0.0931330000000000,
		from nodes,
		linear elastic, 161363.3254531000275165;
		*/

joint: OFFSET + CURR_BLADE_3_PITCH_LINK, total joint, # blade pitch link
	OFFSET + SW_ROT_NODE,
		position, reference, global, -0.0647700345968820, -0.2417249907297934, -0.4000000000000000,
		position orientation, reference, global, eye,
		rotation orientation, reference, global, eye,
	OFFSET + CURR_BLADE_3,
		position, reference, global, -0.0647700345968820, -0.2417249907297934, -0.4000000000000000,
		position orientation, reference, global, eye,
		rotation orientation, reference, global, eye,
		position constraint, 
		0, 0, 1, 
		null,
	orientation constraint, 
		0, 0, 0,
		null;
