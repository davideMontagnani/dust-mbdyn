clear all; clc; close all

filen_list = { ...
          'll_blade1.in', ...
          'll_blade_teo_rev3.in', ...
         };

offset_rr = [ 0.0000, 0.0000, 0.0000 ;
              0.0000, 0.4515, 0.0000 ];
offset_rr = offset_rr';
offset_tw = [];

nb = length(filen_list) ;
for ib = 1 : nb

  chord = [] ;  twist = [] ;
  span  = [] ;  dihed = [] ;  sweep = [] ;
 
  filen = filen_list{ib};

  fid = fopen(filen);
  while ~feof(fid)
      tline = fgetl(fid); s = textscan(tline,'%s'); 
      if ( size(s{1,1}) > 0 )
  
          %> Sections
          if ( strcmp(s{1,1}{1,1},'chord') )
            chord = [ chord; str2num(s{1,1}{3,1}) ];
          end
          if ( strcmp(s{1,1}{1,1},'twist') )
            twist = [ twist; str2num(s{1,1}{3,1})*pi/180. ];
          end
  
          %> Regions
          if ( strcmp(s{1,1}{1,1},'span') )
            span  = [ span ; str2num(s{1,1}{3,1}) ];
          end
          if ( strcmp(s{1,1}{1,1},'sweep') )
            sweep = [ sweep; str2num(s{1,1}{3,1})*pi/180. ];
          end
          if ( strcmp(s{1,1}{1,1},'dihed') )
            dihed = [ dihed; str2num(s{1,1}{3,1})*pi/180. ];
          end
      end
  end
  fclose(fid);
  
  %> Blade points
  ns = size(chord,1); nr = size(span,1);
  rr = zeros(3,2*ns);
  rr(:,1   ) = zeros(3,1);
  rr(:,1+ns) = rr(:,1) + chord(1) * [ cos(twist(1)); 0.0; -sin(twist(1)) ];
  for is = 2 : ns
    rr(:,is   ) = rr(:,is-1) +  span(is-1) * [ sin(sweep(is-1)); 1.0;  sin(dihed(is-1)) ];
    rr(:,is+ns) = rr(:,is  ) + chord(is  ) * [ cos(twist(is  )); 0.0; -sin(twist(is  )) ];
  end
  
  %> Connectivity
  ee = zeros(4, nr);
  for i = 1 : nr
    ee(1,i) = i;
    ee(2,i) = i+ns;
    ee(3,i) = i+ns+1;
    ee(4,i) = i+1;
  end
  
  blade(ib).chord = chord;
  blade(ib).twist = twist;
  blade(ib).span  = span;
  blade(ib).dihed = dihed;
  blade(ib).sweep = sweep;

  blade(ib).rr = rr;
  blade(ib).ee = ee;

end

%> Offset
for ib = 1 : nb
  for ip = 1 : 2*ns
     blade(ib).rr(:,ip) = blade(ib).rr(:,ip) - offset_rr(:,ib);
  end
end

%> Plot
line_color = [ 'b', 'r' ];
figure; hold on
for ib = 1 : nb
  patch('Faces', blade(ib).ee', 'Vertices', blade(ib).rr', ...
        'FaceColor', 'None', ...
        'EdgeColor', line_color(ib) )
end
axis equal, grid on
hold off
