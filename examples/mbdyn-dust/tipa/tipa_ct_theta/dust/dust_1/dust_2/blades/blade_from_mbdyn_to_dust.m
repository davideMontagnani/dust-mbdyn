clear all; clc; close all

filen_in  = 'tipa_blade.in' ;
filen_out = 'tipa_blade.out';

dat = dlmread(filen_in,'',0,0);

span = dat(1,1);
blade.yad = dat(2:end,1);
blade.c   = dat(2:end,2);
blade.xac = dat(2:end,3);
blade.xcp = dat(2:end,4);
blade.th  = dat(2:end,5);

blade.y  = span / (blade.yad(end)-blade.yad(1)) * (blade.yad-blade.yad(1));
blade.dy = ( blade.y(2:end)-blade.y(1:end-1) );

ny = length(blade.yad) - 1;

fid = fopen(filen_out, 'w');

fprintf(fid, [ '!> Section #', num2str(1,'%2.2d') , '\n']);
fprintf(fid, [ 'chord   = ', num2str(blade.c (1)        ,'%7.4f') , '\n' ]);
fprintf(fid, [ 'twist   = ', num2str(blade.th(1)*180./pi,'%7.4f') , '\n' ]);
fprintf(fid, [ 'airfoil = ', 'NACA0012' , '\n' ]);
fprintf(fid, '\n');

for i = 1 : ny
  
  fprintf(fid, [ '!> Region #', num2str(i,'%2.2d') , '\n']);
  fprintf(fid, [ 'span    = ', num2str(blade.dy(i),'%7.4f') , '\n' ]);
  fprintf(fid, [ 'sweep   = 0.0 \n' ]);
  fprintf(fid, [ 'dihed   = 0.0 \n' ]);
  fprintf(fid, [ 'nelem_span = 1 \n' ]);
  fprintf(fid, [ 'type_span = uniform \n' ]);
  fprintf(fid, '\n');
   
  fprintf(fid, [ '!> Section #', num2str(i+1,'%2.2d') , '\n']);
  fprintf(fid, [ 'chord   = ', num2str(blade.c (i+1)        ,'%7.4f') , '\n' ]);
  fprintf(fid, [ 'twist   = ', num2str(blade.th(i+1)*180./pi,'%7.4f') , '\n' ]);
  fprintf(fid, [ 'airfoil = ', 'NACA0012' , '\n' ]);
  fprintf(fid, '\n');

end

fclose(fid);

%> Some plot: LE and TE points
rr = [ zeros(ny+1,1), blade.y ; ...
       zeros(ny+1,1), blade.y ];
for i = 1 : ny+1
  rr(ny+1+i,1) = blade.c(i) ;
end
figure
plot(rr(:,1), rr(:,2), 'o'), grid on, axis equal
