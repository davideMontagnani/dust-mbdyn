clear all; clc; close all

filen_list = { ...
              './../Postpro/test1_rev4_theta+10_loads_rotor.dat' , ...
              './../Postpro/test1_rev4_loads_rotor.dat' , ...
              './../Postpro/test1_rev4_theta+16_loads_rotor.dat' , ...
             };

n_sim = length(filen_list);

%> Rotor
R = 3.81;
A = pi * R^2 ;
Om = 61.68;
rho = 1.202;
Vtip = Om * R;

for i = 1 : n_sim

    filen = filen_list{i};

    dat = dlmread(filen, '', 4, 0);
    sim(i).t  = dat(:,1);
    sim(i).T  = dat(:,4);
    sim(i).Q  = dat(:,7);
    
    sim(i).cT = sim(i).T ./ ( rho * A * Vtip^2 );
    sim(i).cQ = sim(i).Q ./ ( rho * A * Vtip^2 * R );

end


figure
for i = 1 : n_sim
  subplot(1,2,1), plot(sim(i).t, sim(i).T), grid on, xlabel('t [s]'), ylabel('T [N]') ; if (i==1) ; hold on; end
  subplot(1,2,2), plot(sim(i).t, sim(i).Q), grid on, xlabel('t [s]'), ylabel('Q [Nm]'); if (i==1) ; hold on; end
end

figure
for i = 1 : n_sim
  subplot(1,2,1), plot(sim(i).t, sim(i).cT), grid on, xlabel('t [s]'), ylabel('cT'); if (i==1) ; hold on; end
  subplot(1,2,2), plot(sim(i).t, sim(i).cQ), grid on, xlabel('t [s]'), ylabel('cQ'); if (i==1) ; hold on; end
end
