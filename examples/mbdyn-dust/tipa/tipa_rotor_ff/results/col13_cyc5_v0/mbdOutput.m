clear all, close all, clc
%--------------------------------------------------------------------------
%++++++++++++++++      inserting parameters     +++++++++++++++++++++++++++
%--------------------------------------------------------------------------
% pkg load netcdf
name_output = 'output_ll_pitch13+5_uinf0.nc';
node_label = [10000 ];
%--------------------------------------------------------------------------
%--------------------------------------------------------------------------


%time vector
t = ncread(name_output, 'time');
    %to inspect ncf file structure
    xncinfo = ncinfo(name_output);
dt = ncread(name_output, 'run.timestep');
% t_step = dt(2);
%initialize positions vectors fro each node
x = zeros(length(node_label), length(t));
y = zeros(length(node_label), length(t));
z = zeros(length(node_label), length(t));
%initialize positions vectors fro each node
ex = zeros(length(node_label), length(t));
ey = zeros(length(node_label), length(t));
ez= zeros(length(node_label), length(t));

% to read the effective collective
Theta = ncread(name_output,'elem.joint.15100.Phi');
theta_x = Theta(1,:)*180/pi;
theta_y = Theta(2,:)*180/pi;
theta_z = Theta(3,:)*180/pi;
plot(t, theta_x, t, theta_y, t, theta_z)
legend('\phi_x' , '\phi_y' , '\phi_z')



for N = 1 : length(node_label)

%position components
X = ncread(name_output, strcat('node.struct.',num2str(node_label(1 , N)),'.X'));
    x(N,:) = X(1,:);
    y(N,:) = X(2,:);
    z(N,:) = X(3,:);

%velocity components
XP = ncread(name_output, strcat('node.struct.',num2str(node_label(1 , N)),'.XP'));
    u(N,:) = XP(1,:);
    v(N,:) = XP(2,:);
    w(N,:) = XP(3,:);

figure(N)

subplot(2,3,1)
plot(t , x(N,:))
xlabel('time') , ylabel('x position')
subplot(2,3,2)
plot(t, y(N,:))
xlabel('time') , ylabel('y position')
subplot(2,3,3)
plot(t , z(N,:))
xlabel('time') , ylabel('z position')

subplot(2,3,4)
plot(t , u(N,:))
xlabel('time') , ylabel('u')
subplot(2,3,5)
plot(t, v(N,:))
xlabel('time') , ylabel('v')
subplot(2,3,6)
plot(t , z(N,:))
xlabel('time') , ylabel('w')



% trajectory
figure(1000)
title('Trajectory of nodes')
plot3(x(N,:), y(N,:), z(N,:),'HandleVisibility','off')
hold on, grid on
end
plot3(x(:,end), y(:,end), z(:,end), 'bs')
plot3(x(:,1), y(:,1), z(:,1), 'sr')
legend('final position' , 'initial position')


return
figure(N)
subplot(5,3,1)
plot(t , x)
xlabel('time') , ylabel('x position')

subplot(5,3,2)
plot(t, y0)
xlabel('time') , ylabel('y position')

subplot(5,3,3)
plot(t , z0)
xlabel('time') , ylabel('z position')

subplot(5,3,4)
plot(t , x1)
xlabel('time') , ylabel('x position')

subplot(5,3,5)
plot(t, y1)
xlabel('time') , ylabel('y position')

subplot(5,3,6)
plot(t , z1)
xlabel('time') , ylabel('z position')

subplot(5,3,7)
plot(t , x2)
xlabel('time') , ylabel('x position')

subplot(5,3,8)
plot(t, y2)
xlabel('time') , ylabel('y position')

subplot(5,3,9)
plot(t , z2)
xlabel('time') , ylabel('z position')

subplot(5,3,10)
plot(t , x3)
xlabel('time') , ylabel('x position')

subplot(5,3,11)
plot(t, y3)
xlabel('time') , ylabel('y position')

subplot(5,3,12)
plot(t , z3)
xlabel('time') , ylabel('z position')

subplot(5,3,13)
plot(t , x4)
xlabel('time') , ylabel('x position')

subplot(5,3,14)
plot(t, y4)
xlabel('time') , ylabel('y position')

subplot(5,3,15)
plot(t , z4)
xlabel('time') , ylabel('z position')



%force
F = ncread(name_output, 'elem.force.102.F');

