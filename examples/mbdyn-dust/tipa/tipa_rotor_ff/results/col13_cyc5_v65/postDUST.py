import sys
import argparse
import netCDF4 as nc
import matplotlib.pyplot as plt

import sys,getopt
from netCDF4 import Dataset
from scipy.io import netcdf
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import os
from matplotlib.colors import LinearSegmentedColormap
import matplotlib.axes as maxes
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import rc

#=============================================================
rho_inf = 1.202
R = 3.81
Omega = 61.68

#=============================================================

parser = argparse.ArgumentParser(\
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description='Plot variables in Dust output using Matplotlib.',\
        epilog='Multiple --var arguments can be provided.\n' + \
                'Components should be indicated as reported here:\n' + \
'------------------------------------------------------------------------------------------------\n' + \
'|	-fx	: X-force									|\n' + \
'|	-fy	: Y-force									|\n' + \
'|	-fz	: Z-force									|\n' + \
'|	-mx	: X-moment									|\n' + \
'|	-my	: Y-momentx									|\n' + \
'|	-mz	: Z-momenty									|\n' + \
'|	-ct	: thrust coefficient								|\n' + \
'|	-cq	: torque coefficient								|\n' + \
'|												|\n' + \
'------------------------------------------------------------------------------------------------ '
                '\n'+\
                'e.g..\n' + \
                'to plot the X-force component and X-moment component for two components from the corresponding output.dat:\n' + \

                '   >>python postDUST.py output1.dat output1.dat -fx -mx'
)
parser.add_argument('outputfile', metavar='outputfile', \
        help='Dust .dat output file',nargs='+')
parser.add_argument('--fx', '-fx', action='store_true',\
        help='to plot X-Force component')
parser.add_argument('--fy', '-fy', action='store_true',\
        help='to plot Y-Force component')
parser.add_argument('--fz', '-fz', action='store_true',\
        help='to plot Z-Force component')
parser.add_argument('--mx', '-mx', action='store_true',\
        help='to plot X-Moment component')
parser.add_argument('--my', '-my', action='store_true',\
        help='to plot Y-Moment component')
parser.add_argument('--mz', '-mz', action='store_true',\
        help='to plot Z-Moment component')
parser.add_argument('--ct', '-ct', action='store_true',\
        help='to plot thrust coefficient')
parser.add_argument('--cq', '-cq', action='store_true',\
        help='to plot torque coefficient')

args = parser.parse_args()
#=============================================================

output_file = args.outputfile;
number_dt = len(np.genfromtxt(output_file[0],dtype=float))
ddt = np.genfromtxt(output_file[0],dtype=float)
dt = ddt[1 , 0] - ddt[0 ,0]

print('===============================================')
print('Number of components considered:' + str(len(output_file)))
print('===============================================')
print('Number of time steps considered:' +str( number_dt))
print('===============================================')

#--------------------------------------------------------------------------
#++++++++++++      PLOTING PARAMTERS        +++++++++++#
#--------------------------------------------------------------------------
# set parameters for plotting (Interpreter, size, ...)
#import plotting_param
#plotting_param.plot_param()

#+++++++++      LINES        ++++++++#
#++++++++++++++++++++++++++++++++++++++
mpl.rcParams['lines.linewidth'] = 1
mpl.rcParams['lines.markersize'] = 4
mpl.rcParams['markers.fillstyle'] = "none"  ## {full, left, right, bottom, top, none}
mpl.rcParams['xtick.labelsize'] = 10
mpl.rcParams['ytick.labelsize'] = 10
mpl.rcParams['xtick.major.size'] = 3.5     ## major tick size in points
mpl.rcParams['xtick.minor.size'] = 2       ## minor tick size in points

#+++++++++      AXES        ++++++++#
#++++++++++++++++++++++++++++++++++++++
mpl.rcParams['axes.titlesize'] = 12         ## fontsize of the axes title
mpl.rcParams['axes.labelsize'] = 13         ## fontsize of the x any y labels
mpl.rcParams['axes.grid'] = True            ## display grid or not
mpl.rcParams['axes3d.grid'] = True          ## display grid or not for 3Dplot
mpl.rcParams['axes.grid.axis'] = "both"     ## which axis the grid should apply to
mpl.rcParams['axes.grid.which'] = "both"    ## gridlines at {major, minor, both} ticks
mpl.rcParams['axes.facecolor'] = "white"    ## axes background color
mpl.rcParams['axes.edgecolor'] = "black"    ## axes edge color
#mpl.rcParams['legend.loc'] = 'upper right'


#+++++++++      GRID        ++++++++#
#++++++++++++++++++++++++++++++++++++++
mpl.rcParams['grid.color'] = "black"         ## grid color
mpl.rcParams['grid.linestyle'] = "-"         ## solid
mpl.rcParams['grid.linewidth'] = 0.6         ## in points
mpl.rcParams['grid.alpha'] = .5              ## transparency, between 0.0 and 1.0

#+++++++++      FIGURE        ++++++++#
#++++++++++++++++++++++++++++++++++++++
## The figure subplot parameters.  All dimensions are a fraction of the figure width and height.
mpl.rcParams['figure.autolayout'] = False
mpl.rcParams['figure.subplot.left'] = 0.125  ## the left side of the subplots of the figure
mpl.rcParams['figure.subplot.right'] = 0.9    ## the right side of the subplots of the figure
mpl.rcParams['figure.subplot.bottom'] = 0.11   ## the bottom of the subplots of the figure
mpl.rcParams['figure.subplot.top'] = 0.88   ## the top of the subplots of the figure
mpl.rcParams['figure.subplot.wspace'] = 0.4    ## the amount of width reserved for space between subplots,
                                ## expressed as a fraction of the average axis width
mpl.rcParams['figure.subplot.hspace'] = 0.2    ## the amount of height reserved for space between subplots,
                                ## expressed as a fraction of the average axis height

#+++++++++      FONT        ++++++++#
#++++++++++++++++++++++++++++++++++++++
mpl.rcParams['font.family'] = 'serif'
    ## The font.family property has five values:
    ##     - 'serif' (e.g., Times),
    ##     - 'sans-serif' (e.g., Helvetica),
    ##     - 'cursive' (e.g., Zapf-Chancery),
    ##     - 'fantasy' (e.g., Western), and
    ##     - 'monospace' (e.g., Courier).

#====================================================================
#inizialize the forces/moments vectors (raw: time, coloumns: component)
time = np.zeros((number_dt , len(output_file)))
Fx = np.zeros((number_dt , len(output_file)))
Fy = np.zeros((number_dt , len(output_file)))
Fz = np.zeros((number_dt , len(output_file)))
Ct = np.zeros((number_dt , len(output_file)))
Mx = np.zeros((number_dt , len(output_file)))
My = np.zeros((number_dt , len(output_file)))
Mz = np.zeros((number_dt , len(output_file)))
Cq = np.zeros((number_dt , len(output_file)))
#inizialize the MEAN forces/moments vectors (coloumns: component)
Fxm = np.zeros((1 , len(output_file)))
Fym = np.zeros((1 , len(output_file)))
Fzm = np.zeros((1 , len(output_file)))
#====================================================================
#--------------------------------------------------------------------
# Fx
if args.fx:
 for N in np.arange(0, len(output_file), 1):
  im_ar = np.genfromtxt(output_file[N],dtype=float)
  time[:number_dt,N] = im_ar[:number_dt , 0]
  Fx[:number_dt,N] = im_ar[:number_dt , 1]
  Fxm[0,N] = np.mean(Fx[:,N])
  plt.figure(1)
  plt.xlabel('time [sec]'), plt.ylabel('$F_x$ [N]')
  plt.plot(time[:,N] , Fx[:,N], label = '$F_x$:  ' + output_file[N])
  plt.plot(time[:,N] , Fxm[0,N]*np.ones(number_dt), '--',label = '$F_{x, mean}$:  ' + output_file[N] )
 plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left')
 plt.tight_layout()
#--------------------------------------------------------------------
# Fy
if args.fy:
 for N in np.arange(0, len(output_file), 1):
  im_ar = np.genfromtxt(output_file[N],dtype=float)
  time[:number_dt,N] = im_ar[:number_dt , 0]
  Fy[:number_dt,N] = im_ar[:number_dt , 2]
  Fym[0,N] = np.mean(Fy[:,N])
  plt.figure(2)
  plt.xlabel('time [sec]'), plt.ylabel('$F_y$ [N]')
  plt.plot(time[:,N] , Fym[0,N]*np.ones(number_dt), '--',label = '$F_{y, mean}$:  ' + output_file[N] )
  plt.plot(time[:,N] , Fy[:,N], label = '$F_y$:  ' + output_file[N])
 plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left')
 plt.tight_layout()
#--------------------------------------------------------------------
# Fz
if args.fz:
 for N in np.arange(0, len(output_file), 1):
  im_ar = np.genfromtxt(output_file[N],dtype=float)
  time[:number_dt,N] = im_ar[:number_dt , 0]
  Fz[:number_dt,N] = im_ar[:number_dt , 3]
  Fzm[0,N] = np.mean(Fz[:,N])
  Ct[:number_dt,N] = Fz[:number_dt,N]/(1.202*np.pi*3.81**4*Omega**2)
  plt.figure(3)
  plt.xlabel('time [sec]'), plt.ylabel('$F_z$ [N]')
  plt.plot(time[:,N] , Fz[:,N], label = '$F_z$:  ' + output_file[N])
  #plt.plot(time[:,N] , Fzm[0,N]*np.ones(number_dt), '--',label = '$F_{z, mean}$:  ' + output_file[N] )
  #plt.plot(time[:,N] , Ct[:,N], label = '$C_T$:  ' + output_file[N])
 plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left')
 plt.tight_layout()
#--------------------------------------------------------------------
# Mx
if args.mx:
 for N in np.arange(0, len(output_file), 1):
  im_ar = np.genfromtxt(output_file[N],dtype=float)
  time[:number_dt,N] = im_ar[:number_dt , 0]
  Mx[:number_dt,N] = im_ar[:number_dt , 4]
  plt.figure(4)
  plt.xlabel('time [sec]'), plt.ylabel('$M_x$ [Nm]')
  plt.plot(time[:,N] , Mx[:,N], label = '$M_x$:  ' + output_file[N])
 plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left')
 plt.tight_layout()
#--------------------------------------------------------------------
# My
if args.my:
 for N in np.arange(0, len(output_file), 1):
  im_ar = np.genfromtxt(output_file[N],dtype=float)
  time[:number_dt,N] = im_ar[:number_dt , 0]
  My[:number_dt,N] = im_ar[:number_dt , 5]
  plt.figure(5)
  plt.xlabel('time [sec]'), plt.ylabel('$M_y$ [Nm]')
  plt.plot(time[:,N] , My[:,N], label = '$M_y$:  ' + output_file[N])
 plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left')
 plt.tight_layout()

#--------------------------------------------------------------------
# Mz
if args.mz:
 for N in np.arange(0, len(output_file), 1):
  im_ar = np.genfromtxt(output_file[N],dtype=float)
  time[:number_dt,N] = im_ar[:number_dt , 0]
  Mz[:number_dt,N] = im_ar[:number_dt , 6]
  plt.figure(6)
  plt.xlabel('time [sec]'), plt.ylabel('$M_z$ [Nm]')
  plt.plot(time[:,N] , Mz[:,N], label = '$M_z$:  ' + output_file[N])
 plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left')
 plt.tight_layout()
#--------------------------------------------------------------------
# Ct
if args.ct:
 for N in np.arange(0, len(output_file), 1):
  im_ar = np.genfromtxt(output_file[N],dtype=float)
  time[:number_dt,N] = im_ar[:number_dt , 0]
  Fz[:number_dt,N] = im_ar[:number_dt , 3]
  Fzm[0,N] = np.mean(Fz[:,N])
  Ct[:number_dt,N] = Fz[:number_dt,N]/(rho_inf*np.pi*R**4*Omega**2)
  plt.figure(7)
  plt.xlabel('time [sec]'), plt.ylabel('$C_T$ [N]')
  #plt.plot(time[:,N] , Fz[:,N], label = '$F_z$:  ' + output_file[N])
  #plt.plot(time[:,N] , Fzm[0,N]*np.ones(number_dt), '--',label = '$F_{z, mean}$:  ' + output_file[N] )
  plt.plot(time[:,N] , Ct[:,N], label = '$C_T$:  ' + output_file[N])
 plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left')
 plt.tight_layout()
#--------------------------------------------------------------------
# Cq
if args.cq:
 for N in np.arange(0, len(output_file), 1):
  im_ar = np.genfromtxt(output_file[N],dtype=float)
  time[:number_dt,N] = im_ar[:number_dt , 0]
  Mz[:number_dt,N] = im_ar[:number_dt , 6]
  Cq[:number_dt,N] = Mz[:number_dt,N]/(rho_inf*np.pi*R**5*Omega**2)
  plt.figure(8)
  plt.xlabel('time [sec]'), plt.ylabel('$C_Q$ [Nm]')
  plt.plot(time[:,N] , Cq[:,N], label = '$M_z$:  ' + output_file[N])
 plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left')
 plt.tight_layout()
#--------------------------------------------------------------------

plt.show()

