clear all, close all, clc

%==========================================
rho_inf = 1.202;         %air density
R = 3.81;                %rotor radius
Omega = 61.68;           %rotor angular velocity
theta = [13];  %collective angles tested
%==========================================

%importing the data

theta13_5_U65 = dlmread('pitch_13+5_Uinf65_loads_rotor.dat', '', 4, 0);


% % %importing references data
% Fz_ref = 37864.8 * ones(1 , length(theta13_5_U0(:,1)));
% Fx_re = 3536.49 * ones(1 , length(theta13_5_U0(:,1)));
% Fy_re = 74.8555 * ones(1 , length(theta13_5_U0(:,1)));
% Mx_re = -1616.98 * ones(1 , length(theta13_5_U0(:,1)));
% My_re = 1535.16 * ones(1 , length(theta13_5_U0(:,1)));
% Mz_ref = -17487.7 * ones(1 , length(theta13_5_U0(:,1)));
% 
% Fx_ref = Fy_re;
% Fy_ref = -Fx_re;
% Mx_ref = My_re;
% My_ref = -Mx_re;
%%
%importing onlyDUST data
%------------------------
% theta4_onlydust = table2array(importfile('onlydust_4.dat'));
% theta_dust = [4 6 10 13];
%     Tod_4 = theta4_onlydust(:,4);  Qod_4 = theta4_onlydust(:,7); 

% Tm_dust = [Tod_4m Tod_6m Tod_10m Tod_13m ]; 
% Qm_dust = [Qod_4m Qod_6m Qod_10m Qod_13m ]; 
% Ct_dust = Tm_dust./(rho_inf*pi*R^4*Omega^2);
% Cq_dust = -Qm_dust./(rho_inf*pi*R^5*Omega^2);
% FM_dust = Ct_dust./Cq_dust.*(Ct_dust./2).^0.5;
%%

%==========================================
%extracting F and M
Fz_13_5_U0 = theta13_5_U65(:,4);          Mz_13_5_U0 = theta13_5_U65(:,7);
Fx_13_5_U0 = theta13_5_U65(:,2);          Mx_13_5_U0 = theta13_5_U65(:,5);
Fy_13_5_U0 = theta13_5_U65(:,3);          My_13_5_U0 = theta13_5_U65(:,6);

%computing adimensional coeff.
    %thrust coefficient
Ct_13_5_U0 = Fz_13_5_U0/(rho_inf*pi*R^4*Omega^2);

    %torque coefficient
Cq_13_5_U0 = Mz_13_5_U0/(rho_inf*pi*R^5*Omega^2);


%============================================
%---        computing MEAN and SDV        ---
%============================================
%considering the data after time = 1 sec for convergence
T_13_5_U0m = mean(Fz_13_5_U0(252:end,1));
Ct_13_5_U0m = mean(Ct_13_5_U0(252:end,1));
    ert_13_5_U0 = std(Ct_13_5_U0(252:end,1));

%---------------------------------------------------------   
Cq_13_5_U0m = mean(Cq_13_5_U0(252:end,1));
    erq_13_5_U0 = std(Cq_13_5_U0(252:end,1));
  
%%
%==========================================================================
%++++++++++++++++++++++          PLOTTING          ++++++++++++++++++++++++
%==========================================================================
set(0,'defaulttextinterpreter','latex');  
set(0, 'defaultAxesTickLabelInterpreter','latex');  
set(0, 'defaultLegendInterpreter','tex');
set(0,'defaultAxesFontSize',18)

%========= F vs time ===========
figure()
plot(theta13_5_U65(:,1) , Fx_13_5_U0), hold on
grid on
plot(theta13_5_U65(:,1) , Fy_13_5_U0)
plot(theta13_5_U65(:,1) , Fz_13_5_U0)
% plot(theta13_5_U65(:,1) , Fx_ref)
% plot(theta13_5_U65(:,1) , Fy_ref)
% plot(theta13_5_U65(:,1) , Fz_ref)
ylabel('$F [N]$') , xlabel('t [sec]')
title('collective = 13°, longitudinal cyclic 5°, Uinf = 65Knots')
l1 = legend('F_x dust-MBDyn','F_y dust-MBDyn','F_z dust-MBDyn', ...
    'F_x MBDyn with MBDyn aero','F_y MBDyn with MBDyn aero','F_z MBDyn with MBDyn aero')
set(l1,'NumColumns',1,'Location','eastoutside');
%========= M vs time ===========
figure()
plot(theta13_5_U65(:,1) , Mx_13_5_U0), hold on
grid on
plot(theta13_5_U65(:,1) , My_13_5_U0)
plot(theta13_5_U65(:,1) , Mz_13_5_U0)
% plot(theta13_5_U65(:,1) , Mx_ref)
% plot(theta13_5_U65(:,1) , My_ref)
% plot(theta13_5_U65(:,1) , Mz_ref)
ylabel('$M [Nm]$') , xlabel('t [sec]')
title('collective = 13°, longitudinal cyclic 5°, Uinf = 65Knots')
l2 = legend('M_x dust-MBDyn','M_y dust-MBDyn','M_z dust-MBDyn', ...
    'M_x MBDyn with MBDyn aero','M_y MBDyn with MBDyn aero','M_z MBDyn with MBDyn aero')
set(l2,'NumColumns',1,'Location','eastoutside');

