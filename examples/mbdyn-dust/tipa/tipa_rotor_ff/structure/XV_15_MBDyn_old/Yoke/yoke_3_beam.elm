# Defining the beam/sections notation
# Beam OFFSET + CURR_YOKE_3 + 1, 
#                                       A  beam y 
#                                      /   adimensional axis
#                                     /
#                                    /
#           |            |          |----------|-------------|---------> beam x adimensional axis
#          -1       -1/sqrt(3)      0       1/sqrt(3)        1 
#               ________________________________________________
#             /                                                /
#            /        section I             section II        /
#           /_____________|_____________________|____________/ 
#           |           | | |       |         | | |          |  
#           |           | V |       |         | V |          |  
#           |           |   |       |         |   |          |  
#           |  _________|   |_______|_________|   |__________|__ 
#           | /         |  /        | /       |  /           | /
#           |/          | /         |/        | /            |/
#           /___________|/__________/_________|/_____________/
#          node 1                  node 2              node 3
#      0
#      |---\....\----------------------------------------------------------> global X axis 
#
beam3: OFFSET +CURR_YOKE_3 + 1, # beam name
	OFFSET + CURR_YOKE_3 + 1 , # node 1 name
		position, reference, OFFSET + CURR_YOKE_3 + FEATH + 1, 0., 0.0000000000000000, 0.0000000000000000, 
		orientation, reference, OFFSET + CURR_YOKE_3 + FEATH + 1, eye, # orientation for properties
	OFFSET + CURR_YOKE_3 + 100 + 1 , # node 2 name
		position, reference, OFFSET + CURR_YOKE_3 + FEATH + 1 + 100, 0., 0.0000000000000000, 0.0000000000000000, 
		orientation, reference, OFFSET + CURR_YOKE_3 + FEATH + 1 + 100, eye, # orientation for properties
	OFFSET + CURR_YOKE_3 + 4 , # node 3 name
		position, reference, OFFSET + CURR_YOKE_3 + FEATH + 4, 0., 0.0000000000000000, 0.0000000000000000, 
		orientation, reference, OFFSET + CURR_YOKE_3 + FEATH + 4, eye, # orientation for properties
		from nodes, 
	linear elastic generic, matr, #sec I const law
			2.54322005e+12, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, -0.00000000e+00,
			0.00000000e+00, 9.56097765e+11, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 9.56097765e+11, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 1.06201042e+06, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 7.78417771e+05, 0.00000000e+00,
			-0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 7.39720514e+05,
	from nodes, # automatically setting section II orientation
	linear elastic generic, matr, #sec II const law
			1.60644886e+12, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, -0.00000000e+00,
			0.00000000e+00, 6.03928142e+11, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 6.03928142e+11, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 1.06201042e+06, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 6.63485687e+05, 0.00000000e+00,
			-0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 6.53116789e+05;

# Defining the beam/sections notation
# Beam OFFSET + CURR_YOKE_3 + 4, 
#                                       A  beam y 
#                                      /   adimensional axis
#                                     /
#                                    /
#           |            |          |----------|-------------|---------> beam x adimensional axis
#          -1       -1/sqrt(3)      0       1/sqrt(3)        1 
#               ________________________________________________
#             /                                                /
#            /        section I             section II        /
#           /_____________|_____________________|____________/ 
#           |           | | |       |         | | |          |  
#           |           | V |       |         | V |          |  
#           |           |   |       |         |   |          |  
#           |  _________|   |_______|_________|   |__________|__ 
#           | /         |  /        | /       |  /           | /
#           |/          | /         |/        | /            |/
#           /___________|/__________/_________|/_____________/
#          node 1                  node 2              node 3
#      0
#      |---\....\----------------------------------------------------------> global X axis 
#
beam3: OFFSET +CURR_YOKE_3 + 4, # beam name
	OFFSET + CURR_YOKE_3 + 4 , # node 1 name
		position, reference, OFFSET + CURR_YOKE_3 + FEATH + 4, 0., 0.0000000000000000, 0.0000000000000000, 
		orientation, reference, OFFSET + CURR_YOKE_3 + FEATH + 4, eye, # orientation for properties
	OFFSET + CURR_YOKE_3 + 100 + 4 , # node 2 name
		position, reference, OFFSET + CURR_YOKE_3 + FEATH + 4 + 100, 0., 0.0000000000000000, 0.0000000000000000, 
		orientation, reference, OFFSET + CURR_YOKE_3 + FEATH + 4 + 100, eye, # orientation for properties
	OFFSET + CURR_YOKE_3 + 7 , # node 3 name
		position, reference, OFFSET + CURR_YOKE_3 + FEATH + 7, 0., 0.0000000000000000, 0.0000000000000000, 
		orientation, reference, OFFSET + CURR_YOKE_3 + FEATH + 7, eye, # orientation for properties
		from nodes, 
	linear elastic generic, matr, #sec I const law
			1.40264901e+12, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, -0.00000000e+00,
			0.00000000e+00, 5.27311658e+11, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 5.27311658e+11, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 1.06201042e+06, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 7.81269285e+05, 0.00000000e+00,
			-0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 7.81269285e+05,
	from nodes, # automatically setting section II orientation
	linear elastic generic, matr, #sec II const law
			1.78262867e+12, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, -0.00000000e+00,
			0.00000000e+00, 6.70161156e+11, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 6.70161156e+11, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 1.06201042e+06, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 1.21799214e+06, 0.00000000e+00,
			-0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 1.21799214e+06;

# Defining the beam/sections notation
# Beam OFFSET + CURR_YOKE_3 + 7, 
#                                       A  beam y 
#                                      /   adimensional axis
#                                     /
#                                    /
#           |            |          |----------|-------------|---------> beam x adimensional axis
#          -1       -1/sqrt(3)      0       1/sqrt(3)        1 
#               ________________________________________________
#             /                                                /
#            /        section I             section II        /
#           /_____________|_____________________|____________/ 
#           |           | | |       |         | | |          |  
#           |           | V |       |         | V |          |  
#           |           |   |       |         |   |          |  
#           |  _________|   |_______|_________|   |__________|__ 
#           | /         |  /        | /       |  /           | /
#           |/          | /         |/        | /            |/
#           /___________|/__________/_________|/_____________/
#          node 1                  node 2              node 3
#      0
#      |---\....\----------------------------------------------------------> global X axis 
#
beam3: OFFSET +CURR_YOKE_3 + 7, # beam name
	OFFSET + CURR_YOKE_3 + 7 , # node 1 name
		position, reference, OFFSET + CURR_YOKE_3 + FEATH + 7, 0., 0.0000000000000000, 0.0000000000000000, 
		orientation, reference, OFFSET + CURR_YOKE_3 + FEATH + 7, eye, # orientation for properties
	OFFSET + CURR_YOKE_3 + 100 + 7 , # node 2 name
		position, reference, OFFSET + CURR_YOKE_3 + FEATH + 7 + 100, 0., 0.0000000000000000, 0.0000000000000000, 
		orientation, reference, OFFSET + CURR_YOKE_3 + FEATH + 7 + 100, eye, # orientation for properties
	OFFSET + CURR_YOKE_3 + 10 , # node 3 name
		position, reference, OFFSET + CURR_YOKE_3 + FEATH + 10, 0., 0.0000000000000000, 0.0000000000000000, 
		orientation, reference, OFFSET + CURR_YOKE_3 + FEATH + 10, eye, # orientation for properties
		from nodes, 
	linear elastic generic, matr, #sec I const law
			2.01917832e+12, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, -0.00000000e+00,
			0.00000000e+00, 7.59089595e+11, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 7.59089595e+11, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 1.06201042e+06, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 1.18192033e+06, -0.00000000e+00,
			-0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, -0.00000000e+00, 1.37784380e+06,
	from nodes, # automatically setting section II orientation
	linear elastic generic, matr, #sec II const law
			2.28546432e+12, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, -0.00000000e+00,
			0.00000000e+00, 8.59197113e+11, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 8.59197113e+11, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 1.06201042e+06, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 6.46647467e+05, -0.00000000e+00,
			-0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, -0.00000000e+00, 1.37784380e+06;

