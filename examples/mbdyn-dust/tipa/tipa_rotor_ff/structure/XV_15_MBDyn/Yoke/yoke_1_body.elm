# /home/davide/Software/mbdyn/sim/tipa/Tilt_load_data
# Do not modify 

# CURR_ROTOR + CURR_YOKE_1 + 1 : 0.000000e+00 -> 8.051477e-02 m (0.000 -> 0.064 R)
body: CURR_ROTOR + CURR_YOKE_1 + 1, CURR_ROTOR + CURR_YOKE_1 + 1
	,
	8.252120e+00, #1.024920e+02 kg/m; dL=8.051477e-02 m; 
	reference, CURR_ROTOR + CURR_YOKE_1 + BODY + 1,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_YOKE_1 + BODY + 1, 
		diag, 0.000000e+00, 4.457953e-03, 4.457953e-03
;

# CURR_ROTOR + CURR_YOKE_1 + 2 : 8.051477e-02 -> 3.004852e-01 m (0.064 -> 0.240 R)
body: CURR_ROTOR + CURR_YOKE_1 + 2, CURR_ROTOR + CURR_YOKE_1 + 2
	,
	condense, 4
	,
	1.127261e+01, #1.024920e+02 kg/m; dL=1.099852e-01 m; 
	reference, CURR_ROTOR + CURR_YOKE_1 + BODY + 1,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_YOKE_1 + BODY + 1, 
		diag, 0.000000e+00, 1.136349e-02, 1.136349e-02
	,
	4.061143e-07, #1.024920e+02 kg/m; dL=3.962400e-09 m; 
	reference, CURR_ROTOR + CURR_YOKE_1 + BODY + 2,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_YOKE_1 + BODY + 2, 
		diag, 0.000000e+00, 5.313537e-25, 5.313537e-25
	,
	7.460823e-01, #9.791106e+01 kg/m; dL=7.620000e-03 m; 
	reference, CURR_ROTOR + CURR_YOKE_1 + BODY + 3,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_YOKE_1 + BODY + 3, 
		diag, 0.000000e+00, 3.610068e-06, 3.610068e-06
	,
	9.553758e+00, #9.333012e+01 kg/m; dL=1.023652e-01 m; 
	reference, CURR_ROTOR + CURR_YOKE_1 + BODY + 4,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_YOKE_1 + BODY + 4, 
		diag, 0.000000e+00, 8.342532e-03, 8.342532e-03
;

# CURR_ROTOR + CURR_YOKE_1 + 3 : 3.004852e-01 -> 3.558190e-01 m (0.240 -> 0.285 R)
body: CURR_ROTOR + CURR_YOKE_1 + 3, CURR_ROTOR + CURR_YOKE_1 + 3
	,
	5.164309e+00, #9.333012e+01 kg/m; dL=5.533379e-02 m; 
	reference, CURR_ROTOR + CURR_YOKE_1 + BODY + 4,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_YOKE_1 + BODY + 4, 
		diag, 0.000000e+00, 1.317685e-03, 1.317685e-03
;

# CURR_ROTOR + CURR_YOKE_1 + 4 : 3.558190e-01 -> 4.471067e-01 m (0.285 -> 0.358 R)
body: CURR_ROTOR + CURR_YOKE_1 + 4, CURR_ROTOR + CURR_YOKE_1 + 4
	,
	condense, 4
	,
	2.350145e+00, #9.333012e+01 kg/m; dL=2.518099e-02 m; 
	reference, CURR_ROTOR + CURR_YOKE_1 + BODY + 4,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_YOKE_1 + BODY + 4, 
		diag, 0.000000e+00, 1.241821e-04, 1.241821e-04
	,
	7.111755e-01, #9.333012e+01 kg/m; dL=7.620000e-03 m; 
	reference, CURR_ROTOR + CURR_YOKE_1 + BODY + 5,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_YOKE_1 + BODY + 5, 
		diag, 0.000000e+00, 3.441165e-06, 3.441165e-06
	,
	3.200290e+00, #9.333012e+01 kg/m; dL=3.429000e-02 m; 
	reference, CURR_ROTOR + CURR_YOKE_1 + BODY + 6,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_YOKE_1 + BODY + 6, 
		diag, 0.000000e+00, 3.135761e-04, 3.135761e-04
	,
	2.258285e+00, #9.333012e+01 kg/m; dL=2.419675e-02 m; 
	reference, CURR_ROTOR + CURR_YOKE_1 + BODY + 7,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_YOKE_1 + BODY + 7, 
		diag, 0.000000e+00, 1.101823e-04, 1.101823e-04
;

# CURR_ROTOR + CURR_YOKE_1 + 5 : 4.471067e-01 -> 5.042144e-01 m (0.358 -> 0.403 R)
body: CURR_ROTOR + CURR_YOKE_1 + 5, CURR_ROTOR + CURR_YOKE_1 + 5
	,
	condense, 5
	,
	9.420042e-01, #9.333012e+01 kg/m; dL=1.009325e-02 m; 
	reference, CURR_ROTOR + CURR_YOKE_1 + BODY + 7,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_YOKE_1 + BODY + 7, 
		diag, 0.000000e+00, 7.997121e-06, 7.997121e-06
	,
	7.111755e-01, #9.333012e+01 kg/m; dL=7.620000e-03 m; 
	reference, CURR_ROTOR + CURR_YOKE_1 + BODY + 8,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_YOKE_1 + BODY + 8, 
		diag, 0.000000e+00, 3.441165e-06, 3.441165e-06
	,
	2.844702e+00, #9.333012e+01 kg/m; dL=3.048000e-02 m; 
	reference, CURR_ROTOR + CURR_YOKE_1 + BODY + 9,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_YOKE_1 + BODY + 9, 
		diag, 0.000000e+00, 2.202345e-04, 2.202345e-04
	,
	6.354459e-01, #8.339185e+01 kg/m; dL=7.620000e-03 m; 
	reference, CURR_ROTOR + CURR_YOKE_1 + BODY + 10,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_YOKE_1 + BODY + 10, 
		diag, 0.000000e+00, 3.074732e-06, 3.074732e-06
	,
	9.508068e-02, #7.345358e+01 kg/m; dL=1.294432e-03 m; 
	reference, CURR_ROTOR + CURR_YOKE_1 + BODY + 11,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_YOKE_1 + BODY + 11, 
		diag, 0.000000e+00, 1.327607e-08, 1.327607e-08
;

# CURR_ROTOR + CURR_YOKE_1 + 6 : 5.042144e-01 -> 5.790044e-01 m (0.403 -> 0.463 R)
body: CURR_ROTOR + CURR_YOKE_1 + 6, CURR_ROTOR + CURR_YOKE_1 + 6
	,
	condense, 2
	,
	2.423643e+00, #7.345358e+01 kg/m; dL=3.299557e-02 m; 
	reference, CURR_ROTOR + CURR_YOKE_1 + BODY + 11,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_YOKE_1 + BODY + 11, 
		diag, 0.000000e+00, 2.198865e-04, 2.198865e-04
	,
	3.069947e+00, #7.345358e+01 kg/m; dL=4.179439e-02 m; 
	reference, CURR_ROTOR + CURR_YOKE_1 + BODY + 12,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_YOKE_1 + BODY + 12, 
		diag, 0.000000e+00, 4.468745e-04, 4.468745e-04
;

# CURR_ROTOR + CURR_YOKE_1 + 7 : 5.790044e-01 -> 6.096000e-01 m (0.463 -> 0.488 R)
body: CURR_ROTOR + CURR_YOKE_1 + 7, CURR_ROTOR + CURR_YOKE_1 + 7
	,
	2.247358e+00, #7.345358e+01 kg/m; dL=3.059561e-02 m; 
	reference, CURR_ROTOR + CURR_YOKE_1 + BODY + 12,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_YOKE_1 + BODY + 12, 
		diag, 0.000000e+00, 1.753110e-04, 1.753110e-04
;

