
import time
import sys;
# set to path of MBDyn support for python communication
sys.path.append('/usr/local/mbdyn/libexec/mbpy');

import os;
import tempfile;

tmpdir = tempfile.mkdtemp('', '.mbdyn_');
path = tmpdir + '/mbdyn.sock';
print(' path: ', path)

os.environ['MBSOCK'] = path;
os.system('mbdyn -f rigid_rotor -o output_ll_sim02_pitch+13+0_uinf+20 > output_ll_sim02_pitch+13+0_uinf+20.txt 2>&1 &');

from mbc_py_interface import mbcNodal
from numpy import *
import numpy as np

import precice
from precice import *

from mbdynInterface import MBDynInterface
from mbdynAdapter   import MBDynAdapter


#> MBDyn model parameters 
nnodes = 3; # (nEl*2+1)*n_blades   # nnodes exposed by MBDyn through external forces

#> ==============================================================
#> Construct MBDyn/mbc_py interface
#> Initialize MBDyn/mbc_py interface: negotiate and recv()

mbd = MBDynInterface()

mbd.initialize( path=path, verbose=1, nnodes=nnodes, accels=1, \
                dumpAuxFile=True )

#> ==============================================================
#> Build reference "Lagrangian" grid for preCICE-MBDyn solver,
#  from the position negotiated through MBDyn-mbc_py interface
print(" Build structural model ")
print(" ... ")

print(" Initialize MBDyn adapter ")
adapter = MBDynAdapter( mbd, \
           config_file_name='./../../../precice-config.xml' )

if ( adapter.debug ):
  print(' participant: ', adapter.p["name"] )
  print(' solver     : ', adapter.p["mesh"]["name"] )
  print(' fields     : ', adapter.p["fields"] )

#> ==============================================================
#> Start coupled simulation with PreCICE
adapter.runPreCICE()

