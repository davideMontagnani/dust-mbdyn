clear all; clc; close all

filen_mov = './../output_ll_sim02_pitch+13+0_uinf+20.mov';
filen_jnt = './../output_ll_sim02_pitch+13+0_uinf+20.jnt';
filen_nc  = './../output_ll_sim02_pitch+13+0_uinf+20.nc' ;

blade_id = 10000;
joint_id = 15100;

dat_mov = dlmread(filen_mov,'',0,0);
dat_jnt = dlmread(filen_jnt,'',0,0);
phi  = ncread(filen_nc, 'elem.joint.15100.Phi');

x = []; y = []; z = []; th = [];
for i = 1 : size(dat_mov,1)
  if ( dat_mov(i,1) == blade_id )
      x = [ x ; dat_mov(i,2) ] ;
      y = [ y ; dat_mov(i,3) ] ;
      z = [ z ; dat_mov(i,4) ] ;
  end
end
for i = 1 : size(dat_jnt,1)
  if ( dat_jnt(i,1) == joint_id )
      th = [ th ; dat_jnt(i,2) ] ;
  end
end

t = 1:size(x,1);
% figure
% plot(t,x, t,y, t,z ), grid on
% figure
% plot(t,phi ), grid on
figure; hold on
plot(t,x/10, 'LineWidth',2)
plot(t,y/10, 'LineWidth',2)
plot(t,z   , 'LineWidth',2)
plot(t,-phi , 'LineWidth',2)
legend('x/10','y/10','z','D(pitch)'), grid on, hold off
