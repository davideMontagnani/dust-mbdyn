joint: CURR_ROTOR + GROUND_CLAMP, clamp, CURR_ROTOR + GROUND, node, node;

joint: OFFSET + AIRFRAME_CLAMP, total joint, #clamp fix node
	CURR_ROTOR + GROUND, 
		position, reference, node, null,
		position orientation, reference, node, eye,
		rotation orientation, reference, node, eye,
	CURR_ROTOR + AIRFRAME, 
		position, reference, other node, null,
		position orientation, reference, other node, eye,
		rotation orientation, reference, other node, eye,
	position constraint, 
		1, 1, 1, 
		0., 0., 1.,
		cosine, T_0_COLL, pi/(PERIOD_COLL), COLLECTIVE_VERTICAL/2, half, 0.,
	orientation constraint, 
		1, 1, 1, 
		component, 
		cosine, T_0_CYC_X, pi/(PERIOD_CYC_X), PITCH_CYC_X/2, half, 0.,
		cosine, T_0_CYC_Y, pi/(PERIOD_CYC_Y), PITCH_CYC_Y/2, half, 0.,
		0.;

joint: OFFSET + AIRFRAME_SWASH_JOINT, total joint, # airframe to low plate
	CURR_ROTOR + AIRFRAME, 
		position, reference, node, null,
		position orientation, reference, node, eye,
		rotation orientation, reference, node, eye,
	CURR_ROTOR + SW_FIX_NODE, 
		position, reference, other node, null,
		position orientation, reference, other node, eye,
		rotation orientation, reference, other node, eye,
	position constraint, 
		1, 1, 1, 
		null,
	orientation constraint, 
		1, 1, 1,
null;

joint: OFFSET + SWASH_JOINT, total joint, # swashplate joint
	CURR_ROTOR + SW_FIX_NODE, 
		position, reference, node, null,
		position orientation, reference, node, eye,
		rotation orientation, reference, node, eye,
	CURR_ROTOR + SW_ROT_NODE, 
		position, reference, other node, null,
		position orientation, reference, other node, eye,
		rotation orientation, reference, other node, eye,
	position constraint, 
		1, 1, 1, 
		null,
	orientation constraint, 
		1, 1, 0,
		null;

joint: OFFSET + SWASH_HUB_JOINT, total joint, # up plate to hub
	CURR_ROTOR + HUB_NODE, 
		position, reference, node, null,
		position orientation, reference, node, eye,
		rotation orientation, reference, node, eye,
	CURR_ROTOR + SW_ROT_NODE, 
		position, reference, other node, null,
		position orientation, reference, other node, eye,
		rotation orientation, reference, other node, eye,
	position constraint, 
		0, 0, 0, 
		null,
	orientation constraint, 
		0, 0, 1,
		null;

joint: OFFSET + HUB_AIRFRAME, total joint, # ground to hub
	CURR_ROTOR + HUB_NODE, 
		position, reference, node, null,
		position orientation, reference, node, eye,
		rotation orientation, reference, node, eye,
	CURR_ROTOR + GROUND, 
		position, reference, other node, null,
		position orientation, reference, other node, eye,
		rotation orientation, reference, other node, eye,
	position constraint, 
		1, 1, 1, 
		null,
	orientation constraint, 
		0, 0, 0,
		null;

joint: OFFSET + HUB_ANG, angular velocity, # angular velocity
	CURR_ROTOR + HUB_NODE,
	0., 0., 1.,
	cosine, T_0_OMEGA, pi/(PERIOD_OMEGA), (OMEGA_REG - OMEGA_0)/2, half, OMEGA_0;
