clear all; clc; close all

filen_list = { ...
%             './../Postpro/coll+13_cyc+5_uinf+46.3_loads_rotor.dat' , ...
              './../Postpro/sim02_coll+13_cyc+0_uinf+20.6_loads_rotor.dat' , ...
             };

n_sim = length(filen_list);

%> Rotor
R = 3.81;
A = pi * R^2 ;
Om = 61.68;
rho = 1.202;
Vtip = Om * R;

for i = 1 : n_sim

    filen = filen_list{i};

    dat = dlmread(filen, '', 4, 0);
    sim(i).t  = dat(:,1);
    sim(i).F  = dat(:,2:4);
    sim(i).M  = dat(:,5:7);
    
    sim(i).cF = sim(i).F ./ ( rho * A * Vtip^2 );
    sim(i).cM = sim(i).M ./ ( rho * A * Vtip^2 * R );

end


figure
for i = 1 : n_sim
  subplot(1,2,1), plot(sim(i).t, sim(i).F), grid on, xlabel('t [s]'), ylabel('T [N]') ; if (i==1) ; hold on; end
  subplot(1,2,2), plot(sim(i).t, sim(i).M), grid on, xlabel('t [s]'), ylabel('Q [Nm]'); if (i==1) ; hold on; end
end

figure
for i = 1 : n_sim
  subplot(1,2,1), plot(sim(i).t, sim(i).cF), grid on, xlabel('t [s]'), ylabel('cT'); if (i==1) ; hold on; end
  subplot(1,2,2), plot(sim(i).t, sim(i).cM), grid on, xlabel('t [s]'), ylabel('cQ'); if (i==1) ; hold on; end
end
