close all ; clear all ; clc

w = warning ('off','all');

% 
OmegaRPM = 2205. ;
Omega = OmegaRPM / 60. * 2. * pi ;
rho = 1.225 ;
mu = 1.0e-5 ;
a = 343.0 ;

% === blade .xlsx ===
filen = './bladeAirfoils.xlsx' ;
xlRange = 'A2:E20';

% radius , chord , twist , sweep , thickness
num = xlsread(filen) ;

Re = rho .* num(:,2) .* Omega .* num(:,1) ./ mu ;
Ma = Omega .* num(:,1) ./ a ;

epsil = 0.0001 ;
fprintf('\n\n    radius    chord    twist    sweep   thickenss     Re \n')
for i = 1 : size(num,1)
  if ( ( abs( num(i,1) - 0.15 ) < epsil ) || ...
       ( abs( num(i,1) - 0.60 ) < epsil ) || ...
       ( abs( num(i,1) - 0.75 ) < epsil )    )
          
    fprintf(' %9.3f %9.3f %9.3f %9.3f %9.3f %12.2f %9.3f <<<<< \n', num(i,:) , Re(i) , Ma(i) )
  else
    fprintf(' %9.3f %9.3f %9.3f %9.3f %9.3f %12.2f %9.3f \n', num(i,:) , Re(i) , Ma(i) )
  end
end



