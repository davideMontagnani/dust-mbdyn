# Defining the beam/sections notation
# Beam OFFSET + CURR_BLADE_2 + 1, 
#                                       A  beam y 
#                                      /   adimensional axis
#                                     /
#                                    /
#           |            |          |----------|-------------|---------> beam x adimensional axis
#          -1       -1/sqrt(3)      0       1/sqrt(3)        1 
#               ________________________________________________
#             /                                                /
#            /        section I             section II        /
#           /_____________|_____________________|____________/ 
#           |           | | |       |         | | |          |  
#           |           | V |       |         | V |          |  
#           |           |   |       |         |   |          |  
#           |  _________|   |_______|_________|   |__________|__ 
#           | /         |  /        | /       |  /           | /
#           |/          | /         |/        | /            |/
#           /___________|/__________/_________|/_____________/
#          node 1                  node 2              node 3
#      0
#      |---\....\----------------------------------------------------------> global X axis 
#
beam3: OFFSET +CURR_BLADE_2 + 1, # beam name
	OFFSET + CURR_BLADE_2 + 1 , # node 1 name
		position, reference, OFFSET + CURR_BLADE_2 + FEATH + 1, 0., 0.0000000000000000, 0.0000000000000000, 
		orientation, reference, OFFSET + CURR_BLADE_2 + FEATH + 1, eye, # orientation for properties
	OFFSET + CURR_BLADE_2 + 100 + 1 , # node 2 name
		position, reference, OFFSET + CURR_BLADE_2 + FEATH + 1 + 100, 0., -0.0001530821424000, -0.0002961624252000, 
		orientation, reference, OFFSET + CURR_BLADE_2 + FEATH + 1 + 100, eye, # orientation for properties
	OFFSET + CURR_BLADE_2 + 4 , # node 3 name
		position, reference, OFFSET + CURR_BLADE_2 + FEATH + 4, 0., -0.0003061642848000, -0.0005923248504000, 
		orientation, reference, OFFSET + CURR_BLADE_2 + FEATH + 4, eye, # orientation for properties
		from nodes, 
	linear elastic generic, matr, #sec I const law
			9.43232495e+07, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, -0.00000000e+00,
			0.00000000e+00, 3.54598682e+07, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 3.54598682e+07, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 3.03300176e+05, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 5.57617633e+05, -0.00000000e+00,
			-0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, -0.00000000e+00, 1.52389657e+06,
	from nodes, # automatically setting section II orientation
	linear elastic generic, matr, #sec II const law
			2.37999855e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, -0.00000000e+00,
			0.00000000e+00, 8.94736299e+07, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 8.94736299e+07, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 2.34439208e+05, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 8.49656467e+05, -0.00000000e+00,
			-0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, -0.00000000e+00, 2.18842577e+06;

# Defining the beam/sections notation
# Beam OFFSET + CURR_BLADE_2 + 4, 
#                                       A  beam y 
#                                      /   adimensional axis
#                                     /
#                                    /
#           |            |          |----------|-------------|---------> beam x adimensional axis
#          -1       -1/sqrt(3)      0       1/sqrt(3)        1 
#               ________________________________________________
#             /                                                /
#            /        section I             section II        /
#           /_____________|_____________________|____________/ 
#           |           | | |       |         | | |          |  
#           |           | V |       |         | V |          |  
#           |           |   |       |         |   |          |  
#           |  _________|   |_______|_________|   |__________|__ 
#           | /         |  /        | /       |  /           | /
#           |/          | /         |/        | /            |/
#           /___________|/__________/_________|/_____________/
#          node 1                  node 2              node 3
#      0
#      |---\....\----------------------------------------------------------> global X axis 
#
beam3: OFFSET +CURR_BLADE_2 + 4, # beam name
	OFFSET + CURR_BLADE_2 + 4 , # node 1 name
		position, reference, OFFSET + CURR_BLADE_2 + FEATH + 4, 0., -0.0003061642848000, -0.0005923248504000, 
		orientation, reference, OFFSET + CURR_BLADE_2 + FEATH + 4, eye, # orientation for properties
	OFFSET + CURR_BLADE_2 + 100 + 4 , # node 2 name
		position, reference, OFFSET + CURR_BLADE_2 + FEATH + 4 + 100, 0., -0.0010715753016000, -0.0020731371288000, 
		orientation, reference, OFFSET + CURR_BLADE_2 + FEATH + 4 + 100, eye, # orientation for properties
	OFFSET + CURR_BLADE_2 + 6 , # node 3 name
		position, reference, OFFSET + CURR_BLADE_2 + FEATH + 6, 0., -0.0018369863184000, -0.0035539494072000, 
		orientation, reference, OFFSET + CURR_BLADE_2 + FEATH + 6, eye, # orientation for properties
		from nodes, 
	linear elastic generic, matr, #sec I const law
			3.60345829e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, -0.00000000e+00,
			0.00000000e+00, 1.35468357e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 1.35468357e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 1.94318647e+05, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 8.10467733e+05, -0.00000000e+00,
			-0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, -0.00000000e+00, 2.28771334e+06,
	from nodes, # automatically setting section II orientation
	linear elastic generic, matr, #sec II const law
			5.50924638e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, -0.00000000e+00,
			0.00000000e+00, 2.07114526e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 2.07114526e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 1.53568205e+05, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 4.11363285e+05, -0.00000000e+00,
			-0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, -0.00000000e+00, 1.89444284e+06;

# Defining the beam/sections notation
# Beam OFFSET + CURR_BLADE_2 + 6, 
#                                       A  beam y 
#                                      /   adimensional axis
#                                     /
#                                    /
#           |            |          |----------|-------------|---------> beam x adimensional axis
#          -1       -1/sqrt(3)      0       1/sqrt(3)        1 
#               ________________________________________________
#             /                                                /
#            /        section I             section II        /
#           /_____________|_____________________|____________/ 
#           |           | | |       |         | | |          |  
#           |           | V |       |         | V |          |  
#           |           |   |       |         |   |          |  
#           |  _________|   |_______|_________|   |__________|__ 
#           | /         |  /        | /       |  /           | /
#           |/          | /         |/        | /            |/
#           /___________|/__________/_________|/_____________/
#          node 1                  node 2              node 3
#      0
#      |---\....\----------------------------------------------------------> global X axis 
#
beam3: OFFSET +CURR_BLADE_2 + 6, # beam name
	OFFSET + CURR_BLADE_2 + 6 , # node 1 name
		position, reference, OFFSET + CURR_BLADE_2 + FEATH + 6, 0., -0.0018369863184000, -0.0035539494072000, 
		orientation, reference, OFFSET + CURR_BLADE_2 + FEATH + 6, eye, # orientation for properties
	OFFSET + CURR_BLADE_2 + 100 + 6 , # node 2 name
		position, reference, OFFSET + CURR_BLADE_2 + FEATH + 6 + 100, 0., -0.0067356163992000, -0.0130311480804000, 
		orientation, reference, OFFSET + CURR_BLADE_2 + FEATH + 6 + 100, eye, # orientation for properties
	OFFSET + CURR_BLADE_2 + 13 , # node 3 name
		position, reference, OFFSET + CURR_BLADE_2 + FEATH + 13, 0., -0.0116342464800000, -0.0225083467536000, 
		orientation, reference, OFFSET + CURR_BLADE_2 + FEATH + 13, eye, # orientation for properties
		from nodes, 
	linear elastic generic, matr, #sec I const law
			5.96538393e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, -9.38348206e+04, 6.73117667e+05,
			0.00000000e+00, 2.24262554e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 2.24262554e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 1.22157047e+05, 0.00000000e+00, 0.00000000e+00,
			-9.38348206e+04, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 2.23569825e+05, -1.05880655e+02,
			6.73117667e+05, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, -1.05880655e+02, 1.69968737e+06,
	from nodes, # automatically setting section II orientation
	linear elastic generic, matr, #sec II const law
			5.30578679e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, -3.11474839e+05, 2.23434345e+06,
			0.00000000e+00, 1.99465669e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 1.99465669e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 7.70906074e+04, 0.00000000e+00, 0.00000000e+00,
			-3.11474839e+05, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 1.09740764e+05, -1.31166553e+03,
			2.23434345e+06, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, -1.31166553e+03, 1.56745056e+06;

# Defining the beam/sections notation
# Beam OFFSET + CURR_BLADE_2 + 13, 
#                                       A  beam y 
#                                      /   adimensional axis
#                                     /
#                                    /
#           |            |          |----------|-------------|---------> beam x adimensional axis
#          -1       -1/sqrt(3)      0       1/sqrt(3)        1 
#               ________________________________________________
#             /                                                /
#            /        section I             section II        /
#           /_____________|_____________________|____________/ 
#           |           | | |       |         | | |          |  
#           |           | V |       |         | V |          |  
#           |           |   |       |         |   |          |  
#           |  _________|   |_______|_________|   |__________|__ 
#           | /         |  /        | /       |  /           | /
#           |/          | /         |/        | /            |/
#           /___________|/__________/_________|/_____________/
#          node 1                  node 2              node 3
#      0
#      |---\....\----------------------------------------------------------> global X axis 
#
beam3: OFFSET +CURR_BLADE_2 + 13, # beam name
	OFFSET + CURR_BLADE_2 + 13 , # node 1 name
		position, reference, OFFSET + CURR_BLADE_2 + FEATH + 13, 0., -0.0116342464800000, -0.0225083467536000, 
		orientation, reference, OFFSET + CURR_BLADE_2 + FEATH + 13, eye, # orientation for properties
	OFFSET + CURR_BLADE_2 + 100 + 13 , # node 2 name
		position, reference, OFFSET + CURR_BLADE_2 + FEATH + 13 + 100, 0., -0.0164104108164000, -0.0317486153952000, 
		orientation, reference, OFFSET + CURR_BLADE_2 + FEATH + 13 + 100, eye, # orientation for properties
	OFFSET + CURR_BLADE_2 + 20 , # node 3 name
		position, reference, OFFSET + CURR_BLADE_2 + FEATH + 20, 0., -0.0211865751528000, -0.0409888840368000, 
		orientation, reference, OFFSET + CURR_BLADE_2 + FEATH + 20, eye, # orientation for properties
		from nodes, 
	linear elastic generic, matr, #sec I const law
			4.82208161e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, -1.92157299e+05, 3.46273243e+06,
			0.00000000e+00, 1.81281264e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 1.81281264e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 5.14524261e+04, 0.00000000e+00, 0.00000000e+00,
			-1.92157299e+05, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 5.71912955e+04, -1.37987982e+03,
			3.46273243e+06, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, -1.37987982e+03, 1.28856175e+06,
	from nodes, # automatically setting section II orientation
	linear elastic generic, matr, #sec II const law
			4.16017164e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 2.27307686e+05, 5.08039776e+06,
			0.00000000e+00, 1.56397430e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 1.56397430e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 2.64740515e+04, 0.00000000e+00, 0.00000000e+00,
			2.27307686e+05, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 2.79586066e+04, 2.77587937e+03,
			5.08039776e+06, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 2.77587937e+03, 6.62457161e+05;

# Defining the beam/sections notation
# Beam OFFSET + CURR_BLADE_2 + 20, 
#                                       A  beam y 
#                                      /   adimensional axis
#                                     /
#                                    /
#           |            |          |----------|-------------|---------> beam x adimensional axis
#          -1       -1/sqrt(3)      0       1/sqrt(3)        1 
#               ________________________________________________
#             /                                                /
#            /        section I             section II        /
#           /_____________|_____________________|____________/ 
#           |           | | |       |         | | |          |  
#           |           | V |       |         | V |          |  
#           |           |   |       |         |   |          |  
#           |  _________|   |_______|_________|   |__________|__ 
#           | /         |  /        | /       |  /           | /
#           |/          | /         |/        | /            |/
#           /___________|/__________/_________|/_____________/
#          node 1                  node 2              node 3
#      0
#      |---\....\----------------------------------------------------------> global X axis 
#
beam3: OFFSET +CURR_BLADE_2 + 20, # beam name
	OFFSET + CURR_BLADE_2 + 20 , # node 1 name
		position, reference, OFFSET + CURR_BLADE_2 + FEATH + 20, 0., -0.0211865751528000, -0.0409888840368000, 
		orientation, reference, OFFSET + CURR_BLADE_2 + FEATH + 20, eye, # orientation for properties
	OFFSET + CURR_BLADE_2 + 100 + 20 , # node 2 name
		position, reference, OFFSET + CURR_BLADE_2 + FEATH + 20 + 100, 0., -0.0216611298552000, -0.0419069876616000, 
		orientation, reference, OFFSET + CURR_BLADE_2 + FEATH + 20 + 100, eye, # orientation for properties
	OFFSET + CURR_BLADE_2 + 27 , # node 3 name
		position, reference, OFFSET + CURR_BLADE_2 + FEATH + 27, 0., -0.0221356845576000, -0.0428250912864000, 
		orientation, reference, OFFSET + CURR_BLADE_2 + FEATH + 27, eye, # orientation for properties
		from nodes, 
	linear elastic generic, matr, #sec I const law
			4.00304255e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 2.33787857e+05, 3.88032435e+06,
			0.00000000e+00, 1.50490322e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 1.50490322e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 1.54062329e+04, 0.00000000e+00, 0.00000000e+00,
			2.33787857e+05, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 1.60148893e+04, 2.26620802e+03,
			3.88032435e+06, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 2.26620802e+03, 3.82748087e+05,
	from nodes, # automatically setting section II orientation
	linear elastic generic, matr, #sec II const law
			4.23566788e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, -1.09296262e+05, -9.39630862e+05,
			0.00000000e+00, 1.59235635e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 1.59235635e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 1.01467648e+04, 0.00000000e+00, 0.00000000e+00,
			-1.09296262e+05, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 1.25223132e+04, 2.42460325e+02,
			-9.39630862e+05, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 2.42460325e+02, 3.13058741e+05;

# Defining the beam/sections notation
# Beam OFFSET + CURR_BLADE_2 + 27, 
#                                       A  beam y 
#                                      /   adimensional axis
#                                     /
#                                    /
#           |            |          |----------|-------------|---------> beam x adimensional axis
#          -1       -1/sqrt(3)      0       1/sqrt(3)        1 
#               ________________________________________________
#             /                                                /
#            /        section I             section II        /
#           /_____________|_____________________|____________/ 
#           |           | | |       |         | | |          |  
#           |           | V |       |         | V |          |  
#           |           |   |       |         |   |          |  
#           |  _________|   |_______|_________|   |__________|__ 
#           | /         |  /        | /       |  /           | /
#           |/          | /         |/        | /            |/
#           /___________|/__________/_________|/_____________/
#          node 1                  node 2              node 3
#      0
#      |---\....\----------------------------------------------------------> global X axis 
#
beam3: OFFSET +CURR_BLADE_2 + 27, # beam name
	OFFSET + CURR_BLADE_2 + 27 , # node 1 name
		position, reference, OFFSET + CURR_BLADE_2 + FEATH + 27, 0., -0.0221356845576000, -0.0428250912864000, 
		orientation, reference, OFFSET + CURR_BLADE_2 + FEATH + 27, eye, # orientation for properties
	OFFSET + CURR_BLADE_2 + 100 + 27 , # node 2 name
		position, reference, OFFSET + CURR_BLADE_2 + FEATH + 27 + 100, 0., -0.0227786298300000, -0.0440689735332000, 
		orientation, reference, OFFSET + CURR_BLADE_2 + FEATH + 27 + 100, eye, # orientation for properties
	OFFSET + CURR_BLADE_2 + 34 , # node 3 name
		position, reference, OFFSET + CURR_BLADE_2 + FEATH + 34, 0., -0.0234215751024000, -0.0453128557800000, 
		orientation, reference, OFFSET + CURR_BLADE_2 + FEATH + 34, eye, # orientation for properties
		from nodes, 
	linear elastic generic, matr, #sec I const law
			4.26984554e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, -2.92378600e+05, -3.10575273e+06,
			0.00000000e+00, 1.60520509e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 1.60520509e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 7.18172917e+03, 0.00000000e+00, 0.00000000e+00,
			-2.92378600e+05, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 1.06962404e+04, 2.12667093e+03,
			-3.10575273e+06, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 2.12667093e+03, 3.09627445e+05,
	from nodes, # automatically setting section II orientation
	linear elastic generic, matr, #sec II const law
			4.13059530e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, -4.16568109e+05, -3.78909509e+06,
			0.00000000e+00, 1.55285537e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 1.55285537e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 4.34056929e+03, 0.00000000e+00, 0.00000000e+00,
			-4.16568109e+05, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 8.84153228e+03, 3.82128014e+03,
			-3.78909509e+06, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 3.82128014e+03, 2.90558146e+05;

# Defining the beam/sections notation
# Beam OFFSET + CURR_BLADE_2 + 34, 
#                                       A  beam y 
#                                      /   adimensional axis
#                                     /
#                                    /
#           |            |          |----------|-------------|---------> beam x adimensional axis
#          -1       -1/sqrt(3)      0       1/sqrt(3)        1 
#               ________________________________________________
#             /                                                /
#            /        section I             section II        /
#           /_____________|_____________________|____________/ 
#           |           | | |       |         | | |          |  
#           |           | V |       |         | V |          |  
#           |           |   |       |         |   |          |  
#           |  _________|   |_______|_________|   |__________|__ 
#           | /         |  /        | /       |  /           | /
#           |/          | /         |/        | /            |/
#           /___________|/__________/_________|/_____________/
#          node 1                  node 2              node 3
#      0
#      |---\....\----------------------------------------------------------> global X axis 
#
beam3: OFFSET +CURR_BLADE_2 + 34, # beam name
	OFFSET + CURR_BLADE_2 + 34 , # node 1 name
		position, reference, OFFSET + CURR_BLADE_2 + FEATH + 34, 0., -0.0234215751024000, -0.0453128557800000, 
		orientation, reference, OFFSET + CURR_BLADE_2 + FEATH + 34, eye, # orientation for properties
	OFFSET + CURR_BLADE_2 + 100 + 34 , # node 2 name
		position, reference, OFFSET + CURR_BLADE_2 + FEATH + 34 + 100, 0., -0.0244166093328000, -0.0472379117724000, 
		orientation, reference, OFFSET + CURR_BLADE_2 + FEATH + 34 + 100, eye, # orientation for properties
	OFFSET + CURR_BLADE_2 + 41 , # node 3 name
		position, reference, OFFSET + CURR_BLADE_2 + FEATH + 41, 0., -0.0254116435632000, -0.0491629677648000, 
		orientation, reference, OFFSET + CURR_BLADE_2 + FEATH + 41, eye, # orientation for properties
		from nodes, 
	linear elastic generic, matr, #sec I const law
			4.40640775e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, -7.43090608e+05, -5.69790463e+06,
			0.00000000e+00, 1.65654427e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 1.65654427e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 2.68283919e+03, 0.00000000e+00, 0.00000000e+00,
			-7.43090608e+05, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 8.14692084e+03, 9.60886884e+03,
			-5.69790463e+06, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 9.60886884e+03, 2.78958054e+05,
	from nodes, # automatically setting section II orientation
	linear elastic generic, matr, #sec II const law
			5.29919165e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, -1.70352084e+06, -1.12860340e+07,
			0.00000000e+00, 1.99217731e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 1.99217731e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 9.94996206e+02, 0.00000000e+00, 0.00000000e+00,
			-1.70352084e+06, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 1.02710682e+04, 3.62809940e+04,
			-1.12860340e+07, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 3.62809940e+04, 3.38855744e+05;

