aerodynamic beam3:
	OFFSET +CURR_BLADE_3 + 1, # blade aero pan name 
	OFFSET +CURR_BLADE_3 + 1, # blade beam name 
	induced velocity, OFFSET + GROUND,
	reference, OFFSET + CURR_BLADE_3 + AERO + 1, null, 
		1, 0., 1., 0., 3, 1., 0., 0., # orientation wrt to node 
	reference, OFFSET + CURR_BLADE_3 + AERO + 200 + 1, null, 
		1, 0., 1., 0., 3, 1., 0., 0., # orientation wrt to node 
	reference, OFFSET + CURR_BLADE_3 + AERO + 2, null, 
		1, 0., 1., 0., 3, 1., 0., 0., # orientation wrt to node 
	linear, # defining chord shape as linear
		0.507873, # element "root" chord [m]
		0.507873, # element "tip" chord [m]
	linear, # defining AC trend as linear
		0.000000, # element "root" AC pos [adim]
		0.000000, # element "tip" AC pos [adim]
	linear, # defining BC trend as linear
		0.253936, # element "root" BC pos [adim]
		0.253936, # element "tip" BC pos [adim]
	linear, # defining aero surf twist angle as linear
		0.000000, # element "root" twist ang [rad]
		0.000000, # element "tip" twist ang [rad]
		BLADE_3_GAUSS_POINTS, # name of blade Gauss points
		c81, V430301p58,
		unsteady, bielawa, # unsteadiness on
		jacobian, 1; # jacobian output

aerodynamic beam3:
	OFFSET +CURR_BLADE_3 + 4, # blade aero pan name 
	OFFSET +CURR_BLADE_3 + 4, # blade beam name 
	induced velocity, OFFSET + GROUND,
	reference, OFFSET + CURR_BLADE_3 + AERO + 2, null, 
		1, 0., 1., 0., 3, 1., 0., 0., # orientation wrt to node 
	reference, OFFSET + CURR_BLADE_3 + AERO + 200 + 2, null, 
		1, 0., 1., 0., 3, 1., 0., 0., # orientation wrt to node 
	reference, OFFSET + CURR_BLADE_3 + AERO + 3, null, 
		1, 0., 1., 0., 3, 1., 0., 0., # orientation wrt to node 
	linear, # defining chord shape as linear
		0.507873, # element "root" chord [m]
		0.507873, # element "tip" chord [m]
	linear, # defining AC trend as linear
		0.000000, # element "root" AC pos [adim]
		0.000000, # element "tip" AC pos [adim]
	linear, # defining BC trend as linear
		0.253936, # element "root" BC pos [adim]
		0.253936, # element "tip" BC pos [adim]
	linear, # defining aero surf twist angle as linear
		0.000000, # element "root" twist ang [rad]
		0.000000, # element "tip" twist ang [rad]
		BLADE_3_GAUSS_POINTS, # name of blade Gauss points
	c81, multiple, 2, 
		V430301p58, -0.200000, 
		VR73, 1.0, 
		unsteady, bielawa, # unsteadiness on
		jacobian, 1; # jacobian output

aerodynamic beam3:
	OFFSET +CURR_BLADE_3 + 6, # blade aero pan name 
	OFFSET +CURR_BLADE_3 + 6, # blade beam name 
	induced velocity, OFFSET + GROUND,
	reference, OFFSET + CURR_BLADE_3 + AERO + 3, null, 
		1, 0., 1., 0., 3, 1., 0., 0., # orientation wrt to node 
	reference, OFFSET + CURR_BLADE_3 + AERO + 200 + 3, null, 
		1, 0., 1., 0., 3, 1., 0., 0., # orientation wrt to node 
	reference, OFFSET + CURR_BLADE_3 + AERO + 4, null, 
		1, 0., 1., 0., 3, 1., 0., 0., # orientation wrt to node 
	linear, # defining chord shape as linear
		0.507873, # element "root" chord [m]
		0.513207, # element "tip" chord [m]
	linear, # defining AC trend as linear
		0.000000, # element "root" AC pos [adim]
		0.000000, # element "tip" AC pos [adim]
	linear, # defining BC trend as linear
		0.253936, # element "root" BC pos [adim]
		0.256603, # element "tip" BC pos [adim]
	linear, # defining aero surf twist angle as linear
		0.000000, # element "root" twist ang [rad]
		0.000000, # element "tip" twist ang [rad]
		BLADE_3_GAUSS_POINTS, # name of blade Gauss points
		c81, VR73,
		unsteady, bielawa, # unsteadiness on
		jacobian, 1; # jacobian output

aerodynamic beam3:
	OFFSET +CURR_BLADE_3 + 13, # blade aero pan name 
	OFFSET +CURR_BLADE_3 + 13, # blade beam name 
	induced velocity, OFFSET + GROUND,
	reference, OFFSET + CURR_BLADE_3 + AERO + 4, null, 
		1, 0., 1., 0., 3, 1., 0., 0., # orientation wrt to node 
	reference, OFFSET + CURR_BLADE_3 + AERO + 200 + 4, null, 
		1, 0., 1., 0., 3, 1., 0., 0., # orientation wrt to node 
	reference, OFFSET + CURR_BLADE_3 + AERO + 5, null, 
		1, 0., 1., 0., 3, 1., 0., 0., # orientation wrt to node 
	linear, # defining chord shape as linear
		0.513207, # element "root" chord [m]
		0.369951, # element "tip" chord [m]
	linear, # defining AC trend as linear
		0.000000, # element "root" AC pos [adim]
		0.000000, # element "tip" AC pos [adim]
	linear, # defining BC trend as linear
		0.256603, # element "root" BC pos [adim]
		0.184975, # element "tip" BC pos [adim]
	linear, # defining aero surf twist angle as linear
		0.000000, # element "root" twist ang [rad]
		0.000000, # element "tip" twist ang [rad]
		BLADE_3_GAUSS_POINTS, # name of blade Gauss points
	c81, multiple, 2, 
		VR73, 0.858974, 
		VR8mod, 1.0, 
		unsteady, bielawa, # unsteadiness on
		jacobian, 1; # jacobian output

aerodynamic beam3:
	OFFSET +CURR_BLADE_3 + 20, # blade aero pan name 
	OFFSET +CURR_BLADE_3 + 20, # blade beam name 
	induced velocity, OFFSET + GROUND,
	reference, OFFSET + CURR_BLADE_3 + AERO + 5, null, 
		1, 0., 1., 0., 3, 1., 0., 0., # orientation wrt to node 
	reference, OFFSET + CURR_BLADE_3 + AERO + 200 + 5, null, 
		1, 0., 1., 0., 3, 1., 0., 0., # orientation wrt to node 
	reference, OFFSET + CURR_BLADE_3 + AERO + 6, null, 
		1, 0., 1., 0., 3, 1., 0., 0., # orientation wrt to node 
	linear, # defining chord shape as linear
		0.369951, # element "root" chord [m]
		0.345948, # element "tip" chord [m]
	linear, # defining AC trend as linear
		0.000000, # element "root" AC pos [adim]
		0.000000, # element "tip" AC pos [adim]
	linear, # defining BC trend as linear
		0.184975, # element "root" BC pos [adim]
		0.172974, # element "tip" BC pos [adim]
	linear, # defining aero surf twist angle as linear
		0.000000, # element "root" twist ang [rad]
		0.000000, # element "tip" twist ang [rad]
		BLADE_3_GAUSS_POINTS, # name of blade Gauss points
		c81, VR8mod,
		unsteady, bielawa, # unsteadiness on
		jacobian, 1; # jacobian output

aerodynamic beam3:
	OFFSET +CURR_BLADE_3 + 27, # blade aero pan name 
	OFFSET +CURR_BLADE_3 + 27, # blade beam name 
	induced velocity, OFFSET + GROUND,
	reference, OFFSET + CURR_BLADE_3 + AERO + 6, null, 
		1, 0., 1., 0., 3, 1., 0., 0., # orientation wrt to node 
	reference, OFFSET + CURR_BLADE_3 + AERO + 200 + 6, null, 
		1, 0., 1., 0., 3, 1., 0., 0., # orientation wrt to node 
	reference, OFFSET + CURR_BLADE_3 + AERO + 7, null, 
		1, 0., 1., 0., 3, 1., 0., 0., # orientation wrt to node 
	linear, # defining chord shape as linear
		0.345948, # element "root" chord [m]
		0.317373, # element "tip" chord [m]
	linear, # defining AC trend as linear
		0.000000, # element "root" AC pos [adim]
		0.000000, # element "tip" AC pos [adim]
	linear, # defining BC trend as linear
		0.172974, # element "root" BC pos [adim]
		0.158687, # element "tip" BC pos [adim]
	linear, # defining aero surf twist angle as linear
		0.000000, # element "root" twist ang [rad]
		0.000000, # element "tip" twist ang [rad]
		BLADE_3_GAUSS_POINTS, # name of blade Gauss points
		c81, VR8mod,
		unsteady, bielawa, # unsteadiness on
		jacobian, 1; # jacobian output

aerodynamic beam3:
	OFFSET +CURR_BLADE_3 + 34, # blade aero pan name 
	OFFSET +CURR_BLADE_3 + 34, # blade beam name 
	induced velocity, OFFSET + GROUND,
	reference, OFFSET + CURR_BLADE_3 + AERO + 7, null, 
		1, 0., 1., 0., 3, 1., 0., 0., # orientation wrt to node 
	reference, OFFSET + CURR_BLADE_3 + AERO + 200 + 7, null, 
		1, 0., 1., 0., 3, 1., 0., 0., # orientation wrt to node 
	reference, OFFSET + CURR_BLADE_3 + AERO + 8, null, 
		1, 0., 1., 0., 3, 1., 0., 0., # orientation wrt to node 
	linear, # defining chord shape as linear
		0.317373, # element "root" chord [m]
		0.145161, # element "tip" chord [m]
	linear, # defining AC trend as linear
		0.000000, # element "root" AC pos [adim]
		0.000000, # element "tip" AC pos [adim]
	linear, # defining BC trend as linear
		0.158687, # element "root" BC pos [adim]
		0.072580, # element "tip" BC pos [adim]
	linear, # defining aero surf twist angle as linear
		0.000000, # element "root" twist ang [rad]
		0.000000, # element "tip" twist ang [rad]
		BLADE_3_GAUSS_POINTS, # name of blade Gauss points
	c81, multiple, 2, 
		VR8mod, -0.538462, 
		VR8, 1.0, 
		unsteady, bielawa, # unsteadiness on
		jacobian, 1; # jacobian output

