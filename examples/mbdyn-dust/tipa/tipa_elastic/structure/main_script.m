clear;  close all; clc; 
%% Main script 
% Add path
addpath('write_mbdyn','postprocessing','nas2mbdyn','lib'); 
%%
%[file_tip,path] = uigetfile('*.tip'); 
file_tip = 'attila_data_v2.tip';
path = fullfile('..','ATTILA_input'); 

%file_tip = 'XV_15_rotor.tip';

%path = fullfile('..','XV_15_input'); 
[BLADE, ROTOR, RPM, SPRING, POINT, YOKE] = read_input(path, file_tip); 

%% Set path 
%create folder structure, if not present
Tiltrotor_name = 'ATTILA_MBDyn'; % name of the project
%Tiltrotor_name = 'XV_15_MBDyn'; % name of the tiltrotor
if ~exist(Tiltrotor_name, 'dir')
    mkdir(Tiltrotor_name)
    mkdir(Tiltrotor_name, 'Blade')
    mkdir(Tiltrotor_name, 'Yoke')
    mkdir(Tiltrotor_name, 'Rotor')
end

blade_path = fullfile(Tiltrotor_name, 'Blade');
yoke_path = fullfile(Tiltrotor_name, 'Yoke');
rotor_path = fullfile(Tiltrotor_name, 'Rotor');

%% Assembly of the control chain 

%ref_main_rotor = 'ROTOR_CENTER';
ref_main_rotor = 'global';

offset = 10000; 
ROTOR.file.ref = fullfile(rotor_path, 'rotor.ref'); 
ROTOR.file.node = fullfile(rotor_path, 'rotor.nod'); 
ROTOR.file.joint = fullfile(rotor_path,'rotor_joint_wrong.elm'); 
ROTOR.multiblade_flag = 0; % 0 if i want multiblade, 1 if not (FIXME); 
ROTOR.total.node = 5; % modify
ROTOR.Ground.Name = 100; 
ROTOR.Airframe.Name = 200; 
ROTOR.SW_Fix.Name = 300;
ROTOR.SW_Rot.Name = 400; 
ROTOR.Hub.name = 500; % name for mbdyn; 

Omega = 0; % should be replace by rpm
[Label_blade_ref, Label_yoke_ref] = write_rotor_ref(ROTOR, ref_main_rotor, offset); 
Label_rotor_node = write_rotor_node(ROTOR, POINT, Omega, ref_main_rotor);
ROTOR = write_rotor_joint(ROTOR, SPRING, Label_rotor_node); 
% todo write command... 

BLADE.total.nodes = 0; 
BLADE.total.beam = 0; 
BLADE.total.body = 0; 
YOKE.total.nodes = 0; 
YOKE.total.beam = 0; 
YOKE.total.body = 0; 
YOKE.EJX.Value = YOKE.EJX.Value; % test lag
BLADE.GJ.Value = BLADE.GJ.Value; % test torsion 
% BLADE.EJY.Value = 10.*BLADE.EJY.Value; % test rigid blade
% BLADE.EJX.Value = 10.*BLADE.EJX.Value; 

section_blade = BLADE.SEC.Value([1 7 14 21 26 31 36 41 46 48 50 51]); %ATTILA   
section_yoke = YOKE.SEC.Value([1 2 5 8 12 17 21]); 
%section_blade = BLADE.SEC.Value([1 4 6:7:end]);  %XV-15
%section_yoke = YOKE.SEC.Value([1:3:10]);
%section_blade = BLADE.SEC.Value([1:2:51]);  
%section_yoke = YOKE.SEC.Value;
 for i = 1:ROTOR.Nblade.Value
       
    BLADE.file.ref =  fullfile(blade_path, ['blade_' num2str(i) '.ref']);
    BLADE.file.rigid.ref = fullfile(blade_path, ['blade_' num2str(i) '_rigid.ref']);
    BLADE.file.rigid.nod = fullfile(blade_path, ['blade_' num2str(i) '_rigid.nod']);
    BLADE.file.rigid.elm = fullfile(blade_path, ['blade_' num2str(i) '_rigid.elm']);
    
    BLADE.file.node = fullfile(blade_path, ['blade_' num2str(i) '.nod']);
    BLADE.file.beam3 = fullfile(blade_path, ['blade_' num2str(i) '_beam.elm']); 
    BLADE.file.body = fullfile(blade_path, ['blade_' num2str(i) '_body.elm']); 
    BLADE.file.lumped = fullfile(blade_path, ['blade_lumped_' num2str(i) '_body.elm']); 
    BLADE.file.aero.ref = fullfile(blade_path, ['blade_' num2str(i) '_aero.ref']);
    BLADE.file.aero.beam = fullfile(blade_path, ['blade_' num2str(i) '_aero.elm']);
    BLADE.Name = sprintf('BLADE_%d',i); 
    YOKE.file.ref =  fullfile(yoke_path, ['yoke_' num2str(i) '.ref']);
    
    YOKE.file.rigid.ref = fullfile(yoke_path, ['yoke_' num2str(i) '_rigid.ref']);
    YOKE.file.rigid.nod = fullfile(yoke_path, ['yoke_' num2str(i) '_rigid.nod']);
    YOKE.file.rigid.elm = fullfile(yoke_path, ['yoke_' num2str(i) '_rigid.elm']);
    
    YOKE.file.node = fullfile(yoke_path, ['yoke_' num2str(i) '.nod' ]);
    YOKE.file.beam3 = fullfile(yoke_path, ['yoke_' num2str(i) '_beam.elm']); 
    YOKE.file.body = fullfile(yoke_path, ['yoke_' num2str(i) '_body.elm']); 
    YOKE.file.lumped = fullfile(yoke_path, ['yoke_lumped_' num2str(i) '_body.elm']); 
    YOKE.Name = sprintf('YOKE_%d',i);   
    
    Yoke = write_BeamFromTable(YOKE,ROTOR,section_yoke,Label_blade_ref{i},0); 
    Blade = write_BeamFromTable(BLADE,ROTOR,section_blade,Label_blade_ref{i},0.069612500000000, path);
    
    % Connect blade and yoke togheter 
    yokeblade.file = fullfile(rotor_path, ['yoke_blade_wrong' num2str(i) '.elm']);
    Label_yoke_blade{i} = write_total_yoke_blade(Yoke, Blade, ROTOR, Label_blade_ref{i}, yokeblade);
    % PitchLink: connect blade to rotor 
    rotorblade.file = fullfile(rotor_path, ['pitchlink_' num2str(i) '.elm']);
    Label_pitch_link{i}  = write_pitch_link(ROTOR, Blade, POINT, SPRING, ref_main_rotor, i, Label_rotor_node, rotorblade); 
    % Gimbal: connect hub to yoke
    rotoryoke.file = fullfile(rotor_path, ['gimbal_' num2str(i) '.elm']); 
    Label_gimbal{i} = write_gimbal_joint(Yoke, Label_rotor_node, rotoryoke); 
    
       
 end
fclose all; 


MBDYN.param.type = 'initial value';
MBDYN.param.time_step = 1e-3; 
MBDYN.param.initial_time = 0.0; 
MBDYN.param.final_time = 1.0;
MBDYN.param.n_steps = 100;
MBDYN.param.max_iteration = 50;
MBDYN.param.tolerance = 1e-3;
MBDYN.param.der_tolerance = 1e38;
MBDYN.param.der_max_iteration = 30;
MBDYN.param.der_coefficients = 1;
MBDYN.output = '';
MBDYN.analysis = 'eigenanalysis';
MBDYN.param.eig_time = MBDYN.param.initial_time;
MBDYN.param.eig_freq = 200; 








