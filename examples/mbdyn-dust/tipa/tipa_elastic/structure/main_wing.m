clear; close all; clc;
%% The purpose of this script is to convert a generic NASTRAN stick model into a MBDyn
%% model using the finite volume beam formulation 
%% TODO: improve the general organization... (label in particular) 
%file_nas = fullfile('..','..','XV_15\NASTRAN_Cocco\XV15_SOL103.nas');
addpath('write_mbdyn','postprocessing','nas2mbdyn','lib'); 
[file_nas,path] = uigetfile('*.bdf'); 
file_nas = fullfile(path, file_nas); 
NASTRAN = read_nas(file_nas); 

%MBDYN.file.ref_frame_main = 'test_cor2dr2ref.ref';
offset = 10000;  
MBDYN.main_ref = 'global'; % maybe put into the function 
%% File initialization 
MBDYN.file.joint = [];
MBDYN.file.beams = [];
MBDYN.file.bodies = [];
MBDYN.file.nodes = [];
MBDYN.file.ref_frame_nodes = [];
MBDYN.file.beams_aero = []; 
%% Converting the Nastran CORD2R card into a reference MBDyn card this will be the basic reference of the beams that will be created
if isfield(NASTRAN,'CORD2R')
    MBDYN.file.ref_frame_main = 'test_cor2dr2ref.ref';
    MBDYN.name_part = {'RightWingStick'; 'RightWingHinge'; 'LeftWingStick'; 'LeftWignHinge'; ...
                  'ElevatorStick'; 'ElevatorHinge'; 'RudderStick'; 'RudderHinge'; ...
                  'RightWingExtreme'; 'LeftWingExtreme'};
    MBDYN.offset = offset.*[1:1:numel(MBDYN.name_part)]; % can be changed
    NASTRAN = cord2r2ref(NASTRAN,MBDYN); % extract rotation matrix for each cord2r 
    
else 
    MBDYN.offset = 100000; 
end 

[NODE,K,MASS] = set_cbar_properties(NASTRAN); 
%% Converting the Nastran GRID Card into a reference MBDyn card: this will speed up the assembly of the beam nodes.

mat_node = [NODE.ID', NODE.CP', NODE.X1', NODE.X2', NODE.X3'];
[ID,NODE.ia,NODE.ic] =  unique(NODE.ID','stable','rows'); % delete all duplicated nodes GA == GB  
REF.ID = mat_node(NODE.ia,1); 
REF.CP = mat_node(NODE.ia,2);                       
REF.X1 = mat_node(NODE.ia,3); 
REF.X2 = mat_node(NODE.ia,4);
REF.X3 = mat_node(NODE.ia,5); 
MBDYN.file.ref_frame_nodes = [MBDYN.file.ref_frame_nodes; {'wing.ref'}]; 
Label_ref = grid2ref(NASTRAN,REF,MBDYN,offset);    

%% Write mbdyn nodes 
MBDYN.file.nodes = [MBDYN.file.nodes, {'wing.nod'}];
MBDYN.total.nodes = 0; 
[Label_nodes,MBDYN] = write_mbdyn_nodes(MBDYN,Label_ref,REF); 

%% Assemble structure for beam elements 
%% they are written as node1 node2 node3 
BEAM.nodes_label = Label_nodes(NODE.ic);
BEAM.nodes(:,1) = BEAM.nodes_label(1:3:end-2); %GA
BEAM.nodes(:,2) = BEAM.nodes_label(2:3:end-1); %GC
BEAM.nodes(:,3) = BEAM.nodes_label(3:3:end);   %GB
BEAM.ref_label = Label_ref(NODE.ic); 
BEAM.ref(:,1) = BEAM.ref_label(1:3:end-2); % reference GA
BEAM.ref(:,2) = BEAM.ref_label(2:3:end-1); % reference GC
BEAM.ref(:,3) = BEAM.ref_label(3:3:end);   % reference GB 
BEAM.K = K;
BEAM.ID = NASTRAN.CBAR.ID; 
BEAM.damp = 0.01; % damping factor fictitous just to test
MBDYN.file.beams = [MBDYN.file.beams; {'wing_beam.elm'}];  
[MBDYN, Label_beam]= write_cbar2beam(BEAM,MBDYN,offset); 

%% write Aero beam 
leading_edge = [11001:11009]; % id nastran leading edge 
trailing_edge = [21001:21009]; % id nastran trailing edge 
beam_node = [1001:1009]; 
for i = 1:numel(leading_edge)
    id_leading(i) = find(NASTRAN.GRID.ID == leading_edge(i));
    id_trailing(i) = find(NASTRAN.GRID.ID == trailing_edge(i));
    coord_leading(i, :) = [NASTRAN.GRID.X1(id_leading(i)), NASTRAN.GRID.X2(id_leading(i)), NASTRAN.GRID.X3(id_leading(i))];
    coord_trailing(i, :) = [NASTRAN.GRID.X1(id_trailing(i)), NASTRAN.GRID.X2(id_trailing(i)), NASTRAN.GRID.X3(id_trailing(i))];
    chord(i) = norm(coord_leading(i, :) - coord_trailing(i, :));
    coord_ref(i, :) = coord_leading(i, :) + 0.25 * [coord_trailing(i, :) - coord_leading(i, :)];       
    Cp(i) = NASTRAN.GRID.CP(id_leading(i)); 
end

% mid node
for i = 1:numel(leading_edge)-1
    coord_mid(i,:) = (coord_ref(i,:)+coord_ref(i+1,:))/2;
    Cp_mid(i) = Cp(i);
    id_mid(i) = 1000+id_leading(i);
end

% write reference system 
Grid.ID = [id_leading'; id_mid'];
Grid.CP = [Cp'; Cp_mid']; 
Grid.X1 = [coord_ref(:,1); coord_mid(:,1)];
Grid.X2 = [coord_ref(:,2); coord_mid(:,2)]; 
Grid.X3 = [coord_ref(:,3); coord_mid(:,3)];
MBDYN.file.ref_frame_nodes = [MBDYN.file.ref_frame_nodes; {'wing_aero.ref'}]; 
offset = 2000; 
Label_ref = grid2ref(NASTRAN,Grid,MBDYN,offset);  
MBDYN.file.beams_aero = [MBDYN.file.beams_aero; {'wing_aero.elm'}];
MBDYN.total.aero = 0; 
MBDYN = write_beam_aerobeam_nas(BEAM,Label_ref,beam_node,Label_beam,chord,MBDYN); 




%% write body for beam
BEAM.mass = MASS; 
MBDYN.file.bodies = {'wing_body.elm'}; 
MBDYN = write_mass2body(BEAM,MBDYN,offset);

NODE.written = reshape(BEAM.nodes,[1,numel(BEAM.nodes)]) ;
%% write convert conm2 into body 
CONM2.G = NASTRAN.CONM2.G; 

% check if the Grid point is already defined for the beam model
grid2write = [];
for i = 1:numel(CONM2.G)
    if ~any(strcmp(num2str(CONM2.G(i)),NODE.written))
        grid2write(i) = CONM2.G(i);
    end
end


if ~isempty(grid2write)
    Grid.ID = grid2write;
    for i = 1:numel(Grid.ID)
        [m,n] = find(NASTRAN.GRID.ID == Grid.ID(i));
        Grid.CP(i) = NASTRAN.GRID.CP(n);
        Grid.X1(i) = NASTRAN.GRID.X1(n);
        Grid.X2(i) = NASTRAN.GRID.X2(n);
        Grid.X3(i) = NASTRAN.GRID.X3(n);
    end
    MBDYN.file.ref_frame_nodes = [MBDYN.file.ref_frame_nodes; {'wing_body.ref'}]; 
    offset = 1000; 
    Label_ref = grid2ref(NASTRAN,Grid,MBDYN,offset);    
    MBDYN.file.nodes =  [MBDYN.file.nodes; {'wing_body.nod'}]; 
    [Label_nodes,MBDYN] = write_mbdyn_nodes(MBDYN,Label_ref,Grid);  
end

MBDYN.file.bodies = [MBDYN.file.bodies; {'wing_body_lumped.elm'}]; 
MBDYN = conm22body(NASTRAN,MBDYN,offset); 
NODE.written = [NODE.written, Label_nodes]; 
%% convert RBE3 into distance joint
% check if the grid point has been already defined 
MBDYN.total.joint = 0;
if isfield(NASTRAN,'RBE3')

    grid2write = [];
    for i = 1:numel(NASTRAN.RBE3.EID)
        if ~any(strcmp(num2str(NASTRAN.RBE3.REFGRID(i)),NODE.written))
            grid2write.refgrid(i) = NASTRAN.RBE3.REFGRID(i);
        end
        if ~any(strcmp(num2str(NASTRAN.RBE3.G1(i)),NODE.written))
            grid2write.g1(i) = NASTRAN.RBE3.G1(i);
        end
        if ~any(strcmp(num2str(NASTRAN.RBE3.G2(i)),NODE.written))
            grid2write.g2(i) = NASTRAN.RBE3.G2(i);
        end
    end

    if ~isempty(grid2write)
        Grid.ID = [grid2write.refgrid, grid2write.g1, grid2write.g2];
        for i = 1:numel(Grid.ID)
            [m,n] = find(NASTRAN.GRID.ID == Grid.ID(i));
            Grid.CP(i) = NASTRAN.GRID.CP(n);
            Grid.X1(i) = NASTRAN.GRID.X1(n);
            Grid.X2(i) = NASTRAN.GRID.X2(n);
            Grid.X3(i) = NASTRAN.GRID.X3(n);
        end
        MBDYN.file.ref_frame_nodes = [MBDYN.file.ref_frame_nodes; {'connection_body_beam.ref'}]; 
        offset = 2000; 
        Label_ref = grid2ref(NASTRAN,Grid,MBDYN,offset);    
        MBDYN.file.nodes = [MBDYN.file.nodes; {'connection_body_beam.nod'}]; 
        [Label_nodes,MBDYN] = write_mbdyn_nodes(MBDYN,Label_ref,Grid);  
    end
     
    MBDYN.file.joint = [MBDYN.file.joint; {'connection_body_beam.elm'}];
    MBDYN = rbe3todistance(NASTRAN,MBDYN,offset);
    NODE.written = [NODE.written, Label_nodes]; 
end

%% convert RBE2 into total joint 
% check if the grid point has been already defined 
grid2write = []; 
grid2write.GN = []; 
grid2write.g1 = [];
grid2write.g2 = [];
if  isfield(NASTRAN,'RBE2')
    for i = 1:numel(NASTRAN.RBE2.EID)
        if ~any(strcmp(num2str(NASTRAN.RBE2.GN(i)),NODE.written))
            grid2write.GN(i) = NASTRAN.RBE2.GN(i);
        end
        if ~any(strcmp(num2str(NASTRAN.RBE2.GM1(i)),NODE.written))
            grid2write.g1(i) = NASTRAN.RBE2.GM1(i);
        end
        if ~any(strcmp(num2str(NASTRAN.RBE2.GM2(i)),NODE.written))
            grid2write.g2(i) = NASTRAN.RBE2.GM2(i);   
        end    
    end

    if ~isempty(grid2write.GN)
            Grid.ID = nonzeros(unique([grid2write.GN, grid2write.g1, grid2write.g2])); 
            for i = 1:numel(Grid.ID)
                [m,n] = find(NASTRAN.GRID.ID == Grid.ID(i));
                Grid.CP(i) = NASTRAN.GRID.CP(n);
                Grid.X1(i) = NASTRAN.GRID.X1(n);
                Grid.X2(i) = NASTRAN.GRID.X2(n);
                Grid.X3(i) = NASTRAN.GRID.X3(n);
            end
        MBDYN.file.ref_frame_nodes = [MBDYN.file.ref_frame_nodes;  {'wing_joint.ref'}]; 
        offset = 3000; 
        Label_ref = grid2ref(NASTRAN,Grid,MBDYN,offset);    
        MBDYN.file.nodes = [MBDYN.file.nodes; {'wing_joint.nod'}]; 
        [Label_nodes,MBDYN] = write_mbdyn_nodes(MBDYN,Label_ref,Grid);  
        NODE.written = [NODE.written, Label_nodes]; 
    end

    
    MBDYN.file.joint  = [MBDYN.file.joint; {'wing_joint.elm'}]; 
    offset = 100000; 
    MBDYN = write_nas2totjoint(NASTRAN,MBDYN,offset);
end

%% Convert rotational CELAS2 into deformable Axial joint

grid2write = []; 
grid2write.g1 = [];
grid2write.g2 = [];

if isfield(NASTRAN,'CELAS2') 
    % check if the node has already been written (at this point should be unecessary)
    for i = 1:numel(NASTRAN.CELAS2.EID)
        if ~any(strcmp(num2str(NASTRAN.CELAS2.G1(i)),NODE.written))
            grid2write.g1(i) = NASTRAN.CELAS2.G1(i);
        end
        if ~any(strcmp(num2str(NASTRAN.CELAS2.G2(i)),NODE.written))
            grid2write.g2(i) = NASTRAN.CELAS2.G2(i);
        end
    end

    if ~isempty(grid2write.g1)
            Grid.ID = nonzeros(unique([grid2write.g1, grid2write.g2])); 
            for i = 1:numel(Grid.ID)
                [m,n] = find(NASTRAN.GRID.ID == Grid.ID(i));
                Grid.CP(i) = NASTRAN.GRID.CP(n);
                Grid.X1(i) = NASTRAN.GRID.X1(n);
                Grid.X2(i) = NASTRAN.GRID.X2(n);
                Grid.X3(i) = NASTRAN.GRID.X3(n);
            end
        MBDYN.file.ref_frame_nodes = [MBDYN.file.ref_frame_nodes;  {'spring.ref'}]; 
        offset = 4000; 
        Label_ref = grid2ref(NASTRAN,Grid,MBDYN,offset);    
        MBDYN.file.nodes = [MBDYN.file.nodes; {'spring.nod'}]; 
        [Label_nodes,MBDYN] = write_mbdyn_nodes(MBDYN,Label_ref,Grid);  
        NODE.written = [NODE.written, Label_nodes]; 
    end
        MBDYN.file.joint  = [MBDYN.file.joint; {'wing_spring.elm'}]; 
        offset = 200000; 
        MBDYN = celas2defaxial(NASTRAN,MBDYN,offset);
 
end

%% write Clamp 
offset = 200000;
MBDYN.file.joint  = [MBDYN.file.joint; {'wing_clamp.elm'}]; 
MBDYN = spc2clamp(NASTRAN,MBDYN,offset);

%% write main MBDYN file

MBDYN.main.file = 'main_wing.mbd';

% mbdyn parameters 
MBDYN.param.type = 'initial value';
MBDYN.param.time_step = 1e-3; 
MBDYN.param.initial_time = 0.0; 
MBDYN.param.final_time = 1.0;
MBDYN.param.n_steps = 100;
MBDYN.param.max_iteration = 50;
MBDYN.param.tolerance = 1e-3;
MBDYN.param.der_tolerance = 1e38;
MBDYN.param.der_max_iteration = 30;
MBDYN.param.der_coefficients = 1;
MBDYN.output = '';
MBDYN.analysis = 'eigenanalysis';
MBDYN.param.eig_time = MBDYN.param.initial_time;
MBDYN.param.eig_freq = 200;

MBDyn_stick_writer(MBDYN) 
fclose all; 






