function [Label_node, BEAM] = write_beam_node(BEAM,Label_axis)
    
    fileID = fopen(BEAM.file.node, 'wt'); 
    % All dynamic nodes are placed in the Neutral axis, 
    % then the beam axis will be placed in the shear center axis
   
    Lab = BEAM.Name;
    fprintf(fileID, '# this is a matlab generated file to give the nodes their names and cards\n');
    fprintf(fileID, '# %s\n', pwd); 
    fprintf(fileID, '# %s\n', datetime('now')); 
    fprintf(fileID, '# DO NOT MODIFY \n\n'); 
    fprintf(fileID, '\n'); 
    tot_nodes = BEAM.total.nodes;
    
    % place nodes at in the neutral axis 
    for i = 1:numel(Label_axis.neut)
        fprintf(fileID, 'structural:  CURR_ROTOR + CURR_%s + %d, dynamic,\n',Lab,i); 
        Label_node{i} = sprintf('CURR_ROTOR + CURR_%s + %d', Lab, i); 
	    fprintf(fileID, '\treference, %s, null, #abs pos,\n',Label_axis.neut{i});
	    fprintf(fileID, '\treference, %s, eye,  #abs orient\n',Label_axis.neut{i});
	    fprintf(fileID, '\treference, %s, null, #abs initial velocity\n',Label_axis.neut{i});
	    fprintf(fileID, '\treference, %s, null; #abs initial ang speed\n',Label_axis.neut{i});
	    fprintf(fileID, '\n');
        tot_nodes = tot_nodes + 1;
    end 
    
    fclose(fileID);
    BEAM.total.nodes = tot_nodes; 
end