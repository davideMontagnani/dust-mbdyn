function Label_rotor_node = write_rotor_node(ROTOR, POINT, Omega, ref_main_rotor)

    fileID = fopen(ROTOR.file.node, 'wt');

    % preparing geometry
    if strcmpi(POINT.SwashPlate.Unit, 'MM')
        SwashPlate = POINT.SwashPlate.COORD .* 1e-3;
    else
        SwashPlate = POINT.SwashPlate.COORD; % [X Y Z]
    end
    
    fprintf(fileID, 'set: const integer GROUND = %d; # node name\n', ROTOR.Ground.Name);
    fprintf(fileID, '\n');
    Label_rotor_node{1} = sprintf('CURR_ROTOR + GROUND');
    fprintf(fileID, 'structural:  CURR_ROTOR + GROUND, static,\n');
    fprintf(fileID, '\treference, %s, 0., 0., %.16f, #abs pos\n', ref_main_rotor, SwashPlate(3)); %?????? FIXME!!!!  
    fprintf(fileID, '\treference, %s, eye, #abs orient\n', ref_main_rotor);
    fprintf(fileID, '\treference, %s, null, #abs initial velo\n', ref_main_rotor);
    fprintf(fileID, '\treference, %s, 0., 0., 0; #abs initial ang speed\n', ref_main_rotor); %???? check with multiblade
    fprintf(fileID, '\n');
    fprintf(fileID, 'set: const integer AIRFRAME = %d; # node name\n', ROTOR.Airframe.Name);
    fprintf(fileID, '\n');
    Label_rotor_node{2} = sprintf('CURR_ROTOR + AIRFRAME');
    fprintf(fileID, 'structural:  CURR_ROTOR + AIRFRAME, static,\n');
    fprintf(fileID, '\treference, %s, 0., 0., %.16f, #abs pos\n', ref_main_rotor, SwashPlate(3)); %?????? FIXME!!!!  
    fprintf(fileID, '\treference, %s, eye, #abs orient\n', ref_main_rotor);
    fprintf(fileID, '\treference, %s, null, #abs initial velo\n', ref_main_rotor);
    fprintf(fileID, '\treference, %s, 0., 0., -%.16f; #abs initial ang speed\n', ref_main_rotor, Omega); %???? check with multiblade
    fprintf(fileID, '\n');
    fprintf(fileID, 'set: const integer SW_FIX_NODE = %d; # node name\n', ROTOR.SW_Fix.Name);
    fprintf(fileID, 'structural:  CURR_ROTOR + SW_FIX_NODE, static,\n');
    Label_rotor_node{3} = sprintf('CURR_ROTOR + SW_FIX_NODE');
    fprintf(fileID, '\treference, %s, 0., 0., %.16f, #abs pos\n', ref_main_rotor, SwashPlate(3));
    fprintf(fileID, '\treference, %s, eye, #abs orientation\n', ref_main_rotor);
    fprintf(fileID, '\treference, %s, null, #abs initial ang speed\n', ref_main_rotor);
    fprintf(fileID, '\treference, %s, null; #abs initial velo\n', ref_main_rotor);
    fprintf(fileID, '\n');
    fprintf(fileID, 'set: const integer SW_ROT_NODE = %d; # node name\n', ROTOR.SW_Rot.Name);
    fprintf(fileID, '\n');
    fprintf(fileID, 'structural: CURR_ROTOR + SW_ROT_NODE, dynamic,\n');
    Label_rotor_node{4} = sprintf('CURR_ROTOR + SW_ROT_NODE');
    fprintf(fileID, '\treference, %s, 0., 0., %.16f, #abs pos\n', ref_main_rotor, SwashPlate(3));
    fprintf(fileID, '\treference, %s, eye, #abs orientation\n', ref_main_rotor);
    fprintf(fileID, '\treference, %s, null, #abs initial ang speed\n', ref_main_rotor);
    fprintf(fileID, '\treference, %s, null; #abs initial velo\n', ref_main_rotor);
    fprintf(fileID, '\n');
    fprintf(fileID, 'set: const integer HUB_NODE = %d; # node name\n', ROTOR.Hub.name);
    fprintf(fileID, '\n');
    fprintf(fileID, 'structural: CURR_ROTOR + HUB_NODE, static,\n');
    Label_rotor_node{5} = sprintf('CURR_ROTOR + HUB_NODE');
    fprintf(fileID, '\treference, %s, 0., 0., %.16f, #abs pos\n', ref_main_rotor, -ROTOR.Underslung.Value); 
    fprintf(fileID, '\treference, %s, eye, #abs orient\n', ref_main_rotor);
    fprintf(fileID, '\treference, %s, null, #abs initial velo\n', ref_main_rotor);
    fprintf(fileID, '\treference, %s, null; #abs ang speed\n', ref_main_rotor);
    fclose(fileID);

end
