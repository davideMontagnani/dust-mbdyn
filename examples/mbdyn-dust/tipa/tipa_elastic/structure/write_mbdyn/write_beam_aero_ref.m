function Label_axis = write_beam_aero_ref(BEAM, Label_axis, node_idx, lab_basic_ref)
    
    Sweep = -BEAM.Aero.Geometry.Sweep.Value(node_idx);
    Anhedral = BEAM.Aero.Geometry.Anhedral.Value(node_idx); 
    Label_feath = Label_axis.feath(node_idx); 
    Lab = BEAM.Name; % Name of the part

    fileID = fopen(BEAM.file.aero.ref, 'wt');
    Axis = {'AERO'};
    
    fprintf(fileID, '# %s\n', pwd);
    fprintf(fileID, '# %s\n', datetime('now'));
    fprintf(fileID, '# DO NOT MODIFY \n\n');
    fprintf(fileID, '\n');

    for i = 1:numel(node_idx)
        fprintf(fileID, 'reference: CURR_ROTOR + CURR_%s + %s + %d, #gen ref\n', Lab, Axis{1}, i);
        Label_axis.aero{i} = sprintf('CURR_ROTOR + CURR_%s + %s + %d', Lab, Axis{1}, i);
        fprintf(fileID, '\treference, %s, 0., %.16f, %.16f,\n', Label_feath{i}, Sweep(i), Anhedral(i)); % position offset wrt to principal axis
        fprintf(fileID, '\treference, %s, eye, \n', Label_feath{i}); 
        fprintf(fileID, '\treference, %s, null,\n', lab_basic_ref);
        fprintf(fileID, '\treference, %s, null;\n\n', lab_basic_ref);
    end

    fclose(fileID); 
end
