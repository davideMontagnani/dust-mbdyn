function [Label_blade_ref, Label_yoke_ref] = write_rotor_ref(ROTOR, ref_main_rotor, offset)
    % generate main ref frame for blades and yoke 
    fileID = fopen(ROTOR.file.ref, 'wt');

    if strcmpi(ROTOR.Precone.Unit, 'DEG')
        Beta = deg2rad(ROTOR.Precone.Value);
    else
        Beta = ROTOR.Precone.Value; % Precone angle
    end

    if strcmpi(ROTOR.Underslung.Unit, 'MM')
        Undslg = ROTOR.Underslung.Value * 1e-3;
    else
        Undslg = ROTOR.Underslung.Value; % Underslung
    end

    Nblade = ROTOR.Nblade.Value; % Blade Number
    DPsi = 2 * pi / Nblade; % Azimut between blades
    Psi = linspace(0, DPsi * (Nblade - 1), Nblade); % azimut location
    offset = linspace(offset, (Nblade) * offset, Nblade); % label offset

    for i = 1:Nblade

        R1 = [0, 1, 0;
            -1, 0, 0;
            0, 0, 1]; % 1 orientation matrix (x blade align with y global)

        R2 = [cos(Psi(i)), sin(Psi(i)), 0;
            -sin(Psi(i)), cos(Psi(i)), 0;
            0, 0, 1]; % second orientation matrix (different azimut)

        R3 = [cos(Beta), 0, sin(Beta);
            0, 1, 0;
            -sin(Beta), 0, cos(Beta)]; % third orientation matrix (precone)

        R = R3 * R2 * R1; % total orientation matrix

        fprintf(fileID, 'set: const integer BLADE_%d_MAIN  = %d; #set name\n', i, offset(i));
        fprintf(fileID, 'reference: OFFSET + BLADE_%d_MAIN, #gen ref\n', i);
        Label_blade_ref{i} = sprintf('OFFSET + BLADE_%d_MAIN', i);
        fprintf(fileID, '\treference, %s, %.5f,%.5f,%.5f,\n', ref_main_rotor, 0, 0, -Undslg);
        fprintf(fileID, '\treference, %s,\n', ref_main_rotor); %parameters to set blade root angles
        fprintf(fileID, '\t1, %.16f, %.16f, %.16f, \n', R(1, :));
        fprintf(fileID, '\t2, %.16f, %.16f, %.16f, \n', R(2, :));
        fprintf(fileID, '\treference, %s, null,\n', ref_main_rotor);
        fprintf(fileID, '\treference, %s, null;\n', ref_main_rotor);
        
        R_y = R2 * R1; % yoke does not have precone
        fprintf(fileID, 'set: const integer YOKE_%d_MAIN  = %d; #set name\n', i, 10*offset(i));
        fprintf(fileID, 'reference: OFFSET + YOKE_%d_MAIN, #gen ref\n', i);
        Label_yoke_ref{i} = sprintf('OFFSET + YOKE_%d_MAIN', i);
        fprintf(fileID, '\treference, %s, %.5f,%.5f,%.5f,\n', ref_main_rotor, 0, 0, -Undslg);
        fprintf(fileID, '\treference, %s,\n', ref_main_rotor); %parameters to set blade root angles
        fprintf(fileID, '\t1, %.16f, %.16f, %.16f, \n', R_y(1, :));
        fprintf(fileID, '\t2, %.16f, %.16f, %.16f, \n', R_y(2, :));
        fprintf(fileID, '\treference, %s, null,\n', ref_main_rotor);
        fprintf(fileID, '\treference, %s, null;\n', ref_main_rotor);
        
        
    end

    fclose(fileID);
end
