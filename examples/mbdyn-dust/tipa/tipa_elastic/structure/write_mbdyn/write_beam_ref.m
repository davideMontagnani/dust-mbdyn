function Label_axis = write_beam_ref(BEAM, lab_basic_ref, node_idx)
    % The data are already sorted
    % Strategy: the main axis is the feathering axis, the nodes are
    % attached to the neutral axis that have an offest wrt to the
    % feathering axis of YNA and ZNA. 
    % Each body has its own reference system wrt to the feathering axis. 
        
    STA = BEAM.STA.Value; 
    YNA = BEAM.YNA.Value(node_idx); 
    ZNA = BEAM.ZNA.Value(node_idx); 
    XNA = BEAM.STA.Value(node_idx); 
    
    YCG = BEAM.YCG.Value;
    ZCG = BEAM.ZCG.Value; 
    Rotapi = BEAM.ROTAPI.Value; 
    Lab = BEAM.Name; 
       
    if isfield(BEAM,'Aero')
        Twist = BEAM.Aero.Geometry.Twist.Value;
    else 
        Twist = zeros(length(STA), 1);
    end
    
    fileID = fopen(BEAM.file.ref, 'wt');
    Axis = {'FEATH'; 'NEUTR'; 'BODY'}; %TODO axis label (should be an input on the tip file)
    
    fprintf(fileID, '# %s\n', pwd); 
    fprintf(fileID, '# %s\n', datetime('now')); 
    fprintf(fileID, '# Do not modify \n\n'); 
    fprintf(fileID, '# Feathering Axis reference system \n'); 
    
    % Feathering axis ref frame
    for i = 1:numel(STA)
        fprintf(fileID, 'reference: CURR_ROTOR + CURR_%s + %s + %d, #gen ref\n', Lab, Axis{1}, i);
        Label_axis.feath{i} = sprintf('CURR_ROTOR + CURR_%s + %s + %d', Lab, Axis{1}, i);
        fprintf(fileID, '\treference, %s, %.16f, 0., 0.,\n', lab_basic_ref, STA(i)); % position offset wrt to main reference frame
        fprintf(fileID, '\treference, %s, \n', lab_basic_ref); % orientation matrix
        fprintf(fileID, '\t\t1, 1., \t0., \t0.,\n');
        fprintf(fileID, '\t\t2, 0., %.16f, %.16f,\n', cos(Twist(i)), sin(Twist(i)));
        fprintf(fileID, '\treference, %s, null,\n', lab_basic_ref);
        fprintf(fileID, '\treference, %s, null;\n\n', lab_basic_ref);
    end

%     for i = 1:numel(STA)-1 % place mid point
%         X_feath(i) = (STA(i) + STA(i + 1))/2; 
%         Twist_m = (Twist(i) + Twist(i + 1))/2; 
%         fprintf(fileID, 'reference: CURR_ROTOR + CURR_%s + %s + %d + 1000, #gen ref\n', Lab, Axis{1}, i);
%         Label_axis.feath_mid{i} = sprintf('OFFSET    + CURR_%s + %s + %d + 1000', Lab, Axis{1}, i);
%         fprintf(fileID, '\treference, %s, %.16f, 0., 0.,\n', lab_basic_ref, X_feath(i)); % position offset wrt to main reference frame
%         fprintf(fileID, '\treference, %s,\n', lab_basic_ref); % orientation matrix
%         fprintf(fileID, '\t\t1, 1., \t0., \t0.,\n');
%         fprintf(fileID, '\t\t2, 0., %.16f, %.16f,\n', cos(Twist_m), sin(Twist_m));
%         fprintf(fileID, '\treference, %s, null,\n', lab_basic_ref);
%         fprintf(fileID, '\treference, %s, null;\n\n', lab_basic_ref);
%     end
%     
    % Neutral axis ref frame
    fprintf(fileID, '# Neutral Axis reference system \n'); 
    
    for i = 1:numel(XNA)
        fprintf(fileID, 'reference: CURR_ROTOR + CURR_%s + %s + %d, #gen ref\n', Lab, Axis{2}, i);
        Label_axis.neut{i} = sprintf('CURR_ROTOR + CURR_%s + %s + %d', Lab, Axis{2}, i);
        fprintf(fileID, '\treference, %s, 0., %.16f, %.16f,\n', Label_axis.feath{node_idx(i)}, YNA(i), ZNA(i)); % position offset wrt to principal axis
        fprintf(fileID, '\treference, %s, eye,\n', Label_axis.feath{node_idx(i)}); % orientation matrix
        fprintf(fileID, '\treference, %s, null,\n', Label_axis.feath{node_idx(i)});
        fprintf(fileID, '\treference, %s, null;\n\n', Label_axis.feath{node_idx(i)});
    end

    % Place middle node
%     for i = 1:numel(XNA) - 1
%         XNA_m = (XNA(i + 1) + XNA(i)) / 2; 
%         YNA_m = (YNA(i + 1) + YNA(i)) / 2;
%         ZNA_m = (ZNA(i + 1) + ZNA(i)) / 2;
%         idx = find(abs(X_feath - XNA_m)./X_feath < 1e-3); 
%         fprintf(fileID, 'reference: CURR_ROTOR + CURR_%s + %s + %d + %d, #gen ref\n', Lab, Axis{2}, 1000, i);
%         Label_axis.neut_mid{i} = sprintf('CURR_ROTOR + CURR_%s + %s + %d +%d', Lab, Axis{2}, 1000, i);
%         fprintf(fileID, '\treference, %s, 0., %.16f, %.16f,\n', Label_axis.feath_mid{idx}, YNA_m, ZNA_m); % position offset wrt to principal axis
%         fprintf(fileID, '\treference, %s, eye, \n', Label_axis.feath_mid{idx}); % orientation matrix
%         fprintf(fileID, '\treference, %s, null,\n', Label_axis.feath_mid{idx});
%         fprintf(fileID, '\treference, %s, null;\n\n', Label_axis.feath_mid{idx});
%     end
    
    % Body ref frame 
    fprintf(fileID, '# Center of mass reference system \n'); 
    
    for i = 1:numel(STA) 
        fprintf(fileID, 'reference: CURR_ROTOR + CURR_%s + %s + %d, #gen ref\n', Lab, Axis{3}, i); 
        Label_axis.body{i} = sprintf('CURR_ROTOR + CURR_%s + %s + %d', Lab, Axis{3}, i);
        fprintf(fileID, '\treference, %s, 0., %.16f, %.16f,\n',Label_axis.feath{i}, YCG(i), ZCG(i)); % position offset wrt to principal axis
        fprintf(fileID, '\treference, %s, \n',Label_axis.feath{i}); % orientation matrix
        fprintf(fileID, '\t\t1, 1., \t0., \t0.,\n'); 
        fprintf(fileID, '\t\t2, 0., %.16f, %.16f,\n', cos(Rotapi(i)), sin(Rotapi(i)));
        fprintf(fileID, '\treference, %s, null,\n',Label_axis.feath{i});
        fprintf(fileID, '\treference, %s, null;\n\n',Label_axis.feath{i});
    end
    
    % Body ref frame middle 
%     for i = 1:numel(STA)-1  
%         YCG_m = (YCG(i+1) + YCG(i))/2;
%         ZCG_m = (ZCG(i+1) + ZCG(i))/2;
%         Rotapi_m = (Rotapi(i+1) + Rotapi(i))/2; 
%         fprintf(fileID, 'reference: OFFSET + CURR_%s + %s + %d + %d, #gen ref\n', Lab, Axis{3}, 100, Sec(i)); 
%         Label_axis.body_mid{i} = sprintf('OFFSET + CURR_%s + %s + %d + %d', Lab, Axis{3}, 100, Sec(i));
%         fprintf(fileID, '\treference, %s, 0., %.16f, %.16f,\n',Label_axis.feath_mid{i}, YCG_m, ZCG_m); % position offset wrt to principal axis
%         fprintf(fileID, '\treference, %s, \n',Label_axis.feath_mid{i}); % orientation matrix
%         fprintf(fileID, '\t\t1, 1., \t0., \t0.,\n'); 
%         fprintf(fileID, '\t\t2, 0., %.16f, %.16f,\n', cos(Rotapi_m), sin(Rotapi_m));
%         fprintf(fileID, '\treference, %s, null,\n',Label_axis.feath_mid{i});
%         fprintf(fileID, '\treference, %s, null;\n\n',Label_axis.feath_mid{i});
%     end
%     
    fclose(fileID);
       
end
