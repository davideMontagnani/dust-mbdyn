function MBDYN = write_beam_aerobeam_nas(BEAM,Label_ref,beam_node,Label_beam,Chord,MBDYN)
    fileID = fopen(MBDYN.file.beams_aero{end},'wt');
    tot = MBDYN.total.aero;
    Label_ext = Label_ref(1:numel(beam_node));
    Label_mid = Label_ref(numel(beam_node)+1:end);
    strnode = strsplit(num2str(beam_node));
    for i = 1:numel(beam_node)-1
        n = contains(BEAM.nodes,strnode{i});
        beam_id = n(:,1) == 1;
        Label_aero{i} = Label_beam{beam_id};
    end


    for i = 1:numel(beam_node)-1
        fprintf(fileID, 'aerodynamic beam3:\n');
        fprintf(fileID, '\t%s, # blade aero pan name \n', Label_aero{i});
        fprintf(fileID, '\t%s, # blade beam name \n', Label_aero{i});
        fprintf(fileID, '\treference, %s, null, \n', Label_ext{i});
        fprintf(fileID, '\t\t1, -1., 0., 0., 2, 0., 0., 1., # orientation wrt to node \n');
        fprintf(fileID, '\treference, %s, null, \n', Label_mid{i});
        fprintf(fileID, '\t\t1, -1., 0., 0., 2, 0., 0., 1., # orientation wrt to node \n');
        fprintf(fileID, '\treference, %s, null, \n', Label_ext{i + 1});
        fprintf(fileID, '\t\t1, -1., 0., 0., 2, 0., 0., 1., # orientation wrt to node \n');
        fprintf(fileID, '\tlinear, # defining chord shape as linear\n');
        fprintf(fileID, '\t\t%.6f, # element "root" chord [m]\n', Chord(i));
        fprintf(fileID, '\t\t%.6f, # element "tip" chord [m]\n', Chord(i + 1));
        fprintf(fileID, '\tlinear, # defining AC trend as linear\n');
        fprintf(fileID, '\t\t%.6f, # element "root" AC pos [adim]\n', 0); %FIXME
        fprintf(fileID, '\t\t%.6f, # element "tip" AC pos [adim]\n', 0); %FIXME
        fprintf(fileID, '\tlinear, # defining BC trend as linear\n'); %FIXME
        fprintf(fileID, '\t\t%.6f, # element "root" BC pos [adim]\n', 0); %FIXME
        fprintf(fileID, '\t\t%.6f, # element "tip" BC pos [adim]\n', 0); %FIXME
        fprintf(fileID, '\tlinear, # defining aero surf twist angle as linear\n');
        fprintf(fileID, '\t\t%.6f, # element "root" twist ang [rad]\n', 0);
        fprintf(fileID, '\t\t%.6f, # element "tip" twist ang [rad]\n', 0);
        fprintf(fileID, '\t\t%s_GAUSS_POINTS, # name of blade Gauss points\n', 'WING');
        fprintf(fileID, '\t\tc81, WING_c81,\n');
        fprintf(fileID, '\t\tunsteady, 0, # unsteadiness on\n');
        fprintf(fileID, '\t\tjacobian, 1; # jacobian output\n\n');
        tot = tot +1; 
    end
    MBDYN.total.aero = tot; 
end