function BEAM = write_BeamFromTable(BEAM,ROTOR,section,lab_basic_ref,start,varargin) 
    % indicate the position label of the section to keep 
    ReferenceAxis = BEAM.ReferenceAxis;
    
    if strcmpi(ReferenceAxis,'Y') % I want the refence axis aligned with the X axis        
        YCG = BEAM.ZCG.Value;  
        ZCG = BEAM.XCG.Value; 
        YNA = BEAM.ZNA.Value;  
        ZNA = BEAM.XNA.Value; 
        YCT = BEAM.ZCT.Value;  
        ZCT = BEAM.XCT.Value; 
        JY = BEAM.JZ.Value;
        JZ = BEAM.JX.Value;
        EJY = BEAM.EJZ.Value;
        EJZ = BEAM.EJX.Value;
        %cleanup 
        BEAM = rmfield(BEAM, {'XCG', 'XNA', 'XCT', 'JX', 'EJX'}); 
    elseif strcmpi(ReferenceAxis,'Z')        
        YCG = -BEAM.XCG.Value;  
        ZCG = BEAM.YCG.Value; 
        YNA = -BEAM.XNA.Value;  
        ZNA = BEAM.YNA.Value; 
        YCT = -BEAM.XCT.Value;  
        ZCT = BEAM.YCT.Value; 
        JY = BEAM.JX.Value;
        JZ = BEAM.JY.Value;
        EJY = BEAM.EJX.Value;
        EJZ = BEAM.EJY.Value;
        %cleanup
        BEAM = rmfield(BEAM, {'XCG', 'XNA', 'XCT', 'JX', 'EJX'}); 
    end   % conversion of the unit of measurement 
    
    BEAM.YCG.Value = YCG; 
    BEAM.ZCG.Value = ZCG; 
    BEAM.YNA.Value = YNA; 
    BEAM.ZNA.Value = ZNA; 
    BEAM.YCT.Value = YCT; 
    BEAM.ZCT.Value = ZCT; 
    BEAM.JY.Value = JY; 
    BEAM.JZ.Value = JZ; 
    BEAM.EJY.Value = EJY; 
    BEAM.EJZ.Value = EJZ; 
    
    BEAM = conversion(BEAM);
    % get station of interest 
    id_sec = zeros(1,numel(section));
    red_sta = zeros(1,numel(section));    
    for i = 1:numel(section)
        id_sec(i) = find(BEAM.SEC.Value == section(i)); 
        red_sta(i) = BEAM.STA.Value(id_sec(i)); 
    end
    % sort beam element 
    [BEAM.STA.Value, idx] = sort(BEAM.STA.Value); 
    [BEAM.STA.Red, idred] = sort(red_sta);
    
    BEAM.STA.Red_mid = conv(BEAM.STA.Red, [0.5 0.5], 'valid');
    % reshape
    if ~ismember(0, BEAM.STA.Red) % non serve? 
        BEAM.STA.Red = reshape([0 BEAM.STA.Red_mid; BEAM.STA.Red], [], 1);
        BEAM.STA.Red = BEAM.STA.Red(2:end);
    else
        BEAM.STA.Red = reshape([0 BEAM.STA.Red_mid; BEAM.STA.Red], [], 1);
        BEAM.STA.Red = BEAM.STA.Red(2:end);
    end
    BEAM.WEIGHT.Value = BEAM.WEIGHT.Value(idx); 
    BEAM.YCG.Value = BEAM.YCG.Value(idx); 
    BEAM.ZCG.Value = BEAM.ZCG.Value(idx); 
    BEAM.YNA.Value = BEAM.YNA.Value(idx); 
    BEAM.ZNA.Value = BEAM.ZNA.Value(idx); 
    BEAM.YCT.Value = BEAM.YCT.Value(idx); 
    BEAM.ZCT.Value = BEAM.ZCT.Value(idx);      
    BEAM.ROTAPI.Value = -BEAM.ROTAPI.Value(idx); %TODO  put check on idx 
    BEAM.ROTAN.Value = -BEAM.ROTAN.Value(idx); 
    BEAM.JP.Value = BEAM.JP.Value(idx); 
    BEAM.JY.Value = BEAM.JY.Value(idx); 
    BEAM.JZ.Value = BEAM.JZ.Value(idx);     
    BEAM.EA.Value = BEAM.EA.Value(idx);
    BEAM.GJ.Value = BEAM.GJ.Value(idx); 
    BEAM.EJY.Value = BEAM.EJY.Value(idx);
    BEAM.EJZ.Value = BEAM.EJZ.Value(idx);
    % Get normalization 
    BEAM.Length = ROTOR.Radius.Value;
    
    BEAM.STA.adim = BEAM.STA.Red./ROTOR.Radius.Value; 
    
    
    %BEAM = data_reduction(BEAM,section); % postpone 
    
    
    % Get twist if there are presents aerodynamic panels
    if isfield(BEAM,'Aero')
        
        % parse geometry 
        aero_path = varargin{1}; 
        filename = fullfile(aero_path, BEAM.Aero.Geometry.file);  % FIXME  
        BEAM.Aero.Geometry = read_aero_geom(filename);
        % put in dimensional form 
        BEAM = conversion_aero(BEAM);      
        figure
        plot3(BEAM.Aero.Geometry.Radial.Value/1.25, - BEAM.Aero.Geometry.Sweep.Value/1.25, BEAM.Aero.Geometry.Anhedral.Value/1.25)        
        
    end
    
    BEAM = make_evendata(BEAM,start);
    
    if isfield(BEAM,'Aero')
        for i = 1:numel(BEAM.Aero.Geometry.Twist.Value)
            R = [1 0 0;
                0 cos(BEAM.Aero.Geometry.Twist.Value(i)), sin(BEAM.Aero.Geometry.Twist.Value(i)); 
                0 -sin(BEAM.Aero.Geometry.Twist.Value(i)), cos(BEAM.Aero.Geometry.Twist.Value(i))]; 

            NA(:,i) = R'*[BEAM.STA.Value(i); BEAM.YNA.Value(i); BEAM.ZNA.Value(i)];
            CT(:,i) = R'*[BEAM.STA.Value(i); BEAM.YCT.Value(i); BEAM.ZCT.Value(i)];
            CG(:,i) = R'*[BEAM.STA.Value(i); BEAM.YCG.Value(i); BEAM.ZCG.Value(i)];
        end
        hold on 
        plot3(BEAM.STA.Value/1.25, NA(2,:)/1.25, NA(3,:)/1.25)
        plot3(BEAM.STA.Value/1.25, CT(2,:)/1.25, CT(3,:)/1.25)
        plot3(BEAM.STA.Value/1.25, CG(2,:)/1.25, CG(3,:)/1.25)
        axis equal
        legend('AC','NA','CT','CG')
        grid on
    end
    % Get node position
    node_idx = zeros(1,numel(BEAM.STA.Red)); 
    for i = 1:numel(BEAM.STA.Red) 
        node = find(BEAM.STA.Value == BEAM.STA.Red(i));
        node_idx(i) = node(1);
    end
    
    %% Write MBDyn structural beam 
    
    Label_axis = write_beam_ref(BEAM,lab_basic_ref, node_idx);
    
    [Label_node, BEAM] = write_beam_node(BEAM,Label_axis);
    
    [Label_beam, BEAM] = write_beam_beam3(BEAM, Label_node, Label_axis, node_idx);

    BEAM = write_beam_body(BEAM,Label_node, Label_axis, node_idx); 
    
    %% Aerodynamic   
    
    if isfield(BEAM,'Aero')
        
        Label_axis = write_beam_aero_ref(BEAM, Label_axis, node_idx, lab_basic_ref); 
        % write Aerodynamic beam element 
        write_beam_aerobeam(BEAM, Label_axis, Label_beam, node_idx);  
        
    end
    
    %% Joint 
    if isfield(BEAM,'Joint')
        num_joint = numel(fieldnames(BEAM.Joint));
        joint_names = fieldnames(BEAM.Joint);
        for i = 1:num_joint
            if strcmpi(BEAM.Joint.(joint_names{i}).Unit, 'MM')
                Location = BEAM.Joint.(joint_names{i}).Location * 1e-3 / BEAM.Length;
            elseif strcmpi(BEAM.Joint.(joint_names{i}).Unit, 'M')
                Location = BEAM.Joint.(joint_names{i}).Location * 1e-3 / BEAM.Length;
            elseif strcmpi(BEAM.Joint.(joint_names{i}).Unit, 'ADIM')
                Location = BEAM.Joint.(joint_names{i}).Location;
            end
            dist = abs(BEAM.STA.adim - Location);
            minDist = min(dist);
            idx = dist == minDist;
            BEAM.Joint.(joint_names{i}).Node = Label_node(idx);       
        end
    end
    
    rigid_beam(BEAM, lab_basic_ref) % rigid beam 
    %% Lumped Masses  
     
    BEAM.Length = ROTOR.Radius.Value;
    
    if isfield(BEAM,'Body')
        
        num_body = numel(fieldnames(BEAM.Body));
        body_names = fieldnames(BEAM.Body); 
        fileID = fopen(BEAM.file.lumped, 'wt'); 
        for i = 1:num_body
            if strcmpi(BEAM.Body.(body_names{i}).Offset.Unit, 'MM')
                Offset = BEAM.Body.(body_names{i}).Offset.Value .* 1e-3;
                Offset_adim = Offset / BEAM.Length; 
            end
            if strcmpi(BEAM.Body.(body_names{i}).I.Unit, 'KG*MM^2')
               Inertia = BEAM.Body.(body_names{i}).I.Value .* 1e-6;
            end
            M = BEAM.Body.(body_names{i}).M.Value;             
            dist = abs(BEAM.STA.adim - Offset_adim(1));
            minDist = min(dist);
            idx = dist == minDist;
            
            BEAM.Body.(body_names{i}).Node = Label_node(idx);  
            fprintf(fileID, 'body: CURR_ROTOR + CURR_%s + %s, %s, \n', BEAM.Name, char(body_names{i}), char(BEAM.Body.(body_names{i}).Node));
            fprintf(fileID, '\t%+16.9e,\n',M);
            fprintf(fileID, '\treference, %s, %+16.9e, %+16.9e, %+16.9e,\n', lab_basic_ref, Offset);
            fprintf(fileID, '\t\tdiag, %+16.9e, %+16.9e, %+16.9e;\n\n', Inertia(1:3)); 
        end
    end
       BEAM.Node.Lab =  Label_node;  
    

end