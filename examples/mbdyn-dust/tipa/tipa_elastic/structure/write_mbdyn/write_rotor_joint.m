function ROTOR  = write_rotor_joint(ROTOR, SPRING, Label_rotor_node)
    
    %TODO Multiblade!!! 
    Ground = Label_rotor_node{1}; 
    Airframe = Label_rotor_node{2};
    Sw_fix = Label_rotor_node{3};
    Sw_rot = Label_rotor_node{4};
    Hub = Label_rotor_node{5}; % Lable nodes
    
%     CollStiff = SPRING.CollStiff.Value; % Stiffness values
%     CyclLatStiff = SPRING.CyclLatStiff.Value;
%     CyclLongStiff = SPRING.CyclLongStiff.Value; 
    
    fileID = fopen(ROTOR.file.joint,'wt');
    
    fprintf(fileID, 'joint: CURR_ROTOR + GROUND_CLAMP, clamp, %s, node, node;\n\n', Ground);
    ROTOR.joint.lab{1} = sprintf('CURR_ROTOR + GROUND_CLAMP'); 
    
    fprintf(fileID, 'joint: OFFSET + AIRFRAME_CLAMP, total joint, #clamp fix node\n');
    ROTOR.joint.lab{2} = sprintf('OFFSET + AIRFRAME_CLAMP');  
    fprintf(fileID, '\t%s, \n', Ground);
    fprintf(fileID, '\t\tposition, reference, node, null,\n');
    fprintf(fileID, '\t\tposition orientation, reference, node, eye,\n');
    fprintf(fileID, '\t\trotation orientation, reference, node, eye,\n');
    fprintf(fileID, '\t%s, \n',Airframe); 
    fprintf(fileID, '\t\tposition, reference, other node, null,\n');
    fprintf(fileID, '\t\tposition orientation, reference, other node, eye,\n');
    fprintf(fileID, '\t\trotation orientation, reference, other node, eye,\n');
    fprintf(fileID, '\tposition constraint, \n');
    fprintf(fileID, '\t\t1, 1, 1, \n');
    fprintf(fileID, '\t\t0., 0., 1.,\n');
    fprintf(fileID, '\t\tcosine, T_0_COLL, pi/(PERIOD_COLL), COLLECTIVE_VERTICAL/2, half, 0.,\n');
    fprintf(fileID, '\torientation constraint, \n');
    fprintf(fileID, '\t\t1, 1, 1, \n');
    fprintf(fileID, '\t\tcomponent, \n');
    fprintf(fileID, '\t\tcosine, T_0_CYC_X, pi/(PERIOD_CYC_X), PITCH_CYC_X/2, half, 0.,\n');
    fprintf(fileID, '\t\tcosine, T_0_CYC_Y, pi/(PERIOD_CYC_Y), PITCH_CYC_Y/2, half, 0.,\n');
    fprintf(fileID, '\t\t0.;\n\n');
    
    fprintf(fileID, 'joint: OFFSET + AIRFRAME_SWASH_JOINT, total joint, # airframe to low plate\n');
    ROTOR.joint.lab{3} = sprintf('OFFSET + AIRFRAME_SWASH_JOINT');
    fprintf(fileID, '\t%s, \n', Airframe);
    fprintf(fileID, '\t\tposition, reference, node, null,\n'); 
    fprintf(fileID, '\t\tposition orientation, reference, node, eye,\n');
    fprintf(fileID, '\t\trotation orientation, reference, node, eye,\n');
    fprintf(fileID, '\t%s, \n', Sw_fix);
    fprintf(fileID, '\t\tposition, reference, other node, null,\n');
    fprintf(fileID, '\t\tposition orientation, reference, other node, eye,\n');
    fprintf(fileID, '\t\trotation orientation, reference, other node, eye,\n');
    fprintf(fileID, '\tposition constraint, \n');
    
    if isfield(SPRING,'CollStiff') % check if deformability is present
        fprintf(fileID, '\t\t1, 1, 0, \n');
        fprintf(fileID, '\t\tnull,\n');
        fprintf(fileID, '\torientation constraint, \n');
        fprintf(fileID, '\t\t0, 0, 1,\n'); 
        fprintf(fileID, 'null;\n\n'); 
    else 
        fprintf(fileID, '\t\t1, 1, 1, \n');
        fprintf(fileID, '\t\tnull,\n');
        fprintf(fileID, '\torientation constraint, \n');
        fprintf(fileID, '\t\t1, 1, 1,\n'); 
        fprintf(fileID, 'null;\n\n');
    end
        
    
    fprintf(fileID, 'joint: OFFSET + SWASH_JOINT, total joint, # swashplate joint\n'); 
    ROTOR.joint.lab{4} = sprintf('OFFSET + SWASH_JOINT');
    fprintf(fileID, '\t%s, \n', Sw_fix);
    fprintf(fileID, '\t\tposition, reference, node, null,\n');
    fprintf(fileID, '\t\tposition orientation, reference, node, eye,\n');
    fprintf(fileID, '\t\trotation orientation, reference, node, eye,\n');
    fprintf(fileID, '\t%s, \n', Sw_rot);
    fprintf(fileID, '\t\tposition, reference, other node, null,\n');
    fprintf(fileID, '\t\tposition orientation, reference, other node, eye,\n');
    fprintf(fileID, '\t\trotation orientation, reference, other node, eye,\n');
    fprintf(fileID, '\tposition constraint, \n');
    fprintf(fileID, '\t\t1, 1, 1, \n');
    fprintf(fileID, '\t\tnull,\n');
    fprintf(fileID, '\torientation constraint, \n');
    fprintf(fileID, '\t\t1, 1, 0,\n');
    fprintf(fileID, '\t\tnull;\n');
    fprintf(fileID, '\n');
    
    fprintf(fileID, 'joint: OFFSET + SWASH_HUB_JOINT, total joint, # up plate to hub\n');
    ROTOR.joint.lab{5} = sprintf('OFFSET + SWASH_HUB_JOINT');
    fprintf(fileID, '\t%s, \n', Hub);
    fprintf(fileID, '\t\tposition, reference, node, null,\n');
    fprintf(fileID, '\t\tposition orientation, reference, node, eye,\n');
    fprintf(fileID, '\t\trotation orientation, reference, node, eye,\n');
    fprintf(fileID, '\t%s, \n', Sw_rot);
    fprintf(fileID, '\t\tposition, reference, other node, null,\n');
    fprintf(fileID, '\t\tposition orientation, reference, other node, eye,\n');
    fprintf(fileID, '\t\trotation orientation, reference, other node, eye,\n');
    fprintf(fileID, '\tposition constraint, \n');
    fprintf(fileID, '\t\t0, 0, 0, \n');
    fprintf(fileID, '\t\tnull,\n');
    fprintf(fileID, '\torientation constraint, \n');
    fprintf(fileID, '\t\t0, 0, 1,\n');
    fprintf(fileID, '\t\tnull;\n');
    fprintf(fileID, '\n');

    fprintf(fileID, 'joint: OFFSET + HUB_AIRFRAME, total joint, # ground to hub\n');
    ROTOR.joint.lab{6} = sprintf('OFFSET + HUB_AIRFRAME');
    fprintf(fileID, '\t%s, \n', Hub);
    fprintf(fileID, '\t\tposition, reference, node, null,\n');
    fprintf(fileID, '\t\tposition orientation, reference, node, eye,\n');
    fprintf(fileID, '\t\trotation orientation, reference, node, eye,\n');
    fprintf(fileID, '\t%s, \n', Ground);
    fprintf(fileID, '\t\tposition, reference, other node, null,\n');
    fprintf(fileID, '\t\tposition orientation, reference, other node, eye,\n');
    fprintf(fileID, '\t\trotation orientation, reference, other node, eye,\n');
    fprintf(fileID, '\tposition constraint, \n');
    fprintf(fileID, '\t\t1, 1, 1, \n');
    fprintf(fileID, '\t\tnull,\n');
    fprintf(fileID, '\torientation constraint, \n');
    fprintf(fileID, '\t\t0, 0, 0,\n');
    fprintf(fileID, '\t\tnull;\n');
    fprintf(fileID, '\n');

    fprintf(fileID, 'joint: OFFSET + HUB_ANG, angular velocity, # angular velocity\n');
    ROTOR.joint.lab{7} = sprintf('OFFSET + HUB_ANG');
    fprintf(fileID, '\t%s,\n',Hub);
    fprintf(fileID, '\t0., 0., 1.,\n');
    fprintf(fileID, '\tcosine, T_0_OMEGA, pi/(PERIOD_OMEGA), (OMEGA_REG - OMEGA_0)/2, half, OMEGA_0;\n'); 
        
    % add stiffness to the swashplate 
    if isfield(SPRING,'CollStiff')
        
        CollStiff = SPRING.CollStiff.Value; % Stiffness values
        CyclLatStiff = SPRING.CyclLatStiff.Value;
        CyclLongStiff = SPRING.CyclLongStiff.Value; 
        fprintf(fileID, 'constitutive law: OFFSET + CYCLIC_LAW,\n');
        fprintf(fileID, '\t3, linear elastic generic,\n'); % TODO linear viscoelastic
        fprintf(fileID, '\tdiag, %.16f, %.16f, 0.;\n\n', CyclLatStiff, CyclLongStiff); 

        fprintf(fileID, 'constitutive law: OFFSET + COLLECTIVE_LAW,\n');
        fprintf(fileID, '\t3, linear elastic generic,\n'); % TODO linear viscoelastic
        fprintf(fileID, '\tdiag, 0., 0., %.16f;\n\n', CollStiff); 

        fprintf(fileID, 'joint: OFFSET + AIRFRAME_SWASH_DEF_CYCL, deformable hinge,\n');
        ROTOR.joint.lab{8} = sprintf('OFFSET + AIRFRAME_SWASH_DEF_CYCL');
        fprintf(fileID, '\t%s,\n', Sw_fix);
        fprintf(fileID, '\t\tposition, reference, node, null,\n');
        fprintf(fileID, '\t\torientation, reference, node, eye,\n');
        fprintf(fileID, '\t%s,\n', Airframe);
        fprintf(fileID, '\t\tposition, reference, other node, null,\n');
        fprintf(fileID, '\t\torientation, reference, other node, eye,\n');
        fprintf(fileID, '\treference, OFFSET + CYCLIC_LAW;\n\n');

        fprintf(fileID, 'joint: OFFSET + AIRFRAME_SWASH_DEF_COLL, deformable displacement joint,\n'); 
        ROTOR.joint.lab{9} = sprintf('OFFSET + AIRFRAME_SWASH_DEF_COLL');
        fprintf(fileID, '\t%s,\n', Sw_fix);
        fprintf(fileID, '\t\tposition, reference, node, null,\n');
        fprintf(fileID, '\t\torientation, reference, node, eye,\n');
        fprintf(fileID, '\t%s,\n', Airframe);
        fprintf(fileID, '\t\tposition, reference, other node, null,\n');
        fprintf(fileID, '\t\torientation, reference, other node, eye,\n');
        fprintf(fileID, '\treference, OFFSET + COLLECTIVE_LAW;\n\n'); 
        
    end
    fclose(fileID); 
end
    