function Label_pitch_link = write_pitch_link(ROTOR,BLADE,POINT,SPRING, ref_main_rotor,curr_blade,Label_rotor_node,rotorblade)  
    fileID = fopen(rotorblade.file,'wt');
    
%     if any(~cellfun('isempty',strfind(Label_rotor_node,'ROT'))) 
%         id_node = ~cellfun('isempty',strfind(Label_rotor_node,'ROT')) == 1; 
%         Sw_node = Label_rotor_node{id_node};
%     end
    Sw_node = Label_rotor_node{4};
    Blade_node = char(BLADE.Joint.PitchLink.Node); 
    
    if strcmpi(POINT.PitchToHorn.Unit,'MM')
        PitchHornAtt = POINT.PitchToHorn.COORD' .* 1e-3;
        SwashPlateAtt = POINT.SwashPlate.COORD' .* 1e-3;
    else 
        PitchHornAtt = POINT.PitchToHorn.COORD';
        SwashPlateAtt = POINT.SwashPlate.COORD';
    end
    
    %% rotations
    Nblade = ROTOR.Nblade.Value; % Blade Number
    Psi = 2 * pi / Nblade *(curr_blade-1); % Azimut between blades 
    R2 = [cos(Psi), sin(Psi), 0;
            -sin(Psi), cos(Psi), 0;
            0, 0, 1]; % second orientation matrix (different azimut) 
    PitchHornAtt_curr = R2' * PitchHornAtt;
    SwashPlateAtt_curr = R2' * SwashPlateAtt; 
    Length_link = norm(PitchHornAtt - SwashPlateAtt);
    if strcmpi(SPRING.PitchLinkStiff.Unit,'lb/ft')
        Stiff = SPRING.PitchLinkStiff.Value *1.35582*9.81; 
    else
        Stiff = SPRING.PitchLinkStiff.Value;
    end
    fprintf(fileID, 'joint: OFFSET + CURR_%s_PITCH_LINK, rod, # blade pitch link\n', BLADE.Name);
    Label_pitch_link = sprintf('OFFSET + CURR_%s_PITCH_LINK', BLADE.Name); 
    fprintf(fileID, '\t%s,\n', Sw_node);
    fprintf(fileID, '\t\tposition, reference, %s, %.16f, %.16f, %.16f,\n', ref_main_rotor, SwashPlateAtt_curr);
    fprintf(fileID, '\t%s,\n', Blade_node);
    fprintf(fileID, '\t\tposition, reference, %s, %.16f, %.16f, %.16f,\n', ref_main_rotor, PitchHornAtt_curr);
    fprintf(fileID, '\t\tfrom nodes,\n');
    fprintf(fileID, '\t\tlinear viscoelastic, %.16f, proportional, 0.001;\n', Stiff*Length_link);
    fclose(fileID); 
end

