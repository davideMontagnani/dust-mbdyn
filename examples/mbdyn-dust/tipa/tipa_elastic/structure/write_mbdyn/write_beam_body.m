function BEAM = write_beam_body(BEAM, Label_node,Label_axis, node_idx)

    dM = BEAM.WEIGHT.Value;               % Mass per unit length
    dJX = BEAM.JP.Value;                  % Mass polar moment of inertia per unit length - ref centre of gravity
    dJY = BEAM.JY.Value;                  % Mass moment of inertia per unit length - ref centre of gravity
    dJZ = BEAM.JZ.Value;                  % Mass moment of inertia per unit length - ref centre of gravity
    STA = BEAM.STA.Value;                 % Station location on the beam axis
    XNA = BEAM.STA.Value(node_idx);       % Node location along the feathering axis

    N_body = numel(XNA);   % number of body element: one body for each dynamic node
    tot_body = BEAM.total.body; 
    fileID = fopen(BEAM.file.body, 'wt');
    
    fprintf(fileID, '# %s\n', pwd); 
    fprintf(fileID, '# %s\n', datetime('now')); 
    fprintf(fileID, '# Do not modify \n\n'); 
    
    beginx = XNA(1);
	endx   = XNA(2) - .5/sqrt(3) * (XNA(3) - XNA(1));  
    interpolate_body(STA, dM, dJX, dJY, dJZ, beginx, endx, fileID, Label_node{1}, Label_axis)
    
    for count = 1 : 2 : numel(XNA) - 2  
		beginx = endx;
		endx   = XNA(count+1) + .5/sqrt(3) * (XNA(count+2) - XNA(count));
		interpolate_body(STA, dM, dJX, dJY, dJZ, beginx, endx, fileID, Label_node{count+1}, Label_axis);
        
        if  count < numel(XNA) -2
			beginx = endx;
			endx   = XNA(count+3) - .5/sqrt(3) * (XNA(count+3) - XNA(count+1));
			interpolate_body(STA, dM, dJX, dJY, dJZ, beginx, endx, fileID, Label_node{count+2}, Label_axis);
        end 
        
    end

	beginx = XNA(end-1) + .5/sqrt(3) * (XNA(end) - XNA(end-2));
	endx   = XNA(end);
	interpolate_body(STA, dM, dJX, dJY, dJZ, beginx, endx, fileID, Label_node{end}, Label_axis)
    
    BEAM.total.body = tot_body + N_body; 
    fclose(fileID);
end

function interpolate_body(STA, dM, dJX, dJY, dJZ, beginx, endx, fileID, Label_node, Label_axis)  

	ji = find(STA(:) >= beginx);
	jf = find(STA(:) >= endx);
	ji = ji(1);
	jf = jf(1);

	if (ji > 1)
		ji = ji - 1;
	end

	condense = jf - ji;
    fprintf(fileID, '# %s : %e -> %e m (%.3f -> %.3f R)\n', Label_node, beginx, endx, beginx/1.25, endx/1.25); % FIXME ROTOR.radius; 
	fprintf(fileID, 'body: %s, %s\n', Label_node, Label_node);
    
    if  condense > 1
        fprintf(fileID, '\t,\n');
		fprintf(fileID, '\tcondense, %d\n', condense);
    else 
        condense = 1; 
    end
        
    for k = 0:condense - 1  
		%initial part
        if (STA(ji + k) < beginx)
			% interpolate
			dx_start = beginx;
			d1 = STA(ji + k + 1) - dx_start;
			d2 = dx_start - STA(ji + k);
			dm_start  = ([d1 d2]/(d1 + d2))*dM(ji + k + [0 1]);
			djx_start  = ([d1 d2]/(d1 + d2))*dJX(ji + k + [0 1]);
			djy_start  = ([d1 d2]/(d1 + d2))*dJY(ji + k + [0 1]);
			djz_start  = ([d1 d2]/(d1 + d2))*dJZ(ji + k + [0 1]);	
            
		else
			% exact
			dx_start  = STA(ji + k);
			dm_start  = dM(ji + k);
			djx_start  = dJX(ji + k);
			djy_start  = dJY(ji + k);
			djz_start  = dJZ(ji + k);
        end

		% final part
        if (STA(ji + k + 1) > endx)
			% interpolate
			dx_end = endx;
			d1 = STA(ji + k + 1) - dx_end;
			d2 = dx_end - STA(ji + k);
			dm_end  = ([d1 d2]/(d1 + d2))*dM(ji + k + [0 1]);
            djx_end  = ([d1 d2]/(d1 + d2))*dJX(ji + k + [0 1]);
			djy_end  = ([d1 d2]/(d1 + d2))*dJY(ji + k + [0 1]);
			djz_end  = ([d1 d2]/(d1 + d2))*dJZ(ji + k + [0 1]);	
		else
			% exact
			dx_end  = STA(ji + k + 1);
			dm_end  = dM(ji + k + 1);
			djx_end  = dJX(ji + k + 1);
			djy_end  = dJY(ji + k + 1);
			djz_end  = dJZ(ji + k + 1);
        end
        ref_system = Label_axis.body{ji + k}; % take the first ref sys 
		
		dL = dx_end - dx_start;
		%xm = (dx_end + dx_start)/2 - dx_start;
		dm = (dm_end + dm_start)/2;
		djx = (djx_end + djx_start)/2;
		djy = (djy_end + djy_start)/2;
		djz = (djz_end + djz_start)/2;
		
        M = dm * dL; 
        JX = djx * dL; 
        JY = djy * dL + M/12 * dL^2;
        JZ = djz * dL + M/12 * dL^2;
        J = diag([JX, JY, JZ]); 
        fprintf(fileID, '\t,\n');
		fprintf(fileID, '\t%e, #%e kg/m; dL=%e m; \n', M, M/dL, dL);
		fprintf(fileID, '\treference, %s, ', ref_system);
		fprintf(fileID, '%16.9f, 0., 0.,\n', 0); % maybe 0 is ok 
		fprintf(fileID, '\treference, %s, \n', ref_system);
		fprintf(fileID, '\t\tdiag, %e, %e, %e\n', J(1, 1), J(2, 2), J(3, 3));
    end
	fprintf(fileID, ';\n');
	fprintf(fileID, '\n');

end


