function [Label_beam3, BEAM] = write_beam_beam3(BEAM, Label_node, Label_axis, node_idx)

    fileID = fopen(BEAM.file.beam3, 'wt');

    fprintf(fileID, '# %s\n', pwd);
    fprintf(fileID, '# %s\n', datetime('now'));
    fprintf(fileID, '# DO NOT MODIFY \n\n');
    fprintf(fileID, '\n');
    
   
    
    EA = BEAM.EA.Value; % Axial stiffness
    EJY = BEAM.EJY.Value; % Beam-wise bending stiffness
    EJZ = BEAM.EJZ.Value; % Chord-wise bending stiffness
    GJ = BEAM.GJ.Value; % Torsional stiffness
    XNA = BEAM.STA.Value(node_idx); % Neutral axis center X position (reduced)
    YNA = BEAM.YNA.Value; % Neutral axis center Y position
    ZNA = BEAM.ZNA.Value; % Neutral axis center Z position
    YCT = BEAM.YCT.Value; % Shear center Y position
    ZCT = BEAM.ZCT.Value; % Shear center Z position
    STA = BEAM.STA.Value; % Neutral axis location (ends of the element)
    ROTAN = BEAM.ROTAN.Value; % Neutral axis rotation

    YNA_red = BEAM.YNA.Value(node_idx);
    ZNA_red = BEAM.ZNA.Value(node_idx);
    YCT_red = BEAM.YCT.Value(node_idx);
    ZCT_red = BEAM.ZCT.Value(node_idx);

    N_beam = (numel(XNA) - 1) / 2; % Number of beam element
    Lab = BEAM.Name;
    tot_beam = BEAM.total.beam;
    R = BEAM.Length; 
    % with the same for cycle we generate the elements names (in 'wing_elements_m.set')
    % and assign them to the proper node card (in 'wing_m.el')
    % setting i-th element name
    % generating i-th card
    
    for i = 0:N_beam - 1

        beginx = XNA(i * 2 + 1); % start node
        endx = XNA(i * 2 + 3); % end node
        midx = (beginx + endx)/2; % mid node
        ev1 = XNA(i * 2 + 2) - 0.5 / sqrt(3) * (XNA(i * 2 + 3) - XNA(i * 2 + 1)); % 1st evaluation point
        ev2 = XNA(i * 2 + 2) + 0.5 / sqrt(3) * (XNA(i * 2 + 3) - XNA(i * 2 + 1)); %2nd evaluation point

        j1 = find(STA >= ev1);
        j1 = j1(1);
        j2 = find(STA >= ev2);
        j2 = j2(1);
        jis = [j1 j2];
        xis = [ev1 ev2];

        for j = 1:2

            ji = jis(j);
            xs = xis(j);

            if STA(ji) < xs% intepolate
                dxs = xs;
                d1 = STA(ji + 1) -dxs;
                d2 = dxs - STA(ji + 1);
                EA_e = ([d1 d2] / (d1 + d2)) * EA(ji + [0 1]);
                GJ_e = ([d1 d2] / (d1 + d2)) * GJ(ji + [0 1]);
                EJY_e = ([d1 d2] / (d1 + d2)) * EJY(ji + [0 1]);
                EJZ_e = ([d1 d2] / (d1 + d2)) * EJZ(ji + [0 1]);
                YNA_e = ([d1 d2] / (d1 + d2)) * YNA(ji + [0 1]);
                ZNA_e = ([d1 d2] / (d1 + d2)) * ZNA(ji + [0 1]);
                YCT_e = ([d1 d2] / (d1 + d2)) * YCT(ji + [0 1]);
                ZCT_e = ([d1 d2] / (d1 + d2)) * ZCT(ji + [0 1]);
                ROT_e = ([d1 d2] / (d1 + d2)) * ROTAN(ji + [0 1]);
            else % use exact value
                EA_e = EA(ji);
                GJ_e = GJ(ji);
                EJY_e = EJY(ji);
                EJZ_e = EJZ(ji);
                YNA_e = YNA(ji);
                ZNA_e = ZNA(ji);
                YCT_e = YCT(ji);
                ZCT_e = ZCT(ji);
                ROT_e = ROTAN(ji);
            end

            % preparing offset
            Y1 = YCT_e - YNA_e; %Check!!
            Z1 = ZCT_e - ZNA_e;

            % GA
            GAY_e = EA_e / (2 * (1 + 0.33));
            GAZ_e = EA_e / (2 * (1 + 0.33));

            % Build K matrix for section 1 % from MBDyn manual
            A_22 = EJY_e * (cos(ROT_e))^2 + EJZ_e * (sin(ROT_e))^2 + Z1^2 * EA_e;
            A_23 = (EJY_e - EJZ_e) * cos(ROT_e) * sin(ROT_e) - Y1 * Z1 * EA_e;
            A_33 = EJZ_e * (cos(ROT_e))^2 + EJY_e * (sin(ROT_e))^2 + Y1^2 * EA_e;
            Diag_K(j,:) = [EA_e, GAY_e, GAZ_e, GJ_e, EJY_e, EJZ_e];  
            K(:, :, i + 1, j) = [EA_e, 0, 0, 0, Z1 * EA_e, -Y1 * EA_e;
                        0, GAY_e, 0, 0, 0, 0;
                        0, 0, GAZ_e, 0, 0, 0;
                        0, 0, 0, GJ_e, 0, 0;
                        +Z1 * EA_e, 0, 0, 0, A_22, A_23;
                        -Y1 * EA_e, 0, 0, 0, A_23, A_33];
        end

        fprintf(fileID, '# Defining the beam/sections notation\n');
        fprintf(fileID, '# x1=%e xm=%e x2=%e (%.3f -> %.3f -> %.3f R\n', beginx, midx, endx, beginx / R, midx / R, endx / R);
        fprintf(fileID, '# Beam CURR_ROTOR + CURR_%s + %d, \n', Lab, i + 1);
        fprintf(fileID, '#                                       A  beam y \n');
        fprintf(fileID, '#                                      /   adimensional axis\n');
        fprintf(fileID, '#                                     /\n');
        fprintf(fileID, '#                                    /\n');
        fprintf(fileID, '#           |            |          |----------|-------------|---------> beam x adimensional axis\n');
        fprintf(fileID, '#          -1       -1/sqrt(3)      0       1/sqrt(3)        1 \n');
        fprintf(fileID, '#               ________________________________________________\n');
        fprintf(fileID, '#             /                                                /\n');
        fprintf(fileID, '#            /        section I             section II        /\n');
        fprintf(fileID, '#           /_____________|_____________________|____________/ \n');
        fprintf(fileID, '#           |           | | |       |         | | |          |  \n');
        fprintf(fileID, '#           |           | V |       |         | V |          |  \n');
        fprintf(fileID, '#           |           |   |       |         |   |          |  \n');
        fprintf(fileID, '#           |  _________|   |_______|_________|   |__________|__ \n');
        fprintf(fileID, '#           | /         |  /        | /       |  /           | /\n');
        fprintf(fileID, '#           |/          | /         |/        | /            |/\n');
        fprintf(fileID, '#           /___________|/__________/_________|/_____________/\n');
        fprintf(fileID, '#          node 1                  node 2              node 3\n');
        fprintf(fileID, '#      0\n');
        fprintf(fileID, '#      |---\\....\\----------------------------------------------------------> global X axis \n');
        fprintf(fileID, '#\n');
        fprintf(fileID, 'beam3: CURR_ROTOR + CURR_%s + %d, # beam name\n', Lab, i + 1);
        Label_beam3{i + 1} = sprintf('CURR_ROTOR + CURR_%s + %d', Lab, i + 1);
        fprintf(fileID, '\t%s , # node 1 name\n', Label_node{i * 2 + 1});
        % Axis of the beam is collocated on the shear center axis
        fprintf(fileID, '\t\tposition, reference, %s, 0., %.16f, %.16f, \n', Label_axis.neut{i * 2 + 1}, YCT_red(i * 2 + 1) - YNA_red(i * 2 + 1), ZCT_red(i * 2 + 1) - ZNA_red(i * 2 + 1));
        fprintf(fileID, '\t\torientation, reference, %s, eye, # orientation for properties\n', Label_axis.neut{i * 2 + 1});
        fprintf(fileID, '\t%s , # node 2 name\n', Label_node{i * 2 +2});
        fprintf(fileID, '\t\tposition, reference, %s, 0., %.16f, %.16f, \n', Label_axis.neut{i * 2 + 2}, YCT_red(i * 2 + 2) - YNA_red(i * 2 + 2), ZCT_red(i * 2 + 2) - ZNA_red(i * 2 + 2));
        fprintf(fileID, '\t\torientation, reference, %s, eye, # orientation for properties\n', Label_axis.neut{i * 2 + 2});
        fprintf(fileID, '\t%s , # node 3 name\n', Label_node{i * 2 + 3});
        fprintf(fileID, '\t\tposition, reference, %s, 0., %.16f, %.16f, \n', Label_axis.neut{i * 2 + 3}, YCT_red(i * 2 + 3) - YNA_red(i * 2 + 3), ZCT_red(i * 2 + 3) - ZNA_red(i * 2 + 3));
        fprintf(fileID, '\t\torientation, reference, %s, eye, # orientation for properties\n', Label_axis.neut{i * 2 + 3});
        fprintf(fileID, '\t\tfrom nodes, \n');
        fprintf(fileID, '\t\t# EA = %.8e [N], GA_y = %.8e [N], GA_z = %.8e [N], GJ = %.8e [N*M^2], EJ_Y = %.8e [N*M^2], EJ_Z = %.8e [N*M^2]\n', Diag_K(1,1),Diag_K(1,2),Diag_K(1,3),Diag_K(1,4),Diag_K(1,5),Diag_K(1,6));  
        fprintf(fileID, '\tlinear elastic generic, matr, #sec I const law\n'); % or viscoelastic
        fprintf(fileID, '\t\t\t%.8e, %.8e, %.8e, %.8e, %.8e, %.8e,\n', K(1, :, i + 1, 1));
        fprintf(fileID, '\t\t\t%.8e, %.8e, %.8e, %.8e, %.8e, %.8e,\n', K(2, :, i + 1, 1));
        fprintf(fileID, '\t\t\t%.8e, %.8e, %.8e, %.8e, %.8e, %.8e,\n', K(3, :, i + 1, 1));
        fprintf(fileID, '\t\t\t%.8e, %.8e, %.8e, %.8e, %.8e, %.8e,\n', K(4, :, i + 1, 1));
        fprintf(fileID, '\t\t\t%.8e, %.8e, %.8e, %.8e, %.8e, %.8e,\n', K(5, :, i + 1, 1));
        fprintf(fileID, '\t\t\t%.8e, %.8e, %.8e, %.8e, %.8e, %.8e,\n', K(6, :, i + 1, 1));
        %fprintf(fileID, '\t\tproportional, \n');
        %fprintf(fileID, '\t\t\t%.2f, # damping \n',BEAM.damp);
        fprintf(fileID, '\tfrom nodes, # automatically setting section II orientation\n');
        fprintf(fileID, '\t\t# EA = %.8e [N], GA_y = %.8e [N], GA_z = %.8e [N], GJ = %.8e [N*M^2], EJ_Y = %.8e [N*M^2], EJ_Z = %.8e [N*M^2]\n', Diag_K(2,1),Diag_K(2,2),Diag_K(2,3),Diag_K(2,4),Diag_K(2,5),Diag_K(2,6));  
        fprintf(fileID, '\tlinear elastic generic, matr, #sec II const law\n'); % or viscoelastic
        fprintf(fileID, '\t\t\t%.8e, %.8e, %.8e, %.8e, %.8e, %.8e,\n', K(1, :, i + 1, 2));
        fprintf(fileID, '\t\t\t%.8e, %.8e, %.8e, %.8e, %.8e, %.8e,\n', K(2, :, i + 1, 2));
        fprintf(fileID, '\t\t\t%.8e, %.8e, %.8e, %.8e, %.8e, %.8e,\n', K(3, :, i + 1, 2));
        fprintf(fileID, '\t\t\t%.8e, %.8e, %.8e, %.8e, %.8e, %.8e,\n', K(4, :, i + 1, 2));
        fprintf(fileID, '\t\t\t%.8e, %.8e, %.8e, %.8e, %.8e, %.8e,\n', K(5, :, i + 1, 2));
        fprintf(fileID, '\t\t\t%.8e, %.8e, %.8e, %.8e, %.8e, %.8e;\n', K(6, :, i + 1, 2));
        %fprintf(fileID, '\t\tproportional, \n');
        %fprintf(fileID, '\t\t\t%.2f; # damping \n',BEAM.damp);
        fprintf(fileID, '\n');
        tot_beam = tot_beam + 1;
    end

    fclose(fileID);
    BEAM.total.beam = tot_beam;

end
