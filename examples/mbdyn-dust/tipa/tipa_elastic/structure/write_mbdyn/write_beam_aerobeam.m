function write_beam_aerobeam(BEAM, Label_axis, Label_beam, node_idx)

    fileID = fopen(BEAM.file.aero.beam, 'wt');

    fprintf(fileID, '# %s\n', pwd);
    fprintf(fileID, '# %s\n', datetime('now'));
    fprintf(fileID, '# DO NOT MODIFY \n\n');
    fprintf(fileID, '\n');

    Chord = BEAM.Aero.Geometry.Chord.Value;
    %Twist = BEAM.Aero.Geometry.Twist.Value; 
    %Chord_ext = Chord(1:2:end);
    %Chord_mid = Chord(2:2:end-1);

    % Associate profile to beam

    STA_adim = BEAM.STA.adim;
    STA_adim = STA_adim(1:2:end);
    range = BEAM.Aero.C81.range;
    file = BEAM.Aero.C81.file;
    XNA = BEAM.STA.Value(node_idx);
    XNA = XNA(1:2:end);
    R = BEAM.Length;
    node_idx_ext = node_idx(1:2:end);
    Axis_ext = Label_axis.aero(1:2:end);
    Axis_mid = Label_axis.aero(2:2:end-1);
    tiploss =  BEAM.Aero.Tiploss.Value; 
    for i = 1:numel(Label_beam)
        beginx = XNA(i); % start node
        endx = XNA(i + 1); % end node
        midx = (beginx + endx)/2; % mid node
        if endx/R > range(1,1)
            fprintf(fileID, '# x1=%e xm=%e x2=%e (%.3f -> %.3f -> %.3f R)\n', beginx, midx, endx, beginx / R, midx / R, endx / R);
            fprintf(fileID, 'aerodynamic beam3:\n');
            fprintf(fileID, '\t%s, # blade aero pan name \n', Label_beam{i});
            fprintf(fileID, '\t%s, # blade beam name \n', Label_beam{i});
            fprintf(fileID, '\tinduced velocity, CURR_ROTOR,\n');
            fprintf(fileID, '\treference, %s, null, \n', Axis_ext{i});
            fprintf(fileID, '\t\t1, 0., 1., 0., 3, 1., 0., 0., # orientation wrt to ref system \n');
            fprintf(fileID, '\treference, %s, null, \n', Axis_mid{i});
            fprintf(fileID, '\t\t1, 0., 1., 0., 3, 1., 0., 0., # orientation wrt to ref system \n');
            fprintf(fileID, '\treference, %s, null, \n', Axis_ext{i + 1});
            fprintf(fileID, '\t\t1, 0., 1., 0., 3, 1., 0., 0., # orientation wrt to ref system \n');
            n_points = node_idx_ext(i + 1) - node_idx_ext(i) + 1;
            l_interval = BEAM.STA.Value(node_idx_ext(i + 1)) - BEAM.STA.Value(node_idx_ext(i));
            sum_interval = BEAM.STA.Value(node_idx_ext(i + 1)) + BEAM.STA.Value(node_idx_ext(i));
            fprintf(fileID, '\tpiecewise linear, %d, # defining chord shape as linear\n', n_points);

            for j = node_idx_ext(i):node_idx_ext(i+1)
                xi_chord = (2 * BEAM.STA.Value(j) - sum_interval) /l_interval;
                fprintf(fileID, '\t\t%.10f, %.10f, # element chord [m]\n', xi_chord, Chord(j));
            end

            fprintf(fileID, '\tconst, 0., # defining AC \n');
            
            fprintf(fileID, '\tpiecewise linear, %d, # defining BC trend as linear\n', n_points);

            for j = node_idx_ext(i):node_idx_ext(i+1)
                xi_chord = (2 * BEAM.STA.Value(j) - sum_interval) /l_interval;
                bc_chord = - Chord(j)/2;
                fprintf(fileID, '\t\t%.10f, %.10f, # element BC point [m]\n', xi_chord, bc_chord);
            end
            
            fprintf(fileID, '\tconst, 0., # defining Twist \n');

%             fprintf(fileID, '\tpiecewise linear, %d, # defining twist shape as piecewise linear\n', n_points);
% 
%             for j = node_idx_ext(i):node_idx_ext(i+1)
%                 xi_chord = (2 * BEAM.STA.Value(j) - sum_interval) /l_interval;
%                 fprintf(fileID, '\t\t%.10f, %.10f,# element twist ang
%                 [rad]\n', xi_chord, Twist(j)); % FIXME!!!! 
%             end
            if beginx / R <= tiploss && endx / R >= tiploss
                xi_tiploss = (2*tiploss - (endx/R + beginx/R)) / (endx/R - beginx/R);
                fprintf(fileID, '\ttip loss, piecewise linear, 2, \n');
                fprintf(fileID, '\t\t%.10f, 1., \n', xi_tiploss);
                fprintf(fileID, '\t\t%.10f, 0., \n', 1);
            elseif beginx / R >= tiploss
                fprintf(fileID, '\ttip loss, const, 0.,\n');                
            end                
            
            fprintf(fileID, '\t\t%s_GAUSS_POINTS, # name of blade Gauss points\n', BEAM.Name);

            Profile = find(STA_adim(i) <= range(:, 2) & STA_adim(i + 1) >= range(:, 1) );

            if numel(Profile) == 1
                name_profile = char(file{Profile});
                fprintf(fileID, '\t\tc81, %s,\n', name_profile(1:end-4));
            else
                fprintf(fileID, '\tc81, multiple, %d, \n', numel(Profile));

                for j = 1:numel(Profile)

                    if j == numel(Profile)
                        name_profile = char(file{Profile(j)});
                        fprintf(fileID, '\t\t%s, 1.0, #%.3f \n', name_profile(1:end-4), endx/R);
                    else
                        trans_point = 2*(range(Profile(j), 2) - STA_adim(i))/(STA_adim(i+1)-STA_adim(i));
                        trans_point_adim = (trans_point) - 1;
                        name_profile = char(file{Profile(j)});
                        fprintf(fileID, '\t\t%s, %f, #%.3f \n', name_profile(1:end-4), trans_point_adim, (range(Profile(j), 2)));
                    end

                end

            end
      %      fprintf(fileID, '\t\tc81, naca0012,\n'); % testing
            fprintf(fileID, '\t\t#unsteady, bielawa, # unsteadiness on\n');
            fprintf(fileID, '\t\tjacobian, 0; # jacobian output\n\n');

        end
    end
end
