function Label_yoke_blade = write_total_yoke_blade(Yoke, Blade, ROTOR, lab_basic_ref, yokeblade)
    
    fileID = fopen(yokeblade.file,'wt');
    
    Blade_joint = fieldnames(Blade.Joint);
    Yoke_joint = fieldnames(Yoke.Joint);
    Radius = ROTOR.Radius.Value;  

    for i = 1:numel(Blade_joint)
        % check node in common
        if sum(strcmpi(Yoke_joint, Blade_joint{i})) == 1
            ind_yoke = strcmpi(Yoke_joint, Blade_joint{i}) == 1;
            Dof = Blade.Joint.(Blade_joint{i}).Dof;
            Dof = str2num(char(cellstr(num2str(Dof(1))')));
            Node_blade = char(Blade.Joint.(Blade_joint{i}).Node);
            Node_yoke = char(Yoke.Joint.(Yoke_joint{ind_yoke}).Node);
            Location = Blade.Joint.(Blade_joint{i}).Location * Radius;
            
            fprintf(fileID, '\njoint: CURR_ROTOR + CURR_%s + %s, total joint,\n', Blade.Name, Blade_joint{i});
            Label_yoke_blade{i} = sprintf('CURR_ROTOR + CURR_%s + %s', Blade.Name, Blade_joint{i});
            fprintf(fileID, '\t%s,\n', Node_blade);
            fprintf(fileID, '\t\tposition, reference, %s, %.16f, 0., 0.,\n', lab_basic_ref, Location);
            fprintf(fileID, '\t\tposition orientation, reference, %s, eye,\n', lab_basic_ref);
            fprintf(fileID, '\t\trotation orientation, reference, %s, eye,\n', lab_basic_ref);
            fprintf(fileID, '\t%s,\n', Node_yoke); % curr model FIXME!
            fprintf(fileID, '\t\tposition, reference, %s, %.16f, 0., 0.,\n', lab_basic_ref, Location);
            fprintf(fileID, '\t\tposition orientation, reference, %s, eye,\n', lab_basic_ref);
            fprintf(fileID, '\t\trotation orientation, reference, %s, eye,\n', lab_basic_ref);
            fprintf(fileID, '\tposition constraint,\n');

            for j = 1:3
                in = find(j == Dof, 1);

                if isempty(in)
                    fprintf(fileID, '\t\tinactive,\n');
                else
                    fprintf(fileID, '\t\tactive,\n');
                end

            end

            fprintf(fileID, '\tnull,\n');
            fprintf(fileID, '\torientation constraint,\n');

            for j = 4:6
                in = find(j == Dof, 1);

                if isempty(in)
                    fprintf(fileID, '\t\tinactive,\n');
                else
                    fprintf(fileID, '\t\tactive,\n');
                end

            end

            fprintf(fileID, '\tnull;\n');
        end
        
    end
    fclose(fileID); 

end
