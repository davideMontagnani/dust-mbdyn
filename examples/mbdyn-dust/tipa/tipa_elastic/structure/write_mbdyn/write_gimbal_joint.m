function Label_gimbal = write_gimbal_joint(Yoke, Label_rotor_node, rotoryoke)

    fileID = fopen(rotoryoke.file, 'wt');

    if any(~cellfun('isempty', strfind(Label_rotor_node, 'HUB')))
        id_node = ~cellfun('isempty', strfind(Label_rotor_node, 'HUB')) == 1;
        Node_Hub = Label_rotor_node{id_node};
    end

    Node_Yoke = char(Yoke.Joint.Gimbal.Node);
    Dof = Yoke.Joint.Gimbal.Dof;
    Dof = str2num(char(cellstr(num2str(Dof(1))')));
    fprintf(fileID, '\njoint: OFFSET + CURR_%s + GIMBAL, total joint,\n', Yoke.Name);
    Label_gimbal = sprintf('OFFSET + CURR_%s + GIMBAL', Yoke.Name); 
    fprintf(fileID, '\t%s,\n', Node_Hub);
    fprintf(fileID, '\t\tposition, reference, node, null,\n');
    fprintf(fileID, '\t\tposition orientation, reference, node, eye,\n');
    fprintf(fileID, '\t\trotation orientation, reference, node, eye,\n');
    fprintf(fileID, '\t%s,\n', Node_Yoke);
    fprintf(fileID, '\t\tposition, reference, other node, null,\n');
    fprintf(fileID, '\t\tposition orientation, reference, other node, eye,\n');
    fprintf(fileID, '\t\trotation orientation, reference, other node, eye,\n');
    fprintf(fileID, '\tposition constraint,\n');

    for j = 1:3
        in = find(j == Dof, 1);

        if isempty(in)
            fprintf(fileID, '\t\tinactive,\n');
        else
            fprintf(fileID, '\t\tactive,\n');
        end

    end

    fprintf(fileID, '\tnull,\n');
    fprintf(fileID, '\torientation constraint,\n');

    for j = 4:6
        in = find(j == Dof, 1);

        if isempty(in)
            fprintf(fileID, '\t\tinactive,\n');
        else
            fprintf(fileID, '\t\tactive,\n');
        end

    end

    fprintf(fileID, '\tnull;\n');
    fclose(fileID);
