function [Label_node, MBDYN] = write_mbdyn_nodes(MBDYN,Label_ref,REF) 
    % write dynamic mbdyn nodes for each beam
    fileID_ND=fopen(MBDYN.file.nodes{end}, 'wt');
    fprintf(fileID_ND, '# this is a matlab generated file to give the nodes their names and cards\n');
    fprintf(fileID_ND, '# DO NOT MODIFY\n');
    fprintf(fileID_ND, '\n'); 
    tot_nodes = MBDYN.total.nodes; 
     % all nodes are coincident with the proper reference system 
    for i = 1:numel(REF.ID)
        Main_lab = strsplit(Label_ref{i});
        Label_node{i} = sprintf('%d', REF.ID(i) ); 
        fprintf(fileID_ND, 'structural:  %s, dynamic,\n',Label_node{i}); 
	    fprintf(fileID_ND, '\treference, %s, null, #abs pos,\n',Label_ref{i});
	    fprintf(fileID_ND, '\treference, %s, eye,  #abs orient\n',Label_ref{i});
	    fprintf(fileID_ND, '\treference, %s, null, #abs initial velocity\n',Label_ref{i});
	    fprintf(fileID_ND, '\treference, %s, null; #abs initial ang speed\n',Label_ref{i});
	    fprintf(fileID_ND, '\n');
        tot_nodes = tot_nodes + 1;
    end 
    fclose(fileID_ND);
    MBDYN.total.nodes = tot_nodes;  
    
end


