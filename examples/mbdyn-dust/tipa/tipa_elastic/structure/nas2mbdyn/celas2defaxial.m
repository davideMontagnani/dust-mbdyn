function MBDYN =  celas2defaxial(NASTRAN,MBDYN,offset)
    % this function convert the nastran celas2 card into deformable axial joint 

    fileID = fopen(MBDYN.file.joint{end}, 'wt');
    
    fprintf(fileID, 'set: const integer CURR_AXIAL= %d;\n', offset); % FIXME 
    
    EID = NASTRAN.CELAS2.EID;
    G1 = NASTRAN.CELAS2.G1;
    G2 = NASTRAN.CELAS2.G2;
    C1 = NASTRAN.CELAS2.C1;
    K = NASTRAN.CELAS2.K; 
    tot = MBDYN.total.joint; 
    for i = 1:numel(EID)
        if C1(i) == 4 || C1(i) == 5 || C1(i) == 6
            fprintf(fileID, 'joint: CURR_AXIAL + %d, deformable axial joint,\n',EID(i));
            fprintf(fileID, '\t%d,\n',G1(i));
            fprintf(fileID, '\tposition, reference, node, null,\n');
            fprintf(fileID, '\torientation, reference, node,\n');
            if C1(i) == 4
                fprintf(fileID, '\t\t3, 1., 0., 0.,\n');
                fprintf(fileID, '\t\t2, 0., 1., 0.,\n');
            elseif C1(i) == 5
                fprintf(fileID, '\t\t3, 0., 1., 0.,\n');
                fprintf(fileID, '\t\t1, 1., 0., 0.,\n');
            elseif C1(i) == 6
                fprintf(fileID, '\t\teye,\n'); 
            end
            fprintf(fileID, '\t%d,\n',G2(i));
            fprintf(fileID, '\tposition, reference, other node, null,\n');
            fprintf(fileID, '\torientation, reference, other node,\n');
            if C1(i) == 4
                fprintf(fileID, '\t\t3, 1., 0., 0.,\n');
                fprintf(fileID, '\t\t2, 0., 1., 0.,\n');
            elseif C1(i) == 5
                fprintf(fileID, '\t\t3, 0., 1., 0.,\n');
                fprintf(fileID, '\t\t1, 1., 0., 0.,\n');
            elseif C1(i) == 6
                fprintf(fileID, '\t\teye,\n'); 
            end
            fprintf(fileID, '\tlinear elastic, %f;\n',K(i)); 
        end
        tot = tot + 1;
    end
    MBDYN.total.joint = tot; 
    fclose(fileID);

end
