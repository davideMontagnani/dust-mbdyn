function MBDYN = spc2clamp(NASTRAN,MBDYN,offset)
    
    fileID = fopen(MBDYN.file.joint{end}, 'wt');
    SID = nonzeros(NASTRAN.SPC.SID);
    G1 = nonzeros(NASTRAN.SPC.G1);
    C1 = nonzeros(NASTRAN.SPC.C1);
    tot = MBDYN.total.joint; 
    fprintf(fileID, 'set: const integer CURR_SPC= %d;\n', offset); 
    for i = 1:numel(SID)
        if C1(i) == 123456
            fprintf(fileID, 'joint: CURR_SPC + %d, clamp, %d, node, node;\n', SID(i), G1(i));
        end
        % TODO: impose total pin joint 
        tot = tot + 1;
    end
    MBDYN.total.joint = tot; 
    fclose(fileID);
end