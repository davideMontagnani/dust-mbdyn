function [MBDYN, Label_beam] = write_cbar2beam(BEAM,MBDYN,offset)

    Node1 = BEAM.nodes_label(1:3:end-2);
    Node2 = BEAM.nodes_label(2:3:end-1);
    Node3 = BEAM.nodes_label(3:3:end);
    Ref1 = BEAM.ref_label(1:3:end-2);
    Ref2 = BEAM.ref_label(2:3:end-1);
    Ref3 = BEAM.ref_label(3:3:end);
    K_matrix = BEAM.K.Value; 
    fileID_EL = fopen(MBDYN.file.beams{end}, 'wt');
    fprintf(fileID_EL, '# this is a matlab generated file to give the beam elements their names and cards\n');
    fprintf(fileID_EL, '# DO NOT MODIFY\n');
    fprintf(fileID_EL, '\n');
    fprintf(fileID_EL, 'set: const integer CURR_STICK= %d;\n', offset); % FIXME 
    tot_beam = 0;
    for i=1:numel(Node1) %giving the wing beam elements names
        % with the same for cycle we generate the elements names (in 'wing_elements_m.set')
        % and assign them to the proper node card (in 'wing_m.el')
        % setting i-th element name
        % NEW FORMULATION - AUTOMATICALLY ASSINGINING BEAM GAUSS SECTION ORIENTATION
        % generating i-th card
        % fprintf(fileID_EL, 'set: const integer %s = ;\n',i,wing.el_names(i));
        fprintf(fileID_EL, '# Defining the beam/sections notation\n');
        fprintf(fileID_EL, '# Beam %s + %s, \n', 'CURR_STICK',Node1{i});
        fprintf(fileID_EL, '#                                       A  beam y \n');
        fprintf(fileID_EL, '#                                      /   adimensional axis\n');
        fprintf(fileID_EL, '#                                     /\n');
        fprintf(fileID_EL, '#                                    /\n');
        fprintf(fileID_EL, '#           |            |          |----------|-------------|---------> beam x adimensional axis\n');
        fprintf(fileID_EL, '#         -1/2       -1/sqrt(3)     0       1/sqrt(3)       1/2 \n');
        fprintf(fileID_EL, '#               ________________________________________________\n');
        fprintf(fileID_EL, '#             /                                                /\n');
        fprintf(fileID_EL, '#            /        section I             section II        /\n');
        fprintf(fileID_EL, '#           /_____________|_____________________|____________/ \n');
        fprintf(fileID_EL, '#           |           | | |       |         | | |          |  \n');
        fprintf(fileID_EL, '#           |           | V |       |         | V |          |  \n');
        fprintf(fileID_EL, '#           |           |   |       |         |   |          |  \n');
        fprintf(fileID_EL, '#           |  _________|   |_______|_________|   |__________|__ \n');
        fprintf(fileID_EL, '#           | /         |  /        | /       |  /           | /\n');
        fprintf(fileID_EL, '#           |/          | /         |/        | /            |/\n');
        fprintf(fileID_EL, '#           /___________|/__________/_________|/_____________/\n');
        fprintf(fileID_EL, '#       node 1=%s           node 2=%s              node 3=%s\n',Node1{i},Node2{i},Node3{i});
        fprintf(fileID_EL, '#      0\n');
        fprintf(fileID_EL, '#      |---\\....\\----------------------------------------------------------> global X axis \n');
        fprintf(fileID_EL, '#\n');
        fprintf(fileID_EL, 'beam3: %s + %d, # beam name\n', 'CURR_STICK',BEAM.ID(i)) ; 
        Label_beam{i} = sprintf('%s + %d', 'CURR_STICK',BEAM.ID(i));
        fprintf(fileID_EL, '\t%s , # node 1 name\n',Node1{i}); 
        fprintf(fileID_EL, '\t\tposition, reference, node, null, \n');
        fprintf(fileID_EL, '\t\torientation, reference, node, eye, # orientation for properties\n');
        fprintf(fileID_EL, '\t%s , # node 2 name\n',Node2{i});
        fprintf(fileID_EL, '\t\tposition, reference, node, null, \n');
        fprintf(fileID_EL, '\t\torientation, reference, node, eye, # orientation for properties\n');
        fprintf(fileID_EL, '\t%s , # node 3 name\n',Node3{i});
        fprintf(fileID_EL, '\t\tposition, reference, node, null, \n');
        fprintf(fileID_EL, '\t\torientation, reference, node, eye, # orientation for properties\n');
        fprintf(fileID_EL, '\tfrom nodes, # automatically setting section I orientation\n');
        fprintf(fileID_EL, '\tlinear elastic generic, diag, #sec I const law\n'); 
        fprintf(fileID_EL, '\t\t\t%.5e, %.5e, %.5e, \n\t\t\t%.5e, %.5e, %.5e,\n',diag(K_matrix(:,:,i)));
        %fprintf(fileID_EL, '\t\tproportional, \n');
        %fprintf(fileID_EL, '\t\t\t%.2f, # damping \n',BEAM.damp);
        fprintf(fileID_EL, '\tfrom nodes, # automatically setting section II orientation\n');
        fprintf(fileID_EL, '\tlinear elastic generic, diag, #sec II const law\n');
        fprintf(fileID_EL, '\t\t%.5e, %.5e, %.5e, \n\t\t%.5e, %.5e, %.5e;\n',diag(K_matrix(:,:,i)));
        %fprintf(fileID_EL, '\t\tproportional, \n');
        %fprintf(fileID_EL, '\t\t\t%.2f; # damping \n',BEAM.damp);
        fprintf(fileID_EL, '\n');
        tot_beam = tot_beam + 1; 
    end
    fclose(fileID_EL);
        MBDYN.total.beam  = tot_beam; 
end