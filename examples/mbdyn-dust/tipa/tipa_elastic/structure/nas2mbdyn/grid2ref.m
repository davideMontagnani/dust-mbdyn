function Label_ref = grid2ref(NASTRAN,NODE,MBDYN,offset)
    % write a reference system for each node of the beams 
    
    if isfield(NASTRAN,'CORD2R')
        [~, IndGrid] = ismember(NODE.CP,NASTRAN.CORD2R.CID);
        [~,n_ref] = hist(IndGrid,unique(IndGrid));
        lab_basic_ref = MBDYN.name_part;
    else 
        IndGrid = zeros(numel(NODE.CP),1);
        n_ref = 0;
        lab_basic_ref = 'global';
    end
    %file_ref = MBDYN.ref2grid_file; 
    fileID = fopen(MBDYN.file.ref_frame_nodes{end}, 'wt');
     
     % count the occurence of each ref sys
    Lab = zeros(numel(IndGrid),1);
    
    for jj = 1:numel(n_ref)
        it = 0; 
        for i = 1:numel(IndGrid) 
            it = it+1;
            if IndGrid(i) == n_ref(jj)                 
                Lab(i) = Lab(i) + it; 
            end
            
        end
    end
    for i = 1:numel(IndGrid)
        
        origin = [NODE.X1(i), NODE.X2(i), NODE.X3(i)];
        
        if IndGrid(i) ~ 0; 
            fprintf(fileID, 'reference: %s + %d , #gen ref\n', lab_basic_ref{IndGrid(i)}, Lab(i) ); %FIXME 
            Label_ref{i} = sprintf('%s + %d', lab_basic_ref{IndGrid(i)}, Lab(i)); 
            fprintf(fileID, '\treference, %s, %.16f, %.16f, %.16f,\n',lab_basic_ref{IndGrid(i)}, ...
            origin(1),origin(2),origin(3)); % position offset wrt to global reference frame
            fprintf(fileID, '\treference, %s, eye, \n',lab_basic_ref{IndGrid(i)}); % orientation matrix 
            fprintf(fileID, '\treference, %s, null,\n',lab_basic_ref{IndGrid(i)});
            fprintf(fileID, '\treference, %s, null;\n\n',lab_basic_ref{IndGrid(i)});
        else 
            fprintf(fileID, 'reference:  %d+%d , #gen ref\n', Lab(i),offset  ); %FIXME 
            Label_ref{i} = sprintf('%d + %d',  Lab(i),offset);
            fprintf(fileID, '\treference, global, %.16f, %.16f, %.16f,\n', ...
            origin(1),origin(2),origin(3)); % position offset wrt to global reference frame
            fprintf(fileID, '\treference, global, eye, \n'); % orientation matrix 
            fprintf(fileID, '\treference, global, null,\n');
            fprintf(fileID, '\treference, global, null;\n\n');
        end
    end
    
end
