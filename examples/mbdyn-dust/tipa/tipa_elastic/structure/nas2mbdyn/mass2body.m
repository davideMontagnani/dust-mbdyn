function write_mass2body(BEAM,MBDYN)
    %body: <body_label>, <node_label>,
    %      <mass>,
    %      reference, node, <cgx>, <cgy>, <cgz>,
    %      diag, <jx>, <jy>, <jz>;
    %   convert mass matrix into a body mbdyn card
    fileID = fopen(MBDYN.BODY.file, 'wt');
    % try without condense 
    for i = 1:size(BEAM.nodes,1)
        %Write Bodies
        for j = 1:3 
            S = BEAM.mass(4:6,1:3,j,i);
            M = BEAM.mass(1,1,j,i);
            J = BEAM.mass(4:6,4:6,j,i);
            
            fprintf(fileID, 'body: CURR_STICK + CURR_BODY + %s\n',BEAM.nodes{i,j});
            fprintf(fileID, '\t%s\n', BEAM.nodes{i,j});
            
            if M>eps
                cg(3) = S(2,1)/M;
                cg(2) = S(3,1)/M;
                cg(1) = S(3,2)/M;
                fprintf(fileID, '\n\t%f\n',M);
                fprintf(fileID, '\treference, node, %f, %f, %f, \n',cg(1),cg(2),cg(3));
                fprintf(fileID, '\t matr\n,');
                fprintf(fileID, '\t%f,\n', J(1,:));
                fprintf(fileID, '\t%f,\n', J(2,:));
                fprintf(fileID, '\t%f,\n', J(3,1:2));
                fprintf(fileID, '\t%f;\n', J(3,3));
            else 
                fprintf(fileID, '\n\t%f\n',1e-9);
                fprintf(fileID, '\treference, node, null\n');
                fprintf(fileID, '\t matr\n,');
                fprintf(fileID, '\t%f,\n', J(1,:));
                fprintf(fileID, '\t%f,\n', J(2,:));
                fprintf(fileID, '\t%f,\n', J(3,1:2));
                fprintf(fileID, '\t%f;\n', J(3,3));
            end
        end
        
    end
    fclose(fileID);
end