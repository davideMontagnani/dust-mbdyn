function MBDYN = write_nas2totjoint(NASTRAN,MBDYN,offset)
    % this function writes the total joint starting from the rbe2 elements and the celas 2 
    
    fileID = fopen(MBDYN.file.joint{end}, 'wt');
    RBE2ID = NASTRAN.RBE2.EID;
    GN = NASTRAN.RBE2.GN;
    GM1 = NASTRAN.RBE2.GM1;
    GM2 = nonzeros(NASTRAN.RBE2.GM2);
    CM = NASTRAN.RBE2.CM; 
    tot = MBDYN.total.joint;
    
    fprintf(fileID, 'set: const integer CURR_TOTAL_JOINT = %d;\n', offset); % FIXME 
    
    for i = 1:numel(GM1)
        dof = str2num(char(cellstr(num2str(CM(i))')));
        % get node label
        fprintf(fileID,'\njoint: CURR_TOTAL_JOINT + %d, total joint,\n\t%d,', RBE2ID(i)+GM1(i),GN(i));
        fprintf(fileID,'\n\t\tposition, reference, node, null,');
        fprintf(fileID,'\n\t\tposition orientation, reference, node, eye,');
        fprintf(fileID,'\n\t\trotation orientation, reference, node, eye,');
        fprintf(fileID,'\n\t%d,',GM1(i)); % curr model FIXME! 
        fprintf(fileID,'\n\t\tposition, reference, other node, null,');
        fprintf(fileID,'\n\t\tposition orientation, reference, other node, eye,');
        fprintf(fileID,'\n\t\trotation orientation, reference, other node, eye,');
        fprintf(fileID,'\n\tposition constraint,');
        for j = 1:3
            in = find(j == dof, 1);
            if isempty(in)
                fprintf(fileID,'\n\t\tinactive,');
            else
                fprintf(fileID,'\n\t\tactive,');
            end
        end
        fprintf(fileID,'\n\tnull,');
        fprintf(fileID,'\n\torientation constraint,');
        for j = 4:6
            in = find(j == dof, 1);
            if isempty(in)
                fprintf(fileID,'\n\t\tinactive,');
            else
                fprintf(fileID,'\n\t\tactive,');
            end
        end
        fprintf(fileID,'\n\tnull;');
        fprintf(fileID,'\n');
        tot = tot + 1; 
    end
    % write for gm2
    
    for i = 1:numel(GM2)
        dof = str2num(char(cellstr(num2str(CM(i))')));
        % get node label
        fprintf(fileID,'\njoint: CURR_TOTAL_JOINT + %d, total joint,\n\t%d,', RBE2ID(i)+GM2(i),GN(i));
        fprintf(fileID,'\n\t\tposition, reference, node, null,');
        fprintf(fileID,'\n\t\tposition orientation, reference, node, eye,');
        fprintf(fileID,'\n\t\trotation orientation, reference, node, eye,');
        fprintf(fileID,'\n\t%d,',GM2(i)); % curr model FIXME! 
        fprintf(fileID,'\n\t\tposition, reference, other node, null,');
        fprintf(fileID,'\n\t\tposition orientation, reference, other node, eye,');
        fprintf(fileID,'\n\t\trotation orientation, reference, other node, eye,');
        fprintf(fileID,'\n\tposition constraint,');
        for j = 1:3
            in = find(j == dof, 1);
            if isempty(in)
                fprintf(fileID,'\n\t\tinactive,');
            else
                fprintf(fileID,'\n\t\tactive,');
            end
        end
        fprintf(fileID,'\n\tnull,');
        fprintf(fileID,'\n\torientation constraint,');
        for j = 4:6
            in = find(j == dof, 1);
            if isempty(in)
                fprintf(fileID,'\n\t\tinactive,');
            else
                fprintf(fileID,'\n\t\tactive,');
            end
        end
        fprintf(fileID,'\n\tnull;');
        fprintf(fileID,'\n');
        tot = tot + 1; 
    end
    
    MBDYN.total.joint = tot; 
    fclose(fileID);


end