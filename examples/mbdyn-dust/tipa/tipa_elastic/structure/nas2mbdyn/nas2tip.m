clear; close all; clc; 
file_nas = fullfile('..','Table','ATTILA_WT-model_DS-off_v01.bdf');
NASTRAN = read_nas(file_nas); 
filename = fullfile('..','Table','wingprop.tip'); 
PART.IDPOINT.CBAR = 1001:1008; % reference nastran of beam axis, input
PART.IDPOINT.CONM2 = 1012:1019; 
PART.Units = 'SI Fondamental';
[~, IndCbar] = ismember(PART.IDPOINT.CBAR,NASTRAN.CBAR.ID); %get index of beam grid 
[~, IndConm2] = ismember(PART.IDPOINT.CONM2,NASTRAN.CONM2.G); %get index of beam grid 
% select only the necessary point

PART.SEC.Units = 'ADIM';
PART.SEC.Value = 1:numel(PART.IDPOINT.CBAR)+1;

% Get properties of the beam
for i = 1:numel(IndCbar)
    GA(i) = NASTRAN.CBAR.GA(IndCbar(i));
    GB(i) = NASTRAN.CBAR.GB(IndCbar(i));
    V(:,i) = [NASTRAN.CBAR.X1(IndCbar(i)); 
            NASTRAN.CBAR.X2(IndCbar(i));
            NASTRAN.CBAR.X3(IndCbar(i))]; %Station orientation vector
    PID(i) = NASTRAN.CBAR.PID(IndCbar(i));
    
end
%% Get coordinate position of beam nodes

G = union(GA,GB);
[~, IndGrid] = ismember(G,NASTRAN.GRID.ID); %get index of beam grid 

%% Check coordinate system 
CP = NASTRAN.GRID.CP(IndGrid);

for i = 1:numel(G)
    if isempty(CP(i)) || CP(i) == 0 % check basic coordinate system
        PART.X.Value(i) = NASTRAN.GRID.X1(IndGrid(i));
        PART.Y.Value(i) = NASTRAN.GRID.X2(IndGrid(i));   
        PART.Z.Value(i) = NASTRAN.GRID.X3(IndGrid(i));   
    end
end

%% Get orientation matrix of the elements 
for i = 1:numel(IndCbar)
    X_elem = [(PART.X.Value(i+1)-PART.X.Value(i))/norm(PART.X.Value(i+1)-PART.X.Value(i)); 
            (PART.Y.Value(i+1)-PART.Y.Value(i))/norm(PART.Y.Value(i+1)-PART.Y.Value(i)); 
            (PART.Z.Value(i+1)-PART.Z.Value(i))/norm(PART.Z.Value(i+1)-PART.Z.Value(i))]; 
    PART.Length(i) = sqrt(abs(PART.X.Value(i+1)-PART.X.Value(i))^2+abs(PART.Y.Value(i+1)-PART.Y.Value(i))^2 ...
                            + abs(PART.Z.Value(i+1)-PART.Z.Value(i))^2);
    X_elem(isnan(X_elem))=0;
    X_elem(:,i) = X_elem;
    Y_elem(:,i) = cross(V(:,i),X_elem(:,i));
    Z_elem(:,i) = cross(X_elem(:,i),Y_elem(:,i));
    PART.orientation(:,:,i) = [X_elem(:,i),Y_elem(:,i),Z_elem(:,i)];
end


%% Get material properties 

[~, IndPbar] = ismember(PID,NASTRAN.CBAR.PID); 

for i = 1:numel(IndPbar)
    PART.A.Value(i) = NASTRAN.PBAR.A(IndPbar(i));  
    PART.JP.Value(i) = NASTRAN.PBAR.J(IndPbar(i));  
    PART.JY.Value(i) = NASTRAN.PBAR.I2(IndPbar(i));  
    PART.JZ.Value(i) = NASTRAN.PBAR.I2(IndPbar(i));
    MAT_id(i) = NASTRAN.PBAR.MID(IndPbar(i));  
end

[~, IndMat1] = ismember(MAT_id,NASTRAN.MAT1.ID); 

for i = 1:numel(IndMat1)
    PART.E.Value(i) = NASTRAN.MAT1.E(IndMat1(i));
    PART.G.Value(i) = NASTRAN.MAT1.G(IndMat1(i));
end

for i = 1:numel(IndMat1)
    EA.Value(i) = PART.A.Value(i)*PART.E.Value(i);
    EJZ.Value(i) = PART.JZ.Value(i)*PART.E.Value(i);
    EJY.Value(i) = PART.JY.Value(i)*PART.E.Value(i);
    GJ.Value(i) = PART.JP.Value(i)*PART.G.Value(i);
end

%% compute Station value for element ends

for i = 2:numel(IndGrid) - 1 % taking out the extreme
    PART.EA.Value(i) =  (PART.Length(i-1)/2*EA.Value(i) + PART.Length(i)/2*EA.Value(i))/(PART.Length(i-1)/2+PART.Length(i)/2); 
    PART.EJZ.Value(i) = (PART.Length(i-1)/2*EJZ.Value(i) + PART.Length(i)/2*EJZ.Value(i))/(PART.Length(i-1)/2+PART.Length(i)/2); 
    PART.EJY.Value(i) = (PART.Length(i-1)/2*EJY.Value(i) + PART.Length(i)/2*EJY.Value(i))/(PART.Length(i-1)/2+PART.Length(i)/2); 
    PART.GJ.Value(i) =  (PART.Length(i-1)/2*GJ.Value(i) + PART.Length(i)/2*GJ.Value(i))/(PART.Length(i-1)/2+PART.Length(i)/2); 
end 

%% Computing the extreme values
PART.EA.Value(1) = EA.Value(1);
PART.EA.Value(end+1) = EA.Value(end);
PART.EJZ.Value(1) = EJZ.Value(1);
PART.EJZ.Value(end+1) = EJZ.Value(end);
PART.EJY.Value(1) = EJY.Value(1);
PART.EJY.Value(end+1) = EJY.Value(end);
PART.GJ.Value(1) = GJ.Value(1);
PART.GJ.Value(end+1) = GJ.Value(end);

%% Mass values 

for i = 1:numel(IndConm2)
    MASS.GID(i) = NASTRAN.CONM2.G(IndConm2(i)); % id grid whose the CONM2 is associated to 
    MASS.CID(i) = NASTRAN.CONM2.G(IndConm2(i));
    if ~isempty(MASS.CID(i)) || MASS.CID(i) == 0
        MASS.OFFSET.X(i) = NASTRAN.CONM2.X1(IndConm2(i));
        MASS.OFFSET.Y(i) = NASTRAN.CONM2.X2(IndConm2(i));
        MASS.OFFSET.Z(i) = NASTRAN.CONM2.X3(IndConm2(i));
        MASS.M(i) = NASTRAN.CONM2.M(IndConm2(i)); 
        MASS.IXX(i) = NASTRAN.CONM2.I11(IndConm2(i)); % be carefull on the conversion to MBDyn body
        MASS.IYY(i) = NASTRAN.CONM2.I22(IndConm2(i));
        MASS.IZZ(i) = NASTRAN.CONM2.I33(IndConm2(i));
        MASS.IYX(i) = NASTRAN.CONM2.I21(IndConm2(i));
        MASS.IZX(i) = NASTRAN.CONM2.I31(IndConm2(i));
        MASS.IZY(i) = NASTRAN.CONM2.I32(IndConm2(i));
    end    
end

%% Get mass position

[~,IndGridConm2] = ismember(MASS.GID,NASTRAN.GRID.ID);



for i = 1:numel(IndGridConm2)
    MASS.GRID.ID(i) = NASTRAN.GRID.ID(IndGridConm2(i));
    MASS.GRID.CS(i) = NASTRAN.GRID.CS(IndGridConm2(i));
    if ~isempty(MASS.GRID.CS(i)) || MASS.GRID.CS(i) == 0
        MASS.XCG(i) = MASS.OFFSET.X(i)+ NASTRAN.GRID.X1(IndGridConm2(i)); 
        MASS.YCG(i) = MASS.OFFSET.Y(i)+ NASTRAN.GRID.X2(IndGridConm2(i)); 
        MASS.ZCG(i) = MASS.OFFSET.Z(i)+ NASTRAN.GRID.X3(IndGridConm2(i)); 
    end
    
end

%% Store position vector of the mass element: each column represent the three
%% coordinate of the CONM2 in the global reference frame  


MASS.position.global = [MASS.XCG; MASS.YCG; MASS.ZCG];

%% Store position vector of the mass element: each column represent the three
%% coordinate of the CONM2 in the global reference frame  

PART.position.global = [PART.X.Value; PART.Y.Value; PART.Z.Value];

%% Re-orient the vectors along the the element orientation component

for i = 1:numel(IndConm2)
    MASS.position.element(:,i) = PART.orientation(:,:,i)*MASS.position.global(:,i);
end

for i = 1:numel(IndCbar)
    PART.position.element(:,i) = PART.orientation(:,:,i)*PART.position.global(:,i);
end

PART.position.element(:,end+1) = PART.orientation(:,:,end)*PART.position.global(:,end); 
%% Get m coefficients 

for i = 2:numel(IndConm2)
    MASS.m_xz(i) = (MASS.position.element(3,i-1)-MASS.position.element(3,i))/(MASS.position.element(1,i-1)-MASS.position.element(1,i));
    MASS.m_yz(i) = (MASS.position.element(2,i-1)-MASS.position.element(2,i))/(MASS.position.element(1,i-1)-MASS.position.element(1,i));
    MASS.SEC.XCG.element(i) = PART.position.element(1,i); 
    MASS.SEC.ZCG.element(i) = MASS.m_xz(i) * PART.position.element(1,i) + MASS.position.element(3,i-1); 
    MASS.SEC.YCG.element(i) = MASS.m_yz(i) * PART.position.element(1,i) + MASS.position.element(2,i-1); 

end

%% Fix ends

MASS.SEC.XCG.element(1) = PART.position.element(1,1);
MASS.SEC.XCG.element(end+1) = PART.position.element(1,end); 
MASS.SEC.YCG.element(1) = MASS.m_yz(1)* PART.position.element(1,1) + MASS.position.element(2,1);
MASS.SEC.YCG.element(end+1) = MASS.m_yz(end)* PART.position.element(1,end) + MASS.position.element(2,end);
MASS.SEC.ZCG.element(1) = MASS.m_xz(1)* PART.position.element(1,1) + MASS.position.element(3,1);
MASS.SEC.ZCG.element(end+1) = MASS.m_xz(end)* PART.position.element(1,end) + MASS.position.element(3,end);

MASS.SEC.position.element = [MASS.SEC.XCG.element; MASS.SEC.YCG.element; MASS.SEC.ZCG.element];

%% Re-orient in global reference system

for i = 1:numel(IndConm2)
    PART.CG.global.Value(:,i) = PART.orientation(:,:,i)'*MASS.SEC.position.element(:,i);  
end

%% Fix end

PART.CG.global.Value(:,end+1) = PART.orientation(:,:,end)'*MASS.SEC.position.element(:,end);
PART.XCG.Value = PART.CG.global.Value(1,:);
PART.YCG.Value = PART.CG.global.Value(2,:);
PART.ZCG.Value = PART.CG.global.Value(3,:);

for i = 1:numel(IndConm2)
    Mass = MASS.M(i);
    I_xx = MASS.IXX(i);
    I_yy = MASS.IYY(i);
    I_zz = MASS.IZZ(i);
    I_yx = MASS.IYX(i);
    I_zx = MASS.IZX(i);
    I_zy = MASS.IZY(i);
    R_x_p = MASS.XCG(i) - PART.XCG.Value(i);
    R_x_a = MASS.XCG(i) - PART.XCG.Value(i+1);
    R_y_p = MASS.YCG(i) - PART.YCG.Value(i);
    R_y_a = MASS.YCG(i) - PART.YCG.Value(i+1);
    R_z_p = MASS.ZCG(i) - PART.ZCG.Value(i);
    R_z_a = MASS.ZCG(i) - PART.ZCG.Value(i+1);
    R_a = sqrt(abs(R_x_a)^2 + abs(R_y_a)^2 +abs(R_z_a)^2);
    R_p = -sqrt(abs(R_x_p)^2 + abs(R_y_p)^2 +abs(R_z_p)^2);
    % element split 
    m_a = Mass*(R_a/(R_a-R_p));
    m_p = -Mass*(R_p/(R_a-R_p)); 
    I_xx_a = I_xx*(R_a/(R_a-R_p))+m_a*(R_a^2-R_x_a*R_x_a);
    I_yy_a = I_yy*(R_a/(R_a-R_p))+m_a*(R_a^2-R_y_a*R_y_a);
    I_zz_a = I_zz*(R_a/(R_a-R_p))+m_a*(R_a^2-R_z_a*R_z_a);
    I_yx_a = I_yx*(R_a/(R_a-R_p))+m_a*(R_a^2*0-R_y_a*R_x_a);
    I_zx_a = I_zx*(R_a/(R_a-R_p))+m_a*(R_a^2*0-R_z_a*R_x_a);
    I_zy_a = I_zy*(R_a/(R_a-R_p))+m_a*(R_a^2*0-R_z_a*R_y_a);
    I_xx_p = -I_xx*(R_p/(R_a-R_p))+m_p*(abs(R_p)^2-R_x_p*R_x_p);
    I_yy_p = -I_yy*(R_p/(R_a-R_p))+m_p*(abs(R_p)^2-R_y_p*R_y_p);
    I_zz_p = -I_zz*(R_p/(R_a-R_p))+m_p*(abs(R_p)^2-R_z_p*R_z_p);
    I_yx_p = -I_yx*(R_p/(R_a-R_p))+m_p*(abs(R_p)^2*0-R_y_p*R_x_p);
    I_zx_p = -I_zx*(R_p/(R_a-R_p))+m_p*(abs(R_p)^2*0-R_z_p*R_x_p);
    I_zy_p = -I_zy*(R_p/(R_a-R_p))+m_p*(abs(R_p)^2*0-R_z_p*R_y_p);
    Ixx.p(i) = I_xx_p; % collect everything in vectors
    Iyy.p(i) = I_yy_p;
    Izz.p(i) = I_zz_p;
    Iyx.p(i) = I_yx_p;
    Izx.p(i) = I_zx_p;
    Izy.p(i) = I_zy_p;
    m.p(i) = m_p;
    R.p(i) = R_p;
    Ixx.a(i) = I_xx_a;
    Iyy.a(i) = I_yy_a;
    Izz.a(i) = I_zz_a;
    Iyx.a(i) = I_yx_a;
    Izx.a(i) = I_zx_a;
    Izy.a(i) = I_zy_a;
    m.a(i) = m_a;
    R.a(i) = R_a;
end

for i = 2:numel(IndGrid) - 1
    PART.M.Value(i) = (m.a(i) + m.p(i-1))/(R.a(i)-R.p(i-1));
    PART.IXX.Value(i) = (Ixx.a(i) + Ixx.p(i-1))/(R.a(i)-R.p(i-1));
    PART.IYY.Value(i) = (Iyy.a(i) + Iyy.p(i-1))/(R.a(i)-R.p(i-1));
    PART.IZZ.Value(i) = (Izz.a(i) + Izz.p(i-1))/(R.a(i)-R.p(i-1));
    PART.IYX.Value(i) = (Iyx.a(i) + Iyx.p(i-1))/(R.a(i)-R.p(i-1));
    PART.IZX.Value(i) = (Izx.a(i) + Izx.p(i-1))/(R.a(i)-R.p(i-1));
    PART.IZY.Value(i) = (Izy.a(i) + Izy.p(i-1))/(R.a(i)-R.p(i-1));
end

%% Fix ends 

PART.M.Value(1) = m.a(1)/R.a(1);
PART.IXX.Value(1) = Ixx.a(1)/R.a(1);
PART.IYY.Value(1) = Iyy.a(1)/R.a(1);
PART.IZZ.Value(1) = Izz.a(1)/R.a(1);
PART.IYX.Value(1) = Iyx.a(1)/R.a(1);
PART.IZX.Value(1) = Izx.a(1)/R.a(1);
PART.IZY.Value(1) = Izy.a(1)/R.a(1);
PART.M.Value(end+1) = - m.p(end)/R.p(end);
PART.IXX.Value(end+1) = -Ixx.p(end)/R.p(end);
PART.IYY.Value(end+1) = -Iyy.p(end)/R.p(end);
PART.IZZ.Value(end+1) = -Izz.p(end)/R.p(end);
PART.IYX.Value(end+1) = -Iyx.p(end)/R.p(end);
PART.IZX.Value(end+1) = -Izx.p(end)/R.p(end);
PART.IZY.Value(end+1) = -Izy.p(end)/R.p(end);




%% write units for TiPa table 


if strcmpi(PART.Units,'SI Fondamental')
    PART.M.Units = 'KG/M'; 
    PART.IXX.Units = 'KG*M';
    PART.IYY.Units = 'KG*M';
    PART.IZZ.Units = 'KG*M';
    PART.IZY.Units = 'KG*M';
    PART.IYX.Units = 'KG*M';
    PART.IZX.Units = 'KG*M';
    PART.X.Units = 'M';
    PART.Y.Units = 'M';
    PART.Z.Units = 'M';
    PART.XCG.Units = 'M';
    PART.YCG.Units = 'M';
    PART.ZCG.Units = 'M';
    PART.EA.Units = 'N';
    PART.EJY.Units = 'N*M^2';
    PART.EJZ.Units = 'N*M^2';
    PART.GJ.Units = 'N*M^2';

end

%% Write Labels
PART.SEC.Label = 'SEC'; 
PART.M.Label = 'WEIGHT';
PART.IXX.Label = 'IXX'; 
PART.IYY.Label = 'IYY';
PART.IZZ.Label = 'IZZ';
PART.IZY.Label = 'IZY';
PART.IYX.Label = 'IYX';
PART.IZX.Label = 'IZX';
PART.X.Label =  'XNA';
PART.Y.Label =  'YNA';
PART.Z.Label =  'ZNA';
PART.XCG.Label = 'XCG';
PART.YCG.Label = 'YCG';
PART.ZCG.Label = 'ZCG';
PART.EA.Label = 'EA';
PART.EJY.Label = 'EJY';
PART.EJZ.Label = 'EJZ';
PART.GJ.Label = 'GJ';

%% Write table 

fid = fopen(filename,'wt');

fprintf(fid,'\tTABLE\n');
fprintf(fid,'\t%-8s%-16s%-16s%-16s%-16s%-16s%-16s%-16s%-16s%-16s%-16s%-16s%-16s%-16s%-16s%-16s%-16s%-16s\n', ...
                PART.SEC.Label,PART.M.Label,PART.IXX.Label,PART.IYY.Label,PART.IZZ.Label,PART.IZY.Label, ...
                PART.IYX.Label,PART.IZX.Label,PART.X.Label,PART.Y.Label,PART.Z.Label,PART.XCG.Label, ...
                PART.YCG.Label,PART.ZCG.Label,PART.EA.Label,PART.EJY.Label,PART.EJZ.Label,PART.GJ.Label ); 
fprintf(fid,'\t%-8s%-16s%-16s%-16s%-16s%-16s%-16s%-16s%-16s%-16s%-16s%-16s%-16s%-16s%-16s%-16s%-16s%-16s\n', ...
                PART.SEC.Units,PART.M.Units,PART.IXX.Units,PART.IYY.Units,PART.IZZ.Units,PART.IZY.Units, ...
                PART.IYX.Units,PART.IZX.Units,PART.X.Units,PART.Y.Units,PART.Z.Units,PART.XCG.Units, ...
                PART.YCG.Units,PART.ZCG.Units,PART.EA.Units,PART.EJY.Units,PART.EJZ.Units,PART.GJ.Units); 
for i = 1:numel(PART.SEC.Value)
    fprintf(fid,'\t%-8d%-16e%-16e%-16e%-16e%-16e%-16e%-16e%-16e%-16e%-16e%-16e%-16e%-16e%-16e%-16e%-16e%-16e\n', ...
                PART.SEC.Value(i),PART.M.Value(i),PART.IXX.Value(i),PART.IYY.Value(i),PART.IZZ.Value(i),PART.IZY.Value(i), ...
                PART.IYX.Value(i),PART.IZX.Value(i),PART.X.Value(i),PART.Y.Value(i),PART.Z.Value(i),PART.XCG.Value(i), ...
                PART.YCG.Value(i),PART.ZCG.Value(i),PART.EA.Value(i),PART.EJY.Value(i),PART.EJZ.Value(i),PART.GJ.Value(i)); 
end










