function MBDYN = rbe3todistance(NASTRAN,MBDYN,offset)
    % this function convert the rbe3 card into a distance giont in mbdyn
    fileID = fopen(MBDYN.file.joint{end}, 'wt');
    REFGRID = NASTRAN.RBE3.REFGRID;
    G1 = NASTRAN.RBE3.G1;
    G2 = NASTRAN.RBE3.G2;
    C1 = NASTRAN.RBE3.C1;
    tot = MBDYN.total.joint; 
    fprintf(fileID, 'set: const integer CURR_DISTANCE = %d;\n', offset); % FIXME
%     for i = 1:numel(G1)
%         fprintf(fileID, 'joint: CURR_DISTANCE + %d, distance,\n',REFGRID(i)+G1(i));
%         fprintf(fileID, '\t%d, position, null,\n',REFGRID(i));
%         fprintf(fileID, '\t%d, position, null,\n',G1(i));
%         fprintf(fileID, 'from nodes;\n\n');     
%         tot = tot + 1;
%     end
%     if isfield(NASTRAN.RBE3,'G2')
%         G2 = NASTRAN.RBE3.G2;
%         for i = 1:numel(G2)
%             fprintf(fileID, 'joint: CURR_DISTANCE + %d, distance,\n',REFGRID(i)+G2(i));
%             fprintf(fileID, '\t%d, position, null,\n',REFGRID(i));
%             fprintf(fileID, '\t%d, position, null,\n',G2(i));
%             fprintf(fileID, 'from nodes;\n\n');
%             tot = tot + 1; 
%         end
%     end
%      for i = 1:numel(G1)
%         fprintf(fileID, 'joint: CURR_DISTANCE + %d, total joint,\n\t%d,',REFGRID(i)+G1(i),REFGRID(i)); 
%         dof = str2num(char(cellstr(num2str(C1(i))')));
%         fprintf(fileID,'\n\t\tposition orientation, reference, node, eye,');
%         fprintf(fileID,'\n\t\trotation orientation, reference, node, eye,');
%         fprintf(fileID,'\n\t%d,',G1(i)); % curr model FIXME! 
%         fprintf(fileID,'\n\t\tposition, reference, other node, null,');
%         fprintf(fileID,'\n\t\tposition orientation, reference, other node, eye,');
%         fprintf(fileID,'\n\t\trotation orientation, reference, other node, eye,');
%         fprintf(fileID,'\n\tposition constraint,');
%         for j = 1:3
%             in = find(j == dof, 1);
%             if isempty(in)
%                 fprintf(fileID,'\n\t\tinactive,');
%             else
%                 fprintf(fileID,'\n\t\tactive,');
%             end
%         end
%         fprintf(fileID,'\n\tnull,');
%         fprintf(fileID,'\n\torientation constraint,');
%         for j = 4:6
%             in = find(j == dof, 1);
%             if isempty(in)
%                 fprintf(fileID,'\n\t\tinactive,');
%             else
%                 fprintf(fileID,'\n\t\tactive,');
%             end
%         end
%         fprintf(fileID,'\n\tnull;');
%         fprintf(fileID,'\n');
%         tot = tot + 1; 
%     end
%     % write for gm2
    
    for i = 1:numel(REFGRID)
        dof = str2num(char(cellstr(num2str(C1(i))')));
        % get node label
        fprintf(fileID,'\njoint: CURR_DISTANCE + %d, total joint,\n\t%d,', REFGRID(i)+G2(i),REFGRID(i));
        fprintf(fileID,'\n\t\tposition, reference, node, null,');
        fprintf(fileID,'\n\t\tposition orientation, reference, node, eye,');
        fprintf(fileID,'\n\t\trotation orientation, reference, node, eye,');
        fprintf(fileID,'\n\t%d,',G2(i)); % curr model FIXME! 
        fprintf(fileID,'\n\t\tposition, reference, other node, null,');
        fprintf(fileID,'\n\t\tposition orientation, reference, other node, eye,');
        fprintf(fileID,'\n\t\trotation orientation, reference, other node, eye,');
        fprintf(fileID,'\n\tposition constraint,');
        for j = 1:3
            in = find(j == dof, 1);
            if isempty(in)
                fprintf(fileID,'\n\t\tinactive,');
            else
                fprintf(fileID,'\n\t\tactive,');
            end
        end
        fprintf(fileID,'\n\tnull,');
        fprintf(fileID,'\n\torientation constraint,');
        for j = 4:6
            in = find(j == dof, 1);
            if isempty(in)
                fprintf(fileID,'\n\t\tinactive,');
            else
                fprintf(fileID,'\n\t\tactive,');
            end
        end
        fprintf(fileID,'\n\tnull;');
        fprintf(fileID,'\n');
        tot = tot + 1; 
    end
    
   

    fclose(fileID); 
    MBDYN.total.joint = tot; % maybe is better to distinguish between the joints... FIXME 

end

