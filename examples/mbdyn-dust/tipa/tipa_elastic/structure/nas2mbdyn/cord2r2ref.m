function NASTRAN = cord2r2ref(NASTRAN,MBDYN)
    %myFun - Description
    %
    % Syntax: PART = cord2r2ref(NASTRAN)
    %
    % Long description
    % This function translate the NASTRAN CORD2R card into the MBDYN reference system card
    n_reference  = numel(NASTRAN.CORD2R.CID);
    
    
    main_ref = MBDYN.main_ref;
    offset = MBDYN.offset;
    name_part = MBDYN.name_part; 
    fileID = fopen(MBDYN.file.ref_frame_main, 'wt');

    for i = 1:n_reference
        
        %CID(i) = NASTRAN.CORD2R.CID(i);
        RID(i) = NASTRAN.CORD2R.RID(i);
        
        origin = [NASTRAN.CORD2R.A1(i), NASTRAN.CORD2R.A2(i),NASTRAN.CORD2R.A3(i)];
        x3 = [NASTRAN.CORD2R.B1(i), NASTRAN.CORD2R.B2(i),NASTRAN.CORD2R.B3(i)]-origin;
        x1 = [NASTRAN.CORD2R.C1(i), NASTRAN.CORD2R.C2(i),NASTRAN.CORD2R.C3(i)]-origin;
        x2 = cross(x3, x1);
        if det([x1; x2; x3]) == 0
            warning('CORD2R %d definition has collinear points.', NASTRAN.CORD2R.CID(i));
        end
        
        % Build rotation matrix wrt to global reference frame 
        
        x3 = x3 ./ norm(x3);
        x2 = x2 ./ norm(x2);
        x1 = cross(x2, x3);
        x1 = x1 ./ norm(x1);
        R = [x1', x2', x3'];
        NASTRAN.CORD2R.R(:,:,i) = R; % allocate the rotation matrix  
        NASTRAN.CORD2R.origin(i,:) = origin;
        if RID(i) == 0 || isempty(RID(i)) % referecence system is defined with respect to the global ref frame 
            
            
            fprintf(fileID, 'set: const integer %s = %d; #set name\n', name_part{i}, offset(i));
            fprintf(fileID, 'reference: %s , #gen ref\n', name_part{i} );
            fprintf(fileID, '\treference, %s, %.16f,%.16f, %.16f,\n',main_ref,origin(1),origin(2),origin(3)); % position offset wrt to globla reference frame
            fprintf(fileID, '\treference, %s, \n',main_ref); % orientation matrix 
            fprintf(fileID, '\t\t1, %.16f,%.16f,%.16f,\n',R(:,1)');
            fprintf(fileID, '\t\t3, %.16f,%.16f,%.16f,\n',R(:,3)'); % CHECK!!! 
            fprintf(fileID, '\treference, %s, null,\n',main_ref);
            fprintf(fileID, '\treference, %s, null;\n\n',main_ref); 
        
        end
        
    end
    fclose(fileID); 

    
end