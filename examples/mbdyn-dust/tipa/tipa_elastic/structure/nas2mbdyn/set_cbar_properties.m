function  [NODE,K,MASS] =  set_cbar_properties(NASTRAN)

    % This function should replace nas2tip
    
    IndCbar = numel(NASTRAN.CBAR.ID); % get the total number of beam present in the stick model
    
    for i = 1:IndCbar
        GA.ID(i) = NASTRAN.CBAR.GA(i);
        GB.ID(i) = NASTRAN.CBAR.GB(i);
        V(:,i) = [NASTRAN.CBAR.X1(i); 
                NASTRAN.CBAR.X2(i);
                NASTRAN.CBAR.X3(i)]; %Station orientation vector
        PID(i) = NASTRAN.CBAR.PID(i);
    end
    

    [~, IndGA] = ismember(GA.ID,NASTRAN.GRID.ID); 
    [~, IndGB] = ismember(GB.ID,NASTRAN.GRID.ID); 
    
    for i = 1:numel(IndGA)
        GA.ID(i) = NASTRAN.GRID.ID(IndGA(i));
        GA.CP(i) = NASTRAN.GRID.CP(IndGA(i)); % get ref system (if 0 we are in the global refsys)
        GA.X1(i) = NASTRAN.GRID.X1(IndGA(i));
        GA.X2(i) = NASTRAN.GRID.X2(IndGA(i));   
        GA.X3(i) = NASTRAN.GRID.X3(IndGA(i)); 
        GA.coord(i,:) = [GA.X1(i), GA.X2(i), GA.X3(i)];  
    end

    for i = 1:numel(IndGB)
        GB.ID(i) = NASTRAN.GRID.ID(IndGB(i));
        GB.CP(i) = NASTRAN.GRID.CP(IndGB(i)); % get ref system (if 0 we are in the global refsys)
        GB.X1(i) = NASTRAN.GRID.X1(IndGB(i));
        GB.X2(i) = NASTRAN.GRID.X2(IndGB(i));   
        GB.X3(i) = NASTRAN.GRID.X3(IndGB(i));   
        GB.coord(i,:) = [GB.X1(i), GB.X2(i), GB.X3(i)];
    end
    
    % Write all node in the global ref. frame in order to place the middle node better  
    
    
    if isfield(NASTRAN,'CORD2R')
        [~, IndCpGA] = ismember(GA.CP,NASTRAN.CORD2R.CID);
        [~, IndCpGB] = ismember(GB.CP,NASTRAN.CORD2R.CID);
        for i = 1:numel(IndCpGA)
            if IndCpGA(i) == 0
                GA.glob(i,:) = GA.coord(i,:);
            else 
                GA.glob(i,:) = NASTRAN.CORD2R.origin(IndCpGA(i),:) + (NASTRAN.CORD2R.R(:,:,IndCpGA(i))*GA.coord(i,:)')'; 
            end
        end

        for i = 1:numel(IndCpGB)
            if IndCpGB(i) == 0
                GB.glob(i,:) = GB.coord(i,:);
            else 
                GB.glob(i,:) = NASTRAN.CORD2R.origin(IndCpGB(i),:) + (NASTRAN.CORD2R.R(:,:,IndCpGB(i))*GB.coord(i,:)')';
            end
        end 
    else 
        GA.glob = GA.coord;
        GB.glob = GB.coord;
        IndCpGA = zeros(numel(GA.glob),1);
        IndCpGB = zeros(numel(GB.glob),1); 
    end
    
    for i = 1:IndCbar 
        GC.glob(i,:) =[(GA.glob(i,1)+GB.glob(i,1))/2;
                             (GA.glob(i,2)+GB.glob(i,2))/2;
                             (GA.glob(i,3)+GB.glob(i,3))/2];  % place the middle node
        GC.ID(i) = 100000+GA.ID(i)+GB.ID(i)*1000; %FIXME 
    end 

    % setup up the segment of the element GA---1/\3----GC----1/\3---GB 
    %                                     |  L1  |     L2     |  L3  |      
    for i = 1:IndCbar
        % get length of the element 
        L(i) = sqrt((GC.glob(i,1)-GA.glob(i,1))^2+ (GC.glob(i,2)-GA.glob(i,2))^2+ (GC.glob(i,3)-GA.glob(i,3))^2); % half of the length of the element 
        L1(i) = 1/sqrt(3)*L(i);
        L2(i) = 2*(1-1/sqrt(3))*L(i);
        L3(i) = L1(i); 
    end

    %mat = [NASTRAN.CBAR.ID', GA.ID', GA.CP', GA.glob ,GB.ID', GB.CP', GB.glob];  
    
    % plot for check 
    
    for i = 1:IndCbar
        plot3([GA.glob(i,1) GB.glob(i,1)], [GA.glob(i,2) GB.glob(i,2)], [GA.glob(i,3) GB.glob(i,3)],'r')
        hold on
        plot3(GC.glob(i,1), GC.glob(i,2), GC.glob(i,3),'*b')        
        hold on
    end
    axis equal
    grid on; 
    % Write GC node in the reference  system of node GA (this is an aribitrary choice)

    if isfield(NASTRAN,'CORD2R')
        for i = 1:numel(IndCpGA)
            if IndCpGA(i) == 0
                GC.coord(i,:) = GC.glob(i,:);
            else
                GC.coord(i,:) = (GC.glob(i,:)-NASTRAN.CORD2R.origin(IndCpGA(i),:))*(NASTRAN.CORD2R.R(:,:,IndCpGA(i)));
             end    
                GC.CP(i) = int32(IndCpGA(i));
                
        end   
    else
        GC.coord = GC.glob; 
        GC.CP(i) = int32(0);
    end

    NODE.ID = zeros(1,numel(GA.ID)*3);
    NODE.ID(1:3:end-2) = GA.ID';
    NODE.ID(2:3:end-1) = GC.ID'; % offset to node GC
    NODE.ID(3:3:end) = GB.ID';


    NODE.CP = zeros(1,numel(GA.ID)*3);
    NODE.CP(1:3:end-2) = GA.CP';
    NODE.CP(2:3:end-1) = GC.CP';
    NODE.CP(3:3:end) = GB.CP';
    
    NODE.X1 = zeros(1,numel(GA.ID)*3);
    
    NODE.X1(1:3:end-2) = GA.coord(:,1);
    NODE.X1(2:3:end-1) = GC.coord(:,1);
    NODE.X1(3:3:end) = GB.coord(:,1); 
    
    NODE.X2 = zeros(1,numel(GA.ID)*3);
    
    NODE.X2(1:3:end-2) = GA.coord(:,2);
    NODE.X2(2:3:end-1) = GC.coord(:,2);
    NODE.X2(3:3:end) = GB.coord(:,2); 
    
    NODE.X3 = zeros(1,numel(GA.ID)*3);
    
    NODE.X3(1:3:end-2) = GA.coord(:,3);
    NODE.X3(2:3:end-1) = GC.coord(:,3);
    NODE.X3(3:3:end) = GB.coord(:,3); 
    
      
    % build rotation matrix of the reference frame of the cbar (see nastran manual)
    
    for i = 1:IndCbar
        x_elem(i,:) = [(GB.glob(i,1)- GA.glob(i,1)), (GB.glob(i,2)-GA.glob(i,2)), (GB.glob(i,3)-GA.glob(i,3))];
        x_v(i,:) = V(:,i)';
        x_3(i,:) = cross(x_elem(i,:),x_v(i,:));
        if norm(x_3(i,:)) == 0
            warning('Wrong orientation for bar %d', NASTRAN.CBAR.ID(i))
        end
        x_2(i,:) = cross(x_3(i,:),x_elem(i,:));
        % normalization
        x_1(i,:) = x_elem(i,:)./norm(x_elem(i,:));
        x_2(i,:) = x_2(i,:)./norm(x_2(i,:));
        x_3(i,:) = x_3(i,:)./norm(x_3(i,:));

        R(:,:,i) = [x_1(i,:)' x_2(i,:)' x_3(i,:)']; % rotation matrix of the element 

    end

    %% Get material properties to assemble the stiffness matrix 

    [~, IndPbar] = ismember(PID,NASTRAN.PBAR.ID); 


    
    for i = 1:numel(IndPbar) % according to Nastran manual

        A.Value(i) = NASTRAN.PBAR.A(IndPbar(i));  
        JX.Value(i) = NASTRAN.PBAR.J(IndPbar(i));  
        JY.Value(i) = NASTRAN.PBAR.I1(IndPbar(i));  
        JZ.Value(i) = NASTRAN.PBAR.I2(IndPbar(i));
        Rho.Value(i) = NASTRAN.PBAR.NSM(IndPbar(i));  
        MAT_id(i) = NASTRAN.PBAR.MID(IndPbar(i));   
        % assembly of mass matrix 
        M1 = Rho.Value(i) * L1(i);
        M2 = Rho.Value(i) * L2(i);
        M3 = Rho.Value(i) * L3(i);
        J1 = 1/12 * (M1 * L1(i)^2); % rotational inertia respect to CG1
        J2 = 1/12 * (M2 * L2(i)^2); % rotational inertia respect to CG2
        J3 = 1/12 * (M3 * L3(i)^2); % rotational inertia respect to CG3
        Jx1 = (Rho.Value(i)/A.Value(i)*L1(i)) * (JY.Value(i) + JZ.Value(i));
        Jx2 = (Rho.Value(i)/A.Value(i)*L2(i)) * (JY.Value(i) + JZ.Value(i));
        Jx3 = (Rho.Value(i)/A.Value(i)*L3(i)) * (JY.Value(i) + JZ.Value(i));
        MASS(1:3,1:3,1,i) = diag([M1, M1, M1]);
        MASS(1:3,1:3,2,i) = diag([M2, M2, M2]);
        MASS(1:3,1:3,3,i) = diag([M3, M3, M3]);
        
        % BODY 1
        S = zeros(3,3);
        Rot(:,:) = R(:,:,i);
        v = L1(i)/2.*Rot(:,1)'; % cg body 1 in local ref frame.... CHECK!! 
        S(2,1) = M1 * v(3); 
        S(3,1) = -M1 * v(2);
        S(1,2) = -S(2,1);
        S(3,2) = M1 * v(1);
        S(1,3) = -S(3,1);
        S(2,3) = -S(3,2);
        MASS(4:6,1:3,1,i)  = S;
        MASS(1:3,4:6,1,i)  = S';
        J = diag([Jx1, J1, J1]); 
        RJ = Rot(:,:) * sqrt(J);
        MASS(4:6,4:6,1,i) = RJ * RJ' - M1 .* (skew(v) * skew(v)); 
        
        %BODY 2 
        S = zeros(3,3);
        v = [0 0 0];
        S(2,1) = M2 * v(3); 
        S(3,1) = -M2 * v(2);
        S(1,2) = -S(2,1);
        S(3,2) = M2 * v(1);
        S(1,3) = -S(3,1);
        S(2,3) = -S(3,2);
        MASS(4:6,1:3,2,i)  = S;
        MASS(1:3,4:6,2,i)  = S';
        J = diag([Jx2, J2, J2]); 
        RJ = Rot(:,:) * sqrt(J);
        MASS(4:6,4:6,2,i) = RJ * RJ' - M2 .* (skew(v) * skew(v)); 
        
        %BODY 3
        S = zeros(3,3);
        v = -L3(i)/2.*Rot(:,1)'; % cg body 3 in local ref frame.... CHECK!! 
        S(2,1) = M3 * v(3); 
        S(3,1) = -M3 * v(2);
        S(1,2) = -S(2,1);
        S(3,2) = M3 * v(1);
        S(1,3) = -S(3,1);
        S(2,3) = -S(3,2);
        MASS(4:6,1:3,3,i)  = S;
        MASS(1:3,4:6,3,i)  = S';
        J = diag([Jx3, J3, J3]); 
        RJ = Rot(:,:) * sqrt(J);
        MASS(4:6,4:6,3,i) = RJ * RJ' - M3 .* (skew(v) * skew(v)); 
        
    end

    [~, IndMat1] = ismember(MAT_id,NASTRAN.MAT1.ID); 
    
    % assembly of stiffness matrix
    
    for i = 1:numel(IndMat1)
        E.Value(i) = NASTRAN.MAT1.E(IndMat1(i));
        G.Value(i) = NASTRAN.MAT1.G(IndMat1(i));
        EA.Value(i) = A.Value(i)*E.Value(i);
        GAY.Value(i) = G.Value(i)*A.Value(i);
        GAZ.Value(i) = G.Value(i)*A.Value(i);
        EJZ.Value(i) = JZ.Value(i)*E.Value(i);
        EJY.Value(i) = JY.Value(i)*E.Value(i);
        GJ.Value(i) = JX.Value(i)*G.Value(i);
        K.Value(:,:,i) = diag([EA.Value(i),GAY.Value(i),GAZ.Value(i)...
                            GJ.Value(i),EJY.Value(i),EJZ.Value(i)]);
    end     

    % RBE2 








        

end