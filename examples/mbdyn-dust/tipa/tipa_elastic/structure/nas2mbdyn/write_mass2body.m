function MBDYN = write_mass2body(BEAM,MBDYN,offset)
    %body: <body_label>, <node_label>,
    %      <mass>,
    %      reference, node, <cgx>, <cgy>, <cgz>,
    %      diag, <jx>, <jy>, <jz>;
    %   convert mass matrix into a body mbdyn card
    fileID = fopen(MBDYN.file.bodies{end}, 'wt');
    
    
    fprintf(fileID, 'set: const integer CURR_BODY= %d;\n', 3*offset); % FIXME 
    
    [nodes_uniques, ~, rIc]=unique(BEAM.nodes,'stable');
    occurence = histc(rIc, 1:numel(nodes_uniques)); 
    occ_matrix = reshape(occurence(rIc),[size(BEAM.nodes,1),size(BEAM.nodes,2)]); 
    
    for i = 1:size(occ_matrix,1)
        for j = 1:size(occ_matrix,2)
            if occ_matrix(i,j) ~ 0;
                [m,n] = find(strcmp(BEAM.nodes,BEAM.nodes(i,j))); % find the position of the same nodes
                fprintf(fileID, 'body: CURR_BODY + %s,\n',BEAM.nodes{i,j});
                fprintf(fileID, '\t%s,\n', BEAM.nodes{i,j});
                fprintf(fileID, '\tcondense,%d,\n',occ_matrix(i,j));
                for ii = 1:numel(m)
                    
                        S = BEAM.mass(4:6,1:3,n(ii),m(ii));
                        M = BEAM.mass(1,1,n(ii),m(ii));
                        J = BEAM.mass(4:6,4:6,n(ii),m(ii));
                        if M>eps
                            cg(3) = S(2,1)/M;
                            cg(2) = S(3,1)/M;
                            cg(1) = S(3,2)/M;
                            fprintf(fileID, '\t%f,\n',M);
                            fprintf(fileID, '\treference, node, %f, %f, %f, \n',cg(1),cg(2),cg(3));
                            fprintf(fileID, '\t\tmatr,\n');
                            fprintf(fileID, '\t\t%f,\n', J(1,:));
                            fprintf(fileID, '\t\t%f,\n', J(2,:));
                            fprintf(fileID, '\t\t%f,\n', J(3,1:2));
                            if ii == numel(m)
                                fprintf(fileID, '\t\t%f;\n', J(3,3));
                            else
                                fprintf(fileID, '\t\t%f,\n', J(3,3));
                            end
                        else 
                            fprintf(fileID, '\t%e,\n',1e-9);
                            fprintf(fileID, '\treference, node, null,\n');
                            fprintf(fileID, '\t\tmatr,\n');
                            fprintf(fileID, '\t\t%e,\n', 1e-9+J(1,:)); % avoid singularity
                            fprintf(fileID, '\t\t%e,\n', 1e-9+J(2,:));
                            fprintf(fileID, '\t\t%e,\n', 1e-9+J(3,1:2));
                            if ii == numel(m)
                                fprintf(fileID, '\t\t%e;\n', 1e-9+J(3,3));
                            else
                                fprintf(fileID, '\t\t%e,\n', 1e-9+J(3,3));
                            end
                        end
                        occ_matrix(m(ii),n(ii)) = 0; % cleanup  
                end
                
                fprintf(fileID, '\n'); 
            end
        end
    end
    
    tot_bodies = numel(nodes_uniques);


    MBDYN.total.bodies  = tot_bodies; 
    fclose(fileID);
end