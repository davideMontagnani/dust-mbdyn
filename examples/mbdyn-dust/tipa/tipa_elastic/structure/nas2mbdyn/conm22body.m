function MBDYN = conm22body(NASTRAN,MBDYN,offset)
    fileID = fopen(MBDYN.file.bodies{end}, 'wt');
    ID = NASTRAN.CONM2.ID;
    G = NASTRAN.CONM2.G;
    %CID = NASTRAN.CONM2.CID; 
    cg = [NASTRAN.CONM2.X1' NASTRAN.CONM2.X2' NASTRAN.CONM2.X3'] ;
    M = NASTRAN.CONM2.M;
    tot = MBDYN.total.bodies;
    fprintf(fileID, 'set: const integer CURR_CONM2 = %d;\n', offset); % FIXME 
    %fprintf(fileID, 'set: const integer CURR_BODY = %d;\n', offset); % FIXME 
    for i = 1:numel(ID)
        J(:,:,i) = [NASTRAN.CONM2.I11(i), - NASTRAN.CONM2.I21(i), - NASTRAN.CONM2.I31(i);
                    - NASTRAN.CONM2.I21(i), NASTRAN.CONM2.I22(i), - NASTRAN.CONM2.I32(i);
                    - NASTRAN.CONM2.I31(i), - NASTRAN.CONM2.I32(i), NASTRAN.CONM2.I33(i)];
        fprintf(fileID, 'body: CURR_BODY  + CURR_CONM2 + %s,\n', num2str(ID(i)));
        fprintf(fileID, '\t%s,\n', num2str(G(i)));
        fprintf(fileID, '\t%f,\n',M(i));
        fprintf(fileID, '\treference, node, %f, %f, %f, \n',cg(i,1),cg(i,2),cg(i,3));
        fprintf(fileID, '\t\tmatr,\n');
        fprintf(fileID, '\t\t%f,\n', J(1,:,i)+1e-9);
        fprintf(fileID, '\t\t%f,\n', J(2,:,i)+1e-9);
        fprintf(fileID, '\t\t%f,\n', J(3,1:2,i)+1e-9);
        fprintf(fileID, '\t\t%f;\n\n', J(3,3,i)+1e-9);
        tot = tot + 1;
    end
    MBDYN.total.bodies = tot;
    fclose(fileID);

end