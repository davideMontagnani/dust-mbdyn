function m = skew(vec)
    % this function return the skew symmetric matrix given a 3*1 vector
    m = zeros(3,3);

    m(1,2) = -vec(3);
    m(1,3) = vec(2);
    m(2,3) = -vec(1);

    m(2,1) = -m(1,2);
    m(3,1) = -m(1,3);
    m(3,2) = -m(2,3);
end