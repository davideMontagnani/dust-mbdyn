function MBDyn_stick_writer(MBDYN)
    % since many components appear in the model, 
    % the code generates the input file at the end of the code
    % this allows to control much more easily the exact sequence 
    % of commands to provide to MBDyn software

    % starting the input file assembly
    fileID = fopen(MBDYN.main.file,'wt');
    fprintf(fileID, '# MAIN FILE TO CALL THE PARAMETRIC CODE FOR WRATS MULTIBODY MODEL\n');
    fprintf(fileID, '## the main attempt is to try to generate everything with a matlab code. \n');
    fprintf(fileID, '\n');

    % data part calling
    % defining problem type
    fprintf(fileID, '# DATA SECTION\n');
    fprintf(fileID, 'begin: data;\n');
    fprintf(fileID, '\tproblem: %s;\n', MBDYN.param.type);
    fprintf(fileID, 'end: data;\n');
    fprintf(fileID,	'\n');
    %fprintf(fileID, 'include:"initial_para_m.set";\n');
    fprintf(fileID, 'begin: %s;\n', MBDYN.param.type);
    fprintf(fileID, '\ttime step: %e;\n', MBDYN.param.time_step);
    fprintf(fileID, '\tinitial time: %f;\n', MBDYN.param.initial_time);
    fprintf(fileID, '\tfinal time: %f + %e*%d;\n', MBDYN.param.final_time, MBDYN.param.time_step, MBDYN.param.n_steps);
    fprintf(fileID, '\tmax iterations: %d;\n',MBDYN.param.max_iteration);
    fprintf(fileID, '\ttolerance: %e;\n', MBDYN.param.tolerance);
    fprintf(fileID, '\tderivatives tolerance: %e;\n', MBDYN.param.der_tolerance);
    fprintf(fileID, '\tderivatives max iterations: %d;\n', MBDYN.param.der_max_iteration);
    fprintf(fileID, '\tderivatives coefficient: %e;\n', MBDYN.param.der_coefficients);
    fprintf(fileID, '\tlinear solver: naive, colamd, pivot factor, 1.0;\n');
    fprintf(fileID, '\tmethod: ms, 0.4; \n');
    switch MBDYN.output
        case   'residual'
            fprintf(fileID, '\toutput: residual;\n');
    end
    switch MBDYN.analysis % meglio con isfield 
        case 'eigenanalysis'
            fprintf(fileID, '\teigenanalysis: %.5f,\n', MBDYN.param.eig_time);
            fprintf(fileID, '\t\toutput matrices,\n');
            fprintf(fileID, '\t\toutput eigenvectors,\n');
            fprintf(fileID, '\t\toutput geometry,\n');
            fprintf(fileID, '\t\tupper frequency limit, %d,\n', MBDYN.param.eig_freq);
            fprintf(fileID, '\t\tuse lapack;\n');
    end
    fprintf(fileID, 'end: %s;\n',MBDYN.param.type);
    fprintf(fileID, '# CONTROL DATA SECTION\n');
    fprintf(fileID, 'begin: control data;\n');
    fprintf(fileID, '\tinertia:1;\n');
    %if para.force ~= 1
    %    if control.eigenanalysis_on ~=1
    %    fprintf(fileID, '\tgravity;\n');
    %    end
    %    if control.wing_aero_on == 1;
    %        fprintf(fileID, '\tair properties;\n');
    %    end
    %end
    fprintf(fileID, '\tstructural nodes:\n');
    fprintf(fileID, '\t\t%d     # totalnodes\n',MBDYN.total.nodes);
    fprintf(fileID, '\t\t;\n');
    fprintf(fileID, '\trigid bodies:\n');
    fprintf(fileID, '\t\t%d     # rigid bodies\n',MBDYN.total.bodies);
    fprintf(fileID, '\t\t;\n');
    fprintf(fileID, '\tbeams:\n');
    fprintf(fileID, '\t\t%d     # beams\n',MBDYN.total.beam);
    fprintf(fileID, '\t\t;\n');
    fprintf(fileID, '\taerodynamic elements:\n');
    fprintf(fileID, '\t\t%d\n', MBDYN.total.aero);
    fprintf(fileID, '\t\t;\n');
    fprintf(fileID, '\tjoints:\n');
    fprintf(fileID, '\t\t%d     # joints\n',MBDYN.total.joint);
    fprintf(fileID, '\t\t;\n');  
    fprintf(fileID, '\toutput results: netcdf,no text; # printing ''.nc'' file\n');
    fprintf(fileID, '\tdefault output: reference frames;\n');
    fprintf(fileID, '\tprint: all;\n');
    fprintf(fileID, 'end: control data;\n');
    fprintf(fileID, '\n');

    % defining the nodes section
    % assigning all nodes related sets
    fprintf(fileID, '#defining nodes data\n');
    if isfield(MBDYN.file, 'ref_frame_main')
        fprintf(fileID,	'include: "%s";     #main ref frames\n',MBDYN.file.ref_frame_main);
    end
    for i = 1:numel(MBDYN.file.ref_frame_nodes)
        fprintf(fileID,	'include: "%s";     # node ref frames\n', MBDYN.file.ref_frame_nodes{i});
    end
    fprintf(fileID, '\n');
    % defining the section itself
    fprintf(fileID, 'begin: nodes;\n');
    for i = 1:numel(MBDYN.file.nodes)
        fprintf(fileID,	'\tinclude: "%s";     # nodes cards\n',MBDYN.file.nodes{i});
    end
    fprintf(fileID, 'end: nodes;\n');

    fprintf(fileID, 'begin: elements;\n');
    for i = 1:numel(MBDYN.file.beams)
        fprintf(fileID,	'\tinclude: "%s";      # beams cards\n', MBDYN.file.beams{i});
    end
    
    for i = 1:numel(MBDYN.file.bodies)
        fprintf(fileID,	'\tinclude: "%s";      # rigid bodies cards\n', MBDYN.file.bodies{i});
    end
    
    for i = 1:numel(MBDYN.file.joint)
        fprintf(fileID,	'\tinclude: "%s";      # joints cards\n', MBDYN.file.joint{i});
    end
    
    for i = 1:numel(MBDYN.file.beams_aero)
        fprintf(fileID, '\tinclude: "%s";      # aerodynamic beam\n', MBDYN.file.beams_aero{i});
    end
    
    fprintf(fileID, '\tinertia: 1, \n');
    fprintf(fileID,	'\t\tposition, reference, global, null,\n');
    fprintf(fileID, '\t\tbody, all, output, always;\n');
    fprintf(fileID, 'end: elements;\n');
    fclose(fileID);

end
