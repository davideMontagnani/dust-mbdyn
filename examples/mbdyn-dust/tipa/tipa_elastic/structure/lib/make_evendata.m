function BEAM = make_evendata(BEAM,start)
    % this function make even data on the tables: it merge the aerodynamic table and the structural table.
    % All data must be placed in the same order: root -> tip

    % Aerodynamic data
    if isfield(BEAM,'Aero')
        Twist = BEAM.Aero.Geometry.Twist.Value;
        R_aero = BEAM.Aero.Geometry.Radial.Value;
        YCA = BEAM.Aero.Geometry.Sweep.Value;
        ZCA = BEAM.Aero.Geometry.Anhedral.Value;
        Chord = BEAM.Aero.Geometry.Chord.Value;
    else
        Twist = zeros(1,numel(BEAM.STA.Value));
        R_aero = BEAM.STA.Value;
        YCA = zeros(1,numel(BEAM.STA.Value));
        ZCA = zeros(1,numel(BEAM.STA.Value));
        Chord = zeros(1,numel(BEAM.STA.Value));
    end
    
    % Structural data
    R_struct = BEAM.STA.Value;
    R_reduced = BEAM.STA.Red; 
    Weight = BEAM.WEIGHT.Value;
    YCG = BEAM.YCG.Value;
    ZCG = BEAM.ZCG.Value;
    YNA = BEAM.YNA.Value;
    ZNA = BEAM.ZNA.Value;
    YCT = BEAM.YCT.Value;
    ZCT = BEAM.ZCT.Value;
    Rotapi = BEAM.ROTAPI.Value;
    Rotan = BEAM.ROTAN.Value;
    JP = BEAM.JP.Value;
    JY = BEAM.JY.Value;
    JZ = BEAM.JZ.Value;
    EA = BEAM.EA.Value;
    GJ = BEAM.GJ.Value;
    EJY = BEAM.EJY.Value;
    EJZ = BEAM.EJZ.Value;
    BEAM.Length = max(R_struct); 
    % initialize data
    beam_station = 1;
    beam_aero = [R_aero', Twist', YCA', ZCA', Chord']; 
    beam_struct = [R_struct', Weight', YCG', ZCG', YNA', ZNA', YCT', ZCT', Rotapi', Rotan',...
                   JP', JY', JZ', EA', GJ', EJY', EJZ']; 
    beam_data(1, :) = [0, R_aero(1), Twist(1), YCA(1), ZCA(1), Chord(1), ...
                        R_struct(1), Weight(1), YCG(1), ZCG(1), YNA(1), ZNA(1), YCT(1), ZCT(1), ...
                        Rotapi(1), Rotan(1), JP(1), JY(1), JZ(1), EA(1), GJ(1), EJY(1), EJZ(1)];
    beam_label = {'Station', 'R_struct', 'R_aero', 'Twist', 'YCA', 'ZCA', 'Chord', 'dm', 'YCG', 'ZCG', ...
        'YNA', 'ZNA', 'YCT', 'ZCT', 'Rotapi', 'Rotan', 'JP', 'JY', 'JZ', 'EA', 'GJ', 'EJY', 'EJZ'};
    i_beam = 2;
    i_R_aero = 2;
    i_R_struct = 2;
    i_R_red = 2; 
    while 1
        t = [R_aero(i_R_aero), R_struct(i_R_struct), R_reduced(i_R_red)];
        jj = find(t == min(t));
        j = jj(1);
        new_x = t(j);
        beam_data(i_beam, beam_station) = new_x;
        % aero
        dx = R_aero(i_R_aero) - R_aero(i_R_aero - 1);
        d1 = R_aero(i_R_aero) - new_x;
        d2 = new_x - R_aero(i_R_aero - 1);

        beam_data(i_beam, 2:6) = ([d1 d2] / dx) * beam_aero(i_R_aero + [-1, 0], :);

        % structural
        dx = R_struct(i_R_struct) - R_struct(i_R_struct - 1);
        d1 = R_struct(i_R_struct) - new_x;
        d2 = new_x - R_struct(i_R_struct - 1);

        beam_data(i_beam, 7:23) = ([d1 d2] / dx) * beam_struct(i_R_struct + [-1, 0], :);

        if (new_x == BEAM.Length)% adjust the input
            break;
        end

        % copy the previous line

        beam_data(i_beam + 1, :) = beam_data(i_beam, :);

        % change only the required values

        for k = 1:length(jj)
            if jj(k) == 1 % preserve the aerodynamic data
                beam_data(i_beam, 2:6) = beam_aero(i_R_aero,:);
                i_R_aero = i_R_aero + 1;
            elseif jj(k) == 2 % preserve the structural data
                beam_data(i_beam, 7:23) = beam_struct(i_R_struct,:);
                i_R_struct = i_R_struct + 1;
            elseif jj(k) == 3
                i_R_red = i_R_red + 1; 
            end
        end

        i_beam = i_beam + 1;
    end

    BEAM.data = beam_data;
    
    BEAM.label = beam_label;
    % Update data: 
    % check starting point 
    idx = find(BEAM.data(:,1) < start); 
    
    if isempty(idx)
        idstart = 1; 
    else 
        idstart = idx(end) + 1;
    end
    BEAM.STA.Value = BEAM.data(idstart:end ,1); 
    
    if isfield(BEAM, 'Aero')
        BEAM.Aero.Geometry.Twist.Value = BEAM.data(idstart:end,3); 
        BEAM.Aero.Geometry.Sweep.Value = BEAM.data(idstart:end,4); 
        BEAM.Aero.Geometry.Anhedral.Value = BEAM.data(idstart:end,5); 
        BEAM.Aero.Geometry.Chord.Value = BEAM.data(idstart:end,6); 
    end
    
    BEAM.WEIGHT.Value = BEAM.data(idstart:end,8);
    BEAM.YCG.Value = BEAM.data(idstart:end,9); 
    BEAM.ZCG.Value = BEAM.data(idstart:end,10); 
    BEAM.YNA.Value = BEAM.data(idstart:end,11); 
    BEAM.ZNA.Value = BEAM.data(idstart:end,12); 
    BEAM.YCT.Value = BEAM.data(idstart:end,13); 
    BEAM.ZCT.Value = BEAM.data(idstart:end,14);
    BEAM.ROTAPI.Value = BEAM.data(idstart:end,15); 
    BEAM.ROTAN.Value = BEAM.data(idstart:end,16);
    BEAM.JP.Value = BEAM.data(idstart:end,17);
    BEAM.JY.Value = BEAM.data(idstart:end,18);
    BEAM.JZ.Value = BEAM.data(idstart:end,19);
    BEAM.EA.Value = BEAM.data(idstart:end,20);
    BEAM.GJ.Value = BEAM.data(idstart:end,21);
    BEAM.EJY.Value = BEAM.data(idstart:end,22);
    BEAM.EJZ.Value = BEAM.data(idstart:end,23);   
        
end
