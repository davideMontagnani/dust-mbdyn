function BEAM = aero_reduction(BEAM,section)

    if BEAM.STA.red(1)>BEAM.STA.red(end)
        BEAM.STA.red_aero = fliplr(BEAM.STA.red);
    else
        BEAM.STA.red_aero = BEAM.STA.red;
    end
    for i = 1:numel(section)
        dist = abs(BEAM.STA.red_aero(i)-(BEAM.Aero.Geometry.Radial.Value));
        minDist = min(dist);
        idx_n = find(dist == minDist);    
        if numel(idx_n)>1
            idx(i) = idx_n(1);
        else 
            idx(i) = idx_n; 
        end
    end
    %idx = unique(idx); 
    BEAM.Aero.Geometry.Radial.red = BEAM.Aero.Geometry.Radial.Value(idx); 
    BEAM.Aero.Geometry.Sweep.red = BEAM.Aero.Geometry.Sweep.Value(idx); 
    BEAM.Aero.Geometry.Chord.red = BEAM.Aero.Geometry.Chord.Value(idx); 
    BEAM.Aero.Geometry.Twist.red = BEAM.Aero.Geometry.Twist.Value(idx); 
    BEAM.Aero.Geometry.Anhedral.red = BEAM.Aero.Geometry.Anhedral.Value(idx); 
    BEAM.Aero.Geometry.Sec.red = idx; 
    
    
end

