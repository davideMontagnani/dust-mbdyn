function [nodes, beam3, aero3, bodies, angularvelocity, deformablehinge, deformabledisplacement, rod, clamp, joints, label] = parse_log4Tipa(log_file, VERBOSE)
    % [nodes, bodies, muscles, joints, algj_map, torquej_map] = PARSE_LOG(log_file, torquej_labels, algj_labels, muscle_labels)
    %
    % parses MBDyn .log file and stores in output structures the
    % informations about nodes, joints and muscles
    %
    % inputs:
    %   log_file            full or relative path to the .log file
    %
    % outputs:
    % nodes
    % beam3
    % aero3
    % bodies
    % angular velocity
    % deformable hinge
    % deformable displacement
    % rod
    % clamp
    % joints
    % label 
    % TODO add more 

    log_fid = fopen(log_file, 'rt');

    % data initialization
    nodes = struct('lab', '0', 'X', zeros(1, 3), 'R', []);
    nn = 0; % number of nodes

    beam3 = struct('lab', '0', 'node1', 0, 'node2', 0, 'node3', 0, 'f1', zeros(1, 3), ...
        'f2', zeros(1, 3), 'f3', zeros(1, 3));
    nb3 = 0; % number of beam3

    aero3 = struct('lab', '0', 'node1', 0, 'node2', 0, 'node3', 0, 'trail_left', zeros(1, 3), ...
        'lead_left', zeros(1, 3), 'trail_center', zeros(1, 3), 'lead_center', zeros(1, 3), ...
        'trail_right', zeros(1, 3), 'lead_right', zeros(1, 3));
    na = 0; % number of aero3

    bodies = struct('lab', '0', 'node', 0, 'm', 0, 'fG', zeros(1, 3), ...
        'J', zeros(3, 3));
    nb = 0; % number of bodies

    angularvelocity = struct('lab', '0', 'node', 0, 'V', zeros(1, 3));
    nag = 0; % number of angular velocities

    deformablehinge = struct('lab', '0', 'node1', 0, 'node2', 0, 'f1', zeros(1, 3), ...
        'f2', zeros(1, 3), 'R1', zeros(3, 3), 'R2', zeros(3, 3));
    ndh = 0; % number of deformable hinge

    deformabledisplacement = struct('lab', '0', 'node1', 0, 'node2', 0, 'f1', zeros(1, 3), ...
        'f2', zeros(1, 3), 'R1', zeros(3, 3), 'R2', zeros(3, 3));
    ndd = 0; % number of deformable displacement

    joints = struct('lab', '0', 'type', 'none', 'node1', 0, 'node2', 0, ...
        'f1', zeros(1, 3), 'R1h', zeros(3, 3), 'R1hr', zeros(3, 3), ...
        'f2', zeros(1, 3), 'R2h', zeros(3, 3), 'R2hr', zeros(3, 3), ...
        'Xconst', zeros(1, 3), 'Vconst', zeros(1, 3), ...
        'Rconst', zeros(1, 3), 'Wconst', zeros(1, 3), ...
        'pidx', [], 'trqidx', [], 'gdv', []);
    nj = 0; % number of joints

    rod = struct('lab', '0', 'node1', 0, 'node2', 0, 'f1', zeros(1, 3), 'f2', zeros(1, 3));
    nr = 0; % number of rods

    clamp = struct('lab', '0', 'node', 0, 'f1', zeros(1, 3), 'R1', zeros(3, 3));
    nc = 0; % number of clamp constrain

    label = struct('lab', '0', 'num', 0);
    nl = 0; % number of symbolic variable

    skip_line = false;

    while skip_line ||~feof(log_fid)

        if ~skip_line
            log_line = fgets(log_fid);
        else
            skip_line = false;
        end

        card = strtok(log_line, ':');

        switch card

            case 'structural node'

                ns = textscan(log_line, '%s %s %d %f %f %f %s %f %f %f %f %f %f %f %f %f');
                nn = nn + 1;
                nodes(nn).lab = num2str(ns{3});
                nodes(nn).X = [ns{4} ns{5} ns{6}];

                if strcmpi(ns{7}, 'mat')
                    nodes(nn).R = [ns{8} ns{9} ns{10};
                                ns{11} ns{12} ns{13};
                                ns{14} ns{15} ns{16}];
                else
                    nodes(nn).R = [ns{8} ns{9} ns{10}];
                end

            case 'beam3'
                bn = textscan(log_line, '%s %d %d %f %f %f %d %f %f %f %d %f %f %f');
                nb3 = nb3 + 1;
                beam3(nb3).lab = num2str(bn{2});
                beam3(nb3).node1 = bn{3};
                beam3(nb3).node2 = bn{7};
                beam3(nb3).node3 = bn{11};
                beam3(nb3).f1 = [bn{4} bn{5} bn{6}];
                beam3(nb3).f2 = [bn{8} bn{9} bn{10}];
                beam3(nb3).f3 = [bn{12} bn{13} bn{14}];

            case 'body'

                bs = textscan(log_line, '%s %d %d %f %f %f %f %f %f %f %f %f %f %f %f %f');
                nb = nb + 1;
                bodies(nb).lab = num2str(bs{2});
                bodies(nb).node = bs{3};
                bodies(nb).m = bs{4};
                bodies(nb).fG = [bs{5} bs{6} bs{7}];
                bodies(nb).J = [bs{8} bs{9} bs{10};
                            bs{11} bs{12} bs{13};
                            bs{14} bs{15} bs{16}];

            case 'aero3'

                aer = textscan(log_line, '%s %d %d %f %f %f %f %f %f %d %f %f %f %f %f %f %d %f %f %f %f %f %f');
                na = na + 1;
                aero3(na).lab = num2str(aer{2});
                aero3(na).node1 = aer{3};
                aero3(na).node2 = aer{10};
                aero3(na).node3 = aer{17};
                aero3(na).trail_left = [aer{4} aer{5} aer{6}];
                aero3(na).lead_left = [aer{7} aer{8} aer{9}];
                aero3(na).trail_center = [aer{11} aer{12} aer{13}];
                aero3(na).lead_center = [aer{14} aer{15} aer{16}];
                aero3(na).trail_right = [aer{18} aer{19} aer{20}];
                aero3(na).lead_right = [aer{21} aer{22} aer{23}];

            case 'rod'

                ro = textscan(log_line, '%s %d %d %f %f %f %d %f %f %f');
                nr = nr + 1;
                rod(nr).lab = num2str(ro{2});
                rod(nr).node1 = ro{3};
                rod(nr).f1 = [ro{4} ro{5} ro{6}];
                rod(nr).node2 = ro{7};
                rod(nr).f1 = [ro{8} ro{9} ro{10}];

            case 'clamp'

                cl = textscan(log_line, '%s %d %d %f %f %f %f %f %f %f %f %f %f %f %f %d %f %f %f %f %f %f %f %f %f %f %f %f');
                nc = nc + 1;
                clamp(nc).lab = num2str(cl{2});
                clamp(nc).node = cl{3};
                clamp(nc).f1 = [cl{4} cl{5} cl{6}];
                clamp(nc).R1 = [cl{7} cl{8} cl{9};
                            cl{10} cl{11} cl{12};
                            cl{13} cl{14} cl{15}];

            case 'angularvelocity'

                angv = textscan(log_line, '%s %d %d %f %f %f');
                nag = nag + 1;
                angularvelocity(nag).lab = num2str(angv{2});
                angularvelocity(nag).node = angv{3};
                angularvelocity(nag).V = [angv{4} angv{5} angv{6}];

            case 'deformablehinge'

                dh = textscan(log_line, '%s %d %d %f %f %f %f %f %f %f %f %f %f %f %f %d %f %f %f %f %f %f %f %f %f %f %f %f');
                ndh = ndh + 1;
                deformablehinge(ndh).lab = num2str(dh{2});
                deformablehinge(ndh).node1 = dh{3};
                deformablehinge(ndh).f1 = [dh{4} dh{5} dh{6}];
                deformablehinge(ndh).R1 = [dh{7} dh{8} dh{9};
                                        dh{10} dh{11} dh{12};
                                        dh{13} dh{14} dh{15}];
                deformablehinge(ndh).node2 = dh{16};
                deformablehinge(ndh).f2 = [dh{17} dh{18} dh{19}];
                deformablehinge(ndh).R2 = [dh{20} dh{21} dh{22};
                                        dh{23} dh{24} dh{25};
                                        dh{26} dh{27} dh{28}];

            case 'deformabledisplacementjoint'

                dd = textscan(log_line, '%s %d %d %f %f %f %f %f %f %f %f %f %f %f %f %d %f %f %f %f %f %f %f %f %f %f %f %f');
                ndd = ndd + 1;
                deformabledisplacement(ndd).lab = num2str(dd{2});
                deformabledisplacement(ndd).node1 = dd{3};
                deformabledisplacement(ndd).f1 = [dd{4} dd{5} dd{6}];
                deformabledisplacement(ndd).R1 = [dd{7} dd{8} dd{9};
                                            dd{10} dd{11} dd{12};
                                            dd{13} dd{14} dd{15}];
                deformabledisplacement(ndd).node2 = dd{16};
                deformabledisplacement(ndd).f2 = [dd{17} dd{18} dd{19}];
                deformabledisplacement(ndd).R2 = [dd{20} dd{21} dd{22};
                                            dd{23} dd{24} dd{25};
                                            dd{26} dd{27} dd{28}];

            case 'totaljoint'

                tjs = textscan(log_line, '%s %d %d %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %d %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %d %d %d %d %d %d %d %d %d %d %d %d');
                nj = nj + 1;
                joints(nj).lab = num2str(tjs{2});
                joints(nj).node1 = tjs{3};
                joints(nj).f1 = [tjs{4} tjs{5} tjs{6}];
                joints(nj).R1h = [tjs{7} tjs{8} tjs{9};
                            tjs{10} tjs{11} tjs{12};
                            tjs{13} tjs{14} tjs{15}];
                joints(nj).R1hr = [tjs{16} tjs{17} tjs{18};
                                tjs{19} tjs{20} tjs{21};
                                tjs{22} tjs{23} tjs{24}];
                joints(nj).node2 = tjs{25};
                joints(nj).f2 = [tjs{26} tjs{27} tjs{28}];
                joints(nj).R2h = [tjs{29} tjs{30} tjs{31};
                            tjs{32} tjs{33} tjs{34};
                            tjs{35} tjs{36} tjs{37}];
                joints(nj).R2hr = [tjs{38} tjs{39} tjs{40};
                                tjs{41} tjs{42} tjs{43};
                                tjs{44} tjs{45} tjs{46}];
                joints(nj).Xconst = [tjs{47} tjs{48} tjs{49}];
                joints(nj).Vconst = [tjs{50} tjs{51} tjs{52}];
                joints(nj).Rconst = [tjs{53} tjs{54} tjs{55}];
                joints(nj).Wconst = [tjs{56} tjs{57} tjs{58}];

            case 'Symbol table'

                % read continuation
                while skip_line ||~feof(log_fid)

                    if ~skip_line
                        table_line = fgets(log_fid);

                        if strcmpi(table_line, 'Environment')
                            break
                        end

                    else
                        skip_line = false;
                    end

                    % integer value
                    if contains(table_line, 'integer')
                        int_line = strsplit(table_line, 'integer');
                        int_line = int_line{2};
                        lab_int = textscan(int_line, '%s %s %d');
                        nl = nl + 1;
                        label(nl).lab = char(lab_int{1});
                        label(nl).num = lab_int{3};
                    end

                    % real value
                    if contains(table_line, 'real')
                        real_line = strsplit(table_line, 'real');
                        real_line = real_line{2};
                        lab_real = textscan(real_line, '%s %s %f');
                        nl = nl + 1;
                        label(nl).lab = char(lab_real{1});
                        label(nl).num = lab_real{3};
                    end

                    % boolean value
                    if contains(table_line, 'bool')
                        bool_line = strsplit(table_line, 'bool');
                        bool_line = bool_line{2};
                        lab_bool = textscan(bool_line, '%s %s %f');
                        nl = nl + 1;
                        label(nl).lab = char(lab_bool{1});
                        label(nl).num = lab_bool{3};
                    end
                        
                end

        end

    end

    fclose(log_fid);
    
    
    if VERBOSE
        disp(['Log file ' log_file ' parsed:'])
        disp(['Found ' num2str(nn)  ' structural nodes']);
        disp(['Found ' num2str(nb3) ' beam3']);
        disp(['Found ' num2str(na)  ' aerobeam3']);
        disp(['Found ' num2str(nag) ' angular velocity joint']);
        disp(['Found ' num2str(nc)  ' clamp joint']);
        disp(['Found ' num2str(ndh) ' deformable hinge']);
        disp(['Found ' num2str(nr)  ' rod']);
        disp(['Found ' num2str(ndd) ' deformable displacement']);
        disp(['Found ' num2str(nb)  ' bodies'])
        disp(['Found ' num2str(nj)  ' total joints'])
        disp(['Found ' num2str(nl)  ' label'])
    end

end
