function BEAM = data_reduction(BEAM, section, varargin)
    % this function interpolate the values
    BEAM.STA.red = BEAM.STA.Value(section);
    BEAM.YCG.red = BEAM.YCG.Value(section);
    BEAM.ZCG.red = BEAM.ZCG.Value(section);
    BEAM.YNA.red = BEAM.YNA.Value(section);
    BEAM.ZNA.red = BEAM.ZNA.Value(section);
    BEAM.YCT.red = BEAM.YCT.Value(section);
    BEAM.ZCT.red = BEAM.ZCT.Value(section);
    BEAM.ROTAN.red = BEAM.ROTAN.Value(section);
    %BEAM.ROTAPI.red = BEAM.ROTAPI.Value(section);
    BEAM.SEC.red = BEAM.SEC.Value(section);
    
    L = zeros(length(BEAM.STA.Value) - 1, 1); % length of each segment

    for i = 1:numel(BEAM.STA.Value) - 1
        L(i) = sqrt( (BEAM.STA.Value(i) - BEAM.STA.Value(i + 1))^2 + (BEAM.YCG.Value(i) - BEAM.YCG.Value(i+1))^2 + ...
                    (BEAM.ZCG.Value(i) - BEAM.ZCG.Value(i+1))^2); % calculate the length of each element (without intepolation)
    end

    interval = zeros(length(section) - 1, 1);

    for i = 1:numel(section) -1
        interval(i) = section(i) + floor((section(i + 1) - section(i)) / 2);
    end

    % section to take
    interval = [interval; 0];

    for i = 1:numel(section)

        if i == 1
            elements(i).idx = BEAM.SEC.Value(1):interval(i); % sempre così o è meglio partire dalla prima sezione indicata e tenere il resto della trave rigida??
        elseif i == numel(section)
            elements(i).idx = interval(i - 1) + 1:BEAM.SEC.Value(end);
        else
            elements(i).idx = interval(i - 1) +1:interval(i);
        end

    end

    for i = 1:numel(elements)
        % get length
        for jj = elements(i).idx
            kk = jj - elements(i).idx(1) +1;

            if jj == BEAM.SEC.Value(1)
                elements(i).l(kk) = L(jj) / 2;
            elseif jj == BEAM.SEC.Value(end)
                elements(i).l(kk) = L(jj - 1) / 2;
            else
                elements(i).l(kk) = L(jj - 1) / 2 + L(jj) / 2;
            end

        end

    end

    % approximate the stiffness: the idea is to consider the properties as
    % springs in series:
    % Axial and torsional case: K_i = EA_i/l_i is the stiffness of 1 element, then the
    % equvalent stiffness will be K_eq = (sum_{i=1}^N_elm 1/K_i)^{-1}. To obtain
    % the EA_red = K_eq * l where l = sum_{i=1}^N_elm.
    % For the bending case the stiffness is K_i = 3 * EJ_i / l_i^3

    for i = 1:numel(elements)

        K_eq_axial = sum(elements(i).l ./ BEAM.EA.Value(elements(i).idx))^(-1);
        K_eq_bending_y = sum((elements(i).l) ./ BEAM.EJY.Value(elements(i).idx))^(-1);
        K_eq_bending_z = sum((elements(i).l) ./ BEAM.EJZ.Value(elements(i).idx))^(-1);
        K_eq_torsion = sum(elements(i).l ./ BEAM.GJ.Value(elements(i).idx))^(-1);

        BEAM.EA.red(i) = K_eq_axial * sum(elements(i).l);
        BEAM.EJY.red(i) = K_eq_bending_y * sum(elements(i).l);
        BEAM.EJZ.red(i) = K_eq_bending_z * sum(elements(i).l);
        BEAM.GJ.red(i) = K_eq_torsion * sum(elements(i).l); % check couplings

    end

    % mass elements
    
    for i = 1:numel(elements)

        for j = 1:numel(elements(i).idx) 
            m(j, 1, i) = BEAM.WEIGHT.Value(elements(i).idx(j)) * elements(i).l(j);
            JP(j, 1, i) = BEAM.JP.Value(elements(i).idx(j)) * elements(i).l(j);
            Iyy(j, 1, i) = BEAM.JY.Value(elements(i).idx(j)) * elements(i).l(j);
            Izz(j, 1, i) = BEAM.JZ.Value(elements(i).idx(j)) * elements(i).l(j);
            I_p(:, :, j, i) = diag([JP(j, 1, i), Iyy(j, 1, i), Izz(j, 1, i)]); % inertia tensor an elements in principal axis
            delta_rotapi(j, 1, i) = BEAM.ROTAPI.Value(elements(i).idx(j)) - BEAM.ROTAPI.Value(section(i));
            T(:, :, j, i) = [1 0 0;
                        0 cos(delta_rotapi(j, 1, i)) sin(delta_rotapi(j, 1, i));
                        0 -sin(delta_rotapi(j, 1, i)) cos(delta_rotapi(j, 1, i))];
            I_rot(:, :, j, i) = T(:, :, j, i)' * I_p(:, :, j, i) * T(:, :, j, i); % rotated inertia tensor in the ref frame of the selected section
            % Get CG coordinate wrt to the selected section
%             if elements(i).idx(j) == 1
%                 dXCG(j, 1, i) = elements(i).l(j) / 2; % check!!
%             else
                dXCG(j, 1, i) = BEAM.STA.Value(elements(i).idx(j)) - BEAM.STA.Value(section(i));
%             end

            dYCG(j, 1, i) = BEAM.YCG.Value(elements(i).idx(j)) - BEAM.YCG.Value(section(i));
            dZCG(j, 1, i) = BEAM.ZCG.Value(elements(i).idx(j)) - BEAM.ZCG.Value(section(i));

            I_0_rot(1, 1, j, i) = I_rot(1, 1, j, i) + m(j, 1, i) * (dYCG(j, 1, i)^2 + dZCG(j, 1, i)^2);
            I_0_rot(2, 2, j, i) = I_rot(2, 2, j, i) + m(j, 1, i) * (dXCG(j, 1, i)^2 + dZCG(j, 1, i)^2);
            I_0_rot(3, 3, j, i) = I_rot(3, 3, j, i) + m(j, 1, i) * (dXCG(j, 1, i)^2 + dYCG(j, 1, i)^2);
            I_0_rot(1, 2, j, i) = -(I_rot(1, 2, j, i) + m(j, 1, i) * dXCG(j, 1, i) * dYCG(j, 1, i));
            I_0_rot(1, 3, j, i) = -(I_rot(1, 3, j, i) + m(j, 1, i) * dXCG(j, 1, i) * dZCG(j, 1, i));
            I_0_rot(2, 3, j, i) = -(I_rot(2, 3, j, i) + m(j, 1, i) * dYCG(j, 1, i) * dZCG(j, 1, i));
            I_0_rot(2, 1, j, i) = I_0_rot(1, 2, j, i);
            I_0_rot(3, 1, j, i) = I_0_rot(1, 3, j, i);
            I_0_rot(3, 2, j, i) = I_0_rot(2, 3, j, i);
        end

        M(i) = sum(m(:, 1, i), 1);
        L(i) = sum(elements(i).l); % length of the element
        I_0_rot_elm(:, :, i) = sum(I_0_rot(:, :, :, i), 3); % sum the Inertia matrix along the third dimension
        % transform the inertia tensor in principal axis
        [rot(:, :, i), I_0_p(:, :, i)] = eig(I_0_rot_elm(:, :, i)); % eigs maintains the original shape of the matrix
        % Write the red mass elements
        I_0_p(:,:,i) = rot(:,:,i)*I_0_p(:,:,i)*rot(:,:,i)'; 
        BEAM.WEIGHT.red(i) = M(i)/L(i); 
        BEAM.JP.red(i) = I_0_rot_elm(1,1,i)/L(i); 
        BEAM.JY.red(i) = I_0_rot_elm(2,2,i)/L(i);  
        BEAM.JZ.red(i) = I_0_rot_elm(3,3,i)/L(i); 
        BEAM.ROTAPI.red(i) = acos(rot(2,2,i));
    end

    %% Plot Distribution
    
        figure('Renderer', 'painters', 'Position', [0 0 1000 500])
        subplot(2, 2, 1)
        plot(BEAM.WEIGHT.Value, 'b:', 'LineWidth', 2)
        hold on
        plot(section, BEAM.WEIGHT.red, 'r*--', 'LineWidth', 2)
        title(sprintf('%s Weight distribution', BEAM.Name))
        legend('LHD', 'Approx.', 'Location', 'northeastoutside')
        xlabel('Section')
        ylabel('[KG/M]')
        xlim([min(BEAM.SEC.Value), max(BEAM.SEC.Value)]);
        grid on

        subplot(2, 2, 2)
        plot(BEAM.JY.Value, 'b:', 'LineWidth', 2)
        hold on
        plot(section, BEAM.JY.red, 'r*--', 'LineWidth', 2)
        title(sprintf('%s JY distribution', BEAM.Name))
        legend('LHD', 'Approx.', 'Location', 'northeastoutside')
        xlabel('Section')
        ylabel('[KG*M]')
        xlim([min(BEAM.SEC.Value), max(BEAM.SEC.Value)]);
        grid on

        subplot(2, 2, 3)
        plot(BEAM.JZ.Value, 'b:', 'LineWidth', 2)
        hold on
        plot(section, BEAM.JZ.red, 'r*--', 'LineWidth', 2)
        title(sprintf('%s JZ distribution', BEAM.Name))
        legend('LHD', 'Approx.', 'Location', 'northeastoutside')
        xlabel('Section')
        ylabel('[KG*M]')
        xlim([min(BEAM.SEC.Value), max(BEAM.SEC.Value)]);
        grid on

        subplot(2, 2, 4)
        plot(BEAM.JP.Value, 'b:', 'LineWidth', 2)
        hold on
        plot(section, BEAM.JP.red, 'r*--', 'LineWidth', 2)
        title(sprintf('%s JP distribution', BEAM.Name))
        xlabel('Section')
        ylabel('[KG*M]')
        xlim([min(BEAM.SEC.Value), max(BEAM.SEC.Value)]);
        grid on
        legend('LHD', 'Approx.', 'Location', 'northeastoutside')
        %%

        figure('Renderer', 'painters', 'Position', [0 0 1000 500])
        subplot(2, 2, 1)
        plot(BEAM.EA.Value, 'b:', 'LineWidth', 2)
        hold on
        plot(section, BEAM.EA.red, 'r*--', 'LineWidth', 2)
        title(sprintf('%s EA distribution', BEAM.Name))
        legend('LHD', 'Approx.', 'Location', 'northeastoutside')
        xlabel('Section')
        ylabel('[N]')
        xlim([min(BEAM.SEC.Value), max(BEAM.SEC.Value)]);
        grid on

        subplot(2, 2, 2)
        plot(BEAM.EJY.Value, 'b:', 'LineWidth', 2)
        hold on
        plot(section, BEAM.EJY.red, 'r*--', 'LineWidth', 2)
        title(sprintf('%s EJY distribution', BEAM.Name))
        legend('LHD', 'Approx.', 'Location', 'northeastoutside')
        xlabel('Section')
        ylabel('[N*M^2]')
        xlim([min(BEAM.SEC.Value), max(BEAM.SEC.Value)]);
        grid on

        subplot(2, 2, 3)
        plot(BEAM.EJZ.Value, 'b:', 'LineWidth', 2)
        hold on
        plot(section, BEAM.EJZ.red, 'r*--', 'LineWidth', 2)
        title(sprintf('%s EJZ distribution', BEAM.Name))
        legend('LHD', 'Approx.', 'Location', 'northeastoutside')
        xlabel('Section')
        ylabel('[N*M^2]')
        xlim([min(BEAM.SEC.Value), max(BEAM.SEC.Value)]);
        grid on

        subplot(2, 2, 4)
        plot(BEAM.GJ.Value, 'b:', 'LineWidth', 2)
        hold on
        plot(section, BEAM.GJ.red, 'r*--', 'LineWidth', 2)
        title(sprintf('%s GJ distribution', BEAM.Name))
        legend('LHD', 'Approx.', 'Location', 'northeastoutside')
        xlabel('Section')
        ylabel('[N*M^2]')
        xlim([min(BEAM.SEC.Value), max(BEAM.SEC.Value)]);
        grid on
    

end
