function [BLADE, ROTOR, RPM, SPRING, POINT, YOKE] = read_input(path,file_tip)

    % file that contains all data
    %file_tip = fullfile('..','Table','attila_data_test_parser.tip'); % just for testing (better use uiget)...
    %file_tip = fullfile('..','XV-15','XV_15_rotor.tip');
    file_tip = fullfile(path, file_tip); 
    fid = fopen(file_tip,'rt');
    
    if fid==-1
        disp('error on opening the file');
    end
    INCLUDE = {};
    INCLUDE{1} = file_tip;
    Read_include = true;
    Nfile = 0;
    n_c81 = 0;
    % Still some troubles on parsing comments
    it = 0;
    n_include = 1;
    n_rotor = 0;
    delimiterPosition = find(file_tip == '/' | file_tip == '\');
    if isempty(delimiterPosition)
        mainDir = '';
    else
        mainDir = file_tip(1:delimiterPosition(end));
    end
    % start parsing
    while (Read_include)
        Nfile = Nfile + 1;

        fid = fopen(INCLUDE{Nfile},'rt');

        skip_line = false;

        while skip_line || ~feof(fid)

            if ~skip_line
                line_new = fgets(fid);
            else
                skip_line = false;
            end

            it = it+1;


            CARD = strtok(line_new);
            switch CARD
                case 'ROTOR'
                    % format rotor: ROTOR   NAMEDATA
                    %                       Value   Unit
                    RotorLine = textscan(line_new,'%s %s \n','CommentStyle','#');
                    NAMEDATA = char(RotorLine{2});
                    line_new_1 = fgets(fid);
                    if  ( (length(line_new_1) > 1) && (isequal(line_new_1(1),' ') || isequal(line_new_1(1),'+')))
                        RotorLine_2 = textscan(line_new_1,'%f %f %s \n','CommentStyle','#');
                        if isempty(RotorLine_2{2})
                            RotorLine_2 = textscan(line_new_1,'%f %s \n','CommentStyle','#');
                        end
                        ROTOR.(NAMEDATA).Value = cell2mat(RotorLine_2(1:end-1));
                        ROTOR.(NAMEDATA).Unit = char(RotorLine_2{end});
                    end

                case 'RPM'
                    RpmLine = textscan(line_new,'%s %s \n','CommentStyle','#');
                    NAMEDATA = char(RpmLine{2});
                    line_new_1 = fgets(fid);
                    if ( (length(line_new_1) > 1) && (isequal(line_new_1(1),' ') || isequal(line_new_1(1),'+')))
                        RpmLine_2 = textscan(line_new_1,'%f %f %s \n','CommentStyle','#');
                        if isempty(RpmLine_2{2})
                            RpmLine_2 = textscan(line_new_1,'%f %s \n','CommentStyle','#');
                        end
                        RPM.(NAMEDATA).Value = cell2mat(RpmLine_2(1:end-1));
                        RPM.(NAMEDATA).Unit = char(RpmLine_2{end});
                    end

                case 'PYLON'
                    PylonLine = textscan(line_new,'%s %s \n','CommentStyle','#');
                    NAMEDATA = char(PylonLine{2});
                    line_new_1 = fgets(fid);
                    if ( (length(line_new_1) > 1) && (isequal(line_new_1(1),' ') || isequal(line_new_1(1),'+')))
                        PylonLine_2 = textscan(line_new_1,'%f %s \n','CommentStyle','#');
                        PYLON.(NAMEDATA).Value = cell2mat(PylonLine_2(1:end-1));
                        PYLON.(NAMEDATA).Unit = char(PylonLine_2{end});
                        %TODO ADD MORE DATA....
                    end

                case 'MPC'
                    MpcLine = textscan(line_new,'%s %s \n','CommentStyle','#');
                    NAMEDATA = char(MpcLine{2});
                    line_new_1 = fgets(fid);
                    if ( (length(line_new_1) > 1) && (isequal(line_new_1(1),' ') || isequal(line_new_1(1),'+')))
                        MpcLine_2 = textscan(line_new_1,'%f %s \n','CommentStyle','#');
                        MPC.(NAMEDATA).Value = cell2mat(MpcLine_2(1:end-1));
                        MPC.(NAMEDATA).Unit = char(MpcLine_2{end});
                        %TODO ADD MORE DATA....
                    end

                case 'WING'
                    WingLine = textscan(line_new,'%s %s \n','CommentStyle','#');
                    NAMEDATA = char(WingLine{2});
                    if strcmpi(NAMEDATA,'STRUCT')
                        WingLineTab = textscan(line_new,'%s %s %s\n','CommentStyle','#');
                        FormatWing = char(WingLineTab{2});
                        ReferenceAxis = char(WingLineTab{3});
                        WING.ReferenceAxis = ReferenceAxis;
                        if strcmpi(FormatWing,'STRUCT')
                            line_new_1 = fgets(fid);
                            DataFormat = textscan(line_new_1,'%s\n','CommentStyle','#');
                            DataFormat = char(DataFormat{1});
                            if strcmpi(DataFormat,'TABLE')
                                it_table = 0;
                                it_comment = 0;
                                while 1
                                    LineTab = fgets(fid);
                                    LineTab_spl = strsplit(LineTab);
                                    LineTab1 = LineTab_spl{2};
                                    it_table = it_table + 1 - it_comment  ;
                                    if ischar(LineTab1)
                                        LineTab1 = char(LineTab1);
                                        if strcmpi(LineTab1,'ENDTABLE')
                                            break
                                        end

                                        if LineTab(1)=='#' % comment style
                                            LineTab = '';
                                            it_comment = it_table;
                                        end

                                        if it_table == 1 % Label row
                                            LineTab = strsplit(LineTab);
                                            LineTab(strcmp('',LineTab)) = []; % remouve empty space
                                            for i = 1:numel(LineTab)
                                                LABEL = char(LineTab{i});
                                                LABEL_BLADE{i} = LABEL;
                                            end
                                        elseif it_table == 2 %Label measurement
                                            LineTab = strsplit(LineTab);
                                            LineTab(strcmp('',LineTab)) = []; % remouve empty space
                                            for i = 1:numel(LineTab)
                                                Unit = LineTab{i};
                                                LABEL = LABEL_BLADE{i};
                                                WING.(LABEL).Unit = Unit;
                                            end
                                        else
                                            LineTab = strsplit(LineTab);
                                            LineTab(strcmp('',LineTab)) = []; % remouve empty space
                                            Value = [];
                                            for i = 1:numel(LineTab) % insert value on columns
                                                Value(it_table-2) = str2double(LineTab{i});
                                                LABEL = LABEL_BLADE{i};
                                                WING.(LABEL).Value(it_table-2) =  Value(it_table-2);
                                            end
                                        end
                                    end
                                end
                            end
                        end
                    else
                        line_new_1 = fgets(fid);
                        if ( (length(line_new_1) > 1) && (isequal(line_new_1(1),' ') || isequal(line_new_1(1),'+')))
                            WingLine_2 = textscan(line_new_1,'%f %s \n','CommentStyle','#');
                            WING.(NAMEDATA).Value = cell2mat(WingLine_2(1:end-1));
                            WING.(NAMEDATA).Unit = char(WingLine_2{end});
                        end

                    end
                    n_c81 = 0;
                case 'BLADE'
                    BladeLine_1 = textscan(line_new,'%s %s %s %s\n', 'CommentStyle','#');
                    FormatBlade = char(BladeLine_1{2});
                    BLADE.Name = 'BLADE';
                    if strcmpi(FormatBlade,'STRUCT')
                        ReferenceAxis = char(BladeLine_1{3});
                        BLADE.ReferenceAxis = ReferenceAxis;
                        line_new_1 = fgets(fid);
                        DataFormat = textscan(line_new_1,'%s\n','CommentStyle','#');
                        DataFormat = char(DataFormat{1});
                        if strcmpi(DataFormat,'TABLE')
                            it_table = 0;
                            it_comment = 0;
                            while 1
                                LineTab = fgets(fid);
                                LineTab_spl = strsplit(LineTab);
                                LineTab1 = LineTab_spl{2};
                                it_table = it_table + 1 - it_comment  ;
                                if ischar(LineTab1)
                                    LineTab1 = char(LineTab1);
                                    if strcmpi(LineTab1,'ENDTABLE')
                                        break
                                    end

                                    if LineTab(1)=='#' % comment style
                                        LineTab = '';
                                        it_comment = it_table;
                                    end

                                    if it_table == 1 % Label row
                                        LineTab = strsplit(LineTab);
                                        LineTab(strcmp('',LineTab)) = []; % remouve empty space
                                        for i = 1:numel(LineTab)
                                            LABEL = char(LineTab{i});
                                            LABEL_BLADE{i} = LABEL;
                                        end
                                    elseif it_table == 2 %Label measurement
                                        LineTab = strsplit(LineTab);
                                        LineTab(strcmp('',LineTab)) = []; % remouve empty space
                                        for i = 1:numel(LineTab)
                                            Unit = LineTab{i};
                                            LABEL = LABEL_BLADE{i};
                                            BLADE.(LABEL).Unit = Unit;
                                        end
                                    else
                                        LineTab = strsplit(LineTab);
                                        LineTab(strcmp('',LineTab)) = []; % remouve empty space
                                        Value = [];
                                        for i = 1:numel(LineTab) % insert value on columns
                                            Value(it_table-2) = str2double(LineTab{i});
                                            LABEL = LABEL_BLADE{i};
                                            BLADE.(LABEL).Value(it_table-2) =  Value(it_table-2);
                                        end
                                    end
                                end
                            end
                        end
                    elseif strcmpi(FormatBlade,'AERO')
                        Aerofield = char(BladeLine_1{3});
                        if strcmpi(Aerofield, 'GEOMETRY')
                            BLADE.Aero.Geometry.file = char(BladeLine_1{4});
                        elseif strcmpi(Aerofield, 'C81')
                            n_c81 = n_c81 +1;
                            BLADE.Aero.C81.file{n_c81} = char(BladeLine_1{4});
                            line_new_1 = fgets(fid);
                            if ( (length(line_new_1) > 1) && (isequal(line_new_1(1),' ') || isequal(line_new_1(1),'+')))
                                C81_line_2 = textscan(line_new_1,'%f %f %s\n','CommentStyle','#');
                                BLADE.Aero.C81.range(n_c81,:) = cell2mat(C81_line_2(1:2));
                                BLADE.Aero.C81.unit{n_c81} = char(C81_line_2{end});
                            end
                        elseif strcmpi(Aerofield, 'TIPLOSS')
                            line_new_1 = fgets(fid);
                            if ( (length(line_new_1) > 1) && (isequal(line_new_1(1),' ') || isequal(line_new_1(1),'+')))
                                tip_loss_line = textscan(line_new_1,'%f %s\n','CommentStyle','#');
                                BLADE.Aero.Tiploss.Value = cell2mat(tip_loss_line(1));
                                BLADE.Aero.Tiploss.Unit = char(tip_loss_line{2});
                            end
                        end
                    elseif strcmpi(FormatBlade, 'JOINT')
                        JointName = char(BladeLine_1{3});
                        line_new_1 = fgets(fid);
                        if ((length(line_new_1) > 1) && (isequal(line_new_1(1), ' ') || isequal(line_new_1(1), '+')))
                            JointLine = textscan(line_new_1,'%f %s %d\n','CommentStyle', '#');
                            BLADE.Joint.(JointName).Location = cell2mat(JointLine(1));
                            BLADE.Joint.(JointName).Unit = char(JointLine{2});
                            BLADE.Joint.(JointName).Dof = cell2mat(JointLine(3));
                        end
                    elseif strcmp(FormatBlade, 'BODY')
                        BodyName = char(BladeLine_1{3});
                        line_new_1 = fgets(fid);
                        if ((length(line_new_1) > 1) && (isequal(line_new_1(1), ' ') || isequal(line_new_1(1), '+')))
                            BodyLine = textscan(line_new_1, '%f %s \n', 'CommentStyle', '#');
                            BLADE.Body.(BodyName).M.Value = cell2mat(BodyLine(1));
                            BLADE.Body.(BodyName).M.Unit = char(BodyLine{2});
                            line_new_2 = fgets(fid);
                            if ((length(line_new_2) > 1) && (isequal(line_new_2(1), ' ') || isequal(line_new_2(1), '+')))
                                BodyLine = textscan(line_new_2, '%f %f %f %s \n', 'CommentStyle', '#');
                                BLADE.Body.(BodyName).Offset.Value = cell2mat(BodyLine(1:3));
                                BLADE.Body.(BodyName).Offset.Unit = char(BodyLine{4});
                                line_new_3 = fgets(fid);
                                if ((length(line_new_3) > 1) && (isequal(line_new_3(1), ' ') || isequal(line_new_3(1), '+')))
                                    BodyLine = textscan(line_new_3, '%f %f %f %f %f %f %s \n', 'CommentStyle', '#');
                                    BLADE.Body.(BodyName).I.Value = cell2mat(BodyLine(1:6));
                                    BLADE.Body.(BodyName).I.Unit = char(BodyLine{7});
                                end
                            end
                        end
                    end


                case 'POINT'
                    PointLine = textscan(line_new,'%s %s\n','CommentStyle','#');
                    NAMEDATA = char(PointLine{2});
                    line_new_1 = fgets(fid);
                    if ( (length(line_new_1) > 1) && (isequal(line_new_1(1),' ') || isequal(line_new_1(1),'+')))
                        PointLine_2 = textscan(line_new_1,'%f %f %f %s\n','CommentStyle','#');
                        POINT.(NAMEDATA).COORD = cell2mat(PointLine_2(1:end-1));
                        POINT.(NAMEDATA).Unit = char(PointLine_2{end});
                    end

                case 'INCLUDE'
                    n_include = n_include + 1;
                    file2include = line_new(9:end);
                    % split cell
                    file2include_spl = strsplit(file2include);
                    file2include_1 = file2include_spl{1};
                    NAMEPART = file2include_spl{2};

                    % remove ''
                    file2include_1(regexp(file2include_1,'['']')) = [];
                    INCLUDE{n_include} = strtrim(file2include_1);
                    PART{n_include} = NAMEPART;

                case 'YOKE'
                    YokeLine_1 = textscan(line_new,'%s %s %s\n', 'CommentStyle','#');
                    FormatYoke = char(YokeLine_1{2});
                    YOKE.Name = 'YOKE';
                    if strcmpi(FormatYoke,'STRUCT')
                        ReferenceAxis = char(YokeLine_1{3});
                        YOKE.ReferenceAxis = ReferenceAxis;
                        line_new_1 = fgets(fid);
                        DataFormat = textscan(line_new_1,'%s\n','CommentStyle','#');
                        DataFormat = char(DataFormat{1});
                        if strcmpi(DataFormat,'TABLE')
                            it_table = 0;
                            it_comment = 0;
                            while 1
                                LineTab = fgets(fid);
                                LineTab_spl = strsplit(LineTab);
                                LineTab1 = LineTab_spl{2};
                                it_table = it_table + 1 - it_comment  ;
                                if ischar(LineTab1)
                                    LineTab1 = char(LineTab1);
                                    if strcmpi(LineTab1,'ENDTABLE')
                                        break
                                    end

                                    if LineTab(1)=='#' % comment style
                                        LineTab = '';
                                        it_comment = it_table;
                                    end

                                    if it_table == 1 % Label row
                                        LineTab = strsplit(LineTab);
                                        LineTab(strcmp('',LineTab)) = []; % remouve empty space
                                        for i = 1:numel(LineTab)
                                            LABEL = char(LineTab{i});
                                            LABEL_YOKE{i} = LABEL;
                                        end
                                    elseif it_table == 2 %Label measurement
                                        LineTab = strsplit(LineTab);
                                        LineTab(strcmp('',LineTab)) = []; % remouve empty space
                                        for i = 1:numel(LineTab)
                                            Unit = LineTab{i};
                                            LABEL = LABEL_YOKE{i};
                                            YOKE.(LABEL).Unit = Unit;
                                        end
                                    else
                                        LineTab = strsplit(LineTab);
                                        LineTab(strcmp('',LineTab)) = []; % remouve empty space
                                        Value = [];
                                        for i = 1:numel(LineTab) % insert value on columns
                                            Value(it_table-2) = str2double(LineTab{i});
                                            LABEL = LABEL_YOKE{i};
                                            YOKE.(LABEL).Value(it_table-2) =  Value(it_table-2);
                                        end
                                    end
                                end
                            end
                        end
                    elseif strcmpi(FormatYoke, 'JOINT')
                        JointName = char(YokeLine_1{3});
                        line_new_1 = fgets(fid);
                        if ((length(line_new_1) > 1) && (isequal(line_new_1(1), ' ') || isequal(line_new_1(1), '+')))
                            JointLine = textscan(line_new_1, '%f %s %d\n', 'CommentStyle', '#');
                            YOKE.Joint.(JointName).Location = cell2mat(JointLine(1));
                            YOKE.Joint.(JointName).Unit = char(JointLine{2});
                            YOKE.Joint.(JointName).Dof = cell2mat(JointLine(3));
                        end
                    elseif strcmp(FormatYoke, 'BODY')
                        BodyName = char(YokeLine_1{3});
                        line_new_1 = fgets(fid);
                        if ((length(line_new_1) > 1) && (isequal(line_new_1(1), ' ') || isequal(line_new_1(1), '+')))
                            BodyLine = textscan(line_new_1, '%f %s \n', 'CommentStyle', '#');
                            YOKE.Body.(BodyName).M.Value = cell2mat(BodyLine(1));
                            YOKE.Body.(BodyName).M.Unit = char(BodyLine{2});
                            line_new_2 = fgets(fid);
                            if ((length(line_new_2) > 1) && (isequal(line_new_2(1), ' ') || isequal(line_new_2(1), '+')))
                                BodyLine = textscan(line_new_2, '%f %f %f %s \n', 'CommentStyle', '#');
                                YOKE.Body.(BodyName).Offset.Value = cell2mat(BodyLine(1:3));
                                YOKE.Body.(BodyName).Offset.Unit = char(BodyLine{4});
                                line_new_3 = fgets(fid);
                                if ((length(line_new_3) > 1) && (isequal(line_new_3(1), ' ') || isequal(line_new_3(1), '+')))
                                    BodyLine = textscan(line_new_3, '%f %f %f %f %f %f %s \n', 'CommentStyle', '#');
                                    YOKE.Body.(BodyName).I.Value = cell2mat(BodyLine(1:6));
                                    YOKE.Body.(BodyName).I.Unit = char(BodyLine{7});
                                end
                            end
                        end
                    end

                case 'SPRING'
                    SpringLine = textscan(line_new,'%s %s\n','CommentStyle','#');
                    NAMEDATA = char(SpringLine{2});
                    line_new_1 = fgets(fid);
                    if ( (length(line_new_1) > 1) && (isequal(line_new_1(1),' ') || isequal(line_new_1(1),'+')))
                        SpringLine_2 = textscan(line_new_1,'%f %s\n','CommentStyle','#');
                        SPRING.(NAMEDATA).Value = cell2mat(SpringLine_2(1:end-1));
                        SPRING.(NAMEDATA).Unit = char(SpringLine_2{end});
                    end

                case 'MOVBS'
                    MovbsLine = textscan(line_new,'%s %s %s\n','CommentStyle','#');
                    LABLE = char(MovbsLine{2});
                    NAMEDATA = char(MovbsLine{3});
                    line_new_1 = fgets(fid);
                    if ( (length(line_new_1) > 1) && (isequal(line_new_1(1),' ') || isequal(line_new_1(1),'+')))
                        MovbsLine_2 = textscan(line_new_1,'%f %s\n','CommentStyle','#');
                        MOVBS.(NAMEDATA).(LABLE).Value = cell2mat(MovbsLine_2(1:end-1));
                        MOVBS.(NAMEDATA).(LABLE).Unit = char(MovbsLine_2{end});
                    end
            end
        end
        fclose(fid);
        if (length(INCLUDE) == Nfile)
            Read_include = false;
        else
            % If the file cannot be found check for a local path w.r.t. the directory
            % containing the main file
            if ~exist(INCLUDE{Nfile+1}, 'file')
                includedFile = fullfile(mainDir,INCLUDE{Nfile+1});
                if ~exist(includedFile, 'file')
                    error('Unable to find file %s', INCLUDE{Nfile+1});
                end
                INCLUDE{Nfile+1} = includedFile;
            end

            %
        end
    end


end











