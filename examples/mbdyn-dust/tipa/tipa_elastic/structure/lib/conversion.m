function BEAM = conversion(BEAM)
    % this function converts the data into N, m, rad, Kg
    
    if strcmpi(BEAM.STA.Unit,'MM') 
        % conversion of the length in meter
        BEAM.STA.Value = (BEAM.STA.Value) ./1e3; 
        BEAM.YCG.Value = (BEAM.YCG.Value) ./1e3;
        BEAM.ZCG.Value = (BEAM.ZCG.Value) ./1e3;
        BEAM.YNA.Value = (BEAM.YNA.Value) ./1e3;
        BEAM.ZNA.Value = (BEAM.ZNA.Value) ./1e3;
        BEAM.YCT.Value = (BEAM.YCT.Value) ./1e3;
        BEAM.ZCT.Value = (BEAM.ZCT.Value) ./1e3;
        
    elseif strcmpi(BEAM.STA.Unit, 'FT')
        BEAM.STA.Value = (BEAM.STA.Value) .*0.3048; 
        BEAM.YCG.Value = (BEAM.YCG.Value) .*0.3048;
        BEAM.ZCG.Value = (BEAM.ZCG.Value) .*0.3048;
        BEAM.YNA.Value = (BEAM.YNA.Value) .*0.3048;
        BEAM.ZNA.Value = (BEAM.ZNA.Value) .*0.3048;
        BEAM.YCT.Value = (BEAM.YCT.Value) .*0.3048;
        BEAM.ZCT.Value = (BEAM.ZCT.Value) .*0.3048;
    end
        
    if strcmpi(BEAM.ROTAN.Unit,'DEG') 
        % conversion of the angle in radians
        BEAM.ROTAN.Value = deg2rad(BEAM.ROTAN.Value);
        BEAM.ROTAPI.Value = deg2rad(BEAM.ROTAPI.Value);
    end
    if strcmpi(BEAM.WEIGHT.Unit,'KG/MM')
        % conversion of the weight per unit lenght into Kg/m
        BEAM.WEIGHT.Value = (BEAM.WEIGHT.Value) .*1e3;
    elseif strcmpi(BEAM.WEIGHT.Unit,'SLUG/FT')
        BEAM.WEIGHT.Value = (BEAM.WEIGHT.Value) .*47.880259;
    end
    
    if strcmpi(BEAM.GJ.Unit,'N*MM^2')
        % conversion of the stiffenss in N*M^2
        BEAM.EJY.Value = (BEAM.EJY.Value) ./1e6;
        BEAM.EJZ.Value = (BEAM.EJZ.Value) ./1e6;
        BEAM.GJ.Value = (BEAM.GJ.Value) ./1e6;
    elseif strcmpi(BEAM.GJ.Unit,'lb*ft**2')
        BEAM.EJY.Value = (BEAM.EJY.Value) .*0.04214011*9.81;
        BEAM.EJZ.Value = (BEAM.EJZ.Value) .*0.04214011*9.81;
        BEAM.GJ.Value = (BEAM.GJ.Value) .*0.04214011*9.81;      
    end
    
    if strcmpi(BEAM.EA.Unit,'lb')
        BEAM.EA.Value = BEAM.EA.Value .*0.45359237*9.81;
    end
    
    if strcmpi(BEAM.JP.Unit,'KG*MM')
        % conversion of the inertia in KG*M
        BEAM.JY.Value = (BEAM.JY.Value) ./1e3;
        BEAM.JZ.Value = (BEAM.JZ.Value) ./1e3;
        BEAM.JP.Value = (BEAM.JP.Value) ./1e3;
    elseif strcmpi(BEAM.JP.Unit,'SLUG*FT')
        BEAM.JY.Value = (BEAM.JY.Value) .*4.44822162;
        BEAM.JZ.Value = (BEAM.JZ.Value) .*4.44822162;
        BEAM.JP.Value = (BEAM.JP.Value) .*4.44822162;        
    end
    
end