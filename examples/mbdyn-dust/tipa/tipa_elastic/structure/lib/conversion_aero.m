function BEAM = conversion_aero(BEAM)

    Radial = BEAM.Aero.Geometry.Radial;
    Sweep = BEAM.Aero.Geometry.Sweep;
    Chord = BEAM.Aero.Geometry.Chord;
    Anhedral = BEAM.Aero.Geometry.Anhedral;
    Twist = BEAM.Aero.Geometry.Twist;
    R = BEAM.Length;

    if strcmpi(Radial.Unit, 'ADIM')
        BEAM.Aero.Geometry.Radial.Value = Radial.Value.*R;
    end

    if strcmpi(Sweep.Unit, 'ADIM')
        BEAM.Aero.Geometry.Sweep.Value = Sweep.Value .* R;
    end

    if strcmpi(Chord.Unit, 'ADIM')
        BEAM.Aero.Geometry.Chord.Value = Chord.Value .* R;
    end

    if strcmpi(Anhedral.Unit, 'ADIM')
        BEAM.Aero.Geometry.Anhedral.Value = Anhedral.Value .* R;
    end

    if strcmpi(Twist.Unit, 'DEG')
        BEAM.Aero.Geometry.Twist.Value = deg2rad(Twist.Value);
    end
end