function NASTRAN = read_nas(file_data)
%file_data = fullfile('..','Table','ATTILA_WT-model_DS-off_v01.bdf'); % just for testing (better use uiget)...
%fid = fopen(file_data,'rt');

%if fid == -1
%    disp('error on opening the file');
%end
% Still some troubles on parsing comments
%it = 0;

% variable initialization (add more if necessary)
n_grid = 0;
n_cbar = 0;
n_pbar = 0;
n_mat1 = 0;
n_conm2 = 0;
n_rbe3 = 0;
n_rbe2 = 0;
n_plotel = 0;
n_spc = 0;
n_celas2 = 0;
n_include = 1;
n_cord2r = 0;
NASTRAN.INCLUDE = {};
NASTRAN.INCLUDE{1} = file_data;
Read_include = true;
Nfile = 0;

delimiterPosition = find(file_data=='/'| file_data=='\');
if isempty(delimiterPosition)
    mainDir = '';
else
    mainDir = file_data(1:delimiterPosition(end));
end

% start parsing with include
while (Read_include)
    
    
    Nfile = Nfile + 1;
    
    fid = fopen(NASTRAN.INCLUDE{Nfile},'rt');
    %fid = fopen(NASTRAN.INCLUDE{Nfile},'rt');
    
    skip_line = false;
    
    while skip_line || ~feof(fid)
        
        if ~skip_line
            line_new = fgets(fid);
        else
            skip_line = false;
        end
        
        
        %                 if line_new(1)=='$' % comment style
        %                     comment_line = line_new;
        %                 end
        CARD = strtok(line_new);
        switch CARD
            
            case 'GRID'
                n_grid = n_grid + 1;
                NASTRAN.GRID.ID(n_grid) = int32(num_field_parser(line_new, 2));
                NASTRAN.GRID.CP(n_grid) = int32(num_field_parser(line_new, 3));
                NASTRAN.GRID.X1(n_grid) = num_field_parser(line_new, 4);
                NASTRAN.GRID.X2(n_grid) = num_field_parser(line_new, 5);
                NASTRAN.GRID.X3(n_grid) = num_field_parser(line_new, 6);
                NASTRAN.GRID.CD(n_grid) = int32(num_field_parser(line_new, 7));
                NASTRAN.GRID.PS(n_grid) = int32(num_field_parser(line_new, 8));
                
                
            case 'GRID*'
                n_grid = n_grid + 1;
                NASTRAN.GRID.ID(n_grid) = int32(num_field_parser_16(line_new, 2));
                NASTRAN.GRID.CP(n_grid) = int32(num_field_parser_16(line_new, 3));
                NASTRAN.GRID.X1(n_grid) = (num_field_parser_16(line_new, 4));
                NASTRAN.GRID.X2(n_grid) = (num_field_parser_16(line_new, 5));
                line_new_1 = fgets(fid);
                if ( (length(line_new_1) > 1) && (isequal(line_new_1(1),' ') || isequal(line_new_1(1),'*'))) % continuation detected
                    NASTRAN.GRID.X3(n_grid) = (num_field_parser_16(line_new_1, 2));
                    NASTRAN.GRID.CD(n_grid) = int32(num_field_parser_16(line_new_1, 3));
                    NASTRAN.GRID.PS(n_grid) = int32(num_field_parser_16(line_new_1, 4));
                end
                
            case 'CBAR'
                n_cbar = n_cbar +1;
                NASTRAN.CBAR.ID(n_cbar) = int32(num_field_parser(line_new, 2));
                NASTRAN.CBAR.PID(n_cbar) = int32(num_field_parser(line_new,3));
                NASTRAN.CBAR.GA(n_cbar) = int32(num_field_parser(line_new,4));
                NASTRAN.CBAR.GB(n_cbar) = int32(num_field_parser(line_new,5));
                NASTRAN.CBAR.X1(n_cbar) = num_field_parser(line_new,6);
                NASTRAN.CBAR.X2(n_cbar) = num_field_parser(line_new,7);
                NASTRAN.CBAR.X3(n_cbar) = num_field_parser(line_new,8);
                % TODO add other field if they are needed
                
            case 'PBAR'
                n_pbar = n_pbar + 1;
                NASTRAN.PBAR.ID(n_pbar) = int32(num_field_parser(line_new,2));
                NASTRAN.PBAR.MID(n_pbar) = int32(num_field_parser(line_new,3));
                NASTRAN.PBAR.A(n_pbar) = (num_field_parser(line_new,4));
                NASTRAN.PBAR.I1(n_pbar) = (num_field_parser(line_new,5));
                NASTRAN.PBAR.I2(n_pbar) = (num_field_parser(line_new,6));
                NASTRAN.PBAR.J(n_pbar) = (num_field_parser(line_new,7));
                NASTRAN.PBAR.NSM(n_pbar) = (num_field_parser(line_new,8));
                % TODO add other field if they are needed
                
            case 'MAT1'
                n_mat1 = n_mat1 + 1;
                NASTRAN.MAT1.ID(n_mat1) = int32(num_field_parser(line_new,2));
                NASTRAN.MAT1.E(n_mat1) = num_field_parser(line_new,3);
                NASTRAN.MAT1.G(n_mat1) = num_field_parser(line_new,4);
                NASTRAN.MAT1.nu(n_mat1) = num_field_parser(line_new,5);
                NASTRAN.MAT1.Rho(n_mat1) = num_field_parser(line_new,6);
                if (NASTRAN.MAT1.nu(n_mat1) == 0)
                    NASTRAN.MAT1.nu(n_mat1) =  NASTRAN.MAT1.E(n_mat1) / (2 * NASTRAN.MAT1.G(n_mat1)) -1;
                end
                % TODO add other field if they are needed
                
            case 'CONM2'
                n_conm2 = n_conm2 + 1;
                NASTRAN.CONM2.ID(n_conm2) = int32(num_field_parser(line_new,2));
                NASTRAN.CONM2.G(n_conm2) = int32(num_field_parser(line_new,3));
                NASTRAN.CONM2.CID(n_conm2) = int32(num_field_parser(line_new,4));
                NASTRAN.CONM2.M(n_conm2) = num_field_parser(line_new,5);
                NASTRAN.CONM2.X1(n_conm2) = num_field_parser(line_new,6);
                NASTRAN.CONM2.X2(n_conm2) = num_field_parser(line_new,7);
                NASTRAN.CONM2.X3(n_conm2) = num_field_parser(line_new,8);
                line_new_1 = fgets(fid);
                if ( (length(line_new_1) > 1) && (isequal(line_new_1(1),' ') || isequal(line_new_1(1),'+')))
                    NASTRAN.CONM2.I11(n_conm2) = num_field_parser(line_new_1,2);
                    NASTRAN.CONM2.I21(n_conm2) = num_field_parser(line_new_1,3);
                    NASTRAN.CONM2.I22(n_conm2) = num_field_parser(line_new_1,4);
                    NASTRAN.CONM2.I31(n_conm2) = num_field_parser(line_new_1,5);
                    NASTRAN.CONM2.I32(n_conm2) = num_field_parser(line_new_1,6);
                    NASTRAN.CONM2.I33(n_conm2) = num_field_parser(line_new_1,7);
                else
                    NASTRAN.CONM2.I11(n_conm2) = 0;
                    NASTRAN.CONM2.I21(n_conm2) = 0;
                    NASTRAN.CONM2.I22(n_conm2) = 0;
                    NASTRAN.CONM2.I31(n_conm2) = 0;
                    NASTRAN.CONM2.I32(n_conm2) = 0;
                    NASTRAN.CONM2.I33(n_conm2) = 0;
                end
                
            case 'RBE3'
                n_rbe3 = n_rbe3 + 1;
                NASTRAN.RBE3.EID(n_rbe3) = int32(num_field_parser(line_new,2));
                NASTRAN.RBE3.REFGRID(n_rbe3) = int32(num_field_parser(line_new,4));
                NASTRAN.RBE3.REFC(n_rbe3) = int32(num_field_parser(line_new,5));
                NASTRAN.RBE3.WTI1(n_rbe3) = num_field_parser(line_new,6);
                NASTRAN.RBE3.C1(n_rbe3) = int32(num_field_parser(line_new,7));
                NASTRAN.RBE3.G1(n_rbe3) = int32(num_field_parser(line_new,8));
                NASTRAN.RBE3.G2(n_rbe3) = int32(num_field_parser(line_new,9));
                % TODO add more rows
                
            case 'PLOTEL'
                n_plotel = n_plotel + 1;
                NASTRAN.PLOTEL.EID(n_plotel) = int32(num_field_parser(line_new,2));
                NASTRAN.PLOTEL.G1(n_plotel) = int32(num_field_parser(line_new,3));
                NASTRAN.PLOTEL.G2(n_plotel) = int32(num_field_parser(line_new,4));
                
            case 'RBE2'
                n_rbe2 = n_rbe2 + 1;
                NASTRAN.RBE2.EID(n_rbe2) = int32(num_field_parser(line_new,2));
                NASTRAN.RBE2.GN(n_rbe2) = int32(num_field_parser(line_new,3));
                NASTRAN.RBE2.CM(n_rbe2) = int32(num_field_parser(line_new,4));
                NASTRAN.RBE2.GM1(n_rbe2) = int32(num_field_parser(line_new,5));
                NASTRAN.RBE2.GM2(n_rbe2) = int32(num_field_parser(line_new,6));
                
            case 'SPC'
                n_spc = n_spc + 1;
                NASTRAN.SPC.SID(n_spc) = int32(num_field_parser(line_new,2));
                NASTRAN.SPC.G1(n_spc) = int32(num_field_parser(line_new,3));
                NASTRAN.SPC.C1(n_spc) = int32(num_field_parser(line_new,4));
                NASTRAN.SPC.D1(n_spc) = int32(num_field_parser(line_new,5));
                NASTRAN.SPC.G2(n_spc) = int32(num_field_parser(line_new,6));
                NASTRAN.SPC.C2(n_spc) = int32(num_field_parser(line_new,7));
                NASTRAN.SPC.D2(n_spc) = int32(num_field_parser(line_new,8));
                
            case 'CELAS2'
                n_celas2 = n_celas2 + 1;
                NASTRAN.CELAS2.EID(n_celas2) = int32(num_field_parser(line_new,2));
                NASTRAN.CELAS2.K(n_celas2) = num_field_parser(line_new,3);
                NASTRAN.CELAS2.G1(n_celas2) = int32(num_field_parser(line_new,4));
                NASTRAN.CELAS2.C1(n_celas2) = int32(num_field_parser(line_new,5));
                NASTRAN.CELAS2.G2(n_celas2) = int32(num_field_parser(line_new,6));
                NASTRAN.CELAS2.C2(n_celas2) = int32(num_field_parser(line_new,7));
                
            case 'INCLUDE'
                n_include = n_include + 1;
                file2include = line_new(9:end);
                % split cell
                file2include_spl = strsplit(file2include);
                file2include_1 = file2include_spl{1};
                % remove ''
                file2include_1(regexp(file2include_1,'['']')) = [];
                NASTRAN.INCLUDE{n_include} = strtrim(file2include_1);
                
            case 'CORD2R*'
                
                n_cord2r = n_cord2r + 1;
                NASTRAN.CORD2R.CID(n_cord2r) = int32(num_field_parser_16(line_new,2));
                NASTRAN.CORD2R.RID(n_cord2r) = int32(num_field_parser_16(line_new,3));
                NASTRAN.CORD2R.A1(n_cord2r) = num_field_parser_16(line_new,4);
                NASTRAN.CORD2R.A2(n_cord2r) = num_field_parser_16(line_new,5);
                line_new_1 = fgetl(fid);
                
                if ( (length(line_new_1) > 1) && (isequal(line_new_1(1),' ') || isequal(line_new_1(1),'*'))) % continuation detected
                    
                    NASTRAN.CORD2R.A3(n_cord2r) = num_field_parser_16(line_new_1,2);
                    NASTRAN.CORD2R.B1(n_cord2r) = num_field_parser_16(line_new_1,3);
                    NASTRAN.CORD2R.B2(n_cord2r) = num_field_parser_16(line_new_1,4);
                    NASTRAN.CORD2R.B3(n_cord2r) = num_field_parser_16(line_new_1,5);
                end
                
                line_new_2 = fgetl(fid);
                
                if ( (length(line_new_2) > 1) && (isequal(line_new_2(1),' ') || isequal(line_new_2(1),'*'))) % continuation detected
                    
                    NASTRAN.CORD2R.C1(n_cord2r) = num_field_parser_16(line_new_2,2);
                    NASTRAN.CORD2R.C2(n_cord2r) = num_field_parser_16(line_new_2,3);
                    NASTRAN.CORD2R.C3(n_cord2r) = num_field_parser_16(line_new_2,4);
                end
        end
    end
    
    fclose(fid);
    if (length(NASTRAN.INCLUDE) == Nfile)
        Read_include = false;
    else
        % If the file cannot be found check for a local path w.r.t. the directory
        % containing the main file
        if ~exist(NASTRAN.INCLUDE{Nfile+1}, 'file')
            includedFile = fullfile(mainDir,NASTRAN.INCLUDE{Nfile+1});
            if ~exist(includedFile, 'file')
                error('Unable to find file %s', NASTRAN.INCLUDE{Nfile+1});
            end
            NASTRAN.INCLUDE{Nfile+1} = includedFile;
        end
        
        %
    end % INCLUDE
    % clean data
    %n_include = n_include -1;
    %if (n_include>0)
    %    NASTRAN.INCLUDE = NASTRAN.INCLUDE(2:end);
    %end
    
end
end

% Parse NASTRAN field and return it as a double
function num = num_field_parser(line, index)

FIELD = 8;

if length(line) < FIELD * (index-1)
    
    num = 0;
    
else
    
    minc = min(length(line), index * FIELD);
    field = strtok(line((index-1) * FIELD+1:minc));
    
    if ~isempty(field)
        if numel(field) == 8
            if field(7) == '-'
                if ~strcmpi('E',field(6))
                    field(7:9) = strrep(field(7:8),'-','e-');
                end
            elseif field(7) == '+'
                if ~strcmpi('E',field(6))
                    field(7:9) = strrep(field(7:8),'+','e+');
                end
            end
            
        end
        num = str2double(field);
        
    else
        
        num = 0;
        
    end
    
end

end

function num = num_field_parser_16(line, index) % parsing 16 field

FIELD = 16;
FIELD_1 = 8;

if length(line) < (FIELD * (index-1)-FIELD_1)
    
    num = 0;
    
else
    
    minc = min(length(line), (index-1) * FIELD + FIELD_1);
    field = strtok(line((index-1) * FIELD+1-FIELD_1:minc));
    
    if ~isempty(field)
        if numel(field) == 16
            if field(15) == '-'
                if ~strcmpi('E',field(14))
                    field(15:17) = strrep(field(15:16),'-','e-');
                end
            elseif field(15) == '+'
                if  ~strcmpi('E',field(14))
                    field(15:17) = strrep(field(15:16),'+','e+');
                end
            end
        end
        num = str2double(field);
        
    else
        
        num = 0;
        
    end
    
end

end