function BEAM = read_aero_geom(filename)
fileID = fopen(filename,'rt');
it_table = 0;
it_comment = 0;
while 1
    LineTab = fgets(fileID);
    LineTab_spl = strsplit(LineTab);
    LineTab1 = LineTab_spl{1};
    it_table = it_table + 1 - it_comment  ;
    if ischar(LineTab1)
        LineTab1 = char(LineTab1);
        if strcmpi(LineTab1,'ENDTABLE')
            break
        end
        
        if LineTab(1)=='#' % comment style
            LineTab = '';
            it_comment = it_table;
        end
        
        if it_table == 1 % Label row
            LineTab = strsplit(LineTab);
            LineTab(strcmp('',LineTab)) = []; % remouve empty space
            for i = 1:numel(LineTab)
                LABEL = char(LineTab{i});
                LABEL_BEAM{i} = LABEL;
            end
        elseif it_table == 2 %Label measurement
            LineTab = strsplit(LineTab);
            LineTab(strcmp('',LineTab)) = []; % remouve empty space
            for i = 1:numel(LineTab)
                UNIT = LineTab{i};
                LABEL = LABEL_BEAM{i};
                BEAM.(LABEL).Unit = UNIT;
            end
        else
            LineTab = strsplit(LineTab);
            LineTab(strcmp('',LineTab)) = []; % remouve empty space
            VALUE = [];
            for i = 1:numel(LineTab) % insert value on columns
                VALUE(it_table-2) = str2double(LineTab{i});
                LABEL = LABEL_BEAM{i};
                BEAM.(LABEL).Value(it_table-2) =  VALUE(it_table-2);
            end
        end
    end
    
end