joint: OFFSET + CURR_BLADE_1_PITCH_LINK, rod, # blade pitch link
	CURR_ROTOR + SW_ROT_NODE,
		position, reference, global, 0.2417250000000000, 0.0647700000000000, -0.4000000000000000,
	CURR_ROTOR + CURR_BLADE_1 + 1,
		position, reference, global, 0.2417250000000000, 0.0647700000000000, 0.0931330000000000,
		from nodes,
		linear viscoelastic, 161363.3254531000275165, proportional, 0.001;
