# ATTILA ROTOR MODEL 
begin: data;
	problem: initial value;
end: data;

begin: initial value;
	time step: 5.000000e-04;
	initial time: 0.000000;
	final time:  5.000000 ;
	max iterations: 50;
	tolerance: 1.000000e-03;
	derivatives tolerance: 1.000000e+38;
	derivatives max iterations: 30;
	derivatives coefficient: 1.000000e+00;
	linear solver: naive, colamd, pivot factor, 1.e0;
	method: ms, 0.4; 
	#output: residual;
	output: counter; 
	/*
	eigenanalysis: 2.00000,
		output matrices,
		output eigenvectors,
		output geometry,
		upper frequency limit, 200,
		use lapack; */ 
end: initial value;
# CONTROL DATA SECTION
begin: control data;
	
	structural nodes:
		3 * 1 +   # Blade nodes
		3 * 1 +		# Yoke nodes
		5			# Rotor static nodes	
		;
	rigid bodies:
		3 * 1 +   	# Blade bodies
		3 * 1   	# Yoke bodies
		;
	aerodynamic elements:
		3 * 1	   	# Blade aerodynamic beams
		; 	
	joints:
		3 * 2 +     # Connection Blade-Yoke
		3 * 1 +		# Pitch link
		3 * 1 +		# Gimbal joint
		7 			# Rotor joint
		;
		inertia: 1;
	air properties;
	gravity;
	induced velocity elements:
		+1
	;
	output results: netcdf,no text; # printing '.nc' file
	default output: reference frames;
	#default orientation: orientation matrix;
	print: equation description;
end: control data;
set: const integer GIMBAL = 1000; 
set: const integer CURR_BLADE_1_PITCH_LINK = 2100; 
set: const integer CURR_BLADE_2_PITCH_LINK = 2200; 
set: const integer CURR_BLADE_3_PITCH_LINK = 2300; 
set: const integer AIRFRAME_CLAMP = 2400;
set: const integer AIRFRAME_SWASH_JOINT = 2500;
set: const integer SWASH_JOINT = 2600;
set: const integer SWASH_HUB_JOINT = 2700; 
set: const integer CYCLIC_LAW = 2800;
set: const integer COLLECTIVE_LAW = 2900; 
set: const integer AIRFRAME_SWASH_DEF_CYCL = 3000; 
set: const integer AIRFRAME_SWASH_DEF_COLL = 3100; 
set: const integer HUB_AIRFRAME = 3200; 
set: const integer InboardBearing = 5000;
set: const integer OutboardBearing = 5100; 
set: const integer GROUND_CLAMP = 3300;
set: const integer HornAssy = 3300; 
set: const integer BoltsAssy = 3400;
set: const integer HubAssy = 5200;
set: const integer InboardAssy = 5300;
set: const integer OutboardAssy = 5400;

set: const integer CURR_BLADE_1 = 10000;
set: const integer CURR_BLADE_2 = 20000;
set: const integer CURR_BLADE_3 = 30000;
set: const integer CURR_YOKE_1 = 100000;
set: const integer CURR_YOKE_2 = 200000;
set: const integer CURR_YOKE_3 = 300000;
set: const integer OFFSET = 0;
set: const integer FEATH = 0;
set: const integer NEUTR = 1000;
set: const integer BODY = 2000;
set: const integer AERO = 3000; 
set: const integer HUB_ANG = 5200; 
set: const integer BLADE_1_GAUSS_POINTS = 1; # name of blades Gauss points
set: const integer BLADE_2_GAUSS_POINTS = 1; # name of blades Gauss points
set: const integer BLADE_3_GAUSS_POINTS = 1; # name of blades Gauss points
set: const integer V430301p58 = 11; # name of profile
c81 data: V430301p58, "../Profile/V430301p58.c81"; # Check path!! 
set: const integer VR8 = 12; # name of profile
c81 data: VR8, "../Profile/VR8.c81"; 
set: const integer VR8mod = 13; # name of profile
c81 data: VR8mod, "../Profile/VR8mod.c81"; 
set: const integer VR73 = 14; # name of profile
c81 data: VR73, "../Profile/VR73.c81"; 

set: const real THETA_COLL = 10; # input collective angle in deg
set: const real COLLECTIVE_VERTICAL = 0.000000244350555*THETA_COLL^3 + 0.000061328077169*THETA_COLL^2 -0.004217073403049*THETA_COLL + 0.000000119676574; 
set: const real T_0_COLL = 0; # time to start collective [s]\n', rotor.time_COLL);
set: const real PERIOD_COLL = 01; # time to full collective [s]\n', rotor.period_COLL);

set: const real PITCH_CYC_X = 0.0; # pitch cyc abput x [rad]\n', deg2rad(rotor.PITCH_CYC_x));
set: const real T_0_CYC_X = 1; # time to start cyc x [s]\n', rotor.time_CYC_x);
set: const real PERIOD_CYC_X = 1; # time to full cyc x [s]\n', rotor.period_CYC_x);

set: const real PITCH_CYC_Y = 0.; # pitch cyc abput y [rad]\n', deg2rad(rotor.PITCH_CYC_y));
set: const real T_0_CYC_Y = 0.01; # time to start cyc y [s]\n', rotor.time_CYC_y);
set: const real PERIOD_CYC_Y = 1; #  time to full cyc y [s]\n', rotor.period_CYC_y);

set: const real T_0_OMEGA = 0; # pitch cyc abput y [rad]\n', deg2rad(rotor.PITCH_CYC_y));
set: const real PERIOD_OMEGA = 2; # time to start cyc y [s]\n', rotor.time_CYC_y);
set: const real OMEGA_0 = 0; #  time to full cyc y [s]\n', rotor.period_CYC_y);
set: const real OMEGA_REG = 0.8 * 62.936572755; #  time to full cyc y [s]\n', rotor.period_CYC_y);



#defining nodes data
include: "rotor.ref"; 
# Blades
include: "../Blade/blade_1_rigid.ref"; 
include: "../Blade/blade_2_rigid.ref"; 
include: "../Blade/blade_3_rigid.ref"; 

# Yoke
include: "../Yoke/yoke_1_rigid.ref";
include: "../Yoke/yoke_2_rigid.ref";
include: "../Yoke/yoke_3_rigid.ref";
	
begin: nodes;

	include: "rotor.nod"; 

	include: "../Blade/blade_1_rigid.nod";
	include: "../Blade/blade_2_rigid.nod";
	include: "../Blade/blade_3_rigid.nod";

	include: "../Yoke/yoke_1_rigid.nod";
	include: "../Yoke/yoke_2_rigid.nod";
	include: "../Yoke/yoke_3_rigid.nod";
	
end: nodes;

begin: elements;

induced velocity: OFFSET + GROUND,
	rotor, OFFSET + GROUND, OFFSET + HUB_NODE,
	induced velocity,
			#no
	uniform, OMEGA_REG, 3.81, delay, .9, correction, 1.0, 1.0 , tolerance, 1e-3, max iterations, 50;
	
	# Blade beam card

	include: "../Blade/blade_1_rigid.elm";
	include: "../Blade/blade_2_rigid.elm";
	include: "../Blade/blade_3_rigid.elm";
	
	
	# Blade aero card
	# Yoke beam card
	include: "../Yoke/yoke_1_rigid.elm";
	include: "../Yoke/yoke_2_rigid.elm";
	include: "../Yoke/yoke_3_rigid.elm";

	
	# Gimbal joint
	include: "gimbal_1_rigid.elm";
	include: "gimbal_2_rigid.elm";
	include: "gimbal_3_rigid.elm";
	
	# Pitchlink rod
	include: "pitchlink_1_rigid.elm";
	include: "pitchlink_2_rigid.elm";
	include: "pitchlink_3_rigid.elm";
	
	# Rotor joint
	include: "rotor_joint.elm";

	# Connection blade-yoke
	include: "yoke_blade_1_rigid.elm";
	include: "yoke_blade_2_rigid.elm";
	include: "yoke_blade_3_rigid.elm";
	
	inertia: 1, 
		position, reference, global, null,
		body, all, output, always;
	
	air properties: 1.225, 340.0, null;

	
	gravity: uniform, 0., 0., -1., cosine, 3.0*0.001, pi, 9.81/2., half, 0.;
end: elements;
