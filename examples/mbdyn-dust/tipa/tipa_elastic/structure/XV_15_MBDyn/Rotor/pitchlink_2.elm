joint: OFFSET + CURR_BLADE_2_PITCH_LINK, rod, # blade pitch link
	CURR_ROTOR + SW_ROT_NODE,
		position, reference, global, -0.1769549654031180, 0.1769549907297935, -0.4000000000000000,
	CURR_ROTOR + CURR_BLADE_2 + 1,
		position, reference, global, -0.1769549654031180, 0.1769549907297935, 0.0931330000000000,
		from nodes,
		linear viscoelastic, 161363.3254531000275165, proportional, 0.001;
