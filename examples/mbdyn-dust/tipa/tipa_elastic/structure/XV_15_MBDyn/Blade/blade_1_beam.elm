# D:\Clouds\OneDrive - Politecnico di Milano\Dottorato\ATTILA\TiPa_New\Tilt_load_data
# 08-Jul-2020 18:12:53
# DO NOT MODIFY 


# Defining the beam/sections notation
# x1=6.096000e-01 xm=6.286500e-01 x2=6.477000e-01 (0.160 -> 0.165 -> 0.170 R
# Beam CURR_ROTOR + CURR_BLADE_1 + 1, 
#                                       A  beam y 
#                                      /   adimensional axis
#                                     /
#                                    /
#           |            |          |----------|-------------|---------> beam x adimensional axis
#          -1       -1/sqrt(3)      0       1/sqrt(3)        1 
#               ________________________________________________
#             /                                                /
#            /        section I             section II        /
#           /_____________|_____________________|____________/ 
#           |           | | |       |         | | |          |  
#           |           | V |       |         | V |          |  
#           |           |   |       |         |   |          |  
#           |  _________|   |_______|_________|   |__________|__ 
#           | /         |  /        | /       |  /           | /
#           |/          | /         |/        | /            |/
#           /___________|/__________/_________|/_____________/
#          node 1                  node 2              node 3
#      0
#      |---\....\----------------------------------------------------------> global X axis 
#
beam3: CURR_ROTOR + CURR_BLADE_1 + 1, # beam name
	CURR_ROTOR + CURR_BLADE_1 + 1 , # node 1 name
		position, reference, CURR_ROTOR + CURR_BLADE_1 + NEUTR + 1, 0., 0.0000000000000000, 0.0000000000000000, 
		orientation, reference, CURR_ROTOR + CURR_BLADE_1 + NEUTR + 1, eye, # orientation for properties
	CURR_ROTOR + CURR_BLADE_1 + 2 , # node 2 name
		position, reference, CURR_ROTOR + CURR_BLADE_1 + NEUTR + 2, 0., 0.0000000000000000, 0.0000000000000000, 
		orientation, reference, CURR_ROTOR + CURR_BLADE_1 + NEUTR + 2, eye, # orientation for properties
	CURR_ROTOR + CURR_BLADE_1 + 3 , # node 3 name
		position, reference, CURR_ROTOR + CURR_BLADE_1 + NEUTR + 3, 0., 0.0000000000000000, 0.0000000000000000, 
		orientation, reference, CURR_ROTOR + CURR_BLADE_1 + NEUTR + 3, eye, # orientation for properties
		from nodes, 
		# EA = 9.84154673e+11 [N], GA_y = 3.69982960e+11 [N], GA_z = 3.69982960e+11 [N], GJ = 1.06201042e+06 [N*M^2], EJ_Y = 4.50724001e+05 [N*M^2], EJ_Z = 1.37784380e+06 [N*M^2]
	linear elastic generic, matr, #sec I const law
			9.84154673e+11, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, -0.00000000e+00,
			0.00000000e+00, 3.69982960e+11, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 3.69982960e+11, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 1.06201042e+06, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 4.50724001e+05, -0.00000000e+00,
			-0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, -0.00000000e+00, 1.37784380e+06,
	from nodes, # automatically setting section II orientation
		# EA = 5.53432694e+07 [N], GA_y = 2.08057404e+07 [N], GA_z = 2.08057404e+07 [N], GJ = 1.06201042e+06 [N*M^2], EJ_Y = 4.50724001e+05 [N*M^2], EJ_Z = 1.37784380e+06 [N*M^2]
	linear elastic generic, matr, #sec II const law
			5.53432694e+07, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, -0.00000000e+00,
			0.00000000e+00, 2.08057404e+07, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 2.08057404e+07, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 1.06201042e+06, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 4.50724001e+05, 0.00000000e+00,
			-0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 1.37784380e+06;

# Defining the beam/sections notation
# x1=6.477000e-01 xm=7.048500e-01 x2=7.620000e-01 (0.170 -> 0.185 -> 0.200 R
# Beam CURR_ROTOR + CURR_BLADE_1 + 2, 
#                                       A  beam y 
#                                      /   adimensional axis
#                                     /
#                                    /
#           |            |          |----------|-------------|---------> beam x adimensional axis
#          -1       -1/sqrt(3)      0       1/sqrt(3)        1 
#               ________________________________________________
#             /                                                /
#            /        section I             section II        /
#           /_____________|_____________________|____________/ 
#           |           | | |       |         | | |          |  
#           |           | V |       |         | V |          |  
#           |           |   |       |         |   |          |  
#           |  _________|   |_______|_________|   |__________|__ 
#           | /         |  /        | /       |  /           | /
#           |/          | /         |/        | /            |/
#           /___________|/__________/_________|/_____________/
#          node 1                  node 2              node 3
#      0
#      |---\....\----------------------------------------------------------> global X axis 
#
beam3: CURR_ROTOR + CURR_BLADE_1 + 2, # beam name
	CURR_ROTOR + CURR_BLADE_1 + 3 , # node 1 name
		position, reference, CURR_ROTOR + CURR_BLADE_1 + NEUTR + 3, 0., 0.0000000000000000, 0.0000000000000000, 
		orientation, reference, CURR_ROTOR + CURR_BLADE_1 + NEUTR + 3, eye, # orientation for properties
	CURR_ROTOR + CURR_BLADE_1 + 4 , # node 2 name
		position, reference, CURR_ROTOR + CURR_BLADE_1 + NEUTR + 4, 0., 0.0000000000000000, 0.0000000000000000, 
		orientation, reference, CURR_ROTOR + CURR_BLADE_1 + NEUTR + 4, eye, # orientation for properties
	CURR_ROTOR + CURR_BLADE_1 + 5 , # node 3 name
		position, reference, CURR_ROTOR + CURR_BLADE_1 + NEUTR + 5, 0., 0.0000000000000000, 0.0000000000000000, 
		orientation, reference, CURR_ROTOR + CURR_BLADE_1 + NEUTR + 5, eye, # orientation for properties
		from nodes, 
		# EA = 3.68586030e+07 [N], GA_y = 1.38566177e+07 [N], GA_z = 1.38566177e+07 [N], GJ = 2.29243774e+05 [N*M^2], EJ_Y = 4.50724001e+05 [N*M^2], EJ_Z = 2.21042028e+06 [N*M^2]
	linear elastic generic, matr, #sec I const law
			3.68586030e+07, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, -0.00000000e+00,
			0.00000000e+00, 1.38566177e+07, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 1.38566177e+07, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 2.29243774e+05, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 4.50724001e+05, 0.00000000e+00,
			-0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 2.21042028e+06,
	from nodes, # automatically setting section II orientation
		# EA = 1.01476052e+09 [N], GA_y = 3.81488918e+08 [N], GA_z = 3.81488918e+08 [N], GJ = 1.99380157e+05 [N*M^2], EJ_Y = 1.16453225e+06 [N*M^2], EJ_Z = 2.58702265e+06 [N*M^2]
	linear elastic generic, matr, #sec II const law
			1.01476052e+09, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, -0.00000000e+00,
			0.00000000e+00, 3.81488918e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 3.81488918e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 1.99380157e+05, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 1.16453225e+06, 0.00000000e+00,
			-0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 2.58702265e+06;

# Defining the beam/sections notation
# x1=7.620000e-01 xm=1.143000e+00 x2=1.524000e+00 (0.200 -> 0.300 -> 0.400 R
# Beam CURR_ROTOR + CURR_BLADE_1 + 3, 
#                                       A  beam y 
#                                      /   adimensional axis
#                                     /
#                                    /
#           |            |          |----------|-------------|---------> beam x adimensional axis
#          -1       -1/sqrt(3)      0       1/sqrt(3)        1 
#               ________________________________________________
#             /                                                /
#            /        section I             section II        /
#           /_____________|_____________________|____________/ 
#           |           | | |       |         | | |          |  
#           |           | V |       |         | V |          |  
#           |           |   |       |         |   |          |  
#           |  _________|   |_______|_________|   |__________|__ 
#           | /         |  /        | /       |  /           | /
#           |/          | /         |/        | /            |/
#           /___________|/__________/_________|/_____________/
#          node 1                  node 2              node 3
#      0
#      |---\....\----------------------------------------------------------> global X axis 
#
beam3: CURR_ROTOR + CURR_BLADE_1 + 3, # beam name
	CURR_ROTOR + CURR_BLADE_1 + 5 , # node 1 name
		position, reference, CURR_ROTOR + CURR_BLADE_1 + NEUTR + 5, 0., 0.0000000000000000, 0.0000000000000000, 
		orientation, reference, CURR_ROTOR + CURR_BLADE_1 + NEUTR + 5, eye, # orientation for properties
	CURR_ROTOR + CURR_BLADE_1 + 6 , # node 2 name
		position, reference, CURR_ROTOR + CURR_BLADE_1 + NEUTR + 6, 0., 0.0114628876152000, 0.0041529582168000, 
		orientation, reference, CURR_ROTOR + CURR_BLADE_1 + NEUTR + 6, eye, # orientation for properties
	CURR_ROTOR + CURR_BLADE_1 + 7 , # node 3 name
		position, reference, CURR_ROTOR + CURR_BLADE_1 + NEUTR + 7, 0., 0.0146985267624000, 0.0039388670016000, 
		orientation, reference, CURR_ROTOR + CURR_BLADE_1 + NEUTR + 7, eye, # orientation for properties
		from nodes, 
		# EA = 6.08115572e+08 [N], GA_y = 2.28614877e+08 [N], GA_z = 2.28614877e+08 [N], GJ = 1.38652508e+05 [N*M^2], EJ_Y = 3.55395234e+05 [N*M^2], EJ_Z = 1.60479737e+06 [N*M^2]
	linear elastic generic, matr, #sec I const law
			6.08115572e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 8.19787394e+05, -1.95330434e+06,
			0.00000000e+00, 2.28614877e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 2.28614877e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 1.38652508e+05, 0.00000000e+00, 0.00000000e+00,
			8.19787394e+05, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 3.56500371e+05, -2.63320715e+03,
			-1.95330434e+06, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, -2.63320715e+03, 1.61107150e+06,
	from nodes, # automatically setting section II orientation
		# EA = 5.69208044e+08 [N], GA_y = 2.13987987e+08 [N], GA_z = 2.13987987e+08 [N], GJ = 8.18107674e+04 [N*M^2], EJ_Y = 1.14110656e+05 [N*M^2], EJ_Z = 1.79771479e+06 [N*M^2]
	linear elastic generic, matr, #sec II const law
			5.69208044e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 2.28038507e+06, -7.90502956e+06,
			0.00000000e+00, 2.13987987e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 2.13987987e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 8.18107674e+04, 0.00000000e+00, 0.00000000e+00,
			2.28038507e+06, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 1.23246430e+05, -3.16694600e+04,
			-7.90502956e+06, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, -3.16694600e+04, 1.90749801e+06;

# Defining the beam/sections notation
# x1=1.524000e+00 xm=2.244090e+00 x2=2.964180e+00 (0.400 -> 0.589 -> 0.778 R
# Beam CURR_ROTOR + CURR_BLADE_1 + 4, 
#                                       A  beam y 
#                                      /   adimensional axis
#                                     /
#                                    /
#           |            |          |----------|-------------|---------> beam x adimensional axis
#          -1       -1/sqrt(3)      0       1/sqrt(3)        1 
#               ________________________________________________
#             /                                                /
#            /        section I             section II        /
#           /_____________|_____________________|____________/ 
#           |           | | |       |         | | |          |  
#           |           | V |       |         | V |          |  
#           |           |   |       |         |   |          |  
#           |  _________|   |_______|_________|   |__________|__ 
#           | /         |  /        | /       |  /           | /
#           |/          | /         |/        | /            |/
#           /___________|/__________/_________|/_____________/
#          node 1                  node 2              node 3
#      0
#      |---\....\----------------------------------------------------------> global X axis 
#
beam3: CURR_ROTOR + CURR_BLADE_1 + 4, # beam name
	CURR_ROTOR + CURR_BLADE_1 + 7 , # node 1 name
		position, reference, CURR_ROTOR + CURR_BLADE_1 + NEUTR + 7, 0., 0.0146985267624000, 0.0039388670016000, 
		orientation, reference, CURR_ROTOR + CURR_BLADE_1 + NEUTR + 7, eye, # orientation for properties
	CURR_ROTOR + CURR_BLADE_1 + 8 , # node 2 name
		position, reference, CURR_ROTOR + CURR_BLADE_1 + NEUTR + 8, 0., 0.0044261544252960, 0.0005017432769280, 
		orientation, reference, CURR_ROTOR + CURR_BLADE_1 + NEUTR + 8, eye, # orientation for properties
	CURR_ROTOR + CURR_BLADE_1 + 9 , # node 3 name
		position, reference, CURR_ROTOR + CURR_BLADE_1 + NEUTR + 9, 0., 0.0111732578160000, -0.0001813776408000, 
		orientation, reference, CURR_ROTOR + CURR_BLADE_1 + NEUTR + 9, eye, # orientation for properties
		from nodes, 
		# EA = 5.70826085e+08 [N], GA_y = 2.14596272e+08 [N], GA_z = 2.14596272e+08 [N], GJ = 8.18107674e+04 [N*M^2], EJ_Y = 7.49484191e+04 [N*M^2], EJ_Z = 1.48119242e+06 [N*M^2]
	linear elastic generic, matr, #sec I const law
			5.70826085e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 1.78439216e+06, -1.03981891e+07,
			0.00000000e+00, 2.14596272e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 2.14596272e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 8.18107674e+04, 0.00000000e+00, 0.00000000e+00,
			1.78439216e+06, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 8.05263971e+04, -3.25045537e+04,
			-1.03981891e+07, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, -3.25045537e+04, 1.67060622e+06,
	from nodes, # automatically setting section II orientation
		# EA = 3.77767151e+08 [N], GA_y = 1.42017726e+08 [N], GA_z = 1.42017726e+08 [N], GJ = 3.38735436e+04 [N*M^2], EJ_Y = 3.13906400e+04 [N*M^2], EJ_Z = 9.50525442e+05 [N*M^2]
	linear elastic generic, matr, #sec II const law
			3.77767151e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, -9.65316571e+03, -2.14071730e+06,
			0.00000000e+00, 1.42017726e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 1.42017726e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 3.38735436e+04, 0.00000000e+00, 0.00000000e+00,
			-9.65316571e+03, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 3.13908867e+04, 5.47022122e+01,
			-2.14071730e+06, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 5.47022122e+01, 9.62656382e+05;

# Defining the beam/sections notation
# x1=2.964180e+00 xm=3.156585e+00 x2=3.348990e+00 (0.778 -> 0.829 -> 0.879 R
# Beam CURR_ROTOR + CURR_BLADE_1 + 5, 
#                                       A  beam y 
#                                      /   adimensional axis
#                                     /
#                                    /
#           |            |          |----------|-------------|---------> beam x adimensional axis
#          -1       -1/sqrt(3)      0       1/sqrt(3)        1 
#               ________________________________________________
#             /                                                /
#            /        section I             section II        /
#           /_____________|_____________________|____________/ 
#           |           | | |       |         | | |          |  
#           |           | V |       |         | V |          |  
#           |           |   |       |         |   |          |  
#           |  _________|   |_______|_________|   |__________|__ 
#           | /         |  /        | /       |  /           | /
#           |/          | /         |/        | /            |/
#           /___________|/__________/_________|/_____________/
#          node 1                  node 2              node 3
#      0
#      |---\....\----------------------------------------------------------> global X axis 
#
beam3: CURR_ROTOR + CURR_BLADE_1 + 5, # beam name
	CURR_ROTOR + CURR_BLADE_1 + 9 , # node 1 name
		position, reference, CURR_ROTOR + CURR_BLADE_1 + NEUTR + 9, 0., 0.0111732578160000, -0.0001813776408000, 
		orientation, reference, CURR_ROTOR + CURR_BLADE_1 + NEUTR + 9, eye, # orientation for properties
	CURR_ROTOR + CURR_BLADE_1 + 10 , # node 2 name
		position, reference, CURR_ROTOR + CURR_BLADE_1 + NEUTR + 10, 0., 0.0129156306295881, -0.0006192374325314, 
		orientation, reference, CURR_ROTOR + CURR_BLADE_1 + NEUTR + 10, eye, # orientation for properties
	CURR_ROTOR + CURR_BLADE_1 + 11 , # node 3 name
		position, reference, CURR_ROTOR + CURR_BLADE_1 + NEUTR + 11, 0., -0.0065831517816000, 0.0005080924560000, 
		orientation, reference, CURR_ROTOR + CURR_BLADE_1 + NEUTR + 11, eye, # orientation for properties
		from nodes, 
		# EA = 4.07207984e+08 [N], GA_y = 1.53085708e+08 [N], GA_z = 1.53085708e+08 [N], GJ = 2.00171777e+04 [N*M^2], EJ_Y = 1.72516303e+04 [N*M^2], EJ_Z = 3.48911110e+05 [N*M^2]
	linear elastic generic, matr, #sec I const law
			4.07207984e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, -2.22147535e+05, -5.13992575e+06,
			0.00000000e+00, 1.53085708e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 1.53085708e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 2.00171777e+04, 0.00000000e+00, 0.00000000e+00,
			-2.22147535e+05, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 1.73728203e+04, 2.80402615e+03,
			-5.13992575e+06, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 2.80402615e+03, 4.13789103e+05,
	from nodes, # automatically setting section II orientation
		# EA = 4.14695034e+08 [N], GA_y = 1.55900389e+08 [N], GA_z = 1.55900389e+08 [N], GJ = 1.17693408e+04 [N*M^2], EJ_Y = 1.37495004e+04 [N*M^2], EJ_Z = 2.98470814e+05 [N*M^2]
	linear elastic generic, matr, #sec II const law
			4.14695034e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, -3.69007101e+05, -5.81213365e+06,
			0.00000000e+00, 1.55900389e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 1.55900389e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 1.17693408e+04, 0.00000000e+00, 0.00000000e+00,
			-3.69007101e+05, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 1.40778531e+04, 5.17179714e+03,
			-5.81213365e+06, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 5.17179714e+03, 3.79930428e+05;

# Defining the beam/sections notation
# x1=3.348990e+00 xm=3.388995e+00 x2=3.429000e+00 (0.879 -> 0.890 -> 0.900 R
# Beam CURR_ROTOR + CURR_BLADE_1 + 6, 
#                                       A  beam y 
#                                      /   adimensional axis
#                                     /
#                                    /
#           |            |          |----------|-------------|---------> beam x adimensional axis
#          -1       -1/sqrt(3)      0       1/sqrt(3)        1 
#               ________________________________________________
#             /                                                /
#            /        section I             section II        /
#           /_____________|_____________________|____________/ 
#           |           | | |       |         | | |          |  
#           |           | V |       |         | V |          |  
#           |           |   |       |         |   |          |  
#           |  _________|   |_______|_________|   |__________|__ 
#           | /         |  /        | /       |  /           | /
#           |/          | /         |/        | /            |/
#           /___________|/__________/_________|/_____________/
#          node 1                  node 2              node 3
#      0
#      |---\....\----------------------------------------------------------> global X axis 
#
beam3: CURR_ROTOR + CURR_BLADE_1 + 6, # beam name
	CURR_ROTOR + CURR_BLADE_1 + 11 , # node 1 name
		position, reference, CURR_ROTOR + CURR_BLADE_1 + NEUTR + 11, 0., -0.0065831517816000, 0.0005080924560000, 
		orientation, reference, CURR_ROTOR + CURR_BLADE_1 + NEUTR + 11, eye, # orientation for properties
	CURR_ROTOR + CURR_BLADE_1 + 12 , # node 2 name
		position, reference, CURR_ROTOR + CURR_BLADE_1 + NEUTR + 12, 0., -0.0065798847637500, 0.0005484040276500, 
		orientation, reference, CURR_ROTOR + CURR_BLADE_1 + NEUTR + 12, eye, # orientation for properties
	CURR_ROTOR + CURR_BLADE_1 + 13 , # node 3 name
		position, reference, CURR_ROTOR + CURR_BLADE_1 + NEUTR + 13, 0., -0.0065758896168000, 0.0005947449624000, 
		orientation, reference, CURR_ROTOR + CURR_BLADE_1 + NEUTR + 13, eye, # orientation for properties
		from nodes, 
		# EA = 4.22781673e+08 [N], GA_y = 1.58940479e+08 [N], GA_z = 1.58940479e+08 [N], GJ = 8.92932075e+03 [N*M^2], EJ_Y = 1.19719041e+04 [N*M^2], EJ_Z = 2.98470814e+05 [N*M^2]
	linear elastic generic, matr, #sec I const law
			4.22781673e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 2.22152135e+05, 2.78265964e+06,
			0.00000000e+00, 1.58940479e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 1.58940479e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 8.92932075e+03, 0.00000000e+00, 0.00000000e+00,
			2.22152135e+05, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 1.20886347e+04, 1.46215841e+03,
			2.78265964e+06, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 1.46215841e+03, 3.16785692e+05,
	from nodes, # automatically setting section II orientation
		# EA = 4.69638147e+08 [N], GA_y = 1.76555694e+08 [N], GA_z = 1.76555694e+08 [N], GJ = 7.64779786e+03 [N*M^2], EJ_Y = 1.06490418e+04 [N*M^2], EJ_Z = 2.98470814e+05 [N*M^2]
	linear elastic generic, matr, #sec II const law
			4.69638147e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 2.79314922e+05, 3.08828862e+06,
			0.00000000e+00, 1.76555694e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 1.76555694e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 7.64779786e+03, 0.00000000e+00, 0.00000000e+00,
			2.79314922e+05, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 1.08151629e+04, 1.83674410e+03,
			3.08828862e+06, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 1.83674410e+03, 3.18779059e+05;

# Defining the beam/sections notation
# x1=3.429000e+00 xm=3.505200e+00 x2=3.581400e+00 (0.900 -> 0.920 -> 0.940 R
# Beam CURR_ROTOR + CURR_BLADE_1 + 7, 
#                                       A  beam y 
#                                      /   adimensional axis
#                                     /
#                                    /
#           |            |          |----------|-------------|---------> beam x adimensional axis
#          -1       -1/sqrt(3)      0       1/sqrt(3)        1 
#               ________________________________________________
#             /                                                /
#            /        section I             section II        /
#           /_____________|_____________________|____________/ 
#           |           | | |       |         | | |          |  
#           |           | V |       |         | V |          |  
#           |           |   |       |         |   |          |  
#           |  _________|   |_______|_________|   |__________|__ 
#           | /         |  /        | /       |  /           | /
#           |/          | /         |/        | /            |/
#           /___________|/__________/_________|/_____________/
#          node 1                  node 2              node 3
#      0
#      |---\....\----------------------------------------------------------> global X axis 
#
beam3: CURR_ROTOR + CURR_BLADE_1 + 7, # beam name
	CURR_ROTOR + CURR_BLADE_1 + 13 , # node 1 name
		position, reference, CURR_ROTOR + CURR_BLADE_1 + NEUTR + 13, 0., -0.0065758896168000, 0.0005947449624000, 
		orientation, reference, CURR_ROTOR + CURR_BLADE_1 + NEUTR + 13, eye, # orientation for properties
	CURR_ROTOR + CURR_BLADE_1 + 14 , # node 2 name
		position, reference, CURR_ROTOR + CURR_BLADE_1 + NEUTR + 14, 0., -0.0095960230188923, 0.0010257893475692, 
		orientation, reference, CURR_ROTOR + CURR_BLADE_1 + NEUTR + 14, eye, # orientation for properties
	CURR_ROTOR + CURR_BLADE_1 + 15 , # node 3 name
		position, reference, CURR_ROTOR + CURR_BLADE_1 + NEUTR + 15, 0., -0.0182139680424000, 0.0021251789856000, 
		orientation, reference, CURR_ROTOR + CURR_BLADE_1 + NEUTR + 15, eye, # orientation for properties
		from nodes, 
		# EA = 4.20165721e+08 [N], GA_y = 1.57957038e+08 [N], GA_z = 1.57957038e+08 [N], GJ = 6.90368780e+03 [N*M^2], EJ_Y = 9.87599411e+03 [N*M^2], EJ_Z = 2.86482374e+05 [N*M^2]
	linear elastic generic, matr, #sec I const law
			4.20165721e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 2.85389050e+05, 2.75952264e+06,
			0.00000000e+00, 1.57957038e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 1.57957038e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 6.90368780e+03, 0.00000000e+00, 0.00000000e+00,
			2.85389050e+05, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 1.00698388e+04, 1.87434982e+03,
			2.75952264e+06, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 1.87434982e+03, 3.04606092e+05,
	from nodes, # automatically setting section II orientation
		# EA = 3.98781666e+08 [N], GA_y = 1.49917919e+08 [N], GA_z = 1.49917919e+08 [N], GJ = 4.01819434e+03 [N*M^2], EJ_Y = 7.92477216e+03 [N*M^2], EJ_Z = 2.55891183e+05 [N*M^2]
	linear elastic generic, matr, #sec II const law
			3.98781666e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 4.49423709e+05, 3.93538763e+06,
			0.00000000e+00, 1.49917919e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 1.49917919e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 4.01819434e+03, 0.00000000e+00, 0.00000000e+00,
			4.49423709e+05, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 8.43126905e+03, 4.43514999e+03,
			3.93538763e+06, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 4.43514999e+03, 2.94727662e+05;

# Defining the beam/sections notation
# x1=3.581400e+00 xm=3.695700e+00 x2=3.810000e+00 (0.940 -> 0.970 -> 1.000 R
# Beam CURR_ROTOR + CURR_BLADE_1 + 8, 
#                                       A  beam y 
#                                      /   adimensional axis
#                                     /
#                                    /
#           |            |          |----------|-------------|---------> beam x adimensional axis
#          -1       -1/sqrt(3)      0       1/sqrt(3)        1 
#               ________________________________________________
#             /                                                /
#            /        section I             section II        /
#           /_____________|_____________________|____________/ 
#           |           | | |       |         | | |          |  
#           |           | V |       |         | V |          |  
#           |           |   |       |         |   |          |  
#           |  _________|   |_______|_________|   |__________|__ 
#           | /         |  /        | /       |  /           | /
#           |/          | /         |/        | /            |/
#           /___________|/__________/_________|/_____________/
#          node 1                  node 2              node 3
#      0
#      |---\....\----------------------------------------------------------> global X axis 
#
beam3: CURR_ROTOR + CURR_BLADE_1 + 8, # beam name
	CURR_ROTOR + CURR_BLADE_1 + 15 , # node 1 name
		position, reference, CURR_ROTOR + CURR_BLADE_1 + NEUTR + 15, 0., -0.0182139680424000, 0.0021251789856000, 
		orientation, reference, CURR_ROTOR + CURR_BLADE_1 + NEUTR + 15, eye, # orientation for properties
	CURR_ROTOR + CURR_BLADE_1 + 16 , # node 2 name
		position, reference, CURR_ROTOR + CURR_BLADE_1 + NEUTR + 16, 0., -0.0202338166824000, 0.0027630372984000, 
		orientation, reference, CURR_ROTOR + CURR_BLADE_1 + NEUTR + 16, eye, # orientation for properties
	CURR_ROTOR + CURR_BLADE_1 + 17 , # node 3 name
		position, reference, CURR_ROTOR + CURR_BLADE_1 + NEUTR + 17, 0., -0.0243600768576000, 0.0037740750528000, 
		orientation, reference, CURR_ROTOR + CURR_BLADE_1 + NEUTR + 17, eye, # orientation for properties
		from nodes, 
		# EA = 4.59629642e+08 [N], GA_y = 1.72793099e+08 [N], GA_z = 1.72793099e+08 [N], GJ = 1.86027516e+03 [N*M^2], EJ_Y = 5.96941628e+03 [N*M^2], EJ_Z = 1.98015955e+05 [N*M^2]
	linear elastic generic, matr, #sec I const law
			4.59629642e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 1.33670493e+06, 1.02981605e+07,
			0.00000000e+00, 1.72793099e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 1.72793099e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 1.86027516e+03, 0.00000000e+00, 0.00000000e+00,
			1.33670493e+06, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 9.85685066e+03, 2.99493344e+04,
			1.02981605e+07, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 2.99493344e+04, 4.28749788e+05,
	from nodes, # automatically setting section II orientation
		# EA = 5.76427085e+08 [N], GA_y = 2.16701912e+08 [N], GA_z = 2.16701912e+08 [N], GJ = 3.32920354e+02 [N*M^2], EJ_Y = 3.26306042e+03 [N*M^2], EJ_Z = 4.65757780e+04 [N*M^2]
	linear elastic generic, matr, #sec II const law
			5.76427085e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 2.13375597e+06, 1.40480820e+07,
			0.00000000e+00, 2.16701912e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 2.16701912e+08, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 3.32920354e+02, 0.00000000e+00, 0.00000000e+00,
			2.13375597e+06, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 1.11615695e+04, 5.20016838e+04,
			1.40480820e+07, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 5.20016838e+04, 3.88941038e+05;

