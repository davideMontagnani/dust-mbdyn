# D:\Clouds\OneDrive - Politecnico di Milano\Dottorato\ATTILA\TiPa_New\Tilt_load_data
# 08-Jul-2020 18:12:54
# DO NOT MODIFY 


# x1=6.096000e-01 xm=6.286500e-01 x2=6.477000e-01 (0.160 -> 0.165 -> 0.170 R)
aerodynamic beam3:
	CURR_ROTOR + CURR_BLADE_3 + 1, # blade aero pan name 
	CURR_ROTOR + CURR_BLADE_3 + 1, # blade beam name 
	induced velocity, CURR_ROTOR,
	reference, CURR_ROTOR + CURR_BLADE_3 + AERO + 1, null, 
		1, 0., 1., 0., 3, 1., 0., 0., # orientation wrt to ref system 
	reference, CURR_ROTOR + CURR_BLADE_3 + AERO + 2, null, 
		1, 0., 1., 0., 3, 1., 0., 0., # orientation wrt to ref system 
	reference, CURR_ROTOR + CURR_BLADE_3 + AERO + 3, null, 
		1, 0., 1., 0., 3, 1., 0., 0., # orientation wrt to ref system 
	piecewise linear, 3, # defining chord shape as linear
		-1.0000000000, 0.5078730000, # element chord [m]
		0.0000000000, 0.5078730000, # element chord [m]
		1.0000000000, 0.5078730000, # element chord [m]
	const, 0., # defining AC 
	piecewise linear, 3, # defining BC trend as linear
		-1.0000000000, -0.2539365000, # element BC point [m]
		0.0000000000, -0.2539365000, # element BC point [m]
		1.0000000000, -0.2539365000, # element BC point [m]
	const, 0., # defining Twist 
		BLADE_3_GAUSS_POINTS, # name of blade Gauss points
		c81, V430301p58,
		#unsteady, bielawa, # unsteadiness on
		jacobian, 0; # jacobian output

# x1=6.477000e-01 xm=7.048500e-01 x2=7.620000e-01 (0.170 -> 0.185 -> 0.200 R)
aerodynamic beam3:
	CURR_ROTOR + CURR_BLADE_3 + 2, # blade aero pan name 
	CURR_ROTOR + CURR_BLADE_3 + 2, # blade beam name 
	induced velocity, CURR_ROTOR,
	reference, CURR_ROTOR + CURR_BLADE_3 + AERO + 3, null, 
		1, 0., 1., 0., 3, 1., 0., 0., # orientation wrt to ref system 
	reference, CURR_ROTOR + CURR_BLADE_3 + AERO + 4, null, 
		1, 0., 1., 0., 3, 1., 0., 0., # orientation wrt to ref system 
	reference, CURR_ROTOR + CURR_BLADE_3 + AERO + 5, null, 
		1, 0., 1., 0., 3, 1., 0., 0., # orientation wrt to ref system 
	piecewise linear, 6, # defining chord shape as linear
		-1.0000000000, 0.5078730000, # element chord [m]
		-0.8666666667, 0.5078730000, # element chord [m]
		-0.4666666667, 0.5078730000, # element chord [m]
		-0.3333333333, 0.5078730000, # element chord [m]
		0.0000000000, 0.5078730000, # element chord [m]
		1.0000000000, 0.5078730000, # element chord [m]
	const, 0., # defining AC 
	piecewise linear, 6, # defining BC trend as linear
		-1.0000000000, -0.2539365000, # element BC point [m]
		-0.8666666667, -0.2539365000, # element BC point [m]
		-0.4666666667, -0.2539365000, # element BC point [m]
		-0.3333333333, -0.2539365000, # element BC point [m]
		0.0000000000, -0.2539365000, # element BC point [m]
		1.0000000000, -0.2539365000, # element BC point [m]
	const, 0., # defining Twist 
		BLADE_3_GAUSS_POINTS, # name of blade Gauss points
		c81, V430301p58,
		#unsteady, bielawa, # unsteadiness on
		jacobian, 0; # jacobian output

# x1=7.620000e-01 xm=1.143000e+00 x2=1.524000e+00 (0.200 -> 0.300 -> 0.400 R)
aerodynamic beam3:
	CURR_ROTOR + CURR_BLADE_3 + 3, # blade aero pan name 
	CURR_ROTOR + CURR_BLADE_3 + 3, # blade beam name 
	induced velocity, CURR_ROTOR,
	reference, CURR_ROTOR + CURR_BLADE_3 + AERO + 5, null, 
		1, 0., 1., 0., 3, 1., 0., 0., # orientation wrt to ref system 
	reference, CURR_ROTOR + CURR_BLADE_3 + AERO + 6, null, 
		1, 0., 1., 0., 3, 1., 0., 0., # orientation wrt to ref system 
	reference, CURR_ROTOR + CURR_BLADE_3 + AERO + 7, null, 
		1, 0., 1., 0., 3, 1., 0., 0., # orientation wrt to ref system 
	piecewise linear, 10, # defining chord shape as linear
		-1.0000000000, 0.5078730000, # element chord [m]
		-0.9650000000, 0.5078730000, # element chord [m]
		-0.7000000000, 0.5078730000, # element chord [m]
		-0.5000000000, 0.5078730000, # element chord [m]
		-0.3000000000, 0.5078730000, # element chord [m]
		-0.2700000000, 0.5081639455, # element chord [m]
		0.0000000000, 0.5107824545, # element chord [m]
		0.2500000000, 0.5132070000, # element chord [m]
		0.7500000000, 0.5132070000, # element chord [m]
		1.0000000000, 0.5132070000, # element chord [m]
	const, 0., # defining AC 
	piecewise linear, 10, # defining BC trend as linear
		-1.0000000000, -0.2539365000, # element BC point [m]
		-0.9650000000, -0.2539365000, # element BC point [m]
		-0.7000000000, -0.2539365000, # element BC point [m]
		-0.5000000000, -0.2539365000, # element BC point [m]
		-0.3000000000, -0.2539365000, # element BC point [m]
		-0.2700000000, -0.2540819727, # element BC point [m]
		0.0000000000, -0.2553912273, # element BC point [m]
		0.2500000000, -0.2566035000, # element BC point [m]
		0.7500000000, -0.2566035000, # element BC point [m]
		1.0000000000, -0.2566035000, # element BC point [m]
	const, 0., # defining Twist 
		BLADE_3_GAUSS_POINTS, # name of blade Gauss points
	c81, multiple, 2, 
		V430301p58, -0.999, #0.200 
		VR73, 1.0, #0.400 
		#unsteady, bielawa, # unsteadiness on
		jacobian, 0; # jacobian output

# x1=1.524000e+00 xm=2.244090e+00 x2=2.964180e+00 (0.400 -> 0.589 -> 0.778 R)
aerodynamic beam3:
	CURR_ROTOR + CURR_BLADE_3 + 4, # blade aero pan name 
	CURR_ROTOR + CURR_BLADE_3 + 4, # blade beam name 
	induced velocity, CURR_ROTOR,
	reference, CURR_ROTOR + CURR_BLADE_3 + AERO + 7, null, 
		1, 0., 1., 0., 3, 1., 0., 0., # orientation wrt to ref system 
	reference, CURR_ROTOR + CURR_BLADE_3 + AERO + 8, null, 
		1, 0., 1., 0., 3, 1., 0., 0., # orientation wrt to ref system 
	reference, CURR_ROTOR + CURR_BLADE_3 + AERO + 9, null, 
		1, 0., 1., 0., 3, 1., 0., 0., # orientation wrt to ref system 
	piecewise linear, 16, # defining chord shape as linear
		-1.0000000000, 0.5132070000, # element chord [m]
		-0.8677248677, 0.5132070000, # element chord [m]
		-0.6031746032, 0.5132070000, # element chord [m]
		-0.4285714286, 0.5132070000, # element chord [m]
		-0.3386243386, 0.5132070000, # element chord [m]
		-0.2063492063, 0.5132070000, # element chord [m]
		-0.0740740741, 0.5132070000, # element chord [m]
		0.0000000000, 0.5132070000, # element chord [m]
		0.0582010582, 0.5132070000, # element chord [m]
		0.1904761905, 0.5132070000, # element chord [m]
		0.3756613757, 0.5105400000, # element chord [m]
		0.4550264550, 0.5093970000, # element chord [m]
		0.5343915344, 0.4975098000, # element chord [m]
		0.7195767196, 0.4697730000, # element chord [m]
		0.9841269841, 0.4187190000, # element chord [m]
		1.0000000000, 0.4167632000, # element chord [m]
	const, 0., # defining AC 
	piecewise linear, 16, # defining BC trend as linear
		-1.0000000000, -0.2566035000, # element BC point [m]
		-0.8677248677, -0.2566035000, # element BC point [m]
		-0.6031746032, -0.2566035000, # element BC point [m]
		-0.4285714286, -0.2566035000, # element BC point [m]
		-0.3386243386, -0.2566035000, # element BC point [m]
		-0.2063492063, -0.2566035000, # element BC point [m]
		-0.0740740741, -0.2566035000, # element BC point [m]
		0.0000000000, -0.2566035000, # element BC point [m]
		0.0582010582, -0.2566035000, # element BC point [m]
		0.1904761905, -0.2566035000, # element BC point [m]
		0.3756613757, -0.2552700000, # element BC point [m]
		0.4550264550, -0.2546985000, # element BC point [m]
		0.5343915344, -0.2487549000, # element BC point [m]
		0.7195767196, -0.2348865000, # element BC point [m]
		0.9841269841, -0.2093595000, # element BC point [m]
		1.0000000000, -0.2083816000, # element BC point [m]
	const, 0., # defining Twist 
		BLADE_3_GAUSS_POINTS, # name of blade Gauss points
		c81, VR73,
		#unsteady, bielawa, # unsteadiness on
		jacobian, 0; # jacobian output

# x1=2.964180e+00 xm=3.156585e+00 x2=3.348990e+00 (0.778 -> 0.829 -> 0.879 R)
aerodynamic beam3:
	CURR_ROTOR + CURR_BLADE_3 + 5, # blade aero pan name 
	CURR_ROTOR + CURR_BLADE_3 + 5, # blade beam name 
	induced velocity, CURR_ROTOR,
	reference, CURR_ROTOR + CURR_BLADE_3 + AERO + 9, null, 
		1, 0., 1., 0., 3, 1., 0., 0., # orientation wrt to ref system 
	reference, CURR_ROTOR + CURR_BLADE_3 + AERO + 10, null, 
		1, 0., 1., 0., 3, 1., 0., 0., # orientation wrt to ref system 
	reference, CURR_ROTOR + CURR_BLADE_3 + AERO + 11, null, 
		1, 0., 1., 0., 3, 1., 0., 0., # orientation wrt to ref system 
	piecewise linear, 9, # defining chord shape as linear
		-1.0000000000, 0.4167632000, # element chord [m]
		-0.1683168317, 0.3893820000, # element chord [m]
		0.0000000000, 0.3852529101, # element chord [m]
		0.3267326733, 0.3772376179, # element chord [m]
		0.6237623762, 0.3699509886, # element chord [m]
		0.6633663366, 0.3687508395, # element chord [m]
		0.7227722772, 0.3669506160, # element chord [m]
		0.7623762376, 0.3657504669, # element chord [m]
		1.0000000000, 0.3585495726, # element chord [m]
	const, 0., # defining AC 
	piecewise linear, 9, # defining BC trend as linear
		-1.0000000000, -0.2083816000, # element BC point [m]
		-0.1683168317, -0.1946910000, # element BC point [m]
		0.0000000000, -0.1926264550, # element BC point [m]
		0.3267326733, -0.1886188089, # element BC point [m]
		0.6237623762, -0.1849754943, # element BC point [m]
		0.6633663366, -0.1843754198, # element BC point [m]
		0.7227722772, -0.1834753080, # element BC point [m]
		0.7623762376, -0.1828752335, # element BC point [m]
		1.0000000000, -0.1792747863, # element BC point [m]
	const, 0., # defining Twist 
		BLADE_3_GAUSS_POINTS, # name of blade Gauss points
	c81, multiple, 2, 
		VR73, 0.227723, #0.840 
		VR8mod, 1.0, #0.879 
		#unsteady, bielawa, # unsteadiness on
		jacobian, 0; # jacobian output

# x1=3.348990e+00 xm=3.388995e+00 x2=3.429000e+00 (0.879 -> 0.890 -> 0.900 R)
aerodynamic beam3:
	CURR_ROTOR + CURR_BLADE_3 + 6, # blade aero pan name 
	CURR_ROTOR + CURR_BLADE_3 + 6, # blade beam name 
	induced velocity, CURR_ROTOR,
	reference, CURR_ROTOR + CURR_BLADE_3 + AERO + 11, null, 
		1, 0., 1., 0., 3, 1., 0., 0., # orientation wrt to ref system 
	reference, CURR_ROTOR + CURR_BLADE_3 + AERO + 12, null, 
		1, 0., 1., 0., 3, 1., 0., 0., # orientation wrt to ref system 
	reference, CURR_ROTOR + CURR_BLADE_3 + AERO + 13, null, 
		1, 0., 1., 0., 3, 1., 0., 0., # orientation wrt to ref system 
	piecewise linear, 8, # defining chord shape as linear
		-1.0000000000, 0.3585495726, # element chord [m]
		-0.8095238095, 0.3573494236, # element chord [m]
		-0.6190476190, 0.3561492745, # element chord [m]
		-0.4285714286, 0.3549491255, # element chord [m]
		0.0000000000, 0.3522487901, # element chord [m]
		0.3333333333, 0.3501485293, # element chord [m]
		0.5238095238, 0.3489483802, # element chord [m]
		1.0000000000, 0.3459480076, # element chord [m]
	const, 0., # defining AC 
	piecewise linear, 8, # defining BC trend as linear
		-1.0000000000, -0.1792747863, # element BC point [m]
		-0.8095238095, -0.1786747118, # element BC point [m]
		-0.6190476190, -0.1780746373, # element BC point [m]
		-0.4285714286, -0.1774745627, # element BC point [m]
		0.0000000000, -0.1761243951, # element BC point [m]
		0.3333333333, -0.1750742646, # element BC point [m]
		0.5238095238, -0.1744741901, # element BC point [m]
		1.0000000000, -0.1729740038, # element BC point [m]
	const, 0., # defining Twist 
		BLADE_3_GAUSS_POINTS, # name of blade Gauss points
		c81, VR8mod,
		#unsteady, bielawa, # unsteadiness on
		jacobian, 0; # jacobian output

# x1=3.429000e+00 xm=3.505200e+00 x2=3.581400e+00 (0.900 -> 0.920 -> 0.940 R)
aerodynamic beam3:
	CURR_ROTOR + CURR_BLADE_3 + 7, # blade aero pan name 
	CURR_ROTOR + CURR_BLADE_3 + 7, # blade beam name 
	induced velocity, CURR_ROTOR,
	reference, CURR_ROTOR + CURR_BLADE_3 + AERO + 13, null, 
		1, 0., 1., 0., 3, 1., 0., 0., # orientation wrt to ref system 
	reference, CURR_ROTOR + CURR_BLADE_3 + AERO + 14, null, 
		1, 0., 1., 0., 3, 1., 0., 0., # orientation wrt to ref system 
	reference, CURR_ROTOR + CURR_BLADE_3 + AERO + 15, null, 
		1, 0., 1., 0., 3, 1., 0., 0., # orientation wrt to ref system 
	piecewise linear, 8, # defining chord shape as linear
		-1.0000000000, 0.3459480076, # element chord [m]
		-0.9000000000, 0.3443151505, # element chord [m]
		-0.5000000000, 0.3377837219, # element chord [m]
		-0.4000000000, 0.3361508648, # element chord [m]
		0.0000000000, 0.3296194362, # element chord [m]
		0.2500000000, 0.3255372933, # element chord [m]
		0.7500000000, 0.3173730076, # element chord [m]
		1.0000000000, 0.3085338053, # element chord [m]
	const, 0., # defining AC 
	piecewise linear, 8, # defining BC trend as linear
		-1.0000000000, -0.1729740038, # element BC point [m]
		-0.9000000000, -0.1721575752, # element BC point [m]
		-0.5000000000, -0.1688918610, # element BC point [m]
		-0.4000000000, -0.1680754324, # element BC point [m]
		0.0000000000, -0.1648097181, # element BC point [m]
		0.2500000000, -0.1627686467, # element BC point [m]
		0.7500000000, -0.1586865038, # element BC point [m]
		1.0000000000, -0.1542669027, # element BC point [m]
	const, 0., # defining Twist 
		BLADE_3_GAUSS_POINTS, # name of blade Gauss points
		c81, VR8mod,
		#unsteady, bielawa, # unsteadiness on
		jacobian, 0; # jacobian output

# x1=3.581400e+00 xm=3.695700e+00 x2=3.810000e+00 (0.940 -> 0.970 -> 1.000 R)
aerodynamic beam3:
	CURR_ROTOR + CURR_BLADE_3 + 8, # blade aero pan name 
	CURR_ROTOR + CURR_BLADE_3 + 8, # blade beam name 
	induced velocity, CURR_ROTOR,
	reference, CURR_ROTOR + CURR_BLADE_3 + AERO + 15, null, 
		1, 0., 1., 0., 3, 1., 0., 0., # orientation wrt to ref system 
	reference, CURR_ROTOR + CURR_BLADE_3 + AERO + 16, null, 
		1, 0., 1., 0., 3, 1., 0., 0., # orientation wrt to ref system 
	reference, CURR_ROTOR + CURR_BLADE_3 + AERO + 17, null, 
		1, 0., 1., 0., 3, 1., 0., 0., # orientation wrt to ref system 
	piecewise linear, 8, # defining chord shape as linear
		-1.0000000000, 0.3085338053, # element chord [m]
		-0.6666666667, 0.2908554008, # element chord [m]
		-0.3333333333, 0.2731769962, # element chord [m]
		0.0000000000, 0.2366010013, # element chord [m]
		0.1666666667, 0.2183130038, # element chord [m]
		0.5000000000, 0.1817369962, # element chord [m]
		0.8333333333, 0.1451610000, # element chord [m]
		1.0000000000, 0.1451610000, # element chord [m]
	const, 0., # defining AC 
	piecewise linear, 8, # defining BC trend as linear
		-1.0000000000, -0.1542669027, # element BC point [m]
		-0.6666666667, -0.1454277004, # element BC point [m]
		-0.3333333333, -0.1365884981, # element BC point [m]
		0.0000000000, -0.1183005006, # element BC point [m]
		0.1666666667, -0.1091565019, # element BC point [m]
		0.5000000000, -0.0908684981, # element BC point [m]
		0.8333333333, -0.0725805000, # element BC point [m]
		1.0000000000, -0.0725805000, # element BC point [m]
	const, 0., # defining Twist 
	tip loss, piecewise linear, 2, 
		0.8333333333, 1., 
		1.0000000000, 0., 
		BLADE_3_GAUSS_POINTS, # name of blade Gauss points
	c81, multiple, 2, 
		VR8mod, -0.666667, #0.950 
		VR8, 1.0, #1.000 
		#unsteady, bielawa, # unsteadiness on
		jacobian, 0; # jacobian output

