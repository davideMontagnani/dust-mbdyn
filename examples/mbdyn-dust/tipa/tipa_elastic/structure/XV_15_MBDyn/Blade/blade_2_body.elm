# D:\Clouds\OneDrive - Politecnico di Milano\Dottorato\ATTILA\TiPa_New\Tilt_load_data
# 08-Jul-2020 18:12:53
# Do not modify 

# CURR_ROTOR + CURR_BLADE_2 + 1 : 6.096000e-01 -> 6.176515e-01 m (0.488 -> 0.494 R)
body: CURR_ROTOR + CURR_BLADE_2 + 1, CURR_ROTOR + CURR_BLADE_2 + 1
	,
	5.914099e-01, #7.345358e+01 kg/m; dL=8.051477e-03 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 1,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 1, 
		diag, 0.000000e+00, 3.194909e-06, 3.194909e-06
;

# CURR_ROTOR + CURR_BLADE_2 + 2 : 6.176515e-01 -> 6.396485e-01 m (0.494 -> 0.512 R)
body: CURR_ROTOR + CURR_BLADE_2 + 2, CURR_ROTOR + CURR_BLADE_2 + 2
	,
	condense, 2
	,
	8.078809e-01, #7.345358e+01 kg/m; dL=1.099852e-02 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 1,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 1, 
		diag, 0.000000e+00, 8.143944e-06, 8.143944e-06
	,
	8.078809e-01, #7.345358e+01 kg/m; dL=1.099852e-02 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 2,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 2, 
		diag, 0.000000e+00, 8.143944e-06, 8.143944e-06
;

# CURR_ROTOR + CURR_BLADE_2 + 3 : 6.396485e-01 -> 6.828530e-01 m (0.512 -> 0.546 R)
body: CURR_ROTOR + CURR_BLADE_2 + 3, CURR_ROTOR + CURR_BLADE_2 + 3
	,
	condense, 4
	,
	5.914099e-01, #7.345358e+01 kg/m; dL=8.051477e-03 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 2,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 2, 
		diag, 0.000000e+00, 3.194909e-06, 3.194909e-06
	,
	3.518462e-01, #4.617405e+01 kg/m; dL=7.620000e-03 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 3,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 3, 
		diag, 5.554531e-03, 5.445144e-03, 1.127931e-04
	,
	5.136744e-01, #2.247044e+01 kg/m; dL=2.286000e-02 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 4,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 4, 
		diag, 5.612010e-02, 5.502015e-02, 1.144773e-03
	,
	1.232627e-01, #2.637788e+01 kg/m; dL=4.672955e-03 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 5,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 5, 
		diag, 1.708350e-02, 1.674209e-02, 3.418951e-04
;

# CURR_ROTOR + CURR_BLADE_2 + 4 : 6.828530e-01 -> 7.378456e-01 m (0.546 -> 0.590 R)
body: CURR_ROTOR + CURR_BLADE_2 + 4, CURR_ROTOR + CURR_BLADE_2 + 4
	,
	condense, 3
	,
	7.932990e-02, #2.691845e+01 kg/m; dL=2.947045e-03 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 5,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 5, 
		diag, 1.175332e-02, 1.151834e-02, 2.351244e-04
	,
	5.193309e-01, #2.726146e+01 kg/m; dL=1.905000e-02 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 6,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 6, 
		diag, 6.879463e-02, 6.743491e-02, 1.391608e-03
	,
	9.115820e-01, #2.762741e+01 kg/m; dL=3.299557e-02 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 7,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 7, 
		diag, 7.359271e-02, 7.220547e-02, 1.554597e-03
;

# CURR_ROTOR + CURR_BLADE_2 + 5 : 7.378456e-01 -> 1.016517e+00 m (0.590 -> 0.813 R)
body: CURR_ROTOR + CURR_BLADE_2 + 5, CURR_ROTOR + CURR_BLADE_2 + 5
	,
	condense, 5
	,
	6.770304e-01, #2.802924e+01 kg/m; dL=2.415443e-02 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 7,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 7, 
		diag, 1.724783e-02, 1.693808e-02, 3.779204e-04
	,
	3.727147e-01, #2.795011e+01 kg/m; dL=1.333500e-02 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 8,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 8, 
		diag, 9.945495e-04, 9.818621e-04, 2.544836e-05
	,
	2.606521e+00, #2.581609e+01 kg/m; dL=1.009650e-01 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 9,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 9, 
		diag, 8.731376e-03, 1.079706e-02, 2.389385e-03
	,
	1.857567e+00, #2.437752e+01 kg/m; dL=7.620000e-02 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 10,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 10, 
		diag, 8.910822e-03, 9.709773e-03, 1.078636e-03
	,
	1.589158e+00, #2.482400e+01 kg/m; dL=6.401699e-02 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 11,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 11, 
		diag, 9.233243e-03, 9.762376e-03, 7.308767e-04
;

# CURR_ROTOR + CURR_BLADE_2 + 6 : 1.016517e+00 -> 1.362970e+00 m (0.813 -> 1.090 R)
body: CURR_ROTOR + CURR_BLADE_2 + 6, CURR_ROTOR + CURR_BLADE_2 + 6
	,
	condense, 5
	,
	3.024310e-01, #2.482400e+01 kg/m; dL=1.218301e-02 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 11,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 11, 
		diag, 1.863609e-03, 1.877011e-03, 4.197072e-05
	,
	2.837383e-01, #2.482400e+01 kg/m; dL=1.143000e-02 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 12,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 12, 
		diag, 1.779367e-03, 1.795081e-03, 3.966035e-05
	,
	2.553645e+00, #2.482400e+01 kg/m; dL=1.028700e-01 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 13,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 13, 
		diag, 1.757982e-02, 2.018044e-02, 2.617827e-03
	,
	1.816736e+00, #1.907334e+01 kg/m; dL=9.525000e-02 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 14,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 14, 
		diag, 1.530309e-02, 1.709997e-02, 1.694485e-03
	,
	1.627102e+00, #1.304599e+01 kg/m; dL=1.247205e-01 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 15,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 15, 
		diag, 1.696812e-02, 1.958341e-02, 2.465774e-03
;

# CURR_ROTOR + CURR_BLADE_2 + 7 : 1.362970e+00 -> 1.926233e+00 m (1.090 -> 1.541 R)
body: CURR_ROTOR + CURR_BLADE_2 + 7, CURR_ROTOR + CURR_BLADE_2 + 7
	,
	condense, 5
	,
	8.303594e-01, #1.262337e+01 kg/m; dL=6.577955e-02 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 15,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 15, 
		diag, 8.901620e-03, 9.537594e-03, 4.879446e-04
	,
	1.168348e+00, #1.226612e+01 kg/m; dL=9.525000e-02 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 16,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 16, 
		diag, 1.283140e-02, 1.428730e-02, 1.156877e-03
	,
	1.125576e+00, #1.181707e+01 kg/m; dL=9.525000e-02 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 17,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 17, 
		diag, 1.266636e-02, 1.418725e-02, 1.123157e-03
	,
	2.115285e+00, #1.110386e+01 kg/m; dL=1.905000e-01 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 18,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 18, 
		diag, 2.454942e-02, 3.256783e-02, 6.931116e-03
	,
	1.204157e+00, #1.033765e+01 kg/m; dL=1.164827e-01 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 19,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 19, 
		diag, 1.449639e-02, 1.703429e-02, 1.681374e-03
;

# CURR_ROTOR + CURR_BLADE_2 + 8 : 1.926233e+00 -> 2.659834e+00 m (1.541 -> 2.128 R)
body: CURR_ROTOR + CURR_BLADE_2 + 8, CURR_ROTOR + CURR_BLADE_2 + 8
	,
	condense, 11
	,
	9.269341e-02, #1.002383e+01 kg/m; dL=9.247304e-03 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 19,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 19, 
		diag, 1.134106e-03, 1.234171e-03, 2.583421e-05
	,
	6.430665e-01, #9.928462e+00 kg/m; dL=6.477000e-02 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 20,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 20, 
		diag, 7.930934e-03, 8.784192e-03, 3.994947e-04
	,
	9.286750e-01, #9.749869e+00 kg/m; dL=9.525000e-02 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 21,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 21, 
		diag, 1.164876e-02, 1.301748e-02, 9.534556e-04
	,
	9.100439e-01, #9.554266e+00 kg/m; dL=9.525000e-02 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 22,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 22, 
		diag, 1.172376e-02, 1.294738e-02, 9.382266e-04
	,
	5.021942e-01, #9.414963e+00 kg/m; dL=5.334000e-02 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 23,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 23, 
		diag, 6.638292e-03, 7.076801e-03, 2.610629e-04
	,
	3.908387e-01, #9.325667e+00 kg/m; dL=4.191000e-02 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 24,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 24, 
		diag, 5.252563e-03, 5.570585e-03, 1.697252e-04
	,
	8.774394e-01, #9.211962e+00 kg/m; dL=9.525000e-02 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 25,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 25, 
		diag, 1.193513e-02, 1.322913e-02, 9.198291e-04
	,
	1.204600e+00, #9.033369e+00 kg/m; dL=1.333500e-01 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 26,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 26, 
		diag, 1.657722e-02, 1.933930e-02, 2.143289e-03
	,
	4.934677e-01, #8.634606e+00 kg/m; dL=5.715000e-02 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 27,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 27, 
		diag, 7.061371e-03, 7.630679e-03, 2.872976e-04
	,
	4.597969e-01, #8.045440e+00 kg/m; dL=5.715000e-02 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 28,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 28, 
		diag, 7.041046e-03, 7.586636e-03, 2.774214e-04
	,
	2.373706e-01, #7.673416e+00 kg/m; dL=3.093416e-02 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 29,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 29, 
		diag, 3.693821e-03, 3.943237e-03, 9.901668e-05
;

# CURR_ROTOR + CURR_BLADE_2 + 9 : 2.659834e+00 -> 2.893170e+00 m (2.128 -> 2.315 R)
body: CURR_ROTOR + CURR_BLADE_2 + 9, CURR_ROTOR + CURR_BLADE_2 + 9
	,
	condense, 2
	,
	7.516900e-01, #7.339587e+00 kg/m; dL=1.024158e-01 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 29,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 29, 
		diag, 1.063299e-02, 1.215084e-02, 8.916074e-04
	,
	8.844264e-01, #6.755452e+00 kg/m; dL=1.309204e-01 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 30,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 30, 
		diag, 1.002154e-02, 1.260379e-02, 1.494706e-03
;

# CURR_ROTOR + CURR_BLADE_2 + 10 : 2.893170e+00 -> 3.267670e+00 m (2.315 -> 2.614 R)
body: CURR_ROTOR + CURR_BLADE_2 + 10, CURR_ROTOR + CURR_BLADE_2 + 10
	,
	condense, 6
	,
	3.740738e-01, #6.278554e+00 kg/m; dL=5.957962e-02 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 30,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 30, 
		diag, 3.233926e-03, 4.026043e-03, 1.905610e-04
	,
	6.973201e-02, #6.100788e+00 kg/m; dL=1.143000e-02 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 31,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 31, 
		diag, 5.255364e-04, 6.628376e-04, 1.427098e-05
	,
	9.716694e-01, #6.072174e+00 kg/m; dL=1.600200e-01 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 32,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 32, 
		diag, 6.057675e-03, 1.023663e-02, 2.240009e-03
	,
	1.966474e-01, #6.072174e+00 kg/m; dL=3.238500e-02 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 33,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 33, 
		diag, 9.616827e-04, 1.449002e-03, 4.640751e-05
	,
	3.817272e-01, #6.072174e+00 kg/m; dL=6.286500e-02 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 34,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 34, 
		diag, 1.612833e-03, 2.693453e-03, 1.781186e-04
	,
	2.928007e-01, #6.072174e+00 kg/m; dL=4.822008e-02 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 35,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 35, 
		diag, 1.008471e-03, 1.836668e-03, 9.305960e-05
;

# CURR_ROTOR + CURR_BLADE_2 + 11 : 3.267670e+00 -> 3.321904e+00 m (2.614 -> 2.658 R)
body: CURR_ROTOR + CURR_BLADE_2 + 11, CURR_ROTOR + CURR_BLADE_2 + 11
	,
	condense, 5
	,
	5.422404e-02, #6.072174e+00 kg/m; dL=8.929921e-03 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 35,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 35, 
		diag, 1.647954e-04, 3.118864e-04, 6.718009e-06
	,
	5.112791e-02, #6.709700e+00 kg/m; dL=7.620000e-03 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 36,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 36, 
		diag, 1.261680e-04, 2.663033e-04, 5.677105e-06
	,
	9.490780e-02, #8.303395e+00 kg/m; dL=1.143000e-02 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 37,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 37, 
		diag, 1.451529e-04, 4.096339e-04, 9.372057e-06
	,
	7.541582e-02, #9.897089e+00 kg/m; dL=7.620000e-03 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 38,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 38, 
		diag, 1.042132e-04, 2.789438e-04, 6.050198e-06
	,
	2.253520e-01, #1.209359e+01 kg/m; dL=1.863401e-02 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 39,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 39, 
		diag, 2.696648e-04, 7.085938e-04, 2.084870e-05
;

# CURR_ROTOR + CURR_BLADE_2 + 12 : 3.321904e+00 -> 3.412092e+00 m (2.658 -> 2.730 R)
body: CURR_ROTOR + CURR_BLADE_2 + 12, CURR_ROTOR + CURR_BLADE_2 + 12
	,
	condense, 8
	,
	4.311722e-01, #1.591864e+01 kg/m; dL=2.708599e-02 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 39,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 39, 
		diag, 2.240187e-04, 1.099599e-03, 4.826364e-05
	,
	1.409180e-01, #1.849318e+01 kg/m; dL=7.620000e-03 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 40,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 40, 
		diag, 2.859847e-05, 3.106513e-04, 7.007768e-06
	,
	1.431096e-01, #1.878079e+01 kg/m; dL=7.620000e-03 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 41,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 41, 
		diag, 1.752525e-05, 3.059617e-04, 6.922448e-06
	,
	1.427922e-01, #1.873914e+01 kg/m; dL=7.620000e-03 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 42,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 42, 
		diag, 8.316994e-06, 2.980543e-04, 6.759570e-06
	,
	3.201242e-01, #1.867158e+01 kg/m; dL=1.714500e-02 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 43,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 43, 
		diag, -1.497615e-05, 6.480039e-04, 2.090627e-05
	,
	2.478778e-01, #1.858851e+01 kg/m; dL=1.333500e-02 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 44,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 44, 
		diag, -4.402694e-05, 4.739069e-04, 1.326979e-05
	,
	1.412106e-01, #1.853158e+01 kg/m; dL=7.620000e-03 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 45,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 45, 
		diag, -3.781603e-05, 2.585178e-04, 5.945205e-06
	,
	3.963613e-02, #1.850515e+01 kg/m; dL=2.141898e-03 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 46,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 46, 
		diag, -1.227786e-05, 7.106615e-05, 1.465174e-06
;

# CURR_ROTOR + CURR_BLADE_2 + 13 : 3.412092e+00 -> 3.471655e+00 m (2.730 -> 2.777 R)
body: CURR_ROTOR + CURR_BLADE_2 + 13, CURR_ROTOR + CURR_BLADE_2 + 13
	,
	condense, 4
	,
	3.120085e-01, #1.845320e+01 kg/m; dL=1.690810e-02 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 46,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 46, 
		diag, -1.225874e-04, 5.463808e-04, 1.843212e-05
	,
	1.345266e-01, #1.765441e+01 kg/m; dL=7.620000e-03 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 47,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 47, 
		diag, -5.332034e-05, 2.352996e-04, 5.439683e-06
	,
	4.290607e-01, #1.407680e+01 kg/m; dL=3.048000e-02 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 48,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 48, 
		diag, 2.171193e-05, 9.819105e-04, 5.257862e-05
	,
	4.176560e-02, #9.170171e+00 kg/m; dL=4.554506e-03 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 49,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 49, 
		diag, 5.452743e-05, 1.449216e-04, 3.028308e-06
;

# CURR_ROTOR + CURR_BLADE_2 + 14 : 3.471655e+00 -> 3.549194e+00 m (2.777 -> 2.839 R)
body: CURR_ROTOR + CURR_BLADE_2 + 14, CURR_ROTOR + CURR_BLADE_2 + 14
	,
	condense, 4
	,
	1.743457e-02, #5.687361e+00 kg/m; dL=3.065494e-03 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 49,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 49, 
		diag, 6.311497e-05, 9.964534e-05, 2.046953e-06
	,
	1.306446e-01, #4.286241e+00 kg/m; dL=3.048000e-02 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 50,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 50, 
		diag, 7.205915e-04, 1.000304e-03, 3.032235e-05
	,
	8.165289e-02, #4.286241e+00 kg/m; dL=1.905000e-02 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 51,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 51, 
		diag, 4.375590e-04, 6.122028e-04, 1.491288e-05
	,
	1.069164e-01, #4.286241e+00 kg/m; dL=2.494409e-02 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 52,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 52, 
		diag, 5.552068e-04, 7.904301e-04, 2.156177e-05
;

# CURR_ROTOR + CURR_BLADE_2 + 15 : 3.549194e+00 -> 3.640707e+00 m (2.839 -> 2.913 R)
body: CURR_ROTOR + CURR_BLADE_2 + 15, CURR_ROTOR + CURR_BLADE_2 + 15
	,
	condense, 4
	,
	5.638940e-02, #4.286241e+00 kg/m; dL=1.315591e-02 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 52,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 52, 
		diag, 2.837365e-04, 4.076061e-04, 9.115208e-06
	,
	8.165289e-02, #4.286241e+00 kg/m; dL=1.905000e-02 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 53,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 53, 
		diag, 4.021529e-04, 5.825306e-04, 1.430732e-05
	,
	1.633058e-01, #4.286241e+00 kg/m; dL=3.810000e-02 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 54,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 54, 
		diag, 7.659298e-04, 1.148321e-03, 4.278665e-05
	,
	8.773765e-02, #4.137127e+00 kg/m; dL=2.120739e-02 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 55,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 55, 
		diag, 3.836782e-04, 5.882722e-04, 1.522680e-05
;

# CURR_ROTOR + CURR_BLADE_2 + 16 : 3.640707e+00 -> 3.761691e+00 m (2.913 -> 3.009 R)
body: CURR_ROTOR + CURR_BLADE_2 + 16, CURR_ROTOR + CURR_BLADE_2 + 16
	,
	condense, 7
	,
	6.536152e-02, #3.869237e+00 kg/m; dL=1.689261e-02 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 55,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 55, 
		diag, 2.685165e-04, 4.222315e-04, 1.013955e-05
	,
	1.665539e-15, #3.750461e+00 kg/m; dL=4.440892e-16 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 56,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 56, 
		diag, 6.626585e-18, 1.053129e-17, 2.149243e-19
	,
	1.326859e-01, #3.482571e+00 kg/m; dL=3.810000e-02 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 57,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 57, 
		diag, 4.791055e-04, 8.107803e-04, 3.226966e-05
	,
	1.427605e-15, #3.214680e+00 kg/m; dL=4.440892e-16 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 58,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 58, 
		diag, 4.542212e-18, 7.995263e-18, 1.631686e-19
	,
	5.868573e-02, #3.080616e+00 kg/m; dL=1.905000e-02 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 59,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 59, 
		diag, 1.823438e-04, 3.273067e-04, 8.418275e-06
	,
	1.037627e-01, #2.723429e+00 kg/m; dL=3.810000e-02 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 60,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 60, 
		diag, 1.689743e-04, 4.452104e-04, 2.138167e-05
	,
	2.161702e-02, #2.445050e+00 kg/m; dL=8.841136e-03 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 61,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 61, 
		diag, -2.608832e-06, 5.464292e-05, 1.253097e-06
;

# CURR_ROTOR + CURR_BLADE_2 + 17 : 3.761691e+00 -> 3.810000e+00 m (3.009 -> 3.048 R)
body: CURR_ROTOR + CURR_BLADE_2 + 17, CURR_ROTOR + CURR_BLADE_2 + 17
	,
	condense, 2
	,
	6.457214e-02, #2.206926e+00 kg/m; dL=2.925886e-02 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 61,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 61, 
		diag, -4.010094e-05, 1.377836e-04, 7.324480e-06
	,
	3.629017e-02, #1.904996e+00 kg/m; dL=1.905000e-02 m; 
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 62,      0.000000000, 0., 0.,
	reference, CURR_ROTOR + CURR_BLADE_2 + BODY + 62, 
		diag, -5.208667e-05, 4.884770e-05, 2.071977e-06
;

