# D:\Clouds\OneDrive - Politecnico di Milano\Dottorato\ATTILA\TiPa_New\Tilt_load_data
# 08-Jul-2020 18:12:53
# DO NOT MODIFY 


# Defining the beam/sections notation
# x1=0.000000e+00 xm=1.905000e-01 x2=3.810000e-01 (0.000 -> 0.313 -> 0.625 R
# Beam CURR_ROTOR + CURR_YOKE_2 + 1, 
#                                       A  beam y 
#                                      /   adimensional axis
#                                     /
#                                    /
#           |            |          |----------|-------------|---------> beam x adimensional axis
#          -1       -1/sqrt(3)      0       1/sqrt(3)        1 
#               ________________________________________________
#             /                                                /
#            /        section I             section II        /
#           /_____________|_____________________|____________/ 
#           |           | | |       |         | | |          |  
#           |           | V |       |         | V |          |  
#           |           |   |       |         |   |          |  
#           |  _________|   |_______|_________|   |__________|__ 
#           | /         |  /        | /       |  /           | /
#           |/          | /         |/        | /            |/
#           /___________|/__________/_________|/_____________/
#          node 1                  node 2              node 3
#      0
#      |---\....\----------------------------------------------------------> global X axis 
#
beam3: CURR_ROTOR + CURR_YOKE_2 + 1, # beam name
	CURR_ROTOR + CURR_YOKE_2 + 1 , # node 1 name
		position, reference, CURR_ROTOR + CURR_YOKE_2 + NEUTR + 1, 0., 0.0000000000000000, 0.0000000000000000, 
		orientation, reference, CURR_ROTOR + CURR_YOKE_2 + NEUTR + 1, eye, # orientation for properties
	CURR_ROTOR + CURR_YOKE_2 + 2 , # node 2 name
		position, reference, CURR_ROTOR + CURR_YOKE_2 + NEUTR + 2, 0., 0.0000000000000000, 0.0000000000000000, 
		orientation, reference, CURR_ROTOR + CURR_YOKE_2 + NEUTR + 2, eye, # orientation for properties
	CURR_ROTOR + CURR_YOKE_2 + 3 , # node 3 name
		position, reference, CURR_ROTOR + CURR_YOKE_2 + NEUTR + 3, 0., 0.0000000000000000, 0.0000000000000000, 
		orientation, reference, CURR_ROTOR + CURR_YOKE_2 + NEUTR + 3, eye, # orientation for properties
		from nodes, 
		# EA = 2.88610211e+12 [N], GA_y = 1.08500079e+12 [N], GA_z = 1.08500079e+12 [N], GJ = 1.06201042e+06 [N*M^2], EJ_Y = 5.74204950e+05 [N*M^2], EJ_Z = 5.74204944e+05 [N*M^2]
	linear elastic generic, matr, #sec I const law
			2.88610211e+12, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, -0.00000000e+00,
			0.00000000e+00, 1.08500079e+12, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 1.08500079e+12, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 1.06201042e+06, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 5.74204950e+05, 0.00000000e+00,
			-0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 5.74204944e+05,
	from nodes, # automatically setting section II orientation
		# EA = 1.23613809e+12 [N], GA_y = 4.64713568e+11 [N], GA_z = 4.64713568e+11 [N], GJ = 1.06201042e+06 [N*M^2], EJ_Y = 6.60190983e+05 [N*M^2], EJ_Z = 6.60190983e+05 [N*M^2]
	linear elastic generic, matr, #sec II const law
			1.23613809e+12, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, -0.00000000e+00,
			0.00000000e+00, 4.64713568e+11, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 4.64713568e+11, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 1.06201042e+06, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 6.60190983e+05, -0.00000000e+00,
			-0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, -0.00000000e+00, 6.60190983e+05;

# Defining the beam/sections notation
# x1=3.810000e-01 xm=4.229100e-01 x2=4.648200e-01 (0.625 -> 0.694 -> 0.762 R
# Beam CURR_ROTOR + CURR_YOKE_2 + 2, 
#                                       A  beam y 
#                                      /   adimensional axis
#                                     /
#                                    /
#           |            |          |----------|-------------|---------> beam x adimensional axis
#          -1       -1/sqrt(3)      0       1/sqrt(3)        1 
#               ________________________________________________
#             /                                                /
#            /        section I             section II        /
#           /_____________|_____________________|____________/ 
#           |           | | |       |         | | |          |  
#           |           | V |       |         | V |          |  
#           |           |   |       |         |   |          |  
#           |  _________|   |_______|_________|   |__________|__ 
#           | /         |  /        | /       |  /           | /
#           |/          | /         |/        | /            |/
#           /___________|/__________/_________|/_____________/
#          node 1                  node 2              node 3
#      0
#      |---\....\----------------------------------------------------------> global X axis 
#
beam3: CURR_ROTOR + CURR_YOKE_2 + 2, # beam name
	CURR_ROTOR + CURR_YOKE_2 + 3 , # node 1 name
		position, reference, CURR_ROTOR + CURR_YOKE_2 + NEUTR + 3, 0., 0.0000000000000000, 0.0000000000000000, 
		orientation, reference, CURR_ROTOR + CURR_YOKE_2 + NEUTR + 3, eye, # orientation for properties
	CURR_ROTOR + CURR_YOKE_2 + 4 , # node 2 name
		position, reference, CURR_ROTOR + CURR_YOKE_2 + NEUTR + 4, 0., 0.0000000000000000, 0.0000000000000000, 
		orientation, reference, CURR_ROTOR + CURR_YOKE_2 + NEUTR + 4, eye, # orientation for properties
	CURR_ROTOR + CURR_YOKE_2 + 5 , # node 3 name
		position, reference, CURR_ROTOR + CURR_YOKE_2 + NEUTR + 5, 0., 0.0000000000000000, 0.0000000000000000, 
		orientation, reference, CURR_ROTOR + CURR_YOKE_2 + NEUTR + 5, eye, # orientation for properties
		from nodes, 
		# EA = 1.42124732e+12 [N], GA_y = 5.34303505e+11 [N], GA_z = 5.34303505e+11 [N], GJ = 1.06201042e+06 [N*M^2], EJ_Y = 1.01901739e+06 [N*M^2], EJ_Z = 1.01901739e+06 [N*M^2]
	linear elastic generic, matr, #sec I const law
			1.42124732e+12, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, -0.00000000e+00,
			0.00000000e+00, 5.34303505e+11, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 5.34303505e+11, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 1.06201042e+06, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 1.01901739e+06, 0.00000000e+00,
			-0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 1.01901739e+06,
	from nodes, # automatically setting section II orientation
		# EA = 1.42124732e+12 [N], GA_y = 5.34303505e+11 [N], GA_z = 5.34303505e+11 [N], GJ = 1.06201042e+06 [N*M^2], EJ_Y = 1.37784380e+06 [N*M^2], EJ_Z = 1.37784380e+06 [N*M^2]
	linear elastic generic, matr, #sec II const law
			1.42124732e+12, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, -0.00000000e+00,
			0.00000000e+00, 5.34303505e+11, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 5.34303505e+11, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 1.06201042e+06, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 1.37784380e+06, -0.00000000e+00,
			-0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, -0.00000000e+00, 1.37784380e+06;

# Defining the beam/sections notation
# x1=4.648200e-01 xm=5.372100e-01 x2=6.096000e-01 (0.762 -> 0.881 -> 1.000 R
# Beam CURR_ROTOR + CURR_YOKE_2 + 3, 
#                                       A  beam y 
#                                      /   adimensional axis
#                                     /
#                                    /
#           |            |          |----------|-------------|---------> beam x adimensional axis
#          -1       -1/sqrt(3)      0       1/sqrt(3)        1 
#               ________________________________________________
#             /                                                /
#            /        section I             section II        /
#           /_____________|_____________________|____________/ 
#           |           | | |       |         | | |          |  
#           |           | V |       |         | V |          |  
#           |           |   |       |         |   |          |  
#           |  _________|   |_______|_________|   |__________|__ 
#           | /         |  /        | /       |  /           | /
#           |/          | /         |/        | /            |/
#           /___________|/__________/_________|/_____________/
#          node 1                  node 2              node 3
#      0
#      |---\....\----------------------------------------------------------> global X axis 
#
beam3: CURR_ROTOR + CURR_YOKE_2 + 3, # beam name
	CURR_ROTOR + CURR_YOKE_2 + 5 , # node 1 name
		position, reference, CURR_ROTOR + CURR_YOKE_2 + NEUTR + 5, 0., 0.0000000000000000, 0.0000000000000000, 
		orientation, reference, CURR_ROTOR + CURR_YOKE_2 + NEUTR + 5, eye, # orientation for properties
	CURR_ROTOR + CURR_YOKE_2 + 6 , # node 2 name
		position, reference, CURR_ROTOR + CURR_YOKE_2 + NEUTR + 6, 0., 0.0000000000000000, 0.0000000000000000, 
		orientation, reference, CURR_ROTOR + CURR_YOKE_2 + NEUTR + 6, eye, # orientation for properties
	CURR_ROTOR + CURR_YOKE_2 + 7 , # node 3 name
		position, reference, CURR_ROTOR + CURR_YOKE_2 + NEUTR + 7, 0., 0.0000000000000000, 0.0000000000000000, 
		orientation, reference, CURR_ROTOR + CURR_YOKE_2 + NEUTR + 7, eye, # orientation for properties
		from nodes, 
		# EA = 2.96619745e+12 [N], GA_y = 1.11511182e+12 [N], GA_z = 1.11511182e+12 [N], GJ = 1.06201042e+06 [N*M^2], EJ_Y = 4.50724001e+05 [N*M^2], EJ_Z = 1.37784380e+06 [N*M^2]
	linear elastic generic, matr, #sec I const law
			2.96619745e+12, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, -0.00000000e+00,
			0.00000000e+00, 1.11511182e+12, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 1.11511182e+12, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 1.06201042e+06, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 4.50724001e+05, 0.00000000e+00,
			-0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 1.37784380e+06,
	from nodes, # automatically setting section II orientation
		# EA = 1.96825400e+12 [N], GA_y = 7.39945114e+11 [N], GA_z = 7.39945114e+11 [N], GJ = 1.06201042e+06 [N*M^2], EJ_Y = 4.50724001e+05 [N*M^2], EJ_Z = 1.37784380e+06 [N*M^2]
	linear elastic generic, matr, #sec II const law
			1.96825400e+12, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, -0.00000000e+00,
			0.00000000e+00, 7.39945114e+11, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 7.39945114e+11, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 1.06201042e+06, 0.00000000e+00, 0.00000000e+00,
			0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 4.50724001e+05, -0.00000000e+00,
			-0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, -0.00000000e+00, 1.37784380e+06;

