function [node, joint] = parse_eqn(eqn_file)

    node = struct('lab', '0', 'neq', 0, 'veq', [0 0]);
    nn = 0; % number of node
    joint = struct('lab', '0', 'neq', 0, 'veq', [0 0]);
    nj = 0; % number of joint

    fileID = fopen(eqn_file, 'rt');

    skip_line = false;

    while skip_line ||~feof(fileID)

        if ~skip_line
            eqn_line = fgets(fileID);
        else
            skip_line = false;
        end

        if contains(eqn_line, 'Regular steps dof stats')
            % read continuation
            while skip_line ||~feof(fileID)

                if ~skip_line
                    tline = fgets(fileID);
                else
                    skip_line = false;
                end

                if contains(tline, 'Structural')
                    tl = textscan(tline, 'Structural(%d):%d %d->%d');
                    nn = nn + 1;
                    node(nn).lab = num2str(tl{1});
                    node(nn).neq = tl{2};
                    node(nn).veq = [tl{3} tl{4}];
                end

                if contains(tline, 'Joint')
                    tl = textscan(tline, 'Joint(%d):%d %d->%d');
                    nj = nj + 1;
                    joint(nj).lab = num2str(tl{1});
                    joint(nj).neq = tl{2};
                    joint(nj).veq = [tl{3} tl{4}];
                end

            end

        end

    end

end
