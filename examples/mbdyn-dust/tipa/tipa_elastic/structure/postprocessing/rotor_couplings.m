clear; close all; clc;

%% Rotor couplings evaluation
% input
wind_speed = [30.5062 41.4642 50.467 62.1963 67.0835 73.1539 78.5556 85.3463 90.2335];
rotor_speed = 0.84 * 89.55486839943 * ones(1, numel(wind_speed));
pitch_at_the_root = [24.3 31.7 37.1 40.1 42.9 44.8 47.0 48.8 50.9 52.3];
path = fullfile('ATTILA_MBDyn', 'Rotor', 'Rotor_Couplings');
node_blade_1 = 10001:10023;
node_yoke_1 = 100001:100011;
node_075 = 10019;
precone = deg2rad(2.75);
nmodes = [2 4 5]; % id modes to take: gimbal, cone, cyclic lead-lag
node_hub = 500; % needed to get the azimut
scale = 20;
hub = [0 0 0];
inboardbearing = [0 0.05569 * 1.250 0];
blade_tip = [0 1.250 * sin(precone) - 0.00289 0];

for i = 1:1
    file_name{i} = sprintf('rotor_coupling_%dms_%ddeg', floor(wind_speed(i)), floor(pitch_at_the_root(i)));
    file_rot{i} = fullfile(path, [file_name{i} '.rot']);
    file_nc{i} = fullfile(path, [file_name{i} '.nc']);
    file_log{i} = fullfile(path, [file_name{i}, '.log']);

    alpha = ncread(file_nc{i}, 'eig.0.alpha');
    dCoef = ncread(file_nc{i}, 'eig.dCoef');
    lambda_freq = zeros(size(alpha, 2), 1);
    lambda_damp = zeros(size(alpha, 2), 1);
    [nodes_0, beam3, aero3, bodies, angularvelocity, deformablehinge, deformabledisplacement, rod, clamp, joints, label] = parse_log4Tipa(file_log{i}, false);

    for j = 1:size(alpha, 2)
        alpha_r = alpha(1, j);
        alpha_i = alpha(2, j);
        beta = alpha(3, j);
        det = (alpha_r + beta)^2 + alpha_i^2;
        lambda_real = 1 / dCoef * (alpha_r^2 + alpha_i^2 - beta^2) / det;
        lambda_imag = 1 / dCoef * (2 * alpha_i * beta) / det;

        if alpha_i * beta < 0
            lambda_imag = -lambda_imag;
        end

        if lambda_imag <= 2 * 1e-16
            lambda_freq(j) = 0;
            lambda_damp(j) = 100;
        else
            lambda_freq(j) = lambda_imag / (2 * pi);
            lambda_damp(j) = round(((lambda_real / lambda_imag)^2 / (1 + (lambda_real / lambda_imag)^2))^(0.5) * 100);
        end

    end

    % sort lambda_freq
    [lambda_freq, idx] = sort(lambda_freq);
    % idx are the index that comes out from sorting
    lambda_damp = lambda_damp(idx);

    % get eigenvectors
    VR = ncread(file_nc{i}, 'eig.0.VR');
    VR = VR(:, idx, :); % reorder the eigenvalues

    % Discard null eigenvalues
    idx_not_null = find(lambda_freq > 1e-2);
    lambda_freq = lambda_freq(idx_not_null);
    eigvec_real = VR(:, idx_not_null, 1);
    eigvec_im = VR(:, idx_not_null, 2);
    eigvec_abs = sqrt(eigvec_real.^2 + eigvec_im.^2);

    % normalize at unit norm
    for ii = 1:size(eigvec_abs, 2)
        eigvec_abs(:, ii) = eigvec_abs(:, ii) / sqrt(eigvec_abs(:, ii)' * eigvec_abs(:, ii));
    end

    eigvec_phase = atan2(eigvec_im, eigvec_real);

    for ii = 1:size(eigvec_phase, 2)
        eigvec_phase(:, ii) = eigvec_phase(:, ii) / sqrt(eigvec_phase(:, ii)' * eigvec_phase(:, ii));
    end

    % reconstruct reference shape
    [X0, R, nodes] = eig_geom(file_nc{i}, 0, 'R');
    idx = ncread(file_nc{i}, 'eig.idx') + 1;

    % get index of blade 1
    for k = 1:numel(node_blade_1)
        id_blade_1(k) = find(nodes == node_blade_1(k));

    end

    for k = 1:numel(node_yoke_1)
        id_yoke_1(k) = find(nodes == node_yoke_1(k));
    end

    id_blade_075 = find(nodes == node_075);
    X0_blade_075_1 = X0(:, id_blade_075);
    X0_blade_075_0 = X0(:, id_blade_075 - 1);
    n = [0 0 1]';
    vec_orthogonal = R(:, :, id_blade_075) * n;
    vec = (X0_blade_075_1 - X0_blade_075_0) / norm(X0_blade_075_1 - X0_blade_075_0);
    vec_pitch = X0_blade_075_1 + 0.01 * cross(vec_orthogonal, vec);

    id_hub = find(nodes == node_hub);
    R_hub = R(:, :, id_hub);
    X0_blade_1 = X0(:, id_blade_1);
    X0_yoke_1 = X0(:, id_yoke_1);
    X0_blade_1 = X0_blade_1';
    X0_yoke_1 = X0_yoke_1';

    % take only the eigenvectors that matters
    % get indexes

    % blade position
    id_blade_1_vec_px = double(idx(id_blade_1));
    id_blade_1_vec_py = double(idx(id_blade_1)) + 1;
    id_blade_1_vec_pz = double(idx(id_blade_1)) + 2;
    % blade orientation
    id_blade_1_vec_rx = double(idx(id_blade_1)) + 3;
    id_blade_1_vec_ry = double(idx(id_blade_1)) + 4;
    id_blade_1_vec_rz = double(idx(id_blade_1)) + 5;
    % yoke orientation
    id_yoke_1_vec_px = double(idx(id_yoke_1));
    id_yoke_1_vec_py = double(idx(id_yoke_1)) + 1;
    id_yoke_1_vec_pz = double(idx(id_yoke_1)) + 2;
    % yoke orientation
    id_yoke_1_vec_rx = double(idx(id_yoke_1)) + 3;
    id_yoke_1_vec_ry = double(idx(id_yoke_1)) + 4;
    id_yoke_1_vec_rz = double(idx(id_yoke_1)) + 5;

    % take out the neutral axis offset

    for j = 1:numel(id_blade_1)
        coord_blade_1(j, :) = nodes_0(id_blade_1(j)).X;
    end
    Rot = eye(3);
    %Rot = [0 -1 0; 1 0 0; 0 0 1];
    coord_blade_1 = Rot' * coord_blade_1';

    for j = 1:numel(id_yoke_1)
        coord_yoke_1(j, :) = nodes_0(id_yoke_1(j)).X;
    end

    coord_yoke_1 = Rot' * coord_yoke_1';

    eigvec_abs_blade_1 = zeros(numel(node_blade_1), 6, numel(nmodes));
    eigvec_phase_blade_1 = zeros(numel(node_blade_1), 6, numel(nmodes));

    eigvec_abs_yoke_1 = zeros(numel(node_yoke_1), 6, numel(nmodes));
    eigvec_phase_yoke_1 = zeros(numel(node_yoke_1), 6, numel(nmodes));

    for ii = 1:numel(nmodes)

        eigvec_abs_blade_1(:, 1, ii) = eigvec_abs(id_blade_1_vec_px, nmodes(ii));
        eigvec_abs_blade_1(:, 2, ii) = eigvec_abs(id_blade_1_vec_py, nmodes(ii));
        eigvec_abs_blade_1(:, 3, ii) = eigvec_abs(id_blade_1_vec_pz, nmodes(ii));
        eigvec_abs_blade_1(:, 4, ii) = eigvec_abs(id_blade_1_vec_rx, nmodes(ii));
        eigvec_abs_blade_1(:, 5, ii) = eigvec_abs(id_blade_1_vec_ry, nmodes(ii));
        eigvec_abs_blade_1(:, 6, ii) = eigvec_abs(id_blade_1_vec_rz, nmodes(ii));

        eigvec_phase_blade_1(:, 1, ii) = eigvec_phase(id_blade_1_vec_px, nmodes(ii));
        eigvec_phase_blade_1(:, 2, ii) = eigvec_phase(id_blade_1_vec_py, nmodes(ii));
        eigvec_phase_blade_1(:, 3, ii) = eigvec_phase(id_blade_1_vec_pz, nmodes(ii));
        eigvec_phase_blade_1(:, 4, ii) = eigvec_phase(id_blade_1_vec_rx, nmodes(ii));
        eigvec_phase_blade_1(:, 5, ii) = eigvec_phase(id_blade_1_vec_ry, nmodes(ii));
        eigvec_phase_blade_1(:, 6, ii) = eigvec_phase(id_blade_1_vec_rz, nmodes(ii));

        eigvec_abs_yoke_1(:, 1, ii) = eigvec_abs(id_yoke_1_vec_px, nmodes(ii));
        eigvec_abs_yoke_1(:, 2, ii) = eigvec_abs(id_yoke_1_vec_py, nmodes(ii));
        eigvec_abs_yoke_1(:, 3, ii) = eigvec_abs(id_yoke_1_vec_pz, nmodes(ii));
        eigvec_abs_yoke_1(:, 4, ii) = eigvec_abs(id_yoke_1_vec_rx, nmodes(ii));
        eigvec_abs_yoke_1(:, 5, ii) = eigvec_abs(id_yoke_1_vec_ry, nmodes(ii));
        eigvec_abs_yoke_1(:, 6, ii) = eigvec_abs(id_yoke_1_vec_rz, nmodes(ii));

        eigvec_phase_yoke_1(:, 1, ii) = eigvec_phase(id_yoke_1_vec_px, nmodes(ii));
        eigvec_phase_yoke_1(:, 2, ii) = eigvec_phase(id_yoke_1_vec_py, nmodes(ii));
        eigvec_phase_yoke_1(:, 3, ii) = eigvec_phase(id_yoke_1_vec_pz, nmodes(ii));
        eigvec_phase_yoke_1(:, 4, ii) = eigvec_phase(id_yoke_1_vec_rx, nmodes(ii));
        eigvec_phase_yoke_1(:, 5, ii) = eigvec_phase(id_yoke_1_vec_ry, nmodes(ii));
        eigvec_phase_yoke_1(:, 6, ii) = eigvec_phase(id_yoke_1_vec_rz, nmodes(ii));

        figure
        id_75 = find(id_blade_1 == id_blade_075);
        % plot reference geometry (feathering axis)
        ref_x_blade = X0_blade_1(:, 1);
        ref_y_blade = X0_blade_1(:, 2);
        ref_z_blade = X0_blade_1(:, 3);
        ref_blade = [ref_x_blade'; ref_y_blade'; ref_z_blade'];
        ref_rot_blade = R_hub * ref_blade;
        vec_rot_pitch = R_hub * vec_pitch;
        node_rot_075 = R_hub * X0_blade_075_1;
        node_rot_075([1 3]) = node_rot_075([1 3]) - coord_blade_1([1 3], id_75);        
        
        % plot deformed geometry (feathering axis)

        mode_x_blade = X0_blade_1(:, 1) + scale * eigvec_abs_blade_1(:, 1, ii) .* cos(eigvec_phase_blade_1(:, 1, ii));
        mode_y_blade = X0_blade_1(:, 2) + scale * eigvec_abs_blade_1(:, 2, ii) .* cos(eigvec_phase_blade_1(:, 2, ii));
        mode_z_blade = X0_blade_1(:, 3) + scale * eigvec_abs_blade_1(:, 3, ii) .* cos(eigvec_phase_blade_1(:, 3, ii));
        mode_blade = [mode_x_blade'; mode_y_blade'; mode_z_blade'];
        mode_rot_blade = R_hub * mode_blade;

        rot_axis = rotm2axang(R(:, :, id_blade_075));
        omega_075 = rot_axis(1:3);
        
        mode_rx_blade = omega_075(1) + scale * eigvec_abs_blade_1(id_75, 4, ii) .* cos(eigvec_phase_blade_1(id_75, 4, ii));
        mode_ry_blade = omega_075(2) + scale * eigvec_abs_blade_1(id_75, 5, ii) .* cos(eigvec_phase_blade_1(id_75, 5, ii));
        mode_rz_blade = omega_075(3) + scale * eigvec_abs_blade_1(id_75, 6, ii) .* cos(eigvec_phase_blade_1(id_75, 6, ii));
        R_blade_075 = axang2rotm([mode_rx_blade mode_ry_blade mode_rz_blade rot_axis(4)]);
        % get point
        n = [0 0 1]';
        vec_orthogonal = R_blade_075 * n;
        vec = (mode_blade(:, id_75) - mode_blade(:, id_75 - 1)) / norm(mode_blade(:, id_75) - mode_blade(:, id_75 - 1));
        vec_pitch_def = mode_blade(:, id_75) + 0.1 * cross(vec_orthogonal, vec);
        vec_rot_pitch_def = R_hub * vec_pitch_def;
        
        mode_rot_blade([1 3], :) = mode_rot_blade([1 3], :) - coord_blade_1([1 3], :);
        vec_rot_pitch_def([1 3]) = vec_rot_pitch_def([1 3]) - coord_blade_1([1 3], id_75);
        vec_reference  = [-0.1 0 0]' + mode_rot_blade(:,id_75);
        
        ref_x_yoke = X0_yoke_1(:, 1);
        ref_y_yoke = X0_yoke_1(:, 2);
        ref_z_yoke = X0_yoke_1(:, 3);
        ref_yoke = [ref_x_yoke'; ref_y_yoke'; ref_z_yoke'];
        ref_rot_yoke = R_hub * ref_yoke;
        ref_rot_yoke([1 3], :) = ref_rot_yoke([1 3], :) - coord_yoke_1([1 3], :);

        mode_x_yoke = X0_yoke_1(:, 1) + scale * eigvec_abs_yoke_1(:, 1, ii) .* cos(eigvec_phase_yoke_1(:, 1, ii));
        mode_y_yoke = X0_yoke_1(:, 2) + scale * eigvec_abs_yoke_1(:, 2, ii) .* cos(eigvec_phase_yoke_1(:, 2, ii));
        mode_z_yoke = X0_yoke_1(:, 3) + scale * eigvec_abs_yoke_1(:, 3, ii) .* cos(eigvec_phase_yoke_1(:, 3, ii));
        mode_yoke = [mode_x_yoke'; mode_y_yoke'; mode_z_yoke'];
        mode_rot_yoke = R_hub * mode_yoke;
        mode_rot_yoke([1 3], :) = mode_rot_yoke([1 3], :) - coord_yoke_1([1 3], :);

        plot3(ref_rot_blade(1, :), ref_rot_blade(2, :), ref_rot_blade(3, :), '--')
        hold on
        plot3([ref_rot_blade(1, id_75) vec_rot_pitch(1)], [ref_rot_blade(2, id_75) vec_rot_pitch(2)], [ref_rot_blade(3, id_75) vec_rot_pitch(3)])
        plot3(ref_rot_yoke(1, :), ref_rot_yoke(2, :), ref_rot_yoke(3, :), '--')
        % plot deformation
        plot3(mode_rot_blade(1, :), mode_rot_blade(2, :), mode_rot_blade(3, :), 'LineWidth', 2)
        plot3([mode_rot_blade(1, id_75) vec_rot_pitch_def(1)], [mode_rot_blade(2, id_75) vec_rot_pitch_def(2)], [mode_rot_blade(3, id_75) vec_rot_pitch_def(3)])
        plot3([mode_rot_blade(1, id_75) vec_reference(1)], [mode_rot_blade(2, id_75) vec_reference(2)], [mode_rot_blade(3, id_75) vec_reference(3)])
        plot3(mode_rot_yoke(1, :), mode_rot_yoke(2, :), mode_rot_yoke(3, :), 'LineWidth', 2)
        grid on
        xlabel('x')
        ylabel('y')
        zlabel('z')
        axis equal
        
        % Get \theta.75 angles  
        a = vec_rot_pitch_def([1 3]) - mode_rot_blade([1 3],id_75);
        b = vec_reference([1 3]) - mode_rot_blade([1 3],id_75);
        
        theta_effective(ii,i) = rad2deg(acos(dot(a,b)/(norm(a)*norm(b)))); 
        
        % get rotation angles 
        if ii == 1 % gimbal mode
            beta_gimbal(i) = rad2deg(atan2(mode_rot_blade(3,id_75),mode_rot_blade(2,id_75)));
        elseif ii == 2 % cone mode
            beta_cone(i) = rad2deg(atan2((mode_rot_blade(3,id_75+1) - mode_rot_blade(3,id_75-1)),(mode_rot_blade(2,id_75+1)- mode_rot_blade(2,id_75-1))));
        elseif ii == 3 % lead-lag mode
            
        end
        
    end

end
