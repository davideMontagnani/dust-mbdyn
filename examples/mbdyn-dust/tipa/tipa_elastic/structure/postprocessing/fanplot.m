clear all; close all; clc; 
%% --- Rotor fun plot --- 
%% Collective modes 
% Leonardo data
rpm = [70 80 90 100 110 120];

LHD.coll = [12.4 13.8 15.2 16.6 18.0 19.4;
            20.5 21.2 21.8 22.4 22.9 23.4;
            35.8 38.1 40.6 43.2 45.9 48.7;
            51.6 51.7 51.8 51.9 52.0 52.1];
        
MBDYN.coll = [12.8 14.1 15.52 16.8 18.2 19.6;
            24.8 26.2 27.7 29.0 30.3 31.6;
            37.6 39.3 41.3 43.4 45.6 47.8;
            49.3 49.6 49.94 50.3 50.8 51.5]; 
% Adimensional frequencies
rpm_rev = [0 120]; 
rev_1 = [0 17.10374511]; 
rev_2 = [0 34.20749023];
rev_3 = [0 51.31123534]; 
rev_4 = [0 68.41498046]; 
rev_5 = [0 85.51872557]; 
figure('Renderer', 'painters', 'Position', [0 0   800 500])  
R1  = plot(rpm_rev, rev_1, '--', 'LineWidth',1.5,'Color',[0.5 0.5 0.5]);
text(rpm_rev(end)+1, rev_1(end), '1/rev'); 
hold on 
R2 = plot(rpm_rev, rev_2,'--', 'LineWidth',1.5,'Color',[0.5 0.5 0.5]);
text(rpm_rev(end)+1, rev_2(end), '2/rev');
R3 = plot(rpm_rev, rev_3,'--', 'LineWidth',1.5,'Color',[0.5 0.5 0.5]);
text(rpm_rev(end)+1, rev_3(end), '3/rev');
R4  = plot(rpm_rev, rev_4,'--', 'LineWidth',1.5,'Color',[0.5 0.5 0.5]);
text(105, 58, '4/rev');
R5 = plot(rpm_rev, rev_5,'--', 'LineWidth',1.5,'Color',[0.5 0.5 0.5]);
text(85, 58, '5/rev');

hold on
LHD_coll_linestyle = {'*:r', '*:m', '*:c', '*:g'};
MBDYN_coll_linestyle = {'o-r', 'o-m', 'o-c', 'o-g'};

for i = 1:4
    L(i) = plot(rpm,LHD.coll(i,:),LHD_coll_linestyle{i},'LineWidth',2);
    hold on 
    M(i) = plot(rpm,MBDYN.coll(i,:),MBDYN_coll_linestyle{i},'LineWidth',2);
end
grid on 
xlim([70 120]); 
ylim([0,60]);    
xlabel('Rotor Nr [%]')
ylabel('Frequency [Hz]')
title('Collective = 20° - Collective rotor modes comparison') 
legend([L(1) M(1) L(2) M(2) L(3) M(3) L(4) M(4)], {'LHD Cone', 'MBDyn Cone',...
        'LHD 1st Lag', 'MBDyn 1st Lag', ...
        'LHD 1st Beam', 'MBDyn 1st Beam', ...
        'LHD Pitch', 'MBDyn Pitch'}, 'Location','eastoutside')


%% Cyclic modes 

LHD.cycl = [10.1 11.5 12.9 14.3 15.7 17.1;
            19.5 20.4 21.2 21.9 22.5 23.1;
            33.3 35.3 37.4 39.6 42.0 44.4;
            44.5 44.6 44.8 44.9 45.0 45.2;
            63.0 65.7 68.6 71.7 74.9 78.2]; 
        
MBDYN.cycl = [9.92 11.3 12.8 14.2 15.6 17.02;  
            23.2 24.9 26.6 28.1 29.6 31.01;
            35.1 36.4 37.9 39.5 41.3 43.21;
            44.9 45.8 46.6 47.4 48.3 49.2;
            65.5 67.7 69.9 72.5 75.1 77.9]; 
figure('Renderer', 'painters', 'Position', [0 0   800 500])  
R1 = plot(rpm_rev, rev_1,'--', 'LineWidth',1.5,'Color',[0.5 0.5 0.5]);
text(rpm_rev(end)+1, rev_1(end), '1/rev'); 
hold on 
R2 = plot(rpm_rev, rev_2,'--', 'LineWidth',1.5,'Color',[0.5 0.5 0.5]);
text(rpm_rev(end)+1, rev_2(end), '2/rev');
R3 = plot(rpm_rev, rev_3,'--', 'LineWidth',1.5,'Color',[0.5 0.5 0.5]);
text(rpm_rev(end)+1, rev_3(end), '3/rev');
R4 = plot(rpm_rev, rev_4,'--', 'LineWidth',1.5,'Color',[0.5 0.5 0.5]);
text(rpm_rev(end)+1, rev_4(end), '4/rev');
R5 = plot(rpm_rev, rev_5,'--', 'LineWidth',1.5,'Color',[0.5 0.5 0.5]);
text(112, 78, '5/rev');

% Adimensional frequencies
% red yellow ciano green blue 
LHD_cycl_linestyle = {'*:r', '*:m', '*:c', '*:g', '*:b'};
MBDYN_cycl_linestyle = {'o-r', 'o-m', 'o-c', 'o-g', 'o-b'};
for i = 1:5
    L(i) = plot(rpm,LHD.cycl(i,:),LHD_cycl_linestyle{i},'LineWidth',2);
    hold on 
    M(i) = plot(rpm,MBDYN.cycl(i,:),MBDYN_cycl_linestyle{i},'LineWidth',2);
end
grid on 
xlim([70 120]); 
ylim([0,80]);         
xlabel('Rotor Nr [%]')
ylabel('Frequency [Hz]')
title('Collective = 20° - Cyclic rotor modes comparison') 
hLg = legend([L(1) M(1) L(2) M(2) L(3) M(3) L(4) M(4) L(5) M(5)],{'LHD Gimbal', 'MBDyn Gimbal',...
        'LHD 1st Lag', 'MBDyn 1st Lag', ...
        'LHD 1st Beam', 'MBDyn 1st Beam', ...
        'LHD Pitch', 'MBDyn Pitch', ...
        'LHD 2nd Beam', 'MBDyn 2nd beam'}, 'Location','eastoutside');


