clear; close all; clc; 
[file_nc, path] = uigetfile('*.nc');
file_nc = fullfile(path, file_nc); 
%%
Sw_rot = ncread(file_nc, 'node.struct.400.X'); 
Sw_rot = Sw_rot(3,1:1000) - Sw_rot(3,1); 

Node_075 = ncread(file_nc, 'node.struct.10019.E'); 
Node_075 = Node_075(2,1:1000) - Node_075(2,1); 

plot(Node_075, Sw_rot)
p = polyfit(Node_075,Sw_rot,3);
Sw_disp = polyval(p,Node_075); 
hold on
plot(Node_075, Sw_disp, '--')






