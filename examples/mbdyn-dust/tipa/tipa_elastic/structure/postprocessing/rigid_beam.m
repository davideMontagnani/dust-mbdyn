function rigid_beam(BEAM,lab_basic_ref)
    Lab = BEAM.Name; % Name of the part
    
    if BEAM.STA.Value(1) > BEAM.STA.Value(end)
        STA = fliplr(BEAM.STA.Value); % Station location on the beam axis
        YCG = fliplr(BEAM.YCG.Value); % Center of mass Y position
        ZCG = fliplr(BEAM.ZCG.Value); % Center of mass Z position
        Rotapi = -fliplr(BEAM.ROTAPI.Value); % Rotation of the principal axis of inertia
        Sec = fliplr(BEAM.SEC.Value); % Number of beam section
        dM = fliplr(BEAM.WEIGHT.Value); % Mass per unit length
        dJX = fliplr(BEAM.JP.Value); % Mass polar moment of inertia per unit length - ref centre of gravity
        dJY = fliplr(BEAM.JY.Value); % Mass moment of inertia per unit length - ref centre of gravity
        dJZ = fliplr(BEAM.JZ.Value); % Mass moment of inertia per unit length - ref centre of gravity
    else
        STA = BEAM.STA.Value; % Station location on the beam axis
        YCG = BEAM.YCG.Value; % Center of mass Y position
        ZCG = BEAM.ZCG.Value; % Center of mass Z position
        Rotapi = BEAM.ROTAPI.Value; % Rotation of the principal axis of inertia
        Sec = BEAM.SEC.Value; % Number of beam section
        dM = BEAM.WEIGHT.Value; % Mass per unit length
        dJX = BEAM.JP.Value; % Mass polar moment of inertia per unit length - ref centre of gravity
        dJY = BEAM.JY.Value; % Mass moment of inertia per unit length - ref centre of gravity
        dJZ = BEAM.JZ.Value; % Mass moment of inertia per unit length - ref centre of gravity
    end

    if isfield(BEAM, 'Aero')
        Twist = BEAM.Aero.Geometry.Twist.Value;
        Chord = BEAM.Aero.Geometry.Chord.Value;
        Sweep = BEAM.Aero.Geometry.Sweep.Value;
        Anhedral = BEAM.Aero.Geometry.Anhedral.Value;
        Radial = BEAM.Aero.Geometry.Radial.Value;
        range = (BEAM.Aero.C81.range) .* 2 - 1;
        file = BEAM.Aero.C81.file;
    else
        Twist = zeros(length(STA), 1);

    end

    

    % write ref system
    Axis = {'BODY'};

    if isfield(BEAM, 'Aero')

        if BEAM.STA.Value(1) > BEAM.STA.Value(end)
            BEAM.STA.red_aero = fliplr(BEAM.STA.Value);
        else
            BEAM.STA.red_aero = BEAM.STA.Value;
        end

        for i = 1:numel(STA)
            dist = abs(BEAM.STA.red_aero(i) - (BEAM.Aero.Geometry.Radial.Value));
            minDist = min(dist);
            idx_n = find(dist == minDist);

            if numel(idx_n) > 1
                idx(i) = idx_n(1);
            else
                idx(i) = idx_n;
            end

        end

        Twist_red = BEAM.Aero.Geometry.Twist.Value(idx);
    else
        Twist_red = zeros(length(STA), 1);
    end
    
    fileID = fopen(BEAM.file.rigid.ref, 'wt');
    
    for i = 1:numel(STA)
        fprintf(fileID, 'reference: OFFSET + CURR_%s + %s + %d, #gen ref\n', Lab, Axis{1}, Sec(i));
        Label_axis.rigid{i} = sprintf('OFFSET + CURR_%s + %s + %d', Lab, Axis{1}, Sec(i));
        fprintf(fileID, '\treference, %s, %.16f, %.16f, %.16f,\n', lab_basic_ref, STA(i), YCG(i), ZCG(i)); % position offset wrt to main reference frame
        fprintf(fileID, '\treference, %s, \n', lab_basic_ref); % orientation matrix
        fprintf(fileID, '\t\t1, 1., \t0., \t0.,\n');
        fprintf(fileID, '\t\t2, 0., %.16f, %.16f,\n', cos(Twist_red(i)), sin(Twist_red(i)));
        fprintf(fileID, '\treference, %s, null,\n', lab_basic_ref);
        fprintf(fileID, '\treference, %s, null;\n\n', lab_basic_ref);

    end
    
        fprintf(fileID, 'reference: OFFSET + CURR_%s + AERO, #gen ref\n', Lab);
        Label_axis_aero = sprintf('OFFSET + CURR_%s + AERO', Lab);
        fprintf(fileID, '\treference, %s, %.16f, %.16f, %.16f,\n', lab_basic_ref, (STA(1) + STA(end))/2, 0, 0); % position offset wrt to main reference frame
        fprintf(fileID, '\treference, %s, \n', lab_basic_ref); % orientation matrix
        fprintf(fileID, '\t\t1, 1., \t0., \t0.,\n');
        fprintf(fileID, '\t\t2, 0., %.16f, %.16f,\n', cos(0), sin(0));
        fprintf(fileID, '\treference, %s, null,\n', lab_basic_ref);
        fprintf(fileID, '\treference, %s, null;\n\n', lab_basic_ref);
    
    fclose(fileID);
    k = round(median(Sec)); 
    fileID = fopen(BEAM.file.rigid.nod, 'wt');
    fprintf(fileID, 'structural: OFFSET + CURR_%s, dynamic,\n', Lab);
    fprintf(fileID, '\treference, %s, null,\n', Label_axis.rigid{k});
    fprintf(fileID, '\treference, %s, eye,\n', Label_axis.rigid{k});
    fprintf(fileID, '\treference, %s, null,\n', Label_axis.rigid{k});
    fprintf(fileID, '\treference, %s, null;\n\n', Label_axis.rigid{k});
    fclose(fileID);
    
    fileID = fopen(BEAM.file.rigid.elm, 'wt'); 
    
    fprintf(fileID, 'body: OFFSET + CURR_%s + BODY, OFFSET + CURR_%s,\n', Lab, Lab);
    fprintf(fileID, '\tcondense, %d,\n', numel(STA));

    for i = 1:numel(STA)

        if i == 1
            dL = abs(STA(i) - STA(i + 1)) / 2;
        elseif i == numel(STA)
            dL = abs(STA(i) - STA(i - 1)) / 2;
        else 
            dL = abs(STA(i) - STA(i + 1));
        end

        fprintf(fileID, '%.16f,\n', dM(i) * dL);
        fprintf(fileID, '\treference, %s, null,\n', Label_axis.rigid{i});
        fprintf(fileID, '\treference, %s,\n', Label_axis.rigid{i});
        JX = dJX(i) * dL;
        JY = dJY(i) * dL + dM(i) * dL^3/12;
        JZ = dJZ(i) * dL + dM(i) * dL^3/12;
        Rot = [1 0 0;
            0 cos(Rotapi(i)), -sin(Rotapi(i));
            0 sin(Rotapi(i)), cos(Rotapi(i))]; 
        J = Rot * diag([JX, JY, JZ])* Rot';
        fprintf(fileID, '\t%.16f, %.16f, %.16f,\n', J(1, :));
        fprintf(fileID, '\t%.16f, %.16f, %.16f,\n', J(2, :));

        if i == numel(STA)
            fprintf(fileID, '\t%.16f, %.16f, %.16f;\n\n', J(3, :));
        else
            fprintf(fileID, '\t%.16f, %.16f, %.16f,\n', J(3, :));
        end

    end

    if isfield(BEAM, 'Aero')
        fprintf(fileID, 'aerodynamic body: OFFSET + CURR_%s + AERO, OFFSET + CURR_%s,\n', Lab, Lab);
        fprintf(fileID, 'induced velocity, OFFSET + GROUND,\n'); %check!!
        fprintf(fileID, 'reference, %s, null, \n', Label_axis_aero);
        fprintf(fileID, 'reference, %s, \n', lab_basic_ref);
        fprintf(fileID, '\t\t1, 0., 1., 0., 3, 1., 0., 0., # orientation wrt to node \n');
        fprintf(fileID, '\t\t%.16f, \n', (Radial(end) - Radial(1)));
        
        fprintf(fileID, '\tpiecewise linear, %d,\n', numel(Radial));

        for i = 1:numel(Radial)
            fprintf(fileID, '\t\t%.16f, %.16f,\n', (Radial(i) / (Radial(end)))*2-1, Chord(i));
        end

        fprintf(fileID, '\tpiecewise linear, %d,\n', numel(Radial));

        for i = 1:numel(Radial)
            fprintf(fileID, '\t\t%.16f, %.16f,\n', (Radial(i) / (Radial(end)))*2-1, Sweep(i));
        end

        fprintf(fileID, '\tpiecewise linear, %d,\n', numel(Radial));

        for i = 1:numel(Radial)
            fprintf(fileID, '\t\t%.16f, %.16f,\n', (Radial(i) / (Radial(end)))*2-1, Chord(i) / 2); %FIXME!!
        end

        fprintf(fileID, '\tpiecewise linear, %d,\n', numel(Radial));

        for i = 1:numel(Radial)
            fprintf(fileID, '\t\t%.16f, %.16f,\n', (Radial(i) / (Radial(end)))*2-1, Twist(i)); %FIXME!!
        end

        fprintf(fileID, '\t\t%s_GAUSS_POINTS, # name of blade Gauss points\n', BEAM.Name);

        fprintf(fileID, '\tc81, multiple, %d, \n', numel(file));
        %name_profile = char(file{Profile});

        for i = 1:numel(file)
            name_profile = char(file{i});
            fprintf(fileID, '\t\t%s, %.16f,\n', name_profile(1:end - 4), range(i, 2));
        end

        fprintf(fileID, '\t\tunsteady, bielawa, # unsteadiness on\n');
        fprintf(fileID, '\t\tjacobian, 1; # jacobian output\n\n');
    end
    fclose(fileID); 
end
