clear; close all; clc;
%% parse/plot .c81 file
% it requires linspecer
% <https://it.mathworks.com/matlabcentral/fileexchange/42673-beautiful-and-distinguishable-line-colors-colormap>

profile_name = 'AFL4'; 

fileID = fopen(fullfile('ATTILA_MBDyn', 'Profile', [profile_name '.c81']));
it = 0;
skip_line = false;
i_cl = 0;
i_cd = 0; 
i_cm = 0; 
%% Parsing 
while skip_line || ~feof(fileID)
    
    if ~skip_line
        line_new = fgetl(fileID);
    else
        skip_line = false;
    end
    
    % read intestation
    
    it = it+1;
    
    if it == 1
        comment_profile = line_new(1:30);
        ML = str2double(line_new(31:32));
        NL = str2double(line_new(33:34));
        MD = str2double(line_new(35:36));
        ND = str2double(line_new(37:38));
        MM = str2double(line_new(39:40));
        NM = str2double(line_new(41:42));
    end
    
    if it == 2
        CL_mach = textscan(line_new, '%f', 'Delimiter', '');
        CL_mach = permute( CL_mach{1}, [2,1] );
    end
    
    if it>2 && it <= NL + 2        
        i_cl = i_cl + 1;
        CL_new = textscan(line_new, '%8f', 'Delimiter', '');
        CL(i_cl,:) = permute( CL_new{1}, [2,1] );        
    end
    
    if it == NL + 3
        CD_mach = textscan(line_new, '%f', 'Delimiter', '');
        CD_mach = permute( CD_mach{1}, [2,1] );
    end
    
    if it>NL + 3 && it <= (NL + 3 + ND)        
        i_cd = i_cd + 1;
        CD_new = textscan(line_new, '%8f', 'Delimiter', '');
        CD(i_cd,:) = permute( CD_new{1}, [2,1] );        
    end
    
    if it == (NL + 3 + ND + 1)
        CM_mach = textscan(line_new, '%f', 'Delimiter', '');
        CM_mach = permute( CM_mach{1}, [2,1] );
    end
    
    if it>(NL + 3 + ND + 1) && it <= (NL + 3 + ND + 1 + NM )        
        i_cm = i_cm + 1;
        CM_new = textscan(line_new, '%8f', 'Delimiter', '');
        CM(i_cm,:) = permute( CM_new{1}, [2,1] );        
    end
    
end

%% Plot 

% CL 
figure('Renderer', 'painters', 'Position', [200 200 2000 1000])  
sgtitle([profile_name ' Aerodynamic Coefficients'], 'FontSize', 20)
subplot(1,3,1)
C = linspecer(size(CL, 2)); 

for i = 2:size(CL, 2)
    plot(CL(:,1), CL(:, i), 'LineWidth',1.5,'color',C(i,:));
    hold on     
end

grid on
title('CL','FontSize', 14)
xlim([-35, 35])
legend(strsplit(num2str(CL_mach)), 'Location', 'southeast', 'FontSize', 12);
xlabel('\alpha','FontSize', 14)
ylabel('CL','FontSize', 14)

% CD
subplot(1,3,2)
C = linspecer(size(CD, 2)); 

for i = 2:size(CD, 2)
    plot(CD(:,1), CD(:, i),'LineWidth',1.5,'color',C(i,:));
    hold on    
end

grid on
title('CD','FontSize', 14)
xlim([-35, 35])
legend(strsplit(num2str(CD_mach)),'FontSize', 12);
xlabel('\alpha','FontSize', 14)
ylabel('CD','FontSize', 14)

% CM - alpha
subplot(1,3,3)
C = linspecer(size(CM, 2)); 

for i = 2:size(CM, 2)
    plot(CM(:,1), CM(:, i),'LineWidth',1.5,'color',C(i,:));
    hold on     
end

grid on
title('CM','FontSize', 14)
xlim([-35, 35])
legend(strsplit(num2str(CM_mach)),'FontSize', 12);
xlabel('\alpha', 'FontSize',14)
ylabel('CM','FontSize', 14)


