clear; clc; close all; 
path = 'D:\Clouds\OneDrive - Politecnico di Milano\Dottorato\ATTILA\TiPa_New\Tilt_load_data\XV_15_MBDyn\Rotor\'; 
file_rot = fullfile(path, 'rigid_rot.rot');
file_nc = fullfile(path, 'rigid_rot.nc');
data_rot = dlmread(file_rot); 
joint_out = ncread(file_nc, 'elem.joint.15100.Phi'); 
dt = 5e-4;
step = 10/dt;
theta_coll = linspace(0,13,step);
n_0 = numel(0:dt:2);
n_f = numel(2:dt:12) + n_0 - 2;
rho = 1.225;
R = 3.81;
omega = 62.936572755;
A = R^2*pi; 
thrust_camrad =   0.4536*9.81.*[ 30.52 1784.19 5040.10 7403.59];
theta_0_camrad = [0 5 10 13];
CT_camrad = thrust_camrad/(rho*A*(omega*R)^2);

figure('Renderer', 'painters', 'Position', [400 400 800 500]) 
plot(theta_coll, data_rot(n_0:n_f,4)/(rho*A*(omega*R)^2),'LineWidth', 2)
hold on
plot(theta_0_camrad, thrust_camrad/(rho*A*(omega*R)^2),'*-', 'LineWidth', 2)
xlabel('\theta_{controlled} [deg]', 'FontSize', 12)
ylabel('Thrust [N]', 'FontSize', 12)
grid on
title('Collective vs thrust', 'FontSize', 14)
legend('MBDyn', 'Camrad', 'FontSize', 12, 'Location', 'southeast')

%% induced velocity
figure('Renderer', 'painters', 'Position', [1600 400 800 500]) 
plot(theta_coll, data_rot(n_0:n_f,8), 'LineWidth', 2)
hold on
induced_camrad =  0.3048.*[4.333 33.192 55.771 67.556];
plot(theta_0_camrad,induced_camrad, '*-','LineWidth', 2);
grid on
