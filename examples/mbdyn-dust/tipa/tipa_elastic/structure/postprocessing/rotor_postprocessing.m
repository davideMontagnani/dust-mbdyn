%% Post processing

clear all; close all; clc; 
% Flow: - Parse log to collect the name of the joint/nodes
%       - Dof_extractor: organize dof for multiblade 
%       - Multiblade 4 MBDyn
%       - Mode identifier(multiblade and rotating) 
%       - Substructuring
log_file = fullfile('..','MBDyn_model','Rotor','main_rotor.log'); 
[nodes, beam3, aero3, bodies, angularvelocity, deformablehinge, ...
    deformabledisplacement, rod, clamp, joints, label] = parse_log4Tipa(log_file,false);  

eqn_file = fullfile('..','MBDyn_model','Rotor','main_rotor.txt');
[node_eq, joint_eq] = parse_eqn(eqn_file);

Label_rotor_joint = {'OFFSET + GROUND_CLAMP'; 'OFFSET + AIRFRAME_CLAMP'; ...
                'OFFSET + AIRFRAME_SWASH_JOINT'; 'OFFSET + SWASH_JOINT'; ...
                'OFFSET + SWASH_HUB_JOINT'; 'OFFSET + HUB_AIRFRAME'; ...
                'OFFSET + HUB_ANG'; 'OFFSET + AIRFRAME_SWASH_DEF_CYCL'; ...
                'OFFSET + AIRFRAME_SWASH_DEF_COLL'};  % just to test 
Label_rotor_node = {'OFFSET + GROUND'; 'OFFSET + AIRFRAME'; 'OFFSET + SW_FIX_NODE'; ...
                    'OFFSET + SW_ROT_NODE'; 'OFFSET + HUB_NODE'}; % just to test 
                
%% Get rotor joint number label
num = []; 
Rotor_joint_num = []; 
for ii = 1:numel(Label_rotor_joint)
    if contains(Label_rotor_joint{ii}, '+')
        curr_joint = strtrim(strsplit(Label_rotor_joint{ii},'+'));
    else
        curr_joint = strtrim(Label_rotor_joint{ii}); 
    end
    for jj = 1:numel(curr_joint)
        for kk = 1:numel(label)
            if strcmp(label(kk).lab,curr_joint{jj})
                num(jj) = label(kk).num; 
            end
        end
    end
    Rotor_joint_num(ii) = sum(num);  
end

%% Get rotor node number label 
num = []; 
Rotor_node_num = []; 
for ii = 1:numel(Label_rotor_node)
    if contains(Label_rotor_joint{ii}, '+')
        curr_joint = strtrim(strsplit(Label_rotor_node{ii},'+'));
    else
        curr_joint = strtrim(Label_rotor_node{ii}); 
    end
    curr_node = strtrim(strsplit(Label_rotor_node{ii},'+'));
    for jj = 1:numel(curr_node)
        for kk = 1:numel(label)
            if strcmp(label(kk).lab,curr_node{jj})
                num(jj) = label(kk).num; 
            end
        end
    end
    Rotor_node_num(ii) = sum(num);  
end

%% Get rotor nodes equations
idx_node = [];
for ii = 1:numel(Rotor_node_num)
    for jj = 1:numel(node_eq)
        if strcmp(node_eq(jj).lab,num2str(Rotor_node_num(ii)))
            idx_node(ii) = jj; 
        end
    end
end


%% Get rotor joints equations 
idx_joint = [];
for ii = 1:numel(Rotor_joint_num)
    for jj = 1:numel(joint_eq)
        if strcmp(joint_eq(jj).lab,num2str(Rotor_joint_num(ii)))
            idx_joint(ii) = jj; 
        end
    end
end


