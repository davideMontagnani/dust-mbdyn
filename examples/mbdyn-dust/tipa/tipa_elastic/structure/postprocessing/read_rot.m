%% Hover post processing 
clear; close all; clc; 
load('collsweep_fe_102NR.mat'); 
dt = 1e-3;
t_0_coll = 5;
t_f_coll = 13.0;
n_0_steps = numel(0:dt:t_0_coll); 
n_f_steps = n_0_steps + numel(t_0_coll:dt:t_f_coll); 

rpm = 1.02 * 89.55486839943; % rad/s 
dt_azi = ceil(1/(0.15915494327*rpm)/dt);  
Beta = deg2rad(2.75); % precone angle
path = 'D:\Clouds\OneDrive - Politecnico di Milano\Dottorato\ATTILA\TiPa_New\Tilt_load_data\ATTILA_MBDyn\Rotor\'; 
file_rot = fullfile(path, 'test_thrust_no_correction.rot'); 
file_nc = fullfile(path,'test_thrust_no_correction.nc');
R_Node_075 = ncread(file_nc, 'node.struct.10019.R'); 
for i = 1: size(R_Node_075, 3)
    R(:,:,i) = R_Node_075(:,:,i); 
    % conversion into euler angles
    [theta_1(i), psi_1(i), phi_1(i)] = matr2euler(R(:,:,i)); 

end 
R_Node_0223 = ncread(file_nc, 'node.struct.10002.R');
for i = 1: size(R_Node_0223, 3)
    R(:,:,i) = R_Node_0223(:,:,i); 
    % conversion into euler angles
    [theta_1(i), psi_0223(i), phi_1(i)] = matr2euler(R(:,:,i)); 
    
end 

theta_hinge = rad2deg(psi_0223 - psi_0223(1)); 
theta_hinge = theta_hinge(n_0_steps:n_f_steps); 


theta0 = rad2deg(psi_1(n_0_steps:n_f_steps)); 
data_rot = dlmread(file_rot);
thrust = data_rot(n_0_steps:n_f_steps,4);
%% plot theta/thrust
% Johnson ideal thrust
a = 5.7;
sigma = 0.0906;
c = 0.1186;
R = 1.25;
A = R^2*pi;
rho = 1.225;
Omega = rpm; 
theta_075 = deg2rad([0:22]);
lambda = (sigma*a)/16*(sqrt(1+64/(3*sigma*a)*theta_075)-1);

C_T = (sigma*a)/2*(theta_075/3-lambda/2);
T = C_T*rho*A*(Omega*R)^2;

figure('Renderer', 'painters', 'Position', [400 400 800 500])
plot(-1.3:20.7, T, 'k--')
hold on
plot(theta0, thrust, 'b-', 'LineWidth', 2)
plot(0:20, 4.44822.*mean(hubfz), 'r-', 'LineWidth', 2 )
xlim([0 20])
legend('Johnson', 'MBDyn','FLIGHTLAB','Location', 'southeast','Fontsize',12)
xlabel('\theta_{075} [deg]','Fontsize',12)
ylabel('Thrust [N]','Fontsize',12)
title('Collective vs Thrust','Fontsize',14)
grid on
saveas(gcf, 'Collective_Thrust', 'png')

%% plot theta/torque
figure('Renderer', 'painters', 'Position', [400 400 800 500]) 
torque = data_rot(n_0_steps:n_f_steps, 7); 
plot(theta0,torque, 'LineWidth', 2)
hold on
plot(0:20, 4.44822.*hubmz(end,:), 'LineWidth', 2)
xlim([0 20])
legend('MBDyn','FLIGHTLAB','Fontsize',12)
xlabel('\theta_{075} [deg]','Fontsize',12)
ylabel('Torque [N*m]','Fontsize',12)
title('Collective vs Torque','Fontsize',14)
grid on
saveas(gcf, 'Collective_Torque', 'png')

%% plot theta/theta hinge
figure('Renderer', 'painters', 'Position', [400 400 800 500])  
plot(theta_hinge, theta0, 'LineWidth', 2)
hold on
plot(rad2deg(bpitch(end,:)), 0:20, 'LineWidth', 2)
plot(0:20,0:20,'k--')
ylim([0 20])
legend('MBDyn','FLIGHTLAB','Location', 'southeast','Fontsize',12)
xlabel('Outboard-bearing [deg]','Fontsize',12)
ylabel('\theta_{075} [deg]','Fontsize',12)

title('Collective vs Outboardbearing rotation','Fontsize',14)
grid on
saveas(gcf, 'Collective_Hinge', 'png')


%% aerodynamic element post processing 
aero_075 = 10009;

file_aer = fullfile(path, 'test_thrust.aer'); 
data_aer = dlmread(file_aer); 
%aero_075 = 10003; 

idx = find(data_aer(:,1) == aero_075); 
aero_data_075.all_data = data_aer(idx, :); 
 %%
aero_data_075.node1.alpha = aero_data_075.all_data(n_0_steps:n_f_steps,2); 
aero_data_075.node1.beta = aero_data_075.all_data(n_0_steps:n_f_steps,3); 
aero_data_075.node1.mach = aero_data_075.all_data(n_0_steps:n_f_steps,4); 
aero_data_075.node1.CL = aero_data_075.all_data(n_0_steps:n_f_steps,5); 
aero_data_075.node1.CD = aero_data_075.all_data(n_0_steps:n_f_steps,6); 
aero_data_075.node1.CM = aero_data_075.all_data(n_0_steps:n_f_steps,7); 

aero_data_075.node2.alpha = aero_data_075.all_data(n_0_steps:n_f_steps,10); 
aero_data_075.node2.beta = aero_data_075.all_data(n_0_steps:n_f_steps,11); 
aero_data_075.node2.mach = aero_data_075.all_data(n_0_steps:n_f_steps,12); 
aero_data_075.node2.CL = aero_data_075.all_data(n_0_steps:n_f_steps,13); 
aero_data_075.node2.CD = aero_data_075.all_data(n_0_steps:n_f_steps,14); 
aero_data_075.node2.CM = aero_data_075.all_data(n_0_steps:n_f_steps,15); 

aero_data_075.node3.alpha = aero_data_075.all_data(n_0_steps:n_f_steps,18); 
aero_data_075.node3.beta = aero_data_075.all_data(n_0_steps:n_f_steps,19); 
aero_data_075.node3.mach = aero_data_075.all_data(n_0_steps:n_f_steps,20); 
aero_data_075.node3.CL = aero_data_075.all_data(n_0_steps:n_f_steps,21); 
aero_data_075.node3.CD = aero_data_075.all_data(n_0_steps:n_f_steps,22); 
aero_data_075.node3.CM = aero_data_075.all_data(n_0_steps:n_f_steps,23); 

figure('Renderer', 'painters', 'Position', [400 400 800 500])  

plot(aero_data_075.node1.alpha, aero_data_075.node3.CL,'LineWidth', 2)
xlabel('\alpha','FontSize', 12)
ylabel('CL','FontSize', 12)
title('CL-alpha 0.75','FontSize',14)
grid on
% theta check
figure('Renderer', 'painters', 'Position', [400 400 800 500]) 
u_t = data_rot(n_0_steps:n_f_steps,8); % induced velocity
u_f = rpm * R *0.75;
phi = atan2(u_t, u_f); 
theta_analitic = rad2deg(phi) + aero_data_075.node3.alpha;
plot(theta_hinge,  theta_analitic, 'LineWidth', 2)
hold on
plot(theta_hinge, theta0,  'LineWidth', 2)
plot(rad2deg(bpitch(end,:)), 0:20, 'LineWidth', 2)
plot(0:20,0:20,'k--')
legend('\theta_{075} analitic', '\theta_{075} MBDyn', '\theta_{075} FLIGHTLAB' ,'Fontsize',12, 'Location', 'southeast')
xlabel('Outboard-bearing [deg]','Fontsize',12)
ylabel('\theta_{075} [deg]','Fontsize',12)
ylim([0 20])
grid on



%% Function to convert rotation matrix into Euler angles
function [theta_1, psi_1, phi_1] = matr2euler(R)

    if R(3,1,1) ~= 1 && R(3,1,1) ~= -1
        theta_1 = -asin(R(3,1,1));
        psi_1 = atan2((R(3,2,1)/cos(theta_1(1))), (R(3,3,1)/cos(theta_1(1))));
        phi_1 = atan2((R(2,1,1)/cos(theta_1(1))), (R(1,1,1)/cos(theta_1(1))));
        
    else
        phi_1 = 0;
        if R(3,1,1) == -1
            theta_1 = pi/2;
            psi_1 = phi_1 + atan2(R(1,2,1),R(1,3,1));
        else 
            theta_1 = -pi/2;
            psi_1 = -phi_1 + atan2(-R(1,2,1),-R(1,3,1));
        end
    end
end



