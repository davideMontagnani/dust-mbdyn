function [X0,R,nodes] = eig_geom(ncfile, eig, orient)
    %EIG_GEOM -- Extracts the reference configuration of the eigensolution of
    %              index eig from MBDyn NetCDF output file ncfile, that is written
    %              with default orientation orient
    %              INPUTS:%                 - ncfile -- filename (full path) of MBDyn NetCDF output file
    %                 - eig    -- index of eigensolution of interest
    %                 - orient -- string identifying the orientation used for
    %                             the structural nodes output. Please note that this
    %                             function does not handle multiple output
    %                             orientation descriptions % FIXME
    %%              X0 = eig_geom(ncfile, eig, orient)
    step = ncread(ncfile, 'eig.step', eig + 1, 1);
    
    nodes = ncread(ncfile, 'node.struct');
%     if orient == 'R'
%         ncoord = 12;
%     else
%         ncoord = 6;
%     end
    X0 = zeros(3, length(nodes));
    R = zeros(3,3,length(nodes)); 
    for ii = 1:length(nodes)
        nodevar = ['node.struct.' num2str(nodes(ii)) '.'];
        %x_bg = (ii - 1)*ncoord + 1;
        %r_bg = x_bg + 3;
        X0(:,ii) = ncread(ncfile, [nodevar 'X'], [1 step],[3 1]);
        if orient == 'R'
            R(1, :, ii) = ncread(ncfile, [nodevar orient], [1 1 step], [3 1 1]);
            R(2, :, ii) = ncread(ncfile, [nodevar orient], [1 2 step], [3 1 1]);
            R(3, :, ii) = ncread(ncfile, [nodevar orient], [1 3 step], [3 1 1]);
        
            %X0(r_bg:r_bg + 2) = ncread(ncfile, [nodevar orient], [1 step], [3 1]);
        end
    end
end
    