clear; close all; clc;
nc_out = sprintf('ATTILA_MBDyn/Rotor/mono_blade.nc');
addpath('write_mbdyn','postprocessing','nas2mbdyn','lib','tip2nas'); 
[file_tip,path] = uigetfile('*.tip'); 
[BLADE, ROTOR, RPM, SPRING, POINT, YOKE] = read_input(path, file_tip); 
%% Flightlab data
fileToRead1 = 'blademodes_85NR_coll0deg.mat';
importfile(fileToRead1)
freqHz = bladefreq./(2*pi)*(89.55486839943 * 0.85);
p1 = plot(nrn, mode1(1:end-6,3)./max(abs(mode1(1:end-6,3))),'--b','LineWidth',2);
hold on
p2 = plot(nrntt, mode1(end-5:end,3)./max(abs(mode1(1:end-6,3))),'--r','LineWidth',2);
sparse = false;
nmode = 1;
alpha = ncread(nc_out, 'eig.0.alpha');
dCoef = ncread(nc_out, 'eig.dCoef');
scale = 400;
lambda_freq = zeros(size(alpha,2),1);
lambda_damp = zeros(size(alpha,2),1);
LHD_station = [0.07 0.125 0.250 0.375 0.5 0.625 0.750 0.875 1.0 1.125 1.250];
Mode1_flap = [0.0010875 0.045175 0.145275 0.247625 0.354825 0.4724125 0.604725 ...
                0.7531375 0.914275 1.0812625 1.25];
Mode1_lag = [0.0000375 0.0009625 0.003425 0.0076875 0.0156375 0.030275 0.0527375 0.082 0.1157875 0.15105 0.186237];    
Mode2_flap = [-0.0007625 -0.0319 -0.1017375 -0.1652625 -0.2124 -0.223575 -0.189 -0.113125 -0.00895 0.1064625 0.2248375]; 
Mode2_lag = [0.0006 0.0159125 0.0544625 0.107825 0.1871625 0.303575 0.45615 0.636125 0.834625 1.0412375 1.25];

for i = 1:size(alpha,2)
    alpha_r = alpha(1,i); 
    alpha_i = alpha(2,i);
    beta = alpha(3,i);
    det = (alpha_r + beta)^2 + alpha_i^2;
    lambda_real = 1 / dCoef * (alpha_r^2 + alpha_i^2 - beta^2) / det;
    lambda_imag = 1 / dCoef * (2 * alpha_i * beta) / det;

    if alpha_i * beta < 0
        lambda_imag = -lambda_imag;
    end

    if lambda_imag <= 2 * 1e-16
        lambda_freq(i) = 0;
        lambda_damp(i) = 100;
    else
        lambda_freq(i) = lambda_imag / (2 * pi);
        lambda_damp(i) = round(((lambda_real / lambda_imag)^2 / (1 + (lambda_real / lambda_imag)^2))^(0.5) * 100);
    end
    
end
% sort lambda_freq
[lambda_freq, idx] = sort(lambda_freq); 
% idx are the index that comes out from sorting
lambda_damp = lambda_damp(idx); 

% get eigenvectors 
VR = ncread(nc_out, 'eig.0.VR');
VR = VR(:, idx, :); % reorder the eigenvalues 

% Discard null eigenvalues 
idx_not_null = find(lambda_freq > 1e-2); 
lambda_freq = lambda_freq(idx_not_null);
eigvec_real = VR(:, idx_not_null, 1); 
eigvec_im = VR(:, idx_not_null, 2);

eigvec_abs = sqrt(eigvec_real.^2 + eigvec_im.^2);
%eigvec_abs = eigvec_abs ./ max(eigvec_abs); % max norm normalization
eigvec_phase = atan2(eigvec_im, eigvec_real);

% reference geometry
[X0, R, nodes] = eig_geom(nc_out, 0,  'R'); 
% from rotation matrix to rotation vector
X0(2,6:56) = X0(2,6:56) + fliplr(BLADE.XNA.Value./1000);
X0(3,6:56) = X0(3,6:56) - fliplr(BLADE.YNA.Value./1000);
for i = 1:size(R,3)
    if R(3,1,i) ~= 1 || R(3,1,i) ~= -1
        theta = -asin(R(3,1,i));
        psi = atan2(R(3,2,i)/cos(theta), R(3,3,i)/cos(theta));
        phi = atan2(R(2,1,i)/cos(theta), R(1,1,i)/cos(theta));
    else
        phi = 0; 
        if R(3,1,i) == -1
            theta = pi/2; 
            psi = phi + atan2(R(1,2,i),R(1,3,i));
        else 
            theta = -pi/2;
            psi = -phi + atan2(-R(1,2,i),-R(1,3,i));
        end
    end
    ori_ref(:,i) = [phi, theta, psi]';     
        
end
    
%Get index 
idx = ncread(nc_out, 'eig.idx')+1; 

for t = 0.8 
    
    for ii = 1:numel(idx)
        pos(:,ii) = [ scale*[eigvec_abs(idx(ii),nmode)*cos(2*pi*t + eigvec_phase(idx(ii),nmode));
                                        eigvec_abs(idx(ii)+1,nmode)*cos(2*pi*t + eigvec_phase(idx(ii)+1,nmode));
                                        eigvec_abs(idx(ii)+2,nmode)*cos(2*pi*t + eigvec_phase(idx(ii)+2,nmode))]]; % project using the first yoke node
                                
        ori(:,ii) = [  [eigvec_abs(idx(ii) +3,nmode)*cos(2*pi*t + eigvec_phase(idx(ii) + 3,nmode));
                                    eigvec_abs(idx(ii)+4,nmode)*cos(2*pi*t + eigvec_phase(idx(ii)+ 4,nmode));
                                    eigvec_abs(idx(ii)+5,nmode)*cos(2*pi*t + eigvec_phase(idx(ii)+ 5,nmode))]]; % project using the first yoke node
    end
    % plot modes 
    %yyaxis left
    scale_bending = max(abs(pos(3,6:56)));
    scale_lag = max(abs(pos(2,6:56)));
    p3 =  plot((X0(1,6:56))./1.25,-(pos(3,6:56))./scale_bending,'b-', 'LineWidth',2); % lag    
    hold on
    p4 = plot(X0(1,107:127)./1.25,-(pos(3,107:127))./scale_bending,'r-','LineWidth',2); 
    
%     plot(LHD_station, Mode2_lag, 'r-.', 'LineWidth',2)
%     p2 = plot((X0(1,6:56)),-(pos(3,6:56)),'b-o', 'LineWidth',2); % bending
%     p4 = plot((X0(1,107:127)),-(pos(3,107:127)),'b-o', 'LineWidth',2); % bending
%     
    %plot(LHD_station, Mode2_flap, 'b-.', 'LineWidth',2)
    ylabel('Deflection '); 
    %yyaxis right
    %p3 = plot((pos(1,6:50)),(rad2deg(ori(1,6:50))),'g-v', 'LineWidth',2); % pitch
    %ylabel('Pitch [°]')
    hold off
    grid on
    title('Mode 2 Lag')
    %legend([p1 p2 p3 p4],{'Blade Flightlab', 'TorqueTube Flightlab', 'Blade MBDyn', 'Yoke MBDyn'},'Location','south')
    xlabel('Radial Station')
    xlim([0 0.3])
    %ylim([-0.3 0.05])
    drawnow   
    
end


% get E and A matrix
% if sparse
%     Jp_sp = ncread(nc_out,  'eig.0.Aplus');
%     Jm_sp = ncread(nc_out,  'eig.0.Aminus');
%     Jp = sparse(Jp_sp(1, :), Jp_sp(2, :), Jp_sp(3, :));
%     Jm = sparse(Jm_sp(1, :), Jm_sp(2, :), Jm_sp(3, :));
% else
%     Jp = ncread(nc_out,  'eig.0.Aplus');
%     Jm = ncread(nc_out,  'eig.0.Aminus');
% end
% 
% E = (Jp + Jm) / 2;
% A = (Jm - Jp) / (2 * dCoef);
% 
% % need full matrices
% E = full(E);
% A = full(A);
% 
% Jp = full(Jp);
% Jm = full(Jm);

% get eigenvectors
%VL = ncread(nc_out, 'eig.0.VL');





