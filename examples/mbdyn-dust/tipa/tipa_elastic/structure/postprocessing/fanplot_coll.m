function Table = fanplot_coll(NLR, LHD,MBDyn, ang)

    rpm_rev = [0 120]; 
    rev_1 = [0 17.10374511]; 
    ndrpm = [70:10:120];
    figure('Renderer', 'painters', 'Position', [400 400 800 500])  
    for i = 1:11
        R1  = plot(rpm_rev, i.*rev_1, '--', 'LineWidth',1.5,'Color',[0.7 0.7 0.7]);
        hold on 
    end
    %% Fan plot
    LHD_linestyle = {'*:r', '*:c', '*:m', '*:g', '*:b' };
    MBDYN_linestyle = {'o-r', 'o-c', 'o-m', 'o-g', 'o-b'};
    NLR_linestyle = {'v--r', 'v--c', 'v--m', 'v--g', 'v--b'}; 
    
    for i = 1:5
        N(i) = plot(ndrpm, NLR(i,:), NLR_linestyle{i},'LineWidth',2); 
        hold on 
        L(i) = plot(ndrpm, LHD(i,:), LHD_linestyle{i},'LineWidth',2); 
        M(i) = plot(ndrpm, MBDyn(i,:), MBDYN_linestyle{i}, 'LineWidth', 2); 
        
    end
    xlim([70 120]); 
    ylim([0,120]);         
    xlabel('Rotor Nr [%]')
    ylabel('Frequency [Hz]')
    title(sprintf('Collective  = %d° - Collective Rotor Modes', ang)) 
    hLg = legend([L(1) N(1) M(1) L(2) N(2) M(2) L(3) N(3) M(3) L(4) N(4) M(4) L(5) N(5) M(5)],...
            {'LHD Gimbal', 'NLR Gimbal', 'POLIMI Gimbal',...
            'LHD 1st Lag', 'NLR 1st Lag', 'POLIMI 1st Lag', ...
            'LHD 1st Beam', 'NLR 1st Beam', 'POLIMI 1st Beam', ...
            'LHD Pitch', 'NLR Pitch', 'POLIMI Pitch', ...
            'LHD Torsion', 'NLR Torsion', 'POLIMI Torsion'}, 'Location','eastoutside');
    saveas(gcf, sprintf('FanPlot_Coll_%ddeg',ang), 'png')
    
    for i = 0:numel(ndrpm)-1
        Table(:,i*3+1:i*3+3) = [LHD(:,i+1) NLR(:,i+1) MBDyn(:,i+1)]; 
    end
end