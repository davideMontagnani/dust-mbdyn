# #################################################################
#
# Title         Aerodynamic lag functions, divergence, and the British flutter method
# Type          Journal Article
# Author        William P. Rodden and E. Dean Bellinger
# URL           https://doi.org/10.2514/3.44772
# Volume        19
# Issue         7
# Pages         596-598
# Publication   Journal of Aircraft
# Date          1982
# Publisher     American Institute of Aeronautics and Astronautics
# DOI           10.2514/3.44772
#
# #################################################################
#
# Test case AH145 from NASTRAN's aeroelasticity user manual
#
# #################################################################

# distances taken from leading egde
set: const real m = 64.356;  # 19.624421; # 1.3447 slug
set: const real chord = 1.8288; # 6 ft
set: const real x_EA = 0.4 * chord;
set: const real rho_EA = 0.4572; # 1.5 ft
set: const real J_EA = m * rho_EA * rho_EA;
set: const real x_AE = 0.25 * chord;
set: const real omega_h = 1000.; # rad / s
set: const real omega_theta =  25.; # rad / s
set: const real damp_coeff = 0.00 ;
set: const real g = 0.0;

# debug: x_CG = x_EA
# set: const real x_CG = x_EA;
# 37 % chord mass center
set: const real x_CG = 0.37 * chord;
# 45 % chord mass center
# set: const real x_CG = 0.45 * chord;

set: const real J = J_EA - m * (x_EA - x_CG)^2;
set: const real Kh = omega_h^2 * m;
set: const real Ktheta = omega_theta^2 * J_EA;
set: const real Ch = Kh * damp_coeff / omega_h;
set: const real Ctheta = Ktheta * damp_coeff / omega_theta;

set: const real ALPHA_0 = 2.0*deg2rad;
set: const real Voo = 10.0;

begin: data;
  problem: initial value;
end: data;

begin: initial value;
  initial time: 0.;
  final time: 100.;
  time step: 1.e-3;

  method: ms, .6;
  tolerance: 1.E-6;
  max iterations: 100;

  derivatives coefficient: 1.e-9;
  derivatives tolerance: 1.e-6;
  derivatives max iterations: 100;

  nonlinear solver: newton raphson, modified, 4;
  # solver: umfpack, cc, block size, 2;
  # linear solver: naive, colamd, mt, 1, pivot factor, 1e-6;
  linear solver: umfpack, cc;

  #output: iterations;
  #output: residual;
  output: all;
  
  threads: disable;
end: initial value;

begin: control data;
  structural nodes:
    +1    # ground
    +1    # airfoil
  ;
  rigid bodies:
    +1    # airfoil
  ;
  joints:
    +1    # ground
    +1    # in line
    +1    # revolute rotation
    +1    # linear spring (rod)
    +1    # angular spring (deformable hinge)
  ;
  forces:
    +1
  ;

  default orientation: orientation vector;
  # print: all;

end: control data;

reference: 0,
  reference, global, 0.,0.,1.,
  reference, global, eye,
  reference, global, null,
  reference, global, null;
reference: 1,
  reference, 0, 0.,0.,-1.,
  reference, 0,
    1, cos(ALPHA_0), 0.,sin(ALPHA_0),
    3, 0., -1., 0.,
  reference, 0, null,
  reference, 0, null;

begin: nodes;
  structural: 0, static,
    reference, 0, null,
    reference, 0, eye,
    reference, 0, null,
    reference, 0, null,
    output, no;
  structural: 1, dynamic,
    reference, 1, null,
    reference, 1, eye,
    reference, 1, null,
    reference, 1, null;
end: nodes;

begin: elements;
  joint: 0, clamp, 0, node, node;

  joint: 1, in line,
    0,
      position, reference, 0, null,
      orientation, reference, 0, 
        1, 1., 0., 0.,
        2, 0., 1., 0.,
    1
  ;
  joint: 2, revolute rotation,
    0,
      position, reference, 0, null,
      orientation, reference, 0, 
        1, 1., 0., 0.,
        2, 0., 0., 1.,
    1,
      position, reference, 0, null,
      orientation, reference,  0, 
        1, 1., 0., 0.,
        2, 0., 0., 1.
  ;
  joint: 3, rod,
    0,
      position, reference, node, null,
    1,
      position, reference, node, null,
    1.,
    linear viscoelastic generic, Kh, Ch
  ;
  joint: 4, deformable hinge,
    0,
      position, reference, 0, null,
      orientation, reference, 0, eye,
    1,
      position   , reference, 0, null,
      orientation, reference, 0, eye,
    linear viscoelastic generic,
      Ktheta, 0., 0.,
      0., Ktheta, 0.,
      0., 0., Ktheta,
      Ctheta, 0., 0.,
      0., Ctheta, 0.,
      0., 0., Ctheta
  ;
  
  body: 1, 1,
    m,
    reference, node, x_EA-x_CG, 0., 0.,
    diag, J, J, J;

  force: 1, external structural,
    socket,
      create, yes,
      path, "$MBSOCK",
      no signal,
    coupling, tight, # loose,
    labels, no,
    sorted, yes, # yes,
    orientation, orientation vector,
    accelerations, yes,
    # orientation, orientation matrix, # default
    # orientation, euler 123,
    1,
      1;

end: elements;
