clear all; clc; close all;

f = 1.0;
k = 1.0;
m = 1.0;

tv = 0:0.1:10;

csi = 1.001;
s1 = k/m * ( -csi - sqrt(csi^2-1.) );
s2 = k/m * ( -csi + sqrt(csi^2-1.) );

xfun = @(t) f/k * ( -s2/(s2-s1)*exp(s1*t) + ...
                     s1/(s2-s1)*exp(s2*t) + 1.0 );
xv1 = xfun(tv);

csi = 2.000;
s1 = k/m * ( -csi - sqrt(csi^2-1.) );
s2 = k/m * ( -csi + sqrt(csi^2-1.) );

xfun = @(t) f/k * ( -s2/(s2-s1)*exp(s1*t) + ...
                     s1/(s2-s1)*exp(s2*t) + 1.0 );
xv2 = xfun(tv);

figure; hold on
plot(tv,xv1), grid on
plot(tv,xv2), grid on
