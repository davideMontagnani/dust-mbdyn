
clear all; clc; close all

filen = { ...
%        '../uncoupled/z_rot/Rodden1.mov', ...
%        '../output.mov', ...
%        '../damp_coeff+3/output.mov', ...
%        '../uncoupled/results/pitch/damp_coeff+0.0/Rodden1.mov', ...
%        '../uncoupled/results/pitch/damp_coeff+0.5/Rodden1.mov', ...
         '../uncoupled/results/pitch/damp_coeff+0.5/Rodden1.mov', ...
         '../output.mov', ...
        };
%        '../output.mov.wake_pan+5', ...
dt = [ 1.e-4, 1.e-3, 1.e-4 ];

figure('Position',[100 100 1000 600])
for k = 1 : length(filen)

  dat = dlmread(filen{k},'',0,0);
  
  id= dat(:,1);
  x  = dat(:,2); vx = dat(:, 8);
  y  = dat(:,3); vy = dat(:, 9);
  z  = dat(:,4); vz = dat(:,10);
  tx = dat(:,5); ox = dat(:,11);
  ty = dat(:,6); oy = dat(:,12);
  tz = dat(:,7); oz = dat(:,13);
  t = dt(k) * [ 0:size(x,1)-1];
  
  subplot(2,2,1), plot(t,[ x, y, z], 'LineWidth', 2), grid on, hold on % legend( 'x', 'y', 'z')
  subplot(2,2,2), plot(t,[vx,vy,vz], 'LineWidth', 2), grid on, hold on % legend('vx','vy','vz')
  subplot(2,2,3), plot(t,[tx,ty,tz], 'LineWidth', 2), grid on, hold on % legend('tx','ty','tz')
  subplot(2,2,4), plot(t,[ox,oy,oz], 'LineWidth', 2), grid on, hold on % legend('ox','oy','oz')

end
