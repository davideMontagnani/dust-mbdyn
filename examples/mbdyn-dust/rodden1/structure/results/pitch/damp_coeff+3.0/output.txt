
MBDyn - MultiBody Dynamics develop
configured on Mar 15 2020 at 16:12:55

Copyright 1996-2017 (C) Paolo Mantegazza and Pierangelo Masarati,
Dipartimento di Ingegneria Aerospaziale <http://www.aero.polimi.it/>
Politecnico di Milano                   <http://www.polimi.it/>

MBDyn is free software, covered by the GNU General Public License,
and you are welcome to change it and/or distribute copies of it
under certain conditions.  Use 'mbdyn --license' to see the conditions.
There is absolutely no warranty for MBDyn.  Use "mbdyn --warranty"
for details.

reading from file "Rodden1.mbd"
Creating scalar solver with Umfpack linear solver
Reading Structural(0)
Reading Structural(1)
Reading Joint(0)
Reading Joint(1)
Reading Joint(2)
Reading Joint(3)
Reading Joint(4)
Reading Body(1)
Reading Force(1)
LOCAL connection to path="/tmp/.mbdyn_0no37zr9/mbdyn.sock"
ExtSocketHandler: recv()=0 (expected 1)
MBDyn was interrupted
