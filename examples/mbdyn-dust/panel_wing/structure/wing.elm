
body: 2*curr_elem-1, 2*curr_elem-1,
  (dL/2.)*m,
  null,
  diag, (dL/2.)*j, 1./12.*(dL/2.)^3*m, 1./12.*(dL/2.)^3*m;

body: 2*curr_elem, 2*curr_elem,
  (dL/2.)*m,
  null,
  diag, (dL/2.)*j, 1./12.*(dL/2.)^3*m, 1./12.*(dL/2.)^3*m;

beam3: curr_elem,
  2*curr_elem-2, null,
  2*curr_elem-1, null,
  2*curr_elem  , null,
  eye,
  linear elastic generic,
    diag, EA, GAy, GAz, GJ, EJy, EJz,
  same,
  same;
