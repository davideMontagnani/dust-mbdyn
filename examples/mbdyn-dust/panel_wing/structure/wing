# mass-damper-spring chain

# === data ======================================================
begin: data;
  problem: initial value;
end: data;

# === initial value problem =====================================
begin: initial value;
  initial time: 0.;
  final time: 1000.0;
  time step: 1e-2;

  method: ms, .6;
  linear solver: naive, colamd;
  nonlinear solver: newton raphson, modified, 5;

  tolerance: 1e-6;
  max iterations: 100;

  derivatives coefficient: 1e-6; # 1.e-9;
  derivatives tolerance: 1e-6;
  derivatives max iterations: 10;

  # output: iterations;
  # output: residual;
end: initial value;

# === some variables ============================================
set: integer curr_elem;
set: const integer GROUND = 0;
# --- beam ---
set: integer Nel = 5;
set: real L = 1.;
set: real M =  0.1;
set: real dL = L/Nel;
set: real m = M/L;
set: real j = 1.e-2;
set: real EA  = 1.e+1;  # 1.e3;  # 1.e1;  #  TODO: check beam3 definition
set: real GAy = 1.e+1;  # 1.e3;  # 1.e1;  #  - axis direction
set: real GAz = 1.e+1;  # 1.e3;  # 1.e1;  #  - elastic properties:
set: real GJ  = 1.e-1;  # 1.e1;  # 1.e1;  #    - input order
set: real EJy = 1.e-1;  # 1.e1;  # 1.e1;  #    - implicit defition of directions
set: real EJz = 1.e-1;  # 1.e1; 
# force
set: real F = .0;

# === control data ==============================================
begin: control data;
  structural nodes: +1       # ground
                    +2*Nel;  # N.masses
  rigid bodies:     +2*Nel;  # masses
  joints:           +1;      # ground clamp
  beams:            +Nel;    # beam elems
  forces:         # +1       # absolute force
                    +1;      # external force
end: control data;

# === reference frame ===========================================
reference: GROUND,
  reference, global, null,
  reference, global, eye,
  reference, global, null,
  reference, global, null;

# === nodes =====================================================
begin: nodes;
  structural:  GROUND, dynamic,  # static,
    reference, GROUND, null,
    reference, GROUND, eye,
    reference, GROUND, null,
    reference, GROUND, null,
    output, no;

  set: curr_elem = 1;
  include: "wing.nod";
  set: curr_elem = 2;
  include: "wing.nod";
  set: curr_elem = 3;
  include: "wing.nod";
  set: curr_elem = 4;
  include: "wing.nod";
  set: curr_elem = 5;
  include: "wing.nod";

end: nodes;

# === elements ==================================================
begin: elements;

  #> === Bodies ===
  set: curr_elem = 1;
  include: "wing.elm";
  set: curr_elem = 2;
  include: "wing.elm";
  set: curr_elem = 3;
  include: "wing.elm";
  set: curr_elem = 4;
  include: "wing.elm";
  set: curr_elem = 5;
  include: "wing.elm";

  #> === Joints ===
  joint: GROUND, clamp, GROUND, node, node;

# set: curr_elem = 1;
# include: "wing.joi";
# set: curr_elem = 2;
# include: "wing.joi";

  #> === Forces ===
# force: 2*Nel, absolute,
#   2*Nel,
#   position, null,
#   0.,0.,1.,
#   cosine, 0., pi/2., F/2, half, 0.; # cosine, t0, Omega, Ampl, N.cycles, Init.Value
#   # const, 1.0;

  force: 4, external structural,
    socket,
      create, yes,
      path, "$MBSOCK",
      no signal,
    coupling, tight, # loose,
    # labels, yes,
    sorted, yes,
    orientation, orientation vector,
    accelerations, yes,
    # orientation, orientation matrix, # default
    # orientation, euler 123,
    11,
     0,1,2,3,4,5,6,7,8,9,10 ;
end: elements;

# vim:ft=mbd
