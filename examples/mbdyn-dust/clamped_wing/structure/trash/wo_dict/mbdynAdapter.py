
import numpy as np

import precice
# from precice import *

class MBDynAdapter:

  class Participant:
    def __init__(self, name="MBDyn"):
      """ Do nothing """
      self.name  = name
      self.field = [] # list()

    class Mesh:
      def __init__(self, name="MBDynNodes", id=0):
        self.name = "MBDynNodes"
        self.id   = id
    class Field:
      def __init__(self, name="", id=0, data=[]):
        self.name = name 
        self.id   = id   
        self.data = data 

  def __init__(self, mbdInterface, \
               config_file_name = "./../precice-config.xml" ):

    self.debug = False

    #> Use MBDyn interface, containing info about MBDyn-mbc_py
    # interface
    self.mbd = mbdInterface

    self.code      = self.Participant()
    self.code.mesh = self.code.Mesh()
    # All the data that can be exchanged with MBDyn
    fieldlist = [ "Position", "Velocity", "Rotation", \
                  "AngularVelocity", "Force", "Moment" ]

    #> === PreCICE interface ====================================
    comm_rank = 0; comm_size = 1 
    self.interface = precice.Interface( self.code.name , \
                                        config_file_name, \
                                        comm_rank, comm_size )
    self.dim = self.interface.get_dimensions()
    self.code.mesh.id = self.interface.get_mesh_id( self.code.mesh.name )

    #> === Fields ===============================================
    # get_data_id
    for field in fieldlist:
      if ( self.interface.has_data( field, self.code.mesh.id ) ):
        field_id = self.interface.get_data_id( field, self.code.mesh.id )
        self.code.field.append( self.code.Field( name=field, id=field_id ) )

    #> === Mesh =================================================
    #> Nodes: set_mesh_vertices
    self.code.mesh.node_id = self.interface.set_mesh_vertices( \
                                 self.code.mesh.id, self.mbd.pos )

    #> Connectivity
    #> Edges
    # ...
    #> Elements
    # ...

    if ( self.debug ): # check ----------------------------------
      for i in np.arange(self.mbd.socket.nnodes):
        print('  ', i,': ', node_id[i],' , ', self.mbd.pos[i,:])

    #> === Initialize ===========================================
    self.dt_precice = self.interface.initialize()
    self.is_ongoing = self.interface.is_coupling_ongoing()
    if ( self.debug ): # check ----------------------------------
      print(' interface.is_coupling_ongoing(): ', self.is_ongoing )



  def runPreCICE(self):

    # todo: read it
    dt_set = .001
    pos_id = 0; vel_id = 1; for_id = 2  # hardcoded

    n = self.mbd.socket.nnodes; nd = 3

    cowic = precice.action_write_iteration_checkpoint()
    coric = precice.action_read_iteration_checkpoint()

    dt_precice = self.dt_precice

    t = 0.
    is_ongoing = self.interface.is_coupling_ongoing()
    while ( is_ongoing ):
    
      if ( self.interface.is_action_required( cowic ) ):
        pos_t = self.mbd.pos; vel_t = self.mbd.vel
        self.interface.mark_action_fulfilled( cowic )
     
      # Receive data from MBDyn
      if ( self.mbd.nodal.recv() ):
        print('**** break ****')
        break
    
      #> Receive data from AeroSolver and set nodal.n_f field
      force = self.interface.read_block_vector_data( \
                 for_id, \
                 self.code.mesh.node_id )
      print(' self.code.mesh.id: '); print( self.code.mesh.node_id )
      print(' force: '); print( force ) # ; time.sleep(2.0) 
      for i in np.arange(n):
        self.mbd.nodal.n_f[i*nd:(i+1)*nd] = force[i,:]
        self.mbd.nodal.n_m[i*nd:(i+1)*nd] = .0
    
      #> Read position and velocity form MBDyn
      # print(' Reshape MBDyn arrays: nodal.n_x, .n_xp ')
      self.mbd.pos = np.reshape( self.mbd.nodal.n_x , (n, nd)) 
      self.mbd.vel = np.reshape( self.mbd.nodal.n_xp, (n, nd))
    
      #> Compute position and velocity
      for i in np.arange(n):
        print(i,': ', self.mbd.pos[i,:],', ',self.mbd.vel[i,:])
    
      #> 
      if ( self.mbd.nodal.send(True) ):
        break
    
      #> Write to AeroSolver
      print(' Write_block_vector_data ')
      self.interface.write_block_vector_data( pos_id, self.code.mesh.node_id, self.mbd.pos )
      self.interface.write_block_vector_data( vel_id, self.code.mesh.node_id, self.mbd.vel )
    
      dt = min( dt_set, dt_precice )
    
      is_ongoing = self.interface.is_coupling_ongoing()
      if ( is_ongoing ):
        dt_precice = self.interface.advance(dt)
      else:
        break
    
      if ( self.interface.is_action_required( coric ) ): # dt not converged
        self.mbd.pos = pos_t;  self.mbd.vel = vel_t    # reload state
        self.interface.mark_action_fulfilled( coric )
      else: # dt converged
        t = t + dt
    
    print(' before finalize() ')
    self.interface.finalize()
    print(" Finalize auxiliary coupling ")
    
    #> ~~~ PreCICE coupling between mbdyn and aero solver ~~~~~~~~~~~
    
    self.mbd.nodal.destroy();
    # os.rmdir(tmpdir);

