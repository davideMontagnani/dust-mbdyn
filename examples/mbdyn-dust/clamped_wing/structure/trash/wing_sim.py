
import time
import sys;
# set to path of MBDyn support for python communication
sys.path.append('/usr/local/mbdyn/libexec/mbpy');

import os;
import tempfile;
tmpdir = tempfile.mkdtemp('', '.mbdyn_');
path = tmpdir + '/mbdyn.sock';

os.environ['MBSOCK'] = path;
#os.system('mbdyn -f wing -o output > output.txt 2>&1 &');
os.system('./mbdyn.sh -f wing -o output > output.txt 2>&1 &');

from mbc_py_interface import mbcNodal
from numpy import *
import numpy as np

import precice
from precice import *

#> === MBDyn simulation parameters ==============================
dt_set = .001   # dt
nEl = 4;  L   = 1.   ; EJ  = 10.; dL  = L/nEl  # beam
q   = 1.; Om  = pi/2.; A   = .5  # load
#> Constant chord and AOA of the wing
chord = .1 ; twist = 2. * np.pi / 180.

#> === Communication MBDyn-mbc_py ===============================
host = "";
port = 0;
timeout = -1;    # forever
verbose = 0;
data_and_next = 1;
refnode = 0;      # no reference node
nnodes = nEl*2;   # number of nnodes
labels = 0;
rot = 0x100;      # orientation vector
# rot = 0x200;    # orientation matrix
# rot = 0x400;    # Euler 123
accels = 1;

#> Initialize nodal obj and establish first communication =======
nodal = mbcNodal(path, host, port, timeout, verbose, data_and_next, \
                 refnode, nnodes, labels, rot, accels)
nodal.negotiate(); nodal.recv()

#> ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#> Write auxiliary file
#> number of nodes exposed by MBDyn through external structural
nd = 3
n = np.size(nodal.n_x,0) // nd
if ( np.size(nodal.n_x,0) % nd != 0 ):
  sys.exit("\n Inconsistent dimensions. Stop \n")

filen = "./../nnodes.dat"
fid = open(filen, "w")
fid.write("%d" % (n))
fid.close()
#> ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


#> ~~~ PreCICE-aux communication: start ~~~~~~~~~~~~~~~~~~~~~~~~~
#> Precice params: communication
comm_rank = 0; comm_size = 1
config_file_name = "./../precice-config-aux.xml"
solver_name   = "MBDyn"
mesh_name_mbd = "AuxilMBDynMesh"
# mesh_name_flu = "AuxilFluidMesh"
pos_name      = "Position"; rot_name    = "Rotation"
cho_name      = "Chord"   ; twi_name    = "Twist"
dum_name      = "Dummy"

print(" Configure preCICE-aux ...", end='')
interface = precice.Interface( solver_name, config_file_name, \
                               comm_rank, comm_size )
dimensions  = interface.get_dimensions(); nd = dimensions
mesh_id_mbd = interface.get_mesh_id( mesh_name_mbd )
pos_id    = interface.get_data_id( pos_name, mesh_id_mbd )
rot_id    = interface.get_data_id( rot_name, mesh_id_mbd )
cho_id    = interface.get_data_id( cho_name, mesh_id_mbd )
twi_id    = interface.get_data_id( twi_name, mesh_id_mbd )
dum_id    = interface.get_data_id( dum_name, mesh_id_mbd )
print(" done. \n")

rr_mbd = np.zeros((n, nd))
for i in np.arange(n):    # todo with reshape
  rr_mbd[i,:] = np.array([ i, 0., 0. ]) # nodal.n_x[i*nd:(i+1)*nd]
node_id_mbd = interface.set_mesh_vertices( mesh_id_mbd, rr_mbd )

#> Fields
pos = np.ones((n, nd)); cho = np.ones((n))
rot = np.ones((n, nd)); twi = np.ones((n))

for i in np.arange(n):
  pos[i,:] = nodal.n_x[i*nd:(i+1)*nd]
  rot[i,:] = nodal.n_theta[i*nd:(i+1)*nd]  # check !
  cho[i  ] = chord
  twi[i  ] = twist

print(' rr_mbd:', rr_mbd)
print(' pos   :', pos)
print(' rot   :', rot)

dum = np.ones((n)) # dummy field read from fluidSolver

dt_precice = interface.initialize()
is_ongoing = interface.is_coupling_ongoing()
print(" is_ongoing: ", is_ongoing)

# cowic = interface.

while ( is_ongoing ):

  interface.write_block_vector_data( pos_id, node_id_mbd, pos ); print(' pos: ', pos)
  interface.write_block_vector_data( rot_id, node_id_mbd, rot ); print(' rot: ', rot)
  interface.write_block_scalar_data( cho_id, node_id_mbd, cho ); print(' cho: ', cho)
  interface.write_block_scalar_data( twi_id, node_id_mbd, twi ); print(' twi: ', twi)

  dum = interface.read_block_scalar_data( dum_id, node_id_mbd ); print(' dum: ', dum)

  dt_precice = interface.advance(dt_precice)
  is_ongoing = interface.is_coupling_ongoing()

interface.finalize()
print(" Finalize auxiliary coupling ")

#> ~~~ PreCICE-aux communication: end ~~~~~~~~~~~~~~~~~~~~~~~~~~~

print(" Build structural model ")
rr = pos

#> ~~~ PreCICE coupling between mbdyn and aero solver ~~~~~~~~~~~
print(" Start preCICE coupling between solvers ")
#> === Precice parameters ===
comm_rank = 0; comm_size = 1
config_file_name = "./../precice-config.xml"
solver_name = "MBDyn"                               # Participant
mesh_name   = "MBDynNodes"                          # Mesh
pos_name = "Position"; vel_name = "Velocity"        # Fields
for_name = "Force"

print(" +++++ ")
interface = precice.Interface( solver_name, config_file_name, \
                               comm_rank, comm_size )
print(" +++++ ")
dimensions  = interface.get_dimensions(); nd = dimensions
mesh_id = interface.get_mesh_id( mesh_name )

pos_id  = interface.get_data_id( pos_name, mesh_id )
vel_id  = interface.get_data_id( vel_name, mesh_id )
for_id  = interface.get_data_id( for_name, mesh_id )

node_id = interface.set_mesh_vertices( mesh_id, rr )
print(' node_id:', node_id)
print(' rr     :', rr)

#> === MBDYN: Read node position and velocit form MBDyn =========
dummy = np.zeros((n, nd))
pos   = reshape( nodal.n_x , (n, nd)) 
vel   = reshape( nodal.n_xp, (n, nd))
force = dummy              # to be read from AeroSolver

nodal.n_f[:] = 0.
nodal.n_m[:] = 0.
nodal.send(True)

dt_precice = interface.initialize()
is_ongoing = interface.is_coupling_ongoing()
print(" is_ongoing: ", is_ongoing)

cowic = precice.action_write_iteration_checkpoint()
coric = precice.action_read_iteration_checkpoint()

t = 0.
while ( is_ongoing ):

  if ( interface.is_action_required( cowic ) ):
    pos_t = pos; vel_t = vel
    interface.mark_action_fulfilled( cowic )
 
  # Receive data from MBDyn
  if ( nodal.recv() ):
    print('**** break ****')
    break

  #> Receive data from AeroSolver and set nodal.n_f field
  force = interface.read_block_vector_data( for_id, node_id )
  print(' force: '); print( force ) # ; time.sleep(2.0) 
  for i in np.arange(n):
    nodal.n_f[i*nd:(i+1)*nd] = force[i,:]
    nodal.n_m[i*nd:(i+1)*nd] = .0

  #> Read position and velocity form MBDyn
  print(' Reshape MBDyn arrays: nodal.n_x, .n_xp ')
  pos   = reshape( nodal.n_x , (n, nd)) 
  vel   = reshape( nodal.n_xp, (n, nd))

  #> Compute position and velocity
  for i in np.arange(n):
    print(i,': ', pos[i,:],', ',vel[i,:])

  #> 
  if ( nodal.send(True) ):
    break

  #> Write to AeroSolver
  print(' Write_block_vector_data ')
  interface.write_block_vector_data( pos_id, node_id, pos )
  interface.write_block_vector_data( vel_id, node_id, vel )

  dt = min( dt_set, dt_precice )

  is_ongoing = interface.is_coupling_ongoing()
  if ( is_ongoing ):
    dt_precice = interface.advance(dt)
  else:
    break

  if ( interface.is_action_required( coric ) ): # dt not converged
    pos = pos_t; vel = vel_t    # reload state
    interface.mark_action_fulfilled( coric )
  else: # dt converged
    t = t + dt

print(' before finalize() ')
interface.finalize()
print(" Finalize auxiliary coupling ")

#> ~~~ PreCICE coupling between mbdyn and aero solver ~~~~~~~~~~~

nodal.destroy();
os.rmdir(tmpdir);

