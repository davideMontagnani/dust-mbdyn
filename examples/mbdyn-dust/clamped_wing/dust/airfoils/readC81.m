close all ; clear all % ; clc

w = warning ('off','all');

header_nline = 6 ;
filen = { ...
   './propfoil1.c81' , ...
   './propfoil2.c81' , ...
   './propfoil3.c81' , ...
   } ;

Re = [ 750000 , 1100000 , 1000000 ] ;

nMach  = [  9 , 10 , 10 ] ;
nAlpha = [ 64 , 64 , 64 ] ;
for i = 1 : length(filen) 
  dat = dlmread(filen{i},'', header_nline ,0 ) ;
  propfoil(i).mach = dat(1,1:end-1) ;
  propfoil(i).al = dat(2:nAlpha(i)+1,1) ;
  propfoil(i).cl = dat(2:nAlpha(i)+1,2:end) ;
  propfoil(i).cd = dat(nAlpha(i)+3:2*nAlpha(i)+2,2:end) ;
  propfoil(i).cm = dat(2*nAlpha(i)+4:3*nAlpha(i)+3,2:end) ;

end


col = lines ; % 7 different colors 
co = jet ;
co = co(1:10:end,:) ;
%  [ ...
%   0.0 , 0.0 , 0.0 ; ...
%   0.5 , 0.5 , 0.5 ; ...
%    ] ;
set(groot,'defaultAxesColorOrder',co) ;

nMach_plot = 7 ;
figure('Position',[ 30 30 1200 400 ])
subplot(1,3,1) , plot(propfoil(1).al,propfoil(1).cl(:,1:nMach_plot)) ; title(' CL - prop1 '), axis([-40 40 -1.6 1.6]), legend(num2str(propfoil(1).mach(:)),'Location','Best') ; hold on
subplot(1,3,1) , plot(propfoil(1).al,propfoil(1).cl(:,1),'LineWidth',2) ; hold off 
subplot(1,3,2) , plot(propfoil(2).al,propfoil(2).cl(:,1:nMach_plot)) ; title(' CL - prop2 '), axis([-40 40 -1.6 1.6]), legend(num2str(propfoil(2).mach(:)),'Location','Best') ; hold on
subplot(1,3,2) , plot(propfoil(2).al,propfoil(2).cl(:,5),'LineWidth',2) ; hold off 
subplot(1,3,3) , plot(propfoil(3).al,propfoil(3).cl(:,1:nMach_plot)) ; title(' CL - prop3 '), axis([-40 40 -1.6 1.6]), legend(num2str(propfoil(3).mach(:)),'Location','Best') ; hold on
subplot(1,3,3) , plot(propfoil(3).al,propfoil(3).cl(:,6),'LineWidth',2) ; hold off 

figure('Position',[ 30 30 1200 400 ])
subplot(1,3,1) , plot(propfoil(1).al,propfoil(1).cd(:,1:nMach_plot)) ; title(' CD - prop1 '), axis([-40 40 0.0 1.0]), legend(num2str(propfoil(1).mach(:)),'Location','Best') ; hold on
subplot(1,3,1) , plot(propfoil(1).al,propfoil(1).cd(:,1),'LineWidth',2) ; hold off 
subplot(1,3,2) , plot(propfoil(2).al,propfoil(2).cd(:,1:nMach_plot)) ; title(' CD - prop2 '), axis([-40 40 0.0 1.0]), legend(num2str(propfoil(2).mach(:)),'Location','Best') ; hold on
subplot(1,3,2) , plot(propfoil(1).al,propfoil(2).cd(:,5),'LineWidth',2) ; hold off 
subplot(1,3,3) , plot(propfoil(3).al,propfoil(3).cd(:,1:nMach_plot)) ; title(' CD - prop3 '), axis([-40 40 0.0 1.0]), legend(num2str(propfoil(3).mach(:)),'Location','Best') ; hold on
subplot(1,3,3) , plot(propfoil(1).al,propfoil(3).cd(:,6),'LineWidth',2) ; hold off 

figure('Position',[ 30 30 1200 400 ])
subplot(1,3,1) , plot(propfoil(1).al,propfoil(1).cm(:,1:nMach_plot)) ; title(' CM - prop1 '), axis([-180 180 -0.5 0.5]), legend(num2str(propfoil(1).mach(:)),'Location','Best')
subplot(1,3,2) , plot(propfoil(2).al,propfoil(2).cm(:,1:nMach_plot)) ; title(' CM - prop2 '), axis([-180 180 -0.5 0.5]), legend(num2str(propfoil(2).mach(:)),'Location','Best')
subplot(1,3,3) , plot(propfoil(3).al,propfoil(3).cm(:,1:nMach_plot)) ; title(' CM - prop3 '), axis([-180 180 -0.5 0.5]), legend(num2str(propfoil(3).mach(:)),'Location','Best')

