close all ; clear all ; clc

% w = warning ('off','all');

% === Aerodynamic coefficient corrections to take into account Re influence ===
ReC81     =  750000 ; Re2205rpm =  400000 ; % 448000  propfoil1.c81
% ReC81     = 1100000 ; Re2205rpm = 1600000 ; % 448000  propfoil2.c81
% ReC81     = 1000000 ; Re2205rpm = 1100000 ; % 448000  propfoil3.c81

n = 0.2 ; % exponential factor in the correction K = (Re/Re_table)^n

% === Read files ===
header_nline = 6 ;
filen = { ...
   './propfoil1.c81' , ...
  } ;

nMach =  9 ; nAlpha = 64 ;

i = 1 ;
dat = dlmread(filen{i}, '', header_nline, 0 ) ;

propfoil(i).mach = dat(1,1:end-1) ;
propfoil(i).al = dat(2:nAlpha(i)+1,1) ;
propfoil(i).cl = dat(2:nAlpha(i)+1,2:end) ;
propfoil(i).cd = dat(nAlpha(i)+3:2*nAlpha(i)+2,2:end) ;
propfoil(i).cm = dat(2*nAlpha(i)+4:3*nAlpha(i)+3,2:end) ;

% find alpha 0 
alMin = -5.0 ; alMax = 5.0 ; % min and max values of the range where al0 is expected
for iMa = 1 : nMach
  for iAlpha = 1 : nAlpha
     if ( ( propfoil(i).al(iAlpha) >= alMin ) && ...
          ( propfoil(i).al(iAlpha) <  alMax ) ) 
       if ( propfoil(i).cl(iAlpha) * propfoil(i).cl(iAlpha+1) < 0 ) 

         al1 = propfoil(i).al(iAlpha)     ; al2 = propfoil(i).al(iAlpha+1) ;
         cl1 = propfoil(i).cl(iAlpha,iMa) ; cl2 = propfoil(i).cl(iAlpha+1,iMa) ;
         propfoil(i).al0(iMa) = al1 + (al2-al1) * (-cl1)/(cl2-cl1) ;  
         propfoil(i).i_al0(iMa) = iAlpha ;

       end
     end
  end
end

K = ( Re2205rpm / ReC81 )^n ;
clt = zeros(size(propfoil(i).cl,1),size(propfoil(i).cl,2)) ;
for iMa = 1 : nMach

  % find 4 angles ---
  iclmax = propfoil(i).i_al0(iMa) ; iclmin = iclmax ;

  ip = 1 ;
  while ( ip == 1 )
    if ( propfoil(i).cl(iclmax+1,iMa) > propfoil(i).cl(iclmax,iMa) )
      iclmax = iclmax + 1 ;
    else
      ip = 0 ;
    end
  end

  ip = 1 ;
  while ( ip == 1 )
    if ( propfoil(i).cl(iclmin-1,iMa) < propfoil(i).cl(iclmin,iMa) )
      iclmin = iclmin - 1 ;
    else
      ip = 0 ;
    end
  end

  iclp20 = iclmax ; iclm20 = iclmin ;
  ip = 1 ;
  while ( ip == 1 )
    if ( propfoil(i).cl(iclp20+1,iMa) < propfoil(i).cl(iclp20,iMa) )
      iclp20 = iclp20 + 1 ; 
    else
      ip = 0 ;
    end
  end

  ip = 1 ;
  while ( ip == 1 )
    if ( propfoil(i).cl(iclm20-1,iMa) > propfoil(i).cl(iclm20,iMa) )
      iclm20 = iclm20 - 1 ;
    else
      ip = 0 ;
    end
  end

  [ iclm20 , iclmin , iclmax , iclp20 ]

  for iAl = 1 : nAlpha

    dAl = propfoil(i).al(iAl) - propfoil(i).al0(iMa) ;

    if ( iAl <= iclm20  )
      clt(iAl,iMa) = propfoil(i).cl(iAl,iMa) ;
    elseif ( iAl <= iclmin  ) 
      clt(iAl,iMa) = ( propfoil(i).al(iclm20) - propfoil(i).al(iAl) ) / ...
                     ( propfoil(i).al(iclm20) - propfoil(i).al(iclmin) ) * ...
              K * interp1(propfoil(i).al, propfoil(i).cl(:,iMa), dAl/K+propfoil(i).al0(iMa) ) + ...
                     ( propfoil(i).al(iAl   ) - propfoil(i).al(iclmin) ) / ...
                     ( propfoil(i).al(iclm20) - propfoil(i).al(iclmin) ) * ...
              propfoil(i).cl(iAl,iMa) ;
    elseif ( iAl <= iclmax  ) 
      clt(iAl,iMa) = K * interp1(propfoil(i).al, propfoil(i).cl(:,iMa), dAl/K+propfoil(i).al0(iMa) ) ;
    elseif ( iAl <= iclp20 )
      clt(iAl,iMa) = ( propfoil(i).al(iclp20) - propfoil(i).al(iAl) ) / ...
                     ( propfoil(i).al(iclp20) - propfoil(i).al(iclmax) ) * ... 
              K * interp1(propfoil(i).al, propfoil(i).cl(:,iMa), dAl/K+propfoil(i).al0(iMa) ) + ...
                     ( propfoil(i).al(iAl   ) - propfoil(i).al(iclmax) ) / ...
                     ( propfoil(i).al(iclp20) - propfoil(i).al(iclmax) ) * ... 
              propfoil(i).cl(iAl,iMa) ;
    else
      clt(iAl,iMa) = propfoil(i).cl(iAl,iMa) ;
    end

  end

end

for i = 1 : nMach
  figure(i)
  plot(propfoil.al,propfoil.cl(:,i),'--o','LineWidth',2) ; hold on
  title(num2str(propfoil(1).mach(i)))
  % legend(num2str(propfoil.mach(:)))
  plot(propfoil.al,clt(:,i),'-o')
  hold off
end






