
clear all; clc; close all

%> === Wing ===
%> Set airfoil: naca0012
airfoil.id           = 1 ;
airfoil.airfoil_str  = 'NACA0012' ;
airfoil.chord        = 1.0 ;
airfoil.theta        = deg2rad(0.0) ;
airfoil.xcRefPoint   = 0.25 ;
airfoil.refPoint     = [ 0.0 ; 0.0 ] ;
airfoil.nChordPanels = 30 ;

[ ee , rr2 , ee_te , elems , nelems , npoints ] = build_geometry( airfoil ) ;

%> Spanwise surface discretization
y0 = 0.; y1 = 6.; nyp = 15;   % 7, 13, 19
yv = linspace( y0, y1, nyp );

rr = [];
for iy = 1 : nyp
  rrc = [ rr2(1,:); yv(iy)*ones(1,size(rr2,2)); rr2(2,:) ];
  rr = [ rr , rrc ];
end
%> Number of points
np = size(rr,2);

%> === Structure ===
%> Beam
rb0 = [ 0.25; y0; 0.0 ];
rb1 = [ 0.25; y1; 0.0 ];
nspanb = 7;

rrb = rb0 + ( rb1 - rb0 ) * linspace( 0., 1., nspanb );

npb = size(rrb,2);

%> === Interface Weight ===
%> Norm weigth matrix
% %> 1. Identity
% Wnorm = eye(3);
%> 2. Diagonal matrix
wx = 0.001; wy = 1.; wz = wx;
Wnorm = diag([wx, wy, wz]);
%> Interpolation parameters
w_order = 1;
nw_max = 2;      % max. n of relevant weights

wei = zeros(nw_max,np);
ind = zeros(nw_max,np);

for ip = 1 : np

  diff_all = rr(:,ip)*ones(1,npb) - rrb;
  dist_all = sqrt(diag( diff_all' * Wnorm * diff_all )) ;

  [ dist_all, ib ] = sort(dist_all);
  dist = dist_all(1:nw_max);

  v = 1. ./ dist.^w_order;
  v = v ./ sum(v);

  wei(:,ip) = v;
  ind(:,ip) = ib(1:nw_max) ;

end

%> === Current configuration ===
%> Structural nodes
%> Bending (parabolic shape): position ppb, and rotation vector ttb
a2 = 0.10;
ppb = [ rrb(1,:); rrb(2,:); a2 * rrb(2,:).^2 ];
ttb = [ atan(2. * a2 * rrb(2,:)); zeros(2,npb) ];

pp = zeros(size(rr));
for ip  = 1 : np

  for iw = 1 : nw_max

    %> Particular case
    thx = ttb(1,ind(iw,ip));
    Rot = [    1. ,      0. ,      0.  ; ...
               0. , cos(thx),-sin(thx) ; ...
               0. , sin(thx), cos(thx) ] ;

    % === From structural nodes to aerodynamic nodes ===
    %> Position
    pp(:,ip) = pp(:,ip) + wei(iw,ip) * ( ...
      ppb(:,ind(iw,ip)) + Rot * ( rr(:,ip) - rrb(:,ind(iw,ip)) ) );
    %> Velocity
    % ... vv += wei * ( cross( omb, pp-ppb ) ) ...
    % === From aerodynamic nodes to structural nodes ===
    %> Force
    % ... ffb += wei * ff ...
    %> Moments
    % ... mmb += wei * ( ff + cross( pp-ppb, ff ) ) ...

  end

end

%> Reference and actual configurations
figure; hold on
plot3(rr (1,:), rr (2,:), rr (3,:), 'b-', 'LineWidth', 2)
plot3(rrb(1,:), rrb(2,:), rrb(3,:), 'r-o', 'LineWidth', 2)
plot3(ppb(1,:), ppb(2,:), ppb(3,:), 'r-o', 'LineWidth', 2)
plot3(pp (1,:), pp (2,:), pp (3,:), 'b-', 'LineWidth', 2)
axis equal, grid on
