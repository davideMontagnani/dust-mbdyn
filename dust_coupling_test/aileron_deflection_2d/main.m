
clear all; clc; close all

%> === Wing ===
%> Set airfoil: naca0012
airfoil.id           = 1 ;
airfoil.airfoil_str  = 'NACA0012' ;
airfoil.chord        = 1.0 ;
airfoil.theta        = deg2rad(0.0) ;
airfoil.xcRefPoint   = 0.25 ;
airfoil.refPoint     = [ 0.0 ; 0.0 ] ;
airfoil.nChordPanels = 15 ;

airfoil.hinge.pos    = [ .50 ; .0 ];
airfoil.hinge.u      =  .10 ;
airfoil.hinge.off    =  airfoil.hinge.u;
airfoil.hinge.off_s  = -airfoil.hinge.u;
airfoil.hinge.off_dir= [ 1.; 0.];
airfoil.hinge.rot    = deg2rad(-30.);

rfac = 0.4;

%> Reference configuration
[ ee , rr0 , ee_te , elems , nelems , npoints ] = build_geometry( airfoil ) ;
np = size(rr0,2); % number of points

%> Actual configuration, w/ aileron deflection
rr = rr0 ;
for ip = 1 : np

  d = ( rr(:,ip) - airfoil.hinge.pos )' * airfoil.hinge.off_dir ;
  if ( d >= airfoil.hinge.off )

    th  = airfoil.hinge.rot;
    Rot = [ cos(th), -sin(th) ; ...
            sin(th),  cos(th) ] ;
    rr(:,ip) = airfoil.hinge.pos +  Rot * ( rr(:,ip) - airfoil.hinge.pos );
  elseif ( d >= airfoil.hinge.off_s )

    if ( th ~= 0. )
      th = airfoil.hinge.rot;
      u  = airfoil.hinge.u;
      m = - cos(th)/sin(th);
      yc = -m*u*(1.+cos(th)) + u*sin(th);
      thp = 0.5*(d+u)/u*th ;
      rr(:,ip) = [ yc * sin(thp) - u + airfoil.hinge.pos(1) - rr(2,ip)*sin( thp ) ; ...
                   yc * ( 1.-cos(thp) )                     + rr(2,ip)*cos( thp )  ];

    else
      % do nothing
    end

  end
end

figure; hold on
plot(rr0(1,:), rr0(2,:),  '-o', 'LineWidth', 2)
plot(rr (1,:), rr (2,:), 'r-o', 'LineWidth', 2)
plot(airfoil.hinge.pos(1), ...
     airfoil.hinge.pos(2), 'ko', 'MarkerSize', 10)
axis equal, grid on
