
clear all; clc; close all

%> === Wing ===
%> Set airfoil: naca0012
airfoil.id           = 1 ;
airfoil.airfoil_str  = 'NACA0012' ;
airfoil.chord        = 1.0 ;
airfoil.theta        = deg2rad(0.0) ;
airfoil.xcRefPoint   = 0.25 ;
airfoil.refPoint     = [ 0.0 ; 0.0 ] ;
airfoil.nChordPanels = 40 ;

airfoil.hinge.pos    = [ .50 ; .0 ];
airfoil.hinge.off    =  .10 ;
airfoil.hinge.off_s  = -.10 ;
airfoil.hinge.off_dir= [ 1.; 0.];
airfoil.hinge.rot    = deg2rad(60.);

rfac = 0.4;

%> Reference configuration
[ ee , rr0 , ee_te , elems , nelems , npoints ] = build_geometry( airfoil ) ;
np = size(rr0,2); % number of points

%> Actual configuration, w/ aileron deflection
rr = rr0 ;
for ip = 1 : np

  d = ( rr(:,ip) - airfoil.hinge.pos )' * airfoil.hinge.off_dir ;
  if ( d >= airfoil.hinge.off )

    th  = airfoil.hinge.rot;
    Rot = [ cos(th), -sin(th) ; ...
            sin(th),  cos(th) ] ;
    rr(:,ip) = airfoil.hinge.pos +  Rot * ( rr(:,ip) - airfoil.hinge.pos );
  elseif ( d >= airfoil.hinge.off_s )
    th  = airfoil.hinge.rot * ( d - airfoil.hinge.off_s ) / ...
              ( airfoil.hinge.off - airfoil.hinge.off_s ) ;
    Rot = [ cos(th), -sin(th) ; ...
            sin(th),  cos(th) ] ;

    %> Some naif correction for regularization
    eta = - rfac * 4. / ( airfoil.hinge.off - airfoil.hinge.off_s )^2 * ...
          ( rr(1,ip) - airfoil.hinge.pos(1) - airfoil.hinge.off ) * ...
          ( rr(1,ip) - airfoil.hinge.pos(1) - airfoil.hinge.off_s ) ...
          + 1.;

    yp = rr(2,ip);
    rr(:,ip) = airfoil.hinge.pos +  Rot * ( rr(:,ip) - airfoil.hinge.pos ) ;
    % rr(2,ip) = 2*yp - rr(2,ip) ;

  end
end

figure; hold on
plot(rr0(1,:), rr0(2,:),  '-o', 'LineWidth', 2)
plot(rr (1,:), rr (2,:), 'r-o', 'LineWidth', 2)
plot(airfoil.hinge.pos(1), ...
     airfoil.hinge.pos(2), 'ko', 'MarkerSize', 10)
axis equal, grid on
