
function hinge = build_hinge_connectivity( hinge_in, wing )

check_io = 1;

hinge = hinge_in;

%> Some general quantities
dh  = hinge.h' * ( hinge.ref.rr2 - hinge.ref.rr1 ); % hinge span
np = size(wing.surf.ref.rr,2); % n.points of the wing surface
% Weights
nw_max = 2; % n.max weights for each node
w_order = 10;

% Hinge referece frame
hinge.n = cross( hinge.vs, hinge.h ); hinge.n = hinge.n / norm(hinge.n);
hinge.v = cross( hinge.h , hinge.n );

% Surface coordinates in the hinge reference frame
Rot = [ hinge.v, hinge.h, hinge.n ]';
rrb = Rot * ( wing.surf.ref.rr - hinge.ref.rr1 );
rrh = Rot * (     hinge.ref.rr - hinge.ref.rr1 );

if ( check_io ) % check ---
  figure; hold on
  plot3(rrb(1,:), rrb(2,:), rrb(3,:) )
  plot3(rrh(1,:), rrh(2,:), rrh(3,:), '-o', 'LineWidth', 2)
  axis equal; grid on
end% check ---

%> Allocate .rot.node_id, .ble.node_id (id of the nodes performing
% rigid rotation around the hinge and nodes in the blending region)
hinge.rot.node_id = []; hinge.rot.wei = []; hinge.rot.ind = [];
hinge.ble.node_id = []; hinge.ble.wei = []; hinge.ble.ind = [];
for ip = 1 : hinge.np
  hinge.rot.p(ip).p2h = [];  hinge.ble.p(ip).p2h = [];
  hinge.rot.p(ip).w2h = [];  hinge.ble.p(ip).w2h = [];
end

for ip = 1 : np

  if ( ( rrb(2,ip) >= 0. ) && ( rrb(2,ip) <= dh ) )
    if ( rrb(1,ip) < -hinge.u )
      % do nothing
    else
      if     ( rrb(1,ip) >=  hinge.u ) % Rigid rotation
        %> Node id
        hinge.rot.node_id = [ hinge.rot.node_id, ip ];

        %> Node weight
        diff_all = rrb(:,ip)*ones(1,hinge.np) - rrh;
        dist_all = abs(diff_all(2,:)) ;
      
        [ dist_all, ib ] = sort(dist_all);
        dist = dist_all(1:nw_max);
      
        v = 1. ./ dist.^w_order;
        v = v ./ sum(v);
      
        hinge.rot.wei = [ hinge.rot.wei , v'            ] ;
        hinge.rot.ind = [ hinge.rot.ind , ib(1:nw_max)' ] ;
        for iw = 1:nw_max
          hinge.rot.p(ib(iw)).p2h = [ hinge.rot.p(ib(iw)).p2h , ip    ];
          hinge.rot.p(ib(iw)).w2h = [ hinge.rot.p(ib(iw)).w2h , v(iw) ];
        end

      elseif ( rrb(1,ip) >= -hinge.u ) % Blending region
        %> Node id
        hinge.ble.node_id = [ hinge.ble.node_id, ip ];

        %> Node weight
        diff_all = rrb(:,ip)*ones(1,hinge.np) - rrh;
        dist_all = abs(diff_all(2,:)) ;
      
        [ dist_all, ib ] = sort(dist_all);
        dist = dist_all(1:nw_max);
      
        v = 1. ./ dist.^w_order;
        v = v ./ sum(v);
      
        hinge.ble.wei = [ hinge.ble.wei , v'            ] ;
        hinge.ble.ind = [ hinge.ble.ind , ib(1:nw_max)' ] ;
        for iw = 1:nw_max
          hinge.ble.p(ib(iw)).p2h = [ hinge.ble.p(ib(iw)).p2h , ip    ];
          hinge.ble.p(ib(iw)).w2h = [ hinge.ble.p(ib(iw)).w2h , v(iw) ];
        end

      end
    end
  end

end
