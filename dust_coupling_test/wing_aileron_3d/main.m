
clear all; clc; close all

%> === Wing ===
%> Set airfoil: naca0012
airfoil.id           = 1 ;
airfoil.airfoil_str  = 'NACA0012' ;
airfoil.chord        = 1.0 ;
airfoil.theta        = deg2rad(0.0) ;
airfoil.xcRefPoint   = 0.25 ;
airfoil.refPoint     = [ 0.0 ; 0.0 ] ;
airfoil.nChordPanels = 30 ;

[ ee , rr2 , ee_te , elems , nelems , npoints ] = build_geometry( airfoil ) ;

%> Wing
%> Spanwise discretization
y0 = 0.; y1 = 6.; nyp = 15;   % 7, 13, 19
yv = linspace( y0, y1, nyp );
%> Linear chord (tapering), and sweep
c0 = 3.0; c1 = 1.0;
sweep = deg2rad(30);
cv = c0 + ( yv - y0 )/( y1 - y0 ) * ( c1 - c0 );
sv = yv * tan(sweep);

rr = [];
for iy = 1 : nyp
  rrc = [ sv(iy) + cv(iy) * rr2(1,:); ...
          yv(iy)*ones(1,size(rr2,2)); ...
          cv(iy) * rr2(2,:) ];
  rr = [ rr , rrc ];
end
%> Number of points
np = size(rr,2);

%> Build connectivity for patch plot
nel_c = airfoil.nChordPanels;
nel_s = nyp-1;
nel   = 2 * nel_c * nel_s;
ee = zeros(4, nel);
for is = 1 : nyp-1
  ee(1, 1+(is-1)*2*nel_c:is*2*nel_c) = 1+(is-1)*(2*nel_c+1): is   *(2*nel_c+1)-1;
  ee(2, 1+(is-1)*2*nel_c:is*2*nel_c) = 2+(is-1)*(2*nel_c+1): is   *(2*nel_c+1)  ;
  ee(3, 1+(is-1)*2*nel_c:is*2*nel_c) = 2+(is  )*(2*nel_c+1):(is+1)*(2*nel_c+1)  ;
  ee(4, 1+(is-1)*2*nel_c:is*2*nel_c) = 1+(is  )*(2*nel_c+1):(is+1)*(2*nel_c+1)-1;
end

% figure
% patch('Faces',ee','Vertices',rr','FaceColor','None','LineWidth',1.5)
% grid on; axis equal
% stop

%> === Structure ===
%> Beam
rb0 = [ 0.75                    ; y0; 0.0 ];
rb1 = [ 0.75+y1*tan(sweep)*0.85 ; y1; 0.0 ];
nspanb = 7;

rrb = rb0 + ( rb1 - rb0 ) * linspace( 0., 1., nspanb );

npb = size(rrb,2);

% === Wing object ===============================================
wing.surf.ee     = ee;
wing.surf.ref.rr = rr;
wing.surf.act.rr = zeros(size(rr));
wing.str.beam.ref.rr = rrb;
%> Hinges
wing.str.n_hinges = 1;
%> Hinge #1
hinge(1).ref.rr1 = [ 2.7 ; 2.0; 0. ];
hinge(1).ref.rr2 = [ 3.3 ; 4.2; 0. ];
hinge(1).np = 5;
hinge(1).ref.rr = hinge(1).ref.rr1 + ...
                ( hinge(1).ref.rr2 - ...
                  hinge(1).ref.rr1 ) * ...
                  linspace( 0., 1., hinge(1).np );
hinge(1).h   = ( hinge(1).ref.rr2 - hinge(1).ref.rr1 ) / ...
           norm( hinge(1).ref.rr2 - hinge(1).ref.rr1 );
hinge(1).vs  = [ 1.; 0.; 0. ] ; % direction of the offset
hinge(1).th  = deg2rad(40.0)  ; % rotation of the aileron as a whole
hinge(1).u   = 0.20           ; % symmetric offset (for rotation blending)
hinge(1).rot.th = hinge(1).th * ones(1,hinge(1).np);
%>
for ih = 1 : wing.str.n_hinges
  wing.str.hinge(ih) = build_hinge_connectivity( hinge(ih), wing );
end
% === Wing object ===============================================

% Plot reference configuration
figure; hold on
patch('Faces',wing.surf.ee','Vertices',wing.surf.ref.rr','FaceColor','None','LineWidth',1.0)
plot3(wing.str.beam.ref.rr(1,:), ...
      wing.str.beam.ref.rr(2,:), ...
      wing.str.beam.ref.rr(3,:), 'r-o', 'LineWidth', 2)
for ih = 1 : wing.str.n_hinges
  plot3(wing.str.hinge(ih).ref.rr(1,:), ...
        wing.str.hinge(ih).ref.rr(2,:), ...
        wing.str.hinge(ih).ref.rr(3,:), 'r-o', 'LineWidth', 2)
end
axis equal, grid on


%> === Interface Weight ===
%> Norm weigth matrix
% %> 1. Identity
% Wnorm = eye(3);
%> 2. Diagonal matrix
wx = 0.001; wy = 1.; wz = wx;
Wnorm = diag([wx, wy, wz]);
%> Interpolation parameters
w_order = 1;
nw_max = 2;      % max. n of relevant weights

wei = zeros(nw_max,np);
ind = zeros(nw_max,np);

for ip = 1 : np

  diff_all = rr(:,ip)*ones(1,npb) - rrb;
  dist_all = sqrt(diag( diff_all' * Wnorm * diff_all )) ;

  [ dist_all, ib ] = sort(dist_all);
  dist = dist_all(1:nw_max);

  v = 1. ./ dist.^w_order;
  v = v ./ sum(v);

  wei(:,ip) = v;
  ind(:,ip) = ib(1:nw_max) ;

end

%> === Current configuration ===
%> Structural nodes
%> Bending (parabolic shape): position ppb, and rotation vector ttb
a2 = 0.03;
%> Structure actual configuration
%> Beam
wing.str.beam.act.rr = [ wing.str.beam.ref.rr(1,:); ...
                         wing.str.beam.ref.rr(2,:); ...
                      a2*wing.str.beam.ref.rr(2,:).^2 ];
wing.str.beam.act.tt = [ atan(2.*a2*wing.str.beam.ref.rr(2,:)); ...
                       zeros(2,size(wing.str.beam.ref.rr,2)) ];
%> Hinges
for ih = 1 : wing.str.n_hinges
  wing.str.hinge(ih).act.rr = [ wing.str.hinge(ih).ref.rr(1,:); ...
                                wing.str.hinge(ih).ref.rr(2,:); ...
                             a2*wing.str.hinge(ih).ref.rr(2,:).^2 ];
  wing.str.hinge(ih).act.tt = [ atan(2.*a2*wing.str.hinge(ih).ref.rr(2,:)); ...
                              zeros(2,size(wing.str.hinge(ih).ref.rr,2)) ];
end

%> Component surface
%> Wing beam
for ip  = 1 : np

  for iw = 1 : nw_max

    %> Particular case
    thx = wing.str.beam.act.tt(1,ind(iw,ip));
    Rot = [    1. ,      0. ,      0.  ; ...
               0. , cos(thx),-sin(thx) ; ...
               0. , sin(thx), cos(thx) ] ;

    % === From structural nodes to aerodynamic nodes ===
    %> Position
    wing.surf.act.rr(:,ip) = wing.surf.act.rr(:,ip) + wei(iw,ip) * ( ...
      wing.str.beam.act.rr(:,ind(iw,ip)) + Rot * ( ...
          wing.surf.ref.rr(:,ip) - wing.str.beam.ref.rr(:,ind(iw,ip)) ) );
    %> Velocity
    % ... vv += wei * ( cross( omb, pp-ppb ) ) ...
    % === From aerodynamic nodes to structural nodes ===
    %> Force
    % ... ffb += wei * ff ...
    %> Moments
    % ... mmb += wei * ( ff + cross( pp-ppb, ff ) ) ...

  end

end

%> Hinges
for ih = 1 : wing.str.n_hinges

  wing = hinge_deflection( wing, wing.str.hinge(ih) );

end



% Plot reference configuration
figure; hold on
patch('Faces',wing.surf.ee','Vertices',wing.surf.act.rr','FaceColor','White','LineWidth',1.0)
plot3(wing.str.beam.act.rr(1,:), ...
      wing.str.beam.act.rr(2,:), ...
      wing.str.beam.act.rr(3,:), 'r-o', 'LineWidth', 2)
for ih = 1 : wing.str.n_hinges
  plot3(wing.str.hinge(ih).act.rr(1,:), ...
        wing.str.hinge(ih).act.rr(2,:), ...
        wing.str.hinge(ih).act.rr(3,:), 'r-o', 'LineWidth', 2)
end
axis equal, grid on
% %> Reference and actual configurations
% figure; hold on
% patch('Faces',ee','Vertices',rr','FaceColor','None','LineWidth',1.0)
% plot3(rrb(1,:), rrb(2,:), rrb(3,:), 'r-o', 'LineWidth', 2)
% patch('Faces',ee','Vertices',pp','FaceColor','None','LineWidth',1.0)
% plot3(ppb(1,:), ppb(2,:), ppb(3,:), 'r-o', 'LineWidth', 2)
% axis equal, grid on
