

function wing = hinge_deflection( wing_in, hinge )

wing = wing_in;

rr = wing.surf.act.rr;

for ih = 1 : hinge.np

  %> Rotation: angle and axis
  % *** to do *** rotate h in the local nodal reference frame
  th = hinge.rot.th(ih);
  if ( th ~= 0.0 )
    n  = hinge.h; 
    nx = [   0. , -n(3), n(2); ...
           n(3) ,    0.,-n(1); ...
          -n(2) ,  n(1),   0. ];
    Rot_I = sin(th)*nx + ( 1. - cos(th) )*nx*nx;
  
    %> === Rigid rotation ===
    for ip = 1 : length(hinge.rot.p(ih).p2h)
      ii = hinge.rot.p(ih).p2h(ip);
      wing.surf.act.rr(:,ii) = wing.surf.act.rr(:,ii) + ...
                               hinge.rot.p(ih).w2h(ip) * ...
                               Rot_I * ( rr(:,ii) - hinge.act.rr(:,ih) );
    end
  
    %> === Blending region ===
    for ip = 1 : length(hinge.ble.p(ih).p2h)
      th = -th; %%% very dirty %%% 
      ii = hinge.ble.p(ih).p2h(ip);
      u = hinge.u;
      m = -cos(th)/sin(th);
      yc = -m*u*(1.+cos(th)) + u*sin(th);
      % *** todo *** rotate hinge.v, .n, .h with the hinge node
      xq = ( rr(:,ii)- hinge.act.rr(:,ih) )' * hinge.v;
      yq = ( rr(:,ii)- hinge.act.rr(:,ih) )' * hinge.n;
      thp = 0.5*( xq+u )/u*th;
      xqp = yc*sin(thp) - u - yq*sin(thp) - xq; 
      yqp = yc*(1.-cos(thp))+ yq*cos(thp) - yq; 
      wing.surf.act.rr(:,ii) = wing.surf.act.rr(:,ii) + ...
                               hinge.ble.p(ih).w2h(ip) * ...
                             ( xqp * hinge.v + yqp * hinge.n ) ;
      th = -th; %%% very dirty %%% 
           
    end

  else % th == 0
    % do nothing
  end

end
